<?php

use Illuminate\Database\Seeder;

class DiagnosisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list[] = [
            'clinic_id' => 1,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/11 09:30',
            'start_datetime' => '2019/10/11 09:30',
            'end_datetime' => '2019/10/11 10:30',
            'price' => 1000,
            'status' => 0,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 1,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/14 09:30',
            'start_datetime' => '2019/10/14 09:30',
            'end_datetime' => '2019/10/14 10:30',
            'price' => 1000,
            'status' => 0,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 1,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/07 09:30',
            'start_datetime' => '2019/10/07 09:30',
            'end_datetime' => '2019/10/07 10:30',
            'price' => 1000,
            'status' => 1,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 1,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/08 10:30',
            'start_datetime' => '2019/10/08 10:30',
            'end_datetime' => '2019/10/08 11:30',
            'price' => 1000,
            'status' => 2,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 1,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/5 10:30',
            'start_datetime' => '2019/10/5 10:30',
            'end_datetime' => '2019/10/5 11:30',
            'price' => 1000,
            'status' => 3,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 2,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/11 09:30',
            'start_datetime' => '2019/10/11 09:30',
            'end_datetime' => '2019/10/11 10:30',
            'price' => 1000,
            'status' => 0,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 2,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/14 09:30',
            'start_datetime' => '2019/10/14 09:30',
            'end_datetime' => '2019/10/14 10:30',
            'price' => 1000,
            'status' => 0,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 2,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/07 09:30',
            'start_datetime' => '2019/10/07 09:30',
            'end_datetime' => '2019/10/07 10:30',
            'price' => 1000,
            'status' => 1,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 2,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/08 10:30',
            'start_datetime' => '2019/10/08 10:30',
            'end_datetime' => '2019/10/08 11:30',
            'price' => 1000,
            'status' => 2,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 2,
            'clinic_menu_id' => 2,
            'user_id' => 1,
            'pet_id' => 1,
            'reserve_datetime' => '2019/10/5 10:30',
            'start_datetime' => '2019/10/5 10:30',
            'end_datetime' => '2019/10/5 11:30',
            'price' => 1000,
            'status' => 3,
            'files' => json_encode(
                [
                    '1.jpg',
                    '2.jpg',
                ]
            ),
            'content' => '風邪を引いたみたいです。',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        DB::table('diagnosis')->truncate();
        DB::table("diagnosis")->insert($list);
    }
}
