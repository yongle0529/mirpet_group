<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClinicTableSeeder::class);
        $this->call(ClinicMenuTableSeeder::class);
        $this->call(ReserveTimetableTableSeeder::class);
        $this->call(DiagnosisTableSeeder::class);
    }
}
