<?php

use Illuminate\Database\Seeder;

class ClinicMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $list[] = [
            'clinic_id' => 0,
            'name' => '前回の来院での診療の経過相談',
            'description' => "前回病院で診断を受け、治療をしている疾患についての相談になります。\n必要に応じて薬の処方を受けることができます。",
            'price_list' => json_encode(
                [
                    [
                        'title' => '30分相談',
                        'price' => 1000
                    ]
                ]
            ),
            'is_insurance_applicable' => 1,
            'status' => 1,
            'option' => json_encode([
                'timeSlotType' => 2
            ]),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 0,
            'name' => '他院で治療中の疾患に対するセカンドオピニオン',
            'description' => "他動物病院で診断を受け、治療をしている疾患についてセカンドオピニオンをお求めの方のためのものです。\n相談によっては来院をお勧めする場合があります。",
            'price_list' => json_encode(
                [
                    [
                        'title' => '30分相談',
                        'price' => 2000
                    ]
                ]
            ),
            'is_insurance_applicable' => 0,
            'status' => 1,
            'option' => json_encode([
                'timeSlotType' => 2
            ]),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            'clinic_id' => 0,
            'name' => 'その他相談（しつけ、歯磨きなど）',
            'description' => "しつけ相談や、デンタルケア指導など疾患に関するもの以外に対しての相談になります。",
            'price_list' => json_encode(
                [
                    [
                        'title' => '30分相談',
                        'price' => 1000
                    ]
                ]
            ),
            'is_insurance_applicable' => 1,
            'status' => 1,
            'option' => json_encode([
                'timeSlotType' => 2
            ]),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        DB::table('clinic_menus')->truncate();
        DB::table("clinic_menus")->insert($list);
    }
}
