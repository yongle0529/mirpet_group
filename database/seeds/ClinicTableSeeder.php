<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ClinicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $list[] = [
            "name" => 'みるぺっとクリニック',
            "name_kana" => 'ミルペットクリニック',
            "manager_name" => '佐藤',
            "manager_name_kana" => '山下',
            "option_pet" => json_encode(
                [
                    'dog' => true,
                    'cat' => true,
                    'hamster' => true,
                    'ferret' => true,
                    'rabbit' => true,
                    'bird' => false,
                    'reptile' => false, //爬虫類
                    'others' => false,
                ]
            ),
            "option_medical_course" => json_encode(
                [
                    'internal' => true, //内科
                    'orthopedic' => false, //整形外科
                    'neurosurgery' => false, //神経外科
                    'dermatology' => false, //皮膚科
                    'cardiology' => false, //循環器科
                    'urology' => false, //泌尿器科
                    'ophthalmology' => false, //眼科
                    'dentistry' => false, //歯科
                    'rehabilitation' => false, //リハビリテーション
                    'others' => false,
                ]
            ),
            "option_other" => null,
            "clinic_info" => json_encode(
                [
                    'owner' => '浅沼',
                    'businessHour' => [
                        1 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        2 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        3 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        4 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        5 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        6 => [
                            ['start' => '9:00', 'end' => '12:00']
                        ],
                        0 => null,
                        'holiday' => null,
                    ],
                    'description' => "みるぺっとクリニックの説明文。",
                    'insurance' => ['アイペット'],
                    'eyecatchImages' => ['header_3.jpg']
                ]
            ),
            "tel" => '03-6910-3242',
            "email" => 'info@mirpet.co.jp',
            "password" => Hash::make('123qwe'),
            "zip_code" => '104-0061',
            "prefecture" => '東京都',
            "city" => '中央区',
            "address" => '銀座７丁目１８ー１３　クオリア銀座５０４',
            "url" => 'https://mirpet.co.jp/',
            "logo_image_path" => 'logo.png',
            "ref_code" => 'abc1234',
            "bank_info" => json_encode(
                [
                    'bank_name' => '三井住友銀行',
                    'bank_branch_name' => '新橋支店',
                    'bank_type' => "普通",
                    'branch_number' => '001',
                    'bank_account_number' => '0123456',
                    'bank_account_name' => 'カブシキガイシャミルペットクリニック'
                ]
            ),
            "status" => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        $list[] = [
            "name" => 'みるぺっとクリニック2',
            "name_kana" => 'ミルペットクリニック2',
            "manager_name" => '山本',
            "manager_name_kana" => '岡田',
            "option_pet" => json_encode(
                [
                    'dog' => true,
                    'cat' => true,
                    'hamster' => true,
                    'ferret' => true,
                    'rabbit' => true,
                    'bird' => true,
                    'reptile' => true, //爬虫類
                    'others' => false,
                ]
            ),
            "option_medical_course" => json_encode(
                [
                    'internal' => true, //内科
                    'orthopedic' => false, //整形外科
                    'neurosurgery' => false, //神経外科
                    'dermatology' => false, //皮膚科
                    'cardiology' => false, //循環器科
                    'urology' => false, //泌尿器科
                    'ophthalmology' => false, //眼科
                    'dentistry' => false, //歯科
                    'rehabilitation' => false, //リハビリテーション
                    'others' => false,
                ]
            ),
            "option_other" => null,
            "clinic_info" => json_encode(
                [
                    'owner' => '浅沼',
                    'businessHour' => [
                        1 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        2 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        3 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        4 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        5 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        6 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        0 => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ],
                        'holiday' => [
                            ['start' => '9:00', 'end' => '12:00'],
                            ['start' => '13:00', 'end' => '18:00']
                        ]
                    ],
                    'description' => "みるぺっとクリニックの説明文。",
                    'insurance' => ['アイペット'],
                    'eyecatchImages' => ['header_3.jpg']
                ]
            ),
            "tel" => '03-6910-3242',
            "email" => 'info2@mirpet.co.jp',
            "password" => Hash::make('123qwe'),
            "zip_code" => '104-0061',
            "prefecture" => '東京都',
            "city" => '中央区',
            "address" => '銀座７丁目１８ー１３',
            "url" => 'https://mirpet.co.jp/',
            "logo_image_path" => 'logo.png',
            "ref_code" => 'abc1234',
            "bank_info" => json_encode(
                [
                    'bank_name' => '三井住友銀行',
                    'bank_branch_name' => '新橋支店',
                    'bank_type' => "普通",
                    'branch_number' => '001',
                    'bank_account_number' => '0123456',
                    'bank_account_name' => 'カブシキガイシャミルペットクリニック'
                ]
            ),
            "status" => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        DB::table('clinics')->truncate();
        DB::table("clinics")->insert($list);
    }
}
