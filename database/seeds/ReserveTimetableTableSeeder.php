<?php

use Illuminate\Database\Seeder;

class ReserveTimetableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [];

        for ($day = 1; $day <= 31; $day++) {
            for ($i = 19; $i <= 37; $i++) {
                $list[] = [
                    'clinic_id' => 1,
                    'date' => \Carbon\Carbon::parse('2019-06-' . $day)->format('Y-m-d'),
                    'time_slot_type' => 2,
                    'time_slot' => $i,
                    'acceptable_count' => 1,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ];
            }
        }

        DB::table('reserve_timetables')->truncate();
        DB::table("reserve_timetables")->insert($list);
    }
}
