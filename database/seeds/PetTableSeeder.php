<?php

use Illuminate\Database\Seeder;

class PetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list[] = [
            "user_id" => '1',
            "name" => 'たろう',
            "name_kana" => 'タロウ',
            "gender" => '1',
            "type" => '1',
            "type_other" => null,
            "genre" => 'ポメラニアン',
            "birthday" => '2016-02-05',
            "profile_picture_path" => null,
            "insurance_front" => null,
            "insurance_back" => null,
            "del_flg" => '0',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ];

        DB::table('pets')->truncate();
        DB::table("pets")->insert($list);
    }
}
