<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('clinic_id');
            $table->integer('diagnosis_id');
            $table->string('origi_filename')->nullable();
            $table->string('save_filename')->nullable();
            $table->string('extension')->nullable();
            $table->string('medical_expenses')->nullable();
            $table->string('fee')->nullable();
            $table->string('delivery_cost')->nullable();
            $table->string('sum')->nullable();
            $table->string('comment')->nullable();
            $table->integer('status')->nullable();
            $table->date('next_remind_date')->nullable();
            $table->string('clinic_fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountings');
    }
}
