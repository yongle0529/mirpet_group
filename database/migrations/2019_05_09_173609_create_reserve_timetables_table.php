<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReserveTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve_timetables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_id');
            $table->integer('diag_id');
            $table->date('date');
            $table->unsignedTinyInteger('time_slot_type');
            $table->integer('time_slot');
            $table->integer('acceptable_count')->default(1);
            $table->integer('reserved_count')->default(0);
            $table->timestamps();
            $table->index('clinic_id', 'index01');
            $table->index('date', 'index02');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve_timetables');
    }
}
