<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cata_no');
            $table->integer('clinic_id');
            $table->integer('clinic_menu_id');
            $table->integer('user_id');
            $table->integer('pet_id');
            $table->dateTime('reserve_datetime');
            $table->dateTime('start_datetime')->nullable();
            $table->dateTime('end_datetime')->nullable();
            $table->integer('price')->nullable();
            $table->json('option')->nullable()->comment('診察方法、病院コード');
            $table->string('content')->nullable();
            $table->json('files')->nullable();
            $table->string('comment')->nullable();
            $table->dateTime('next_remind_datetime');
            $table->integer('real_price')->nullable();
            $table->unsignedTinyInteger('status')->default(0)->comment('0: 予約状態 / 1: 診療完了 / 2 : キャンセル');
            $table->unsignedTinyInteger('medicine_status')->default(0)->comment('0: 薬発行なし / 1: 薬発行あり / 2 : 薬発送完了');
            $table->unsignedTinyInteger('payment_status')->default(0)->comment('0: 未払い / 1: 支払い済');
            $table->unsignedTinyInteger('reserve_diagnosis_id')->nullable();
            $table->string('res_time')->nullable();
            $table->string('hospital_code')->nullable();
            $table->integer('reserve_method')->nullable();
            $table->string('clinic_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosis');
    }
}
