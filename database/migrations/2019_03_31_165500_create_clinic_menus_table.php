<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_id');
            $table->integer('base_id')->nullable();
            $table->string('name')->nullable();
            $table->text('description');
            $table->json('price_list')->nullable();
            $table->unsignedTinyInteger('is_insurance_applicable')->default(0)->comment('0: 保険適用不可, 1:保険適用可');
            $table->json('option')->nullable();
            $table->longtext('insurance_company')->nullable();
            $table->unsignedTinyInteger('status')->default(0)->comment('0: 初期登録状態, 1:公開中, 2:掲載停止');
            $table->unsignedTinyInteger('del_flg')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_menus');
    }
}
