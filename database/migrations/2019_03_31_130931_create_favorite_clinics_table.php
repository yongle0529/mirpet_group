<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteClinicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_clinics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('clinic_id');
            $table->unsignedTinyInteger('del_flg')->default(0);
            $table->timestamps();
            $table->index(['user_id', 'clinic_id'], 'index01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorite_clinics');
    }
}
