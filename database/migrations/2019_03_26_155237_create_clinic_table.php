<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_kana');
            $table->string('manager_name')->nullable();
            $table->string('manager_name_kana')->nullable();
            $table->json('option_pet')->nullable();
            $table->json('option_medical_course')->nullable();
            $table->json('option_other')->nullable();
            $table->json('menu_time_option')->nullable();
            $table->json('clinic_info')->nullable();
            $table->string('eyecatchimage')->nullable();
            $table->string('clinic_info_photos')->nullable();
            $table->string('insurance')->nullable();
            $table->text('clinic_description')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('tel');
            $table->string('zip_code');
            $table->string('prefecture');
            $table->string('city');
            $table->string('address');
            $table->string('logo_image_path')->nullable();
            $table->string('url')->nullable();
            $table->string('ref_code')->nullable();
            $table->json('bank_info')->nullable();
            $table->unsignedTinyInteger('status')->default(0)->comment('0: 初期登録状態, 1:公開中, 2:掲載停止');
            $table->rememberToken();
            $table->string('payment_token')->nullable();
            $table->string('reset_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinics');
    }
}
