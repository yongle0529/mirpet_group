<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('name_kana');
            $table->unsignedTinyInteger('gender');
            $table->unsignedTinyInteger('type');
            $table->string('type_other')->nullable();
            $table->string('genre');
            $table->date('birthday');
            $table->string('profile_picture_path')->nullable();
            $table->string('insurance_picture_path')->nullable();
            $table->unsignedTinyInteger('del_flg')->default(0);
            $table->string('insurance_name');
            $table->string('insurance_no');
            $table->date('insurance_from_date')->nullable();
            $table->date('insurance_to_date')->nullable();
            $table->unsignedTinyInteger('insurance_option');
            $table->timestamps();
            $table->index('user_id', 'index01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
