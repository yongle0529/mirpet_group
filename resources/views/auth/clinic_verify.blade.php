@extends('layouts.clinic_register')

@section('body')
    <div style="min-height: 80px; margin-top: 100px">
    </div>
    <div class="container">
        <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header m-3">
                    <h3 class="card-title">{{ __('Verify Your Email Address') }}</h3>
                </div>

                <div class="card-body">
                    登録のお申し込みを受け付けました。<br/>ご登録メールアドレスにメールを送信しましたので、ご確認ください。
                </div>
            </div>
        </div>
    </div>
    </div>
    <div style="min-height: 80px; margin-top: 74px">
    </div>
@endsection
