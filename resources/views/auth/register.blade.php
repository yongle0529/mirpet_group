@extends('layouts.app.app')

@push('extra-script')

<script type="text/javascript">
$(function() {
    $('#submit').prop('disabled', true);

    $('#agree').on('click', function() {
        if ($(this).prop('checked') == false) {
            $('#submit').prop('disabled', true);
        } else {
            $('#submit').prop('disabled', false);
        }
    });
    $('#password-confirm').on('change', function() {
        var pwd = document.getElementById('password').value;
        var pwd_con = document.getElementById('password-confirm').value;
        console.log(pwd);
        console.log(pwd_con);
        if (pwd == pwd_con) {
            document.getElementById('pwd_alert').innerHTML = "";
        } else {
            document.getElementById('pwd_alert').innerHTML = "パスワードが一致しません。もう一度お試しください。";
        }
    });
});
</script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header m-3">
                        <h3 class="card-title">ユーザー登録</h3>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">氏名：姓</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="山本" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">氏名：名</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="太郎" value="{{ old('first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：セイ</label>

                                <div class="col-md-6">
                                    <input id="last_name_kana" type="text" class="form-control{{ $errors->has('last_name_kana') ? ' is-invalid' : '' }}" name="last_name_kana" placeholder="ヤマモト" value="{{ old('last_name_kana') }}"
                                           required autofocus>

                                    @if ($errors->has('last_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：メイ</label>

                                <div class="col-md-6">
                                    <input id="first_name_kana" type="text" class="form-control{{ $errors->has('first_name_kana') ? ' is-invalid' : '' }}" name="first_name_kana" placeholder="タロウ" value="{{ old('first_name_kana') }}" required
                                           autofocus>

                                    @if ($errors->has('first_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="taro.yamamoto@mirpet.co.jp" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">電話番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" placeholder="09012345678" value="{{ old('tel') }}" required autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="zip_code" class="col-md-4 col-form-label text-md-right">郵便番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="zip_code" type="text" onKeyUp="AjaxZip3.zip2addr('zip_code', '', 'prefecture', 'city');" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" placeholder="1040061" value="{{ old('zip_code') }}" required autofocus>

                                    @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('prefecture') ? ' is-invalid' : '' }}" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            <option
                                                value="{{ $pref }}"
                                                {{ $pref == old('prefecture') ? 'selected' : $pref == '東京都' ? 'selected' : '' }}
                                            >{{ $pref }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('prefecture'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prefecture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">郡市区(島)</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="中央区" value="{{ old('city') }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">それ以降の住所</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="銀座7丁目18-13 クオリア銀座504" value="{{ old('address') }}" required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">パスワード</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">パスワード(確認用)</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    {{-- @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert" id="pwd_alert">
                                        <strong>パスワードが一致しません。もう一度お試しください。</strong>
                                    </span>
                                    @endif --}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" id="agree" name="agreement" value="agree" data-label="電話" autocomplete="off" required="required">ご利用規約およびプライバシーポリシーに同意する
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="submit" type="submit" class="btn btn-primary" readonly="readonly" disabled>
                                        登録する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
