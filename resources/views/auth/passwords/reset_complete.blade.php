@extends('layouts.clinic_app')

@section('body')
    <div class="container" style="margin-top:100px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>パスワードの再設定が完了しました。</h4><br/>
                            <a href="{{URL::to('/clinic/login')}}" class="btn btn-primary btn-lg">獣医師ログイン</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
