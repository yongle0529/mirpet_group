@extends('layouts.app.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header m-3">
                    <h3 class="card-title">{{ __('Reset Password') }}</h3>
                </div>

                <div class="card-body">
                    @if ($flag == 1)
                        <div class="alert alert-success" role="alert">
                            みるペットのログインパスワードの再設定のためのEメールをあなたに送りました。<br/>
                            メールに記載されているリンクをクリックして、パスワードを再設定してください
                        </div>
                    @elseif($flag == 2)
                        <div class="alert alert-primary" role="alert">
                            このメールアドレスは登録されていません。別のアドレスをお試しください。
                        </div>
                    @endif

                    <form method="POST" action="{{ route('clinic.password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="{{URL::to('/clinic/login')}}" class="btn btn-dark">戻る</a>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
