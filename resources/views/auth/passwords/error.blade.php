@extends('layouts.app.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header m-3">
                    <h3 class="card-title">{{ __('Reset Password') }}</h3>
                </div>

                <div class="card-body">
                    お客様の再設定トークンが満了しました。<br/>
                    再設定トークンを再度お受け取りになるには, <a href="{{ URL::to('/clinic/password/reset/0') }}">ここをクリックしてください。</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
