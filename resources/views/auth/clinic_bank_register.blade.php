@extends('layouts.clinic')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
@endsection

@section('body')
    <div style="min-height: 80px;">
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header m-3">
                        <h3 class="card-title">お取引銀行口座設定</h3>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ URL::to('/clinic/bank/register/'.$clinicId.'/'.$last_name.'/'. $first_name) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="bank_name" class="col-md-4 col-form-label text-md-right">銀行名</label>

                                <div class="col-md-6">
                                    <input id="bank_name" type="text" class="form-control{{ $errors->has('bank_name') ? ' is-invalid' : '' }}" name="bank_name" placeholder="三井住友銀行" value="{{ old('bank_name') }}" required autofocus>

                                    @if ($errors->has('bank_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_branch_name" class="col-md-4 col-form-label text-md-right">支店名</label>

                                <div class="col-md-6">
                                    <input id="bank_branch_name" type="text" class="form-control{{ $errors->has('bank_branch_name') ? ' is-invalid' : '' }}" name="bank_branch_name" placeholder="新橋支店" value="{{ old('bank_branch_name') }}" required autofocus>

                                    @if ($errors->has('bank_branch_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_branch_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_type" class="col-md-4 col-form-label text-md-right">口座種別</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('bank_type') ? ' is-invalid' : '' }}" id="bank_type" name="bank_type">

                                        <option value="普通" {{ "普通" == old('bank_type') ? 'selected' : '' }}>普通</option>
                                        <option value="当座" {{ "当座" == old('bank_type') ? 'selected' : '' }}>当座</option>
                                        <option value="貯蓄" {{ "貯蓄" == old('bank_type') ? 'selected' : '' }}>貯蓄</option>
                                        <option value="その他" {{ "貯蓄" == old('bank_type') ? 'selected' : '' }}>その他</option>

                                    </select>
                                    @if ($errors->has('bank_type'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="branch_number" class="col-md-4 col-form-label text-md-right">店番</label>

                                <div class="col-md-6">
                                    <input id="branch_number" type="text" class="form-control{{ $errors->has('branch_number') ? ' is-invalid' : '' }}" name="branch_number" placeholder="001"
                                           value="{{ old('branch_number') }}" required autofocus>

                                    @if ($errors->has('branch_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('branch_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_account_number" class="col-md-4 col-form-label text-md-right">口座番号</label>

                                <div class="col-md-6">
                                    <input id="bank_account_number" type="text" class="form-control{{ $errors->has('bank_account_number') ? ' is-invalid' : '' }}" name="bank_account_number" placeholder="0123456" value="{{ old('bank_account_number') }}" required autofocus>

                                    @if ($errors->has('bank_account_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_account_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_account_name" class="col-md-4 col-form-label text-md-right">口座名義(カタカナ)</label>

                                <div class="col-md-6">
                                    <input id="bank_account_name" type="text" class="form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" name="bank_account_name" placeholder="カブシキガイシャミルペットクリニック" value="{{ old('bank_account_name') }}" required autofocus>

                                    @if ($errors->has('bank_account_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_account_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    {{-- <a href="/app/clinic" class="btn btn-primary btn-block">登録する</a> --}}
                                    <button type="submit" class="btn btn-primary">
                                        登録する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
