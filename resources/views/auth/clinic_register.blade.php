@extends('layouts.clinic_register')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $('#submit').prop('disabled', true);
            $('input[name="agreement"]').change(function () {
                if ($(this).is(':checked')) {
                    $('#submit').prop('disabled', false);
                } else {
                    $('#submit').prop('disabled', true);
                }
            });
        });
    </script>
@endsection

@section('body')
    <div style="min-height: 80px;">
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header m-3">
                        <h3 class="card-title">獣医師用：新規ご利用登録</h3>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{URL::to('/clinic/register')}}">
                            @csrf

                            <div class="form-group row">
                                <label for="clinic_name" class="col-md-4 col-form-label text-md-right">病院名</label>

                                <div class="col-md-6">
                                    <input id="clinic_name" type="text" class="form-control{{ $errors->has('clinic_name') ? ' is-invalid' : '' }}" name="clinic_name" placeholder="みるぺっとクリニック" value="{{ old('clinic_name') }}" required
                                           autofocus>

                                    @if ($errors->has('clinic_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('clinic_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="clinic_name_kana" class="col-md-4 col-form-label text-md-right">病院名(フリガナ)</label>

                                <div class="col-md-6">
                                    <input id="clinic_name_kana" type="text" class="form-control{{ $errors->has('clinic_name_kana') ? ' is-invalid' : '' }}" name="clinic_name_kana" placeholder="ミルペットクリニック"
                                           value="{{ old('clinic_name_kana') }}" required autofocus>

                                    @if ($errors->has('clinic_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('clinic_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">院長名：姓</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="山本" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">院長名：名</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="太郎" value="{{ old('first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name_kana" class="col-md-4 col-form-label text-md-right">院長名(フリガナ)：セイ</label>

                                <div class="col-md-6">
                                    <input id="last_name_kana" type="text" class="form-control{{ $errors->has('last_name_kana') ? ' is-invalid' : '' }}" name="last_name_kana" placeholder="ヤマモト" value="{{ old('last_name_kana') }}"
                                           required autofocus>

                                    @if ($errors->has('last_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name_kana" class="col-md-4 col-form-label text-md-right">院長名(フリガナ)：メイ</label>

                                <div class="col-md-6">
                                    <input id="first_name_kana" type="text" class="form-control{{ $errors->has('first_name_kana') ? ' is-invalid' : '' }}" name="first_name_kana" placeholder="タロウ" value="{{ old('first_name_kana') }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('first_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="taro.yamamoto@mirpet.co.jp" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">電話番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" placeholder="09012345678" value="{{ old('tel') }}" required autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="zip_code" class="col-md-4 col-form-label text-md-right">郵便番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" onKeyUp="AjaxZip3.zip2addr('zip_code', '', 'prefecture', 'city');" name="zip_code" placeholder="1040061" value="{{ old('zip_code') }}" required autofocus>

                                    @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('prefecture') ? ' is-invalid' : '' }}" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            <option
                                                value="{{ $pref }}"
                                                {{ $pref == old('prefecture') ? 'selected' : $pref == '東京都' ? 'selected' : '' }}
                                            >{{ $pref }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('prefecture'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prefecture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">郡市区(島)</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="中央区" value="{{ old('city') }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">それ以降の住所</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="銀座7丁目18-13 クオリア銀座504" value="{{ old('address') }}" required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="clinic_url" class="col-md-4 col-form-label text-md-right">病院ホームページURL※任意</label>

                                <div class="col-md-6">
                                    <input id="clinic_url" type="text" class="form-control{{ $errors->has('clinic_url') ? ' is-invalid' : '' }}" name="clinic_url" placeholder="https://mirpet.co.jp" value="{{ old('clinic_url') }}" autofocus>

                                    @if ($errors->has('clinic_url'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('clinic_url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label for="zip_code" class="col-md-12 col-form-label text-md-left">診察対象動物</label>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/dog_icon.png" style="width: 1.2rem;" alt="イヌ">
                                                <input class="form-check-input" type="checkbox" id="dog"  name="dog" value="agree" data-label="" autocomplete="off" >

                                                イヌ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/cat_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="cats" name="cat"   value="agree" data-label="" autocomplete="off" >

                                                ネコ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/hamster_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="hamsters" name="hamster"  value="agree" data-label="" autocomplete="off" >

                                                ハムスター
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/ferret_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="ferret" value="agree" data-label="" autocomplete="off" >

                                                フェレット
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/reptile_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="reptile"   value="agree" data-label="" autocomplete="off" >

                                                爬虫類
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/rabbit_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="rabbit" data-label="" autocomplete="off" >

                                                ウサギ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/bird_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="bird" value="agree" data-label="" autocomplete="off" >

                                                鳥
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/other_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                    <input class="form-check-input" type="checkbox" id="agree" name="other" value="agree" data-label="" autocomplete="off" >

                                                    その他
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    <div class="col-md-12"><hr></div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">パスワード</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        {{-- <strong>パスワードの長さは8文字以上で、英字の大文字と小文字、数字をそれぞれ1つ以上ずつ使用してください。</strong> --}}
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">パスワード(確認用)</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12 offset-md-3">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" id="agree" name="agreement" value="agree" data-label="" autocomplete="off" required="required">ご利用規約およびプライバシーポリシーに同意する
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0 justify-content-center">
                                <div class="col-md-6">
                                    {{-- <a id="submit" href="/clinic/bank/register" class="btn btn-primary btn-block">登録する</a> --}}
                                    <button id="submit" type="submit" class="btn btn-primary btn-block" readonly="readonly" disabled>
                                        登録する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
