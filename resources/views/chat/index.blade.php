<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>みるペット | 通話</title>
    <meta name="theme-color" content="#ffffff">
    <link rel="preload" href="{{URL::to('/')}}/assets/chat/vendor.js" as="script">
    <link rel="preload" href="{{URL::to('/')}}/assets/chat/app.js" as="script">
    <link rel="preload" href="{{URL::to('/')}}/assets/chat/app.css" as="style">
    <link rel="preload" href="{{URL::to('/')}}/assets/chat/manifest.js" as="script">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/assets/chat/font-awesome.min.css">
    <script src="{{URL::to('/')}}/assets/chat/jquery.min.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script src="{{URL::to('/')}}/assets/chat/skyway-latest.js"></script>
    <link href="{{URL::to('/')}}/assets/chat/app.css" rel="stylesheet">
</head>
<body>
<div id="app">
</div>
<script>
    var myPageUrl = "{{$returnUrl}}";
    var diagId = "{{$diagId}}";
    var flag = "{{$flag}}";
    var logo_path = "{{URL::to('/assets/img/mirpet_logo.png')}}";
    (function () {
        'use strict';
        // Check to make sure service workers are supported in the current browser,
        // and that the current page is accessed from a secure origin. Using a
        // service worker from an insecure origin will trigger JS console errors.
        var isLocalhost = Boolean(window.location.hostname === 'localhost' ||
            // [::1] is the IPv6 localhost address.
            window.location.hostname === '[::1]' ||
            // 127.0.0.1/8 is considered localhost for IPv4.
            window.location.hostname.match(
                /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
            )
        );
    })();
</script>
<script type="text/javascript" src="{{URL::to('/')}}/assets/chat/manifest.js"></script>
<script type="text/javascript" src="{{URL::to('/')}}/assets/chat/vendor.js"></script>
<script type="text/javascript" src="{{URL::to('/')}}/assets/chat/app.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{URL::to('/')}}/assets/chat/ga.js"></script>
<script type="text/javascript" src="{{URL::to('/')}}/assets/chat/add-download-btn.js"></script>
</body>
</html>
