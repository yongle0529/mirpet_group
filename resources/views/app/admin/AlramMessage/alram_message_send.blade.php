
@extends('layouts.app.admin.header')

@section('content')
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>お知らせ－送信トレイ</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
    <section class="content">
        <div class="card">
            <div class="card-body">
                <div class="card-body table-responsive p-0" style="height: 600px;">
                    <table id="example2" class="table table-bordered table-hover dataTable table-head-fixed" style="font-size:12px">
                        <thead>
                            <tr align="center">
                                <th width="10%">日付</th>
                                <th width="15%">相手（メールアドレス）</th>
                                <th width="25%">題名</th>
                                <th width="50%">内容</th>
                            </tr>
                        </thead>
                            @if(count($replyMails)>0)

                            @foreach ($replyMails as $replyMail)
                                <tr>
                                    <td width="10%" align="center">{{$replyMail->created_at}}</td>
                                    <td width="15%" align="center">{{$replyMail->email}}</td>
                                    <td width="25%">{{$replyMail->subject}}</td>
                                    <td width="50%">
                                        <?php
                                        $content=htmlspecialchars($replyMail->comment);
                                        $content=str_replace("\n", "<br>", $content);
                                        $content=str_replace(" ", "&nbsp;", $content);
                                        echo $content;
                                        ?>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <tr>

                               <td align="center" colspan="4">データがありません</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        });
    });
</script>
@endsection
