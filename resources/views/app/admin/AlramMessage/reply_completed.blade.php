
@extends('layouts.app.admin.header')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="card">
            <div class="card-body">
                <div class="card-body table-responsive p-1" style="height: 300px; margin-top; 300px" align="center">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h3><b>{{$username}}</b>様のお問い合わせに対する返信が正確に送信されました。</h3>
                    <br/>
                    <a href="{{URL::to('/app/admin/alram_message_receive')}}" class="btn btn-primary btn-lg">メールの画面へ</a>
                </div>
            </div>
        </div>
    </section>
@endsection
