
@extends('layouts.app.admin.header')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <a href="{{URL::to('/app/admin/alram_message_receive')}}">
                    <h3 class="card-title"> &nbsp;メールの画面へ</h3>
                </a>
            </div>
            <form method="POST" action="{{ URL::to("/app/admin/reply_inquiry/".$id."/".$flag) }}" enctype="multipart/form-data">
            @csrf
                <div class="card-body">
                    <input type="hidden" id="username" name="username" value="{{$username}}" />
                    <input type="hidden" id="clinicname" name="clinicname" value="{{$clinicname}}" />
                    <div class="form-group">
                        <input class="form-control" required placeholder="主題:" name="subject" id="subject">
                    </div>
                    <div class="form-group">
                        <textarea id="compose-textarea" required class="form-control" style="height: 400px" placeholder="内容:" name="comment" id="comment"></textarea>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <div class="float-right">
                        @if($flag==0)
                        <button type="button" class="btn btn-default" onclick="insertOwnerTemplate()"> 承認テンプレート</button>
                        @else
                            @if($user->type == "病院登録申請")
                            <button type="button" class="btn btn-default" onclick="insertClinicTemplate(1)"> 承認テンプレート</button>
                            @else
                            <button type="button" class="btn btn-default" onclick="insertClinicTemplate(2)"> 承認テンプレート</button>
                            @endif
                        @endif
                        <button type="submit" class="btn btn-primary">返信</button>
                    </div>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
    </section>
<script>
    function insertOwnerTemplate() {
        document.getElementById('subject').value = '【みるペット】 返信: ' + document.getElementById('username').value + '様ご質問';
        document.getElementById('compose-textarea').value = document.getElementById('username').value +
        '様\n\nお世話になっております。\nみるペットサポートです。\n\n' +
        'お問い合わせいただき、誠にありがとうございます。\n' +
        '下記、回答になります。ご確認ください。\n' +
        '--------------------------------------------------\n' +
        '回答内容記入欄\n' +
        '--------------------------------------------------\n\n' +
        '追加でご質問等ございましたら、こちらにそのままご返信ください。\n' +
        '以上、よろしくお願い致します。\n' +
        '──────────────────────\n' +
        '株式会社みるペット\n' +
        'https://mirpet.co.jp/\n' +
        '〒106-0061\n' +
        '東京都中央区銀座7丁目18番13クオリア銀座504\n' +
        '──────────────────────\n';
    }
    function insertClinicTemplate(id) {
        if (id == 1) {
            document.getElementById('subject').value = '【みるペット】ご登録完了のお知らせ';
            document.getElementById('compose-textarea').value = document.getElementById('clinicname').value + " " + document.getElementById('username').value +
            '様\n\nお世話になっております。\nみるペットサポートです。\n\n' +
            'この度はみるペットにお申込みいただき、ありがとうございました。\n' +
            'お申し込みいただいた内容の確認が終了し、登録を完了致しました。\n' +
            '下記URLより、お申込みいただいたメールアドレスとパスワードにてログインしていただけます。\n' +
            'https://mirpet.co.jp/clinic/login\n' +
            '今後とも、みるペットをよろしくお願いいたします。\n' +
            'ご質問、お問い合わせは以下よりお願いいたします。\n\n' +
            'お問い合わせページ\n' +
            'https://mirpet.co.jp/clinic#inquiry\n\n' +
            '──────────────────────\n' +
            '株式会社みるペット\n' +
            'https://mirpet.co.jp/\n' +
            '〒106-0061\n' +
            '東京都中央区銀座7丁目18番13クオリア銀座504\n' +
            '──────────────────────\n';
        } else {
            document.getElementById('subject').value = '【みるペット】 返信: ' + document.getElementById('username').value + '様ご質問';
            document.getElementById('compose-textarea').value = document.getElementById('username').value +
            '動物病院\n' + document.getElementById('username').value +
            '様\n\nお世話になっております。\nみるペットサポートです。\n\n' +
            'お問い合わせいただき、誠にありがとうございます。\n' +
            '下記、回答になります。ご確認ください。\n' +
            '--------------------------------------------------\n' +
            '回答内容記入欄\n' +
            '--------------------------------------------------\n\n' +
            '追加でご質問等ございましたら、こちらにそのままご返信ください。\n' +
            '以上、よろしくお願い致します。\n' +
            '──────────────────────\n' +
            '株式会社みるペット\n' +
            'https://mirpet.co.jp/\n' +
            '〒106-0061\n' +
            '東京都中央区銀座7丁目18番13クオリア銀座504\n' +
            '──────────────────────\n';
        }
    }
//   $(function () {
//     //Add text editor
//     $('#compose-textarea').summernote()
//   })
</script>
@endsection
