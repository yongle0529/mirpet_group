
@extends('layouts.app.admin.header')

@section('content')
<section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>お知らせ－受信トレイ</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
    <section class="content">
        <div class="card">
            <div class="card-body">
                <div class="card-body table-responsive p-1" style="height: 600px;">
                    <table id="example2" class="table table-head-fixed dataTable no-footer" style="font-size:12px">
                        <thead>
                            <tr align="center">
                                <th width="10%">日付</th>
                                <th width="14%">相手（メールアドレス）</th>
                                <th width="20%">題名</th>
                                <th width="40%">内容</th>
                                <th width="8%">ステータス</th>
                                <th width="8%">アクション</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @if(count($inquiries) == 0 && count($ownerInquiries) == 0)
                            <tr>このビューに表示される項目はありません。</tr>
                            @else

                            @endif --}}
                            @foreach ($inquiries as $inquiry)
                                @php($clinic = \App\Clinic::where('email', $inquiry->email)->first())
                                <tr>
                                    <td width="10%" align="center">{{$inquiry->created_at}}</td>
                                    <td width="14%" align="center">{{$inquiry->email}}</td>
                                    <td width="20%">{{$inquiry->type}}</td>
                                    <td width="40%">
                                        @if($inquiry->type == "病院解約申請")

                                            <b>病院ID :</b> {{$clinic->id}}<br/>
                                            <b>病院名 :</b> {{$clinic->name}}<br/>
                                            <b>電話番号 : </b> {{$inquiry->tel}}<br/>
                                            <b>病院住所 : </b> {{$clinic['prefecture'] . " " . $clinic['city'] . " " . $clinic['address']}}<br/>
                                            <b>ホームページURL : </b> <a target="_blank" href="{{$clinic['url']}}">{{$clinic['url']}}</a><br/>
                                        @elseif($inquiry->type == "病院登録申請")

                                            <b>お名前 :</b> {{$inquiry->last_name . $inquiry->first_name}}<br/>
                                            <b>フリガナ :</b> {{$inquiry->last_name_kana . $inquiry->first_name_kana}}<br/>
                                            <b>医院名 : </b> {{$inquiry->clinic_name}}<br/>
                                            <b>役職 : </b> {{$inquiry->role}}<br/>
                                            <b>年代 : </b> {{$inquiry->age}}<br/>
                                            <b>電話番号 : </b> {{$inquiry->tel}}<br/>
                                            <b>病院住所 : </b> {{$clinic['prefecture'] . " " . $clinic['city'] . " " . $clinic['address']}}<br/>
                                            <b>ホームページURL : </b> <a target="_blank" href="{{$clinic['url']}}">{{$clinic['url']}}</a><br/>
                                            <b>みるペットを知ったきっかけ : </b> {{$inquiry->referer}}
                                        @else
                                            <b>お名前 :</b> {{$inquiry->last_name . $inquiry->first_name}}<br/>
                                            <b>フリガナ :</b> {{$inquiry->last_name_kana . $inquiry->first_name_kana}}<br/>
                                            <b>医院名 : </b> {{$inquiry->clinic_name}}<br/>
                                            <b>役職 : </b> {{$inquiry->role}}<br/>
                                            <b>年代 : </b> {{$inquiry->age}}<br/>
                                            <b>電話番号 : </b> {{$inquiry->tel}}<br/>
                                            <b>みるペットを知ったきっかけ : </b> {{$inquiry->referer}}<br/><br/>
                                            <b>お問い合わせ詳細</b><br/>
                                            <?php
                                            $content=htmlspecialchars($inquiry->comment);
                                            $content=str_replace("\n", "<br>", $content);
                                            $content=str_replace(" ", "&nbsp;", $content);
                                            echo $content;
                                            ?>
                                        @endif
                                    </td>
                                    <td width="8%" align="center">
                                        @if($inquiry->status == 0)
                                        未
                                        @else
                                        済
                                        @endif
                                    </td>
                                    <td width="8%" align="center">
                                        @if($inquiry->type == "病院解約申請")
                                        <a class="btn btn-block btn-primary btn-sm" href="{{URL::to('/app/admin/delete_clinic/'.$inquiry->id)}}">解約確定</a>
                                        @else
                                            @if($inquiry->status == 0)
                                            <a class="btn btn-block btn-warning btn-sm" href="{{URL::to('/app/admin/alram_message_new/'.$inquiry->id.'/1')}}">詳細</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            @foreach ($ownerInquiries as $ownerInquiry)
                                <tr>
                                    <td width="10%" align="center">{{$ownerInquiry->created_at}}</td>
                                    <td width="14%" align="center">{{$ownerInquiry->email}}</td>
                                    <td width="20%">{{$ownerInquiry->type}}</td>
                                    <td width="40%">
                                        <b>お名前 :</b> {{$ownerInquiry->name}}<br/>
                                        <b>電話番号 : </b> {{$ownerInquiry->tel}}<br/><br/>
                                        <b>お問い合わせ詳細</b><br/>
                                        <?php
                                        $content=htmlspecialchars($ownerInquiry->comment);
                                        $content=str_replace("\n", "<br>", $content);
                                        $content=str_replace(" ", "&nbsp;", $content);
                                        echo $content;
                                        ?>
                                    </td>
                                    <td width="8%" align="center">
                                        @if($ownerInquiry->status == 0)
                                        未
                                        @else
                                        済
                                        @endif
                                    </td>
                                    <td width="8%" align="center">
                                        @if($ownerInquiry->status == 0)
                                        <a class="btn btn-block btn-warning btn-sm" href="{{URL::to('/app/admin/alram_message_new/'.$ownerInquiry->id.'/0')}}">詳細</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        });
    });
</script>
@endsection
