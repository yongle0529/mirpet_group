<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::to('/') }}/admins/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
        <!-- jQuery -->
        <script src="{{ URL::to('/') }}/admins/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::to('/') }}/admins/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{ URL::to('/') }}/admins/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="{{ URL::to('/') }}/admins/plugins/datatables/jquery.dataTables.js"></script>
        <script src="{{ URL::to('/') }}/admins/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <!-- overlayScrollbars -->
        <script src="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::to('/') }}/admins/dist/js/adminlte.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>PDF</title>
<style>
        @font-face{
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        @font-face{
            font-family: ipag;
            font-style: bold;
            font-weight: bold;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        body {
        font-family: ipag;
        }

body {
font-family: ipag;
}
</style>
</head>
<body>
    <p>
    平均利用時間:
    {{ $hours}}:{{ $minutes }}:{{ $seconds }}
    /件
    </p>
    <p>
    平均利用時間:
    {{ $hospital_average_num }}
    回/日/病院
</p><p>
    平均利用時間:
    {{ $pet_average_num }}
    回/月/飼い主様
</p><p>
    アクティブ率（動物病院）:
    {{ $hos_percent }}
    %
</p><p>
    アクティブ率（飼い主様）:
    {{ $pets_percent}}
    %
</p>

<p>
        利用曜日内訳
</p>
<p>
        月:
        {{ $mo }}
        %
</p>
<p>
        火:
        {{ $tu }}
        %
</p>
<p>
        水:
        {{ $we }}
        %
</p>
<p>
        木:
        {{ $th }}
        %
</p>
<p>
        金:
        {{ $fr }}
        %
</p>
<p>
        土:
        {{ $sa }}
        %
</p>
<p>
        日:
        {{ $su }}
        %
</p>
<p>
        利用時間帯内訳
</p>
<p>
        0-9:
        {{ $h09 }}
        %
</p>
<p>
        9-12:
        {{ $h912 }}
        %
</p>
<p>
        12-16:
        {{ $h1216 }}
        %
</p>
<p>
        16-19:
        {{ $h1619 }}
        %
</p>
<p>
        19-24:
        {{ $h1924 }}
        %
</p>
<p>
        動物種内訳
</p>
<p>
        犬:
        {{ $hos_animal0 }}
        %
</p>
<p>
        猫:
        {{ $hos_animal1 }}
        %
</p>
<p>
        ハムスター:
        {{ $hos_animal3 }}
        %
</p>
<p>
        フェレット:
        {{ $hos_animal4 }}
        %
</p>
<p>
        うさぎ:
        {{ $hos_animal5 }}
        %
</p>
<p>
        鳥:
        {{ $hos_animal6 }}
        %
</p>
<p>
        爬虫類:
        {{ $hos_animal7 }}
        %
</p>
<p>
        その他:
        {{ $hos_animal2 }}
        %
</p>

</body>
</html>
