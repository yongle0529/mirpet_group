
@extends('layouts.app.admin.header')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>その他</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/other_analysis_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-0 col-form-label">表示&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-1">
                                    <select name="year" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <label for="pet" class="col-md-1 col-form-label text-md-center"></label>
                                <label for="pet" class="col-md-0 col-form-label">期間指定して表示</label>

                                <div class="col-md-1">
                                    <select name="year1" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year1'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month1" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month1'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day1" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ $pre_data['day1'] == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日~</label>
                                <div class="col-md-1">
                                    <select name="year2" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year2'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month2" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month2'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day2" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ $pre_data['day2'] == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日</label>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-0 col-form-label text-md-right">県別&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-2">

                                       <div class="form-group">
                                        <select class="select2" name="prefecture[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            {{-- @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else --}}
                                                <option
                                                    value="{{ $pref }}"
                                                    @if(!empty($prefecture))
                                                    @for($i=0; $i<count($prefecture);$i++)
                                                    {{ $pref == $prefecture[$i] ? 'selected' : '' }}
                                                    @endfor
                                                    @endif
                                                >{{ $pref }}</option>
                                            {{-- @endif --}}
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3"></div>
                                <label for="price" class="col-md-0 col-form-label text-md-right">病院別</label>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="select2" name="clinics[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @if(!empty($clinic_query))
                                            @foreach ($clinic_query as $row)
                                                <option value="{{ $row->name }}"
                                                    @if(!empty($clinics))
                                                    @for($i=0; $i<count($clinics);$i++)
                                                    {{ $row->name == $clinics[$i] ? 'selected' : '' }}
                                                    @endfor
                                                    @endif
                                                    >{{ $row->name }}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
    </form>
    <form name="exam_frm" action="{{URL::to('/app/admin/other_pdf_download')}}" method="POST">
        @csrf
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-12">
                        <div>
                            <?php
                            $search_count = 0;
                            $average_time=0;
                            $average_time_count = 0;
                            $hospital_average_num = 0;
                            $pet_average_num = 0;
                            $hos_percent = 0;
                            $pets_percent = 0;
                            $hos_day = array();
                            $hos_month = array();

                            $hos_num = array();
                            $hos_week[0] = 0;
                            $hos_week[1] = 0;
                            $hos_week[2] = 0;
                            $hos_week[3] = 0;
                            $hos_week[4] = 0;
                            $hos_week[5] = 0;
                            $hos_week[6] = 0;

                            $hos_hour[0] = 0;
                            $hos_hour[1] = 0;
                            $hos_hour[2] = 0;
                            $hos_hour[3] = 0;
                            $hos_hour[4] = 0;

                            $hos_animal[0] = 0;
                            $hos_animal[1] = 0;
                            $hos_animal[2] = 0;
                            $hos_animal[3] = 0;
                            $hos_animal[4] = 0;
                            $hos_animal[5] = 0;
                            $hos_animal[6] = 0;
                            $hos_animal[7] = 0;

                            $su=0;
                            $mo=0;
                            $tu=0;
                            $we=0;
                            $th=0;
                            $fr=0;
                            $sa=0;

                            $h09=0;
                            $h912=0;
                            $h1216=0;
                            $h1619=0;
                            $h1924=0;

                            $hours=0;
                            $minutes=0;
                            $seconds=0;

                            //$hos_num[0] = 0;
                            $pet_num = array();
                            $num=0;
                            $flag=0;
                            $pet_flag=0;
                            $day_flag=0;
                            $month_flag=0;
                            $day_n=0;
                            $month_n=0;
                            $hos_n=0;
                            $pet_n=0;
                            if(count($query)>0){

                                foreach($query as $diag)
                                {
                                    if((empty($clinics) and empty($prefecture)))
                                    {
                                        $search_count++;
                                        //동물종류 리용회수 구하기
                                        $Petquery = \App\Pet::where('id', $diag->pet_id)->first();
                                        if($Petquery['type']==1)$hos_animal[0]++;
                                        elseif($Petquery['type']==2)$hos_animal[1]++;
                                        elseif($Petquery['type']==3)$hos_animal[2]++;
                                        elseif($Petquery['type']==4)$hos_animal[3]++;
                                        elseif($Petquery['type']==5)$hos_animal[4]++;
                                        elseif($Petquery['type']==6)$hos_animal[5]++;
                                        elseif($Petquery['type']==7)$hos_animal[6]++;
                                        elseif($Petquery['type']==8)$hos_animal[7]++;
                                        //시간별 리용회수 구하기
                                        $d_time=substr($diag->reserve_datetime,11,2);
                                        if($d_time == 0 or $d_time < 9)$hos_hour[0]++;
                                        elseif($d_time > 8  or $d_time < 12)$hos_hour[1]++;
                                        elseif($d_time > 11 or $d_time < 16)$hos_hour[2]++;
                                        elseif($d_time > 15 or $d_time < 19)$hos_hour[3]++;
                                        elseif($d_time > 18 or $d_time < 24)$hos_hour[4]++;
                                        //주별 리용회수 구하기
                                        $wday = substr($diag->reserve_datetime,0,10);
                                        $currentWeekNumber = date('w',strtotime($wday));
                                        $hos_week[$currentWeekNumber]++;

                                        //평균리용시간  합 $average_time
                                        if($diag->end_datetime !=null and $diag->start_datetime != null)
                                        {
                                            $average_time_count++;
                                            $average_time += hour_seconds($diag->end_datetime,$diag->start_datetime);
                                        }

                                        //하루당 한병원에서의 리용회수 합 $hospital_average_num
                                        $hospital_average_num++;
                                        for($i=0;$i<count($hos_day);$i++)
                                        {
                                            if(substr($diag->reserve_datetime,0,10) == $hos_day[$i]){
                                                $day_flag = 1;
                                                break;
                                            }

                                        }
                                        if($day_flag==0){
                                            $hos_day[$day_n]=substr($diag->reserve_datetime,0,10);
                                            $day_n++;
                                        }else{
                                            $day_flag = 0;
                                            //continue;
                                        }
                                        //
                                        for($i=0;$i<count($hos_month);$i++)
                                        {
                                            if(substr($diag->reserve_datetime,0,7) == $hos_month[$i]){
                                                $month_flag = 1;
                                                break;
                                            }

                                        }
                                        if($month_flag==0){
                                            $hos_month[$month_n]=substr($diag->reserve_datetime,0,7);
                                            $month_n++;
                                        }else{
                                            $month_flag = 0;
                                            //continue;
                                        }
                                        //
                                        for($i=0;$i<count($hos_num);$i++)
                                        {
                                            if($diag->clinic_id == $hos_num[$i]){
                                                $flag = 1;
                                                break;
                                            }

                                        }
                                        if($flag==0){
                                            $hos_num[$hos_n]=$diag->clinic_id;
                                            $hos_n++;
                                        }else{
                                            $flag = 0;
                                            //continue;
                                        }

                                        //한달에 한펫주인의 평균리용회수
                                        $pet_average_num++;
                                        for($i=0;$i<count($pet_num);$i++)
                                        {
                                        if($diag->pet_id == $pet_num[$i]){
                                            $pet_flag = 1;
                                                break;
                                        }
                                        }
                                        if($pet_flag==0){
                                            $pet_num[$pet_n]=$diag->pet_id;
                                            $pet_n++;
                                        }else{
                                            $pet_flag = 0;
                                            //continue;
                                        }
                                   // echo substr($diag->end_datetime, 11, 2)."<br>";
                                    }elseif(!empty($clinics) and empty($prefecture)){

                                            for($k = 0; $k < count($clinics); $k++)
                                            {
                                                if(($diag->name == $clinics[$k]))
                                                {
                                                    $search_count++;
                                                    //동물종류 리용회수 구하기
                                                    $Petquery = \App\Pet::where('id', $diag->pet_id)->first();
                                                    if($Petquery['type']==1)$hos_animal[0]++;
                                                    elseif($Petquery['type']==2)$hos_animal[1]++;
                                                    elseif($Petquery['type']==3)$hos_animal[2]++;
                                                    elseif($Petquery['type']==4)$hos_animal[3]++;
                                                    elseif($Petquery['type']==5)$hos_animal[4]++;
                                                    elseif($Petquery['type']==6)$hos_animal[5]++;
                                                    elseif($Petquery['type']==7)$hos_animal[6]++;
                                                    elseif($Petquery['type']==8)$hos_animal[7]++;
                                                    //시간별 리용회수 구하기
                                                    $d_time=substr($diag->reserve_datetime,11,2);
                                                    if($d_time == 0 or $d_time < 9)$hos_hour[0]++;
                                                    elseif($d_time > 8  or $d_time < 12)$hos_hour[1]++;
                                                    elseif($d_time > 11 or $d_time < 16)$hos_hour[2]++;
                                                    elseif($d_time > 15 or $d_time < 19)$hos_hour[3]++;
                                                    elseif($d_time > 18 or $d_time < 24)$hos_hour[4]++;
                                                    //주별 리용회수 구하기
                                                    $wday = substr($diag->reserve_datetime,0,10);
                                                    $currentWeekNumber = date('w',strtotime($wday));
                                                    $hos_week[$currentWeekNumber]++;

                                                    //평균리용시간  합 $average_time
                                                    if($diag->end_datetime !=null and $diag->start_datetime != null)
                                                    {
                                                        $average_time_count++;
                                                        $average_time += hour_seconds($diag->end_datetime,$diag->start_datetime);
                                                    }

                                                    //하루당 한병원에서의 리용회수 합 $hospital_average_num
                                                    $hospital_average_num++;
                                                    for($i=0;$i<count($hos_day);$i++)
                                                    {
                                                        if(substr($diag->reserve_datetime,0,10) == $hos_day[$i]){
                                                            $day_flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($day_flag==0){
                                                        $hos_day[$day_n]=substr($diag->reserve_datetime,0,10);
                                                        $day_n++;
                                                    }else{
                                                        $day_flag = 0;
                                                        //continue;
                                                    }
                                                    //
                                                    for($i=0;$i<count($hos_month);$i++)
                                                    {
                                                        if(substr($diag->reserve_datetime,0,7) == $hos_month[$i]){
                                                            $month_flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($month_flag==0){
                                                        $hos_month[$month_n]=substr($diag->reserve_datetime,0,7);
                                                        $month_n++;
                                                    }else{
                                                        $month_flag = 0;
                                                        //continue;
                                                    }
                                                    //
                                                    for($i=0;$i<count($hos_num);$i++)
                                                    {
                                                        if($diag->clinic_id == $hos_num[$i]){
                                                            $flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($flag==0){
                                                        $hos_num[$hos_n]=$diag->clinic_id;
                                                        $hos_n++;
                                                    }else{
                                                        $flag = 0;
                                                        //continue;
                                                    }

                                                    //한달에 한펫주인의 평균리용회수
                                                    $pet_average_num++;
                                                    for($i=0;$i<count($pet_num);$i++)
                                                    {
                                                    if($diag->pet_id == $pet_num[$i]){
                                                        $pet_flag = 1;
                                                            break;
                                                    }
                                                    }
                                                    if($pet_flag==0){
                                                        $pet_num[$pet_n]=$diag->pet_id;
                                                        $pet_n++;
                                                    }else{
                                                        $pet_flag = 0;
                                                        //continue;
                                                    }
                                                }
                                            }



                                    }elseif(empty($clinics)==0 and !empty($prefecture)){
                                        for($k = 0; $k < count($prefecture); $k++)
                                        {
                                            if(($diag->prefecture == $prefecture[$k]))
                                            {
                                                $search_count++;
                                                //동물종류 리용회수 구하기
                                                $Petquery = \App\Pet::where('id', $diag->pet_id)->first();
                                                if($Petquery['type']==1)$hos_animal[0]++;
                                                elseif($Petquery['type']==2)$hos_animal[1]++;
                                                elseif($Petquery['type']==3)$hos_animal[2]++;
                                                elseif($Petquery['type']==4)$hos_animal[3]++;
                                                elseif($Petquery['type']==5)$hos_animal[4]++;
                                                elseif($Petquery['type']==6)$hos_animal[5]++;
                                                elseif($Petquery['type']==7)$hos_animal[6]++;
                                                elseif($Petquery['type']==8)$hos_animal[7]++;
                                                //시간별 리용회수 구하기
                                                $d_time=substr($diag->reserve_datetime,11,2);
                                                if($d_time == 0 or $d_time < 9)$hos_hour[0]++;
                                                elseif($d_time > 8  or $d_time < 12)$hos_hour[1]++;
                                                elseif($d_time > 11 or $d_time < 16)$hos_hour[2]++;
                                                elseif($d_time > 15 or $d_time < 19)$hos_hour[3]++;
                                                elseif($d_time > 18 or $d_time < 24)$hos_hour[4]++;
                                                //주별 리용회수 구하기
                                                $wday = substr($diag->reserve_datetime,0,10);
                                                $currentWeekNumber = date('w',strtotime($wday));
                                                $hos_week[$currentWeekNumber]++;

                                                //평균리용시간  합 $average_time
                                                if($diag->end_datetime !=null and $diag->start_datetime != null)
                                                {
                                                    $average_time_count++;
                                                    $average_time += hour_seconds($diag->end_datetime,$diag->start_datetime);
                                                }

                                                //하루당 한병원에서의 리용회수 합 $hospital_average_num
                                                $hospital_average_num++;
                                                for($i=0;$i<count($hos_day);$i++)
                                                {
                                                    if(substr($diag->reserve_datetime,0,10) == $hos_day[$i]){
                                                        $day_flag = 1;
                                                        break;
                                                    }

                                                }
                                                if($day_flag==0){
                                                    $hos_day[$day_n]=substr($diag->reserve_datetime,0,10);
                                                    $day_n++;
                                                }else{
                                                    $day_flag = 0;
                                                    //continue;
                                                }
                                                //
                                                for($i=0;$i<count($hos_month);$i++)
                                                {
                                                    if(substr($diag->reserve_datetime,0,7) == $hos_month[$i]){
                                                        $month_flag = 1;
                                                        break;
                                                    }

                                                }
                                                if($month_flag==0){
                                                    $hos_month[$month_n]=substr($diag->reserve_datetime,0,7);
                                                    $month_n++;
                                                }else{
                                                    $month_flag = 0;
                                                    //continue;
                                                }
                                                //
                                                for($i=0;$i<count($hos_num);$i++)
                                                {
                                                    if($diag->clinic_id == $hos_num[$i]){
                                                        $flag = 1;
                                                        break;
                                                    }

                                                }
                                                if($flag==0){
                                                    $hos_num[$hos_n]=$diag->clinic_id;
                                                    $hos_n++;
                                                }else{
                                                    $flag = 0;
                                                    //continue;
                                                }

                                                //한달에 한펫주인의 평균리용회수
                                                $pet_average_num++;
                                                for($i=0;$i<count($pet_num);$i++)
                                                {
                                                if($diag->pet_id == $pet_num[$i]){
                                                    $pet_flag = 1;
                                                        break;
                                                }
                                                }
                                                if($pet_flag==0){
                                                    $pet_num[$pet_n]=$diag->pet_id;
                                                    $pet_n++;
                                                }else{
                                                    $pet_flag = 0;
                                                    //continue;
                                                }
                                            }
                                        }
                                    }elseif(!empty($clinics) and !empty($prefecture)){
                                        for($k = 0; $k < count($clinics); $k++)
                                        {
                                            for($kk = 0; $kk < count($prefecture); $kk++){
                                                if(($diag->name == $clinics[$k]) and ($diag->prefecture == $prefecture[$kk]))
                                                {
                                                    $search_count++;
                                                    //동물종류 리용회수 구하기
                                                    $Petquery = \App\Pet::where('id', $diag->pet_id)->first();
                                                    if($Petquery['type']==1)$hos_animal[0]++;
                                                    elseif($Petquery['type']==2)$hos_animal[1]++;
                                                    elseif($Petquery['type']==3)$hos_animal[2]++;
                                                    elseif($Petquery['type']==4)$hos_animal[3]++;
                                                    elseif($Petquery['type']==5)$hos_animal[4]++;
                                                    elseif($Petquery['type']==6)$hos_animal[5]++;
                                                    elseif($Petquery['type']==7)$hos_animal[6]++;
                                                    elseif($Petquery['type']==8)$hos_animal[7]++;
                                                    //시간별 리용회수 구하기
                                                    $d_time=substr($diag->reserve_datetime,11,2);

                                                    if($d_time < 9)$hos_hour[0]++;
                                                    elseif($d_time > 8  or $d_time < 12)$hos_hour[1]++;
                                                    elseif($d_time > 11 or $d_time < 16)$hos_hour[2]++;
                                                    elseif($d_time > 15 or $d_time < 19)$hos_hour[3]++;
                                                    elseif($d_time > 18 or $d_time < 24)$hos_hour[4]++;
                                                    //주별 리용회수 구하기
                                                    $wday = substr($diag->reserve_datetime,0,10);
                                                    $currentWeekNumber = date('w',strtotime($wday));
                                                    $hos_week[$currentWeekNumber]++;

                                                    //평균리용시간  합 $average_time
                                                    if($diag->end_datetime !=null and $diag->start_datetime != null)
                                                    {
                                                        $average_time_count++;
                                                        $average_time += hour_seconds($diag->end_datetime,$diag->start_datetime);
                                                    }

                                                    //하루당 한병원에서의 리용회수 합 $hospital_average_num
                                                    $hospital_average_num++;
                                                    for($i=0;$i<count($hos_day);$i++)
                                                    {
                                                        if(substr($diag->reserve_datetime,0,10) == $hos_day[$i]){
                                                            $day_flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($day_flag==0){
                                                        $hos_day[$day_n]=substr($diag->reserve_datetime,0,10);
                                                        $day_n++;
                                                    }else{
                                                        $day_flag = 0;
                                                        //continue;
                                                    }
                                                    //
                                                    for($i=0;$i<count($hos_month);$i++)
                                                    {
                                                        if(substr($diag->reserve_datetime,0,7) == $hos_month[$i]){
                                                            $month_flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($month_flag==0){
                                                        $hos_month[$month_n]=substr($diag->reserve_datetime,0,7);
                                                        $month_n++;
                                                    }else{
                                                        $month_flag = 0;
                                                        //continue;
                                                    }
                                                    //
                                                    for($i=0;$i<count($hos_num);$i++)
                                                    {
                                                        if($diag->clinic_id == $hos_num[$i]){
                                                            $flag = 1;
                                                            break;
                                                        }

                                                    }
                                                    if($flag==0){
                                                        $hos_num[$hos_n]=$diag->clinic_id;
                                                        $hos_n++;
                                                    }else{
                                                        $flag = 0;
                                                        //continue;
                                                    }

                                                    //한달에 한펫주인의 평균리용회수
                                                    $pet_average_num++;
                                                    for($i=0;$i<count($pet_num);$i++)
                                                    {
                                                    if($diag->pet_id == $pet_num[$i]){
                                                        $pet_flag = 1;
                                                            break;
                                                    }
                                                    }
                                                    if($pet_flag==0){
                                                        $pet_num[$pet_n]=$diag->pet_id;
                                                        $pet_n++;
                                                    }else{
                                                        $pet_flag = 0;
                                                        //continue;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                //평균 상담시간
                                if($average_time != 0)$average_time/=$average_time_count;
                                $hours = floor($average_time / 3600);
                                $minutes = floor(($average_time / 60) % 60);
                                $seconds = $average_time % 60;

                                //평균 병원리용회수
                                if($hospital_average_num != 0) $hospital_average_num = floor($hospital_average_num / count($hos_day));

                                //한달 펫주인 평균리용 회수
                                if($pet_average_num != 0)$pet_average_num = floor(($pet_average_num / count($pet_num)));

                                //1달에 1번이라도 리용하고 있는 병원프로수
                                $hos_percent = count($hos_num)*100 / count($clinic_query);
                                $hos_percent=number_format((float)$hos_percent, 2, '.', '');
                                //3달에 한번이라고 리용하고 있는 펫주인 프로수
                                $pets_percent = count($pet_num)*100 / count($pets_query);
                                $pets_percent=number_format((float)$pets_percent, 2, '.', '');
                                //주별 리용회수 프로수
                                if(count($hos_day) !=0)
                                {
                                    $su =($hos_week[0] / $search_count)*100;//count($hos_day))*100;
                                    $mo =($hos_week[1] / $search_count)*100;//count($hos_day))*100;
                                    $tu =($hos_week[2] / $search_count)*100;//count($hos_day))*100;
                                    $we =($hos_week[3] / $search_count)*100;//count($hos_day))*100;
                                    $th =($hos_week[4] / $search_count)*100;//count($hos_day))*100;
                                    $fr =($hos_week[5] / $search_count)*100;//count($hos_day))*100;
                                    $sa =($hos_week[6] / $search_count)*100;//count($hos_day))*100;
                                    $su = number_format((float)$su, 2, '.', '');
                                    $mo = number_format((float)$mo, 2, '.', '');
                                    $tu = number_format((float)$tu, 2, '.', '');
                                    $we = number_format((float)$we, 2, '.', '');
                                    $th = number_format((float)$th, 2, '.', '');
                                    $fr = number_format((float)$fr, 2, '.', '');
                                    $sa = number_format((float)$sa, 2, '.', '');

                                    //시간별 리용회수 프로수
                                    $h09 = ($hos_hour[0] / $search_count)*100;//count($hos_day))*100;
                                    $h912 = ($hos_hour[1] / $search_count)*100;//count($hos_day))*100;
                                    $h1216 = ($hos_hour[2] / $search_count)*100;//count($hos_day))*100;
                                    $h1619 = ($hos_hour[3] / $search_count)*100;//count($hos_day))*100;
                                    $h1924 = ($hos_hour[4] / $search_count)*100;//count($hos_day))*100;
                                    $h09 = number_format((float)$h09, 2, '.', '');
                                    $h912 = number_format((float)$h912, 2, '.', '');
                                    $h1216 = number_format((float)$h1216, 2, '.', '');
                                    $h1619 = number_format((float)$h1619, 2, '.', '');
                                    $h1924 = number_format((float)$h1924, 2, '.', '');
                                    //동물종류별 리용회수 프로수
                                    $hos_animal[0] = ($hos_animal[0] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[1] = ($hos_animal[1] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[2] = ($hos_animal[2] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[3] = ($hos_animal[3] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[4] = ($hos_animal[4] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[5] = ($hos_animal[5] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[6] = ($hos_animal[6] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[7] = ($hos_animal[7] / $search_count)*100;//count($hos_day))*100;
                                    $hos_animal[0] = number_format((float)$hos_animal[0], 2, '.', '');
                                    $hos_animal[1] = number_format((float)$hos_animal[1], 2, '.', '');
                                    $hos_animal[2] = number_format((float)$hos_animal[2], 2, '.', '');
                                    $hos_animal[3] = number_format((float)$hos_animal[3], 2, '.', '');
                                    $hos_animal[4] = number_format((float)$hos_animal[4], 2, '.', '');
                                    $hos_animal[5] = number_format((float)$hos_animal[5], 2, '.', '');
                                    $hos_animal[6] = number_format((float)$hos_animal[6], 2, '.', '');
                                    $hos_animal[7] = number_format((float)$hos_animal[7], 2, '.', '');

                                }

                                //echo $currentWeekNumber = date('w',strtotime('2019-11-3'));

                            }
                            ?>
                            <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly value="{{ $hours}}:{{ $minutes }}:{{ $seconds }}" class="form-control">
                                    <input type="hidden" name="hours" value="{{ $hours}}">
                                    <input type="hidden" name="minutes" value="{{ $minutes }}">
                                    <input type="hidden" name="seconds" value="{{  $seconds }}">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">/件</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly value="{{ $hospital_average_num }}" class="form-control">
                                    <input type="hidden" name="hospital_average_num" value="{{ $hospital_average_num}}">

                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">回/日/病院</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly value="{{ $pet_average_num }}" class="form-control">
                                    <input type="hidden" name="pet_average_num" value="{{ $pet_average_num}}">

                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">回/月/飼い主様</label>
                            </div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-md-right">アクティブ率（動物病院）</label>

                                <div class="col-md-1">
                                    <input type="text" readonly value="{{ $hos_percent }}" class="form-control">
                                    <input type="hidden" name="hos_percent" value="{{ $hos_percent}}">

                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">%</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">アクティブ率（飼い主様）</label>

                                <div class="col-md-1">
                                    <input type="hidden" name="pets_percent" value="{{ $pets_percent}}">
                                    <input type="text" readonly value="{{ $pets_percent }}" class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">%</label>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="mb-2"><hr></div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">利用曜日内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>月</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $mo }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="mo" value="{{ $mo}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $mo }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>火</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $tu }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="tu" value="{{ $tu}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $tu }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>水</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $we }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="we" value="{{ $we}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $we }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>木</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $th }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="th" value="{{ $th}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $th }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>金</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $fr }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="fr" value="{{ $fr}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $fr }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>土</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $sa }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="sa" value="{{ $sa}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $sa }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>日</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $su }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="su" value="{{ $su}}">
                                                            <div class="progress-bar bg-success" style="width: {{ $su }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">利用時間帯内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>0-9</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $h09 }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="h09" value="{{ $h09}}">
                                                            <div class="progress-bar bg-warning" style="width: {{ $h09 }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>9-12</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $h912 }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="h912" value="{{ $h912}}">
                                                            <div class="progress-bar bg-warning" style="width: {{ $h912 }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>12-16</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $h1216 }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="h1216" value="{{ $h1216}}">
                                                            <div class="progress-bar bg-warning" style="width: {{ $h1216 }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>16-19</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $h1619 }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="h1619" value="{{ $h1619}}">
                                                            <div class="progress-bar bg-warning" style="width: {{ $h1619 }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>19-24</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $h1924 }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <input type="hidden" name="h1924" value="{{ $h1924}}">
                                                            <div class="progress-bar bg-warning" style="width: {{ $h1924 }}%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">動物種内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>犬</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[0] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[0] }}%"></div>
                                                            <input type="hidden" name="hos_animal0" value="{{ $hos_animal[0]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>猫</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[1] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[1] }}%"></div>
                                                            <input type="hidden" name="hos_animal1" value="{{ $hos_animal[1]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>ハムスター</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[3] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[3] }}%"></div>
                                                            <input type="hidden" name="hos_anima3" value="{{ $hos_animal[3]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>フェレット</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[4] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[4] }}%"></div>
                                                            <input type="hidden" name="hos_animal4" value="{{ $hos_animal[4]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>うさぎ</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[5] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[5] }}%"></div>
                                                            <input type="hidden" name="hos_animal5" value="{{ $hos_animal[5]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>鳥</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[6] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[6] }}%"></div>
                                                            <input type="hidden" name="hos_animal6" value="{{ $hos_animal[6]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>爬虫類</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[7] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[7] }}%"></div>
                                                            <input type="hidden" name="hos_animal7" value="{{ $hos_animal[7]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>その他</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>{{ $hos_animal[2] }}%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: {{ $hos_animal[2] }}%"></div>
                                                            <input type="hidden" name="hos_animal2" value="{{ $hos_animal[2]}}">

                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                        {{-- <div class="col-12 col-sm-6 col-md-3">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">科目内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>内部の</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>整形外科の</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>神経外科</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>皮膚科学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>心臓学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>尿学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>眼科</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>歯科学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>リハビリテーション</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>他</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div> --}}
                        <!-- /.col -->
                    </div>

                    <div class="form-group col-md-12 text-right">
                            <button type="submit" class="btn btn-primary" id="">PDF出力</button>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->
<?php
    function hour_seconds($endtime, $starttime)
    {
        $end_second = 3600*substr($endtime, 11, 2) + 60*substr($endtime, 14, 2) + substr($endtime, 17, 2);
        $start_second = 3600*substr($starttime, 11, 2) + 60*substr($starttime, 14, 2) + substr($starttime, 17, 2);
        return $end_second - $start_second;

    }
?>
<script>
  $(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
