
@extends('layouts.app.admin.header')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>売上動物病院</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/hospital_analysis_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-0 col-form-label">表示&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-1">
                                    <select name="year" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <label for="pet" class="col-md-1 col-form-label text-md-center"></label>
                                <label for="pet" class="col-md-0 col-form-label">期間指定して表示</label>

                                <div class="col-md-1">
                                    <select name="year1" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month1" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day1" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日~</label>
                                <div class="col-md-1">
                                    <select name="year2" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month2" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day2" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日</label>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-0 col-form-label text-md-right">県別&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-2">

                                       <div class="form-group">
                                        <select class="select2" name="prefecture[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            {{-- @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else --}}
                                                <option
                                                    value="{{ $pref }}"
                                                    {{-- {{ $pref == '東京都' ? 'selected' : '' }} --}}
                                                >{{ $pref }}</option>
                                            {{-- @endif --}}
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3"></div>
                                <label for="price" class="col-md-0 col-form-label text-md-right">病院別</label>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="select2" name="clinics[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @if(!empty($clinic_query))
                                            @foreach ($clinic_query as $row)
                                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                <div class="card-body table-responsive p-1" style="height: 300px;">
                    <table id="example2" class="table table-head-fixed" style="font-size:12px">
                        <thead>
                        <tr style="height:80px">
                            <th>動物病院No</th>
                            <th>動物病院名</th>
                            <th>予約料</th>
                            <th>診察料</th>
                            <th>システム利用料</th>
                            <th>配送資材料</th>
                            <th>振込手数料</th>
                            <th>振込金額</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9"></td>
                        </tr>
                        </tbody>
                    </table>
                 </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->

<script>
  $(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
