<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::to('/') }}/admins/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        {{-- <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> --}}
        {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
        <!-- jQuery -->
        <script src="{{ URL::to('/') }}/admins/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::to('/') }}/admins/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{ URL::to('/') }}/admins/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <!-- Tempusdominus Bootstrap 4 -->
        <!-- overlayScrollbars -->
        <script src="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::to('/') }}/admins/dist/js/adminlte.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>PDF</title>
<style>
        @font-face{
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        @font-face{
            font-family: ipag;
            font-style: bold;
            font-weight: bold;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        body {
        font-family: ipag;
        }

body {
font-family: ipag;
}
</style>
</head>
<body>

<table border="1"  width="100%" style="width:100%;font-size:12px;">
        <thead>
        <tr style="height:80px;border-bottom:1pt solid black;">
            <th>動物病院No</th>
            <th>動物病院名</th>
            <th>予約料</th>
            <th>診察料</th>
            <th>システム利用料</th>
            <th>配送資材料</th>
            <th>振込手数料</th>
            <th>振込金額</th>
        </tr>
        </thead>
        <tbody>
            @if(!empty($clinicId))
            <?php $sum_a=0;$sum_b=0;$sum_c=0;$sum_d=0;$sum_e=0;$sum_hos=0; ?>
                @for($i=0;$i<count($clinicId);$i++)
                    <tr style="width:100%;font-size:12px;border-bottom:1pt solid black;">
                        <td>v{{ $clinicId[$i] }}</td>
                        <td>{{ $clinicName[$i] }}</td>
                        <td>¥{{ $clinic_a[$i] }}</td>
                        <td>¥{{ $clinic_b[$i] }}</td>
                        <td>¥{{ $clinic_c[$i] }}</td>
                        <td>¥{{ $clinic_d[$i] }}</td>
                        <td>-¥{{ $clinic_e[$i] }}</td>
                        <td>¥{{ $clinic_sum[$i] }}</td>
                    </tr>
                    <?php
                    $sum_a+=$clinic_a[$i];
                    $sum_b+=$clinic_b[$i];
                    $sum_c+=$clinic_c[$i];
                    $sum_d+=$clinic_d[$i];
                    $sum_e+=$clinic_e[$i];
                    $sum_hos+=$clinic_sum[$i];
                    ?>
                @endfor
                <tr >
                    <td></td>
                    <td align="right">合計</td>
                    <td>¥{{ $sum_a }}</td>
                    <td>¥{{ $sum_b }}</td>
                    <td>¥{{ $sum_c }}</td>
                    <td>¥{{ $sum_d }}</td>
                    <td>¥{{ $sum_e }}</td>
                    <td>¥{{ $sum_hos }}</td>
                </tr>
            @endif
        </tbody>
</table>
</body>
</html>
