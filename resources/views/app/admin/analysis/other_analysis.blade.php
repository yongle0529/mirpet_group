
@extends('layouts.app.admin.header')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>その他</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/other_analysis_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-0 col-form-label">表示&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-1">
                                    <select name="year" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <label for="pet" class="col-md-1 col-form-label text-md-center"></label>
                                <label for="pet" class="col-md-0 col-form-label">期間指定して表示</label>

                                <div class="col-md-1">
                                    <select name="year1" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month1" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day1" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日~</label>
                                <div class="col-md-1">
                                    <select name="year2" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month2" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day2" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日</label>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-0 col-form-label text-md-right">県別&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-2">

                                       <div class="form-group">
                                        <select class="select2" name="prefecture[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            {{-- @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else --}}
                                                <option
                                                    value="{{ $pref }}"
                                                    {{-- {{ $pref == '東京都' ? 'selected' : '' }} --}}
                                                >{{ $pref }}</option>
                                            {{-- @endif --}}
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3"></div>
                                <label for="price" class="col-md-0 col-form-label text-md-right">病院別</label>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="select2" name="clinics[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @if(!empty($clinic_query))
                                            @foreach ($clinic_query as $row)
                                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">/件</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">回/日/病院</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">平均利用時間</label>

                                <div class="col-md-1">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">回/月/飼い主様</label>
                            </div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-md-right">アクティブ率（動物病院）</label>

                                <div class="col-md-1">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">%</label>
                                <label for="pet" class="col-md-2 col-form-label text-md-right">アクティブ率（飼い主様）</label>

                                <div class="col-md-1">
                                    <input type="text" readonly class="form-control">
                                </div>
                                <label for="pet" class="col-md-1 col-form-label text-md-left">%</label>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="mb-2"><hr></div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">利用曜日内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>月</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>火</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>水</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>木</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>金</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>土</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>日</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-success" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">利用時間帯内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>0-9</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>9-12</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>12-16</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>16-19</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>19-24</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-warning" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">動物種内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>犬</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>猫</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>ハムスター</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>フェレット</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>うさぎ</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>鳥</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>爬虫類</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>その他</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-primary" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                        {{-- <div class="col-12 col-sm-6 col-md-3">
                            <div class="card">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="hospital_name">科目内訳</label>
                                        <table class="table table-bordered table-hove">
                                            <thead>
                                            <tr>
                                                <th>内部の</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>整形外科の</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>神経外科</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>皮膚科学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>心臓学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>尿学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>眼科</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>歯科学</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>リハビリテーション</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>他</th>
                                                <th>
                                                    <div class="progress-group">
                                                        <span class="float-right"><b>0%</b></span>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar bg-danger" style="width: 0%"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div> --}}
                        <!-- /.col -->
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->

<script>
  $(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
