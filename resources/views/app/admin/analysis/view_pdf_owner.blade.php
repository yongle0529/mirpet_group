<!doctype html>
<html>
        <head>
                <meta charset="utf-8"/>
                <!-- Theme style -->
                <link rel="stylesheet" href="{{ URL::to('/') }}/admins/dist/css/adminlte.min.css">
                <!-- overlayScrollbars -->
                <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
                {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
                <!-- jQuery -->
                <script src="{{ URL::to('/') }}/admins/plugins/jquery/jquery.min.js"></script>
                <!-- jQuery UI 1.11.4 -->
                <script src="{{ URL::to('/') }}/admins/plugins/jquery-ui/jquery-ui.min.js"></script>
                <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
                <script>
                    $.widget.bridge('uibutton', $.ui.button)
                </script>
                <!-- Bootstrap 4 -->
                <script src="{{ URL::to('/') }}/admins/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                <!-- DataTables -->
                <script src="{{ URL::to('/') }}/admins/plugins/datatables/jquery.dataTables.js"></script>
                <script src="{{ URL::to('/') }}/admins/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
                <!-- Tempusdominus Bootstrap 4 -->
                <!-- overlayScrollbars -->
                <script src="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
                <!-- AdminLTE App -->
                <script src="{{ URL::to('/') }}/admins/dist/js/adminlte.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>PDF</title>
<style>
        @font-face{
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        @font-face{
            font-family: ipag;
            font-style: bold;
            font-weight: bold;
            src:url('{{ storage_path('fonts/ipag.ttf')}}');
        }
        body {
        font-family: ipag;
        }

body {
font-family: ipag;
}
</style>
</head>
<body>

<table border="1" width="100%" style="width:100%;font-size:12px">
        <thead>
        <tr style="height:80px">
                <th>飼い主様番号</th>
                <th>飼い主様名</th>
                <th>患者名</th>
                <th>動物病院名</th>
                <th>予約料</th>
                <th>診察料</th>
                <th>システム利用料</th>
                <th>配送手数料</th>
                <th>ご請求金額</th>
        </tr>
        </thead>
        <tbody>
            @if(!empty($petID))
            <?php $sum_a=0;$sum_b=0;$sum_c=0;$sum_d=0;$sum_e=0;$sum_hos=0; ?>
                @for($i=0;$i<count($petID);$i++)
                    <tr>
                            <td>p{{ $petID[$i] }}</td>
                            <td>{{ $petName[$i] }}</td>
                            <td>{{ $ownerName[$i] }}</td>
                            <td>{{ $clinicName[$i] }}</td>
                            <td>¥{{ $pet_a[$i] }}</td>
                            <td>¥{{ $pet_b[$i] }}</td>
                            <td>¥{{ $pet_c[$i] }}</td>
                            <td>¥{{ $pet_d[$i] }}</td>
                            <td>¥{{ $pet_sum[$i] }}</td>
                    </tr>
                    <?php
                    $sum_a+=$pet_a[$i];
                    $sum_b+=$pet_b[$i];
                    $sum_c+=$pet_c[$i];
                    $sum_d+=$pet_d[$i];
                    $sum_hos+=$pet_sum[$i];
                    ?>
                @endfor
                <tr >
                        <td></td>
                        <td></td>
                        <td></td>
                        <td  align="right">合計</td>
                        <td>¥{{ $sum_a }}</td>
                        <td>¥{{ $sum_b }}</td>
                        <td>¥{{ $sum_c }}</td>
                        <td>¥{{ $sum_d }}</td>
                        <td>¥{{ $sum_hos }}</td>
                </tr>
            @endif
        </tbody>
</table>
</body>
</html>
