
@extends('layouts.app.admin.header')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>売上飼い主様</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/owner_analysis_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-0 col-form-label">表示&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-1">
                                    <select name="year" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <label for="pet" class="col-md-1 col-form-label text-md-center"></label>
                                <label for="pet" class="col-md-0 col-form-label">期間指定して表示</label>

                                <div class="col-md-1">
                                    <select name="year1" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year1'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month1" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month1'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day1" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ $pre_data['day1'] == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日~</label>
                                <div class="col-md-1">
                                    <select name="year2" class="form-control select2" style="width: 100%;">
                                        <option value="0">----</option>
                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                        @foreach($years as $year)
                                            <option
                                                value="{{ $year }}"
                                                {{ $pre_data['year2'] == $year ? 'selected' : '' }}
                                            >{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">年</label>
                                <div class="col-md-1">
                                    <select name="month2" class="form-control select2 " style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 12) as $month)
                                            <option
                                                value="{{ $month }}"
                                                {{ $pre_data['month2'] == $month ? 'selected' : '' }}
                                            >{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">月</label>
                                <div class="col-md-1">
                                    <select name="day2" class="form-control select2" style="width: 100%;">
                                        <option value="0">--</option>
                                        @foreach(range(1, 31) as $day)
                                            <option
                                                value="{{ $day }}"
                                                {{ $pre_data['day2'] == $day ? 'selected' : '' }}
                                            >{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet" class="col-md-0 col-form-label">日</label>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-0 col-form-label text-md-right">県別&nbsp;&nbsp;&nbsp;&nbsp;</label>

                                <div class="col-md-2">

                                       <div class="form-group">
                                         <select class="select2" name="prefecture[]" multiple="multiple" data-placeholder="" style="width: 100%;">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            {{-- @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else --}}
                                                <option
                                                    value="{{ $pref }}"
                                                    @if(!empty($prefecture))
                                                    @for($i=0; $i<count($prefecture);$i++)
                                                    {{ $pref == $prefecture[$i] ? 'selected' : '' }}
                                                    @endfor
                                                    @endif
                                                >{{ $pref }}</option>
                                            {{-- @endif --}}
                                        @endforeach
                                        </select>
                                    </div>

                                </div>
                                <label for="price" class="col-md-2 col-form-label text-md-right">飼い主様番号</label>

                                <div class="col-md-2">
                                       <div class="form-group">
                                            <input type="number" name="pet_id" value="{{ $pre_data['pet_id'] }}" class="form-control">
                                        </div>
                                </div>

                                <div class="col-md-12 mb-3"></div>

                                <div class="col-md-4"></div>
                                <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
    </form>
    <form name="exam_frm" action="{{URL::to('/app/admin/owner_pdf_download')}}" method="POST">
        @csrf
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                <div class="card-body table-responsive p-1" style="height: 600px;">
                <table id="example2" class="table table-head-fixed" style="font-size:12px">
                    <thead>
                    <tr style="height:80px">
                    <th>飼い主様番号</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物病院名</th>
                    <th>予約料</th>
                    <th>診察料</th>
                    <th>システム利用料</th>
                    <th>配送手数料</th>
                    <th>ご請求金額</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($query)>0)
                    <?php
                    $system_fee = \App\SystemFee::first();
                    $sum_a=0;$sum_b=0;$sum_c=0;$sum_d=0;$sum_hos=0;$sum_e=0;$sum_f=0;$sum_p=0;$pr_len=0;$count=0;$flag=0;
                    $petID[0] = 0;
                    $petName[0] = 0;
                    $ownerName[0]=0;
                    $clinicName[0]=0;
                    $pet_a[0]=0;
                    $pet_b[0]=0;
                    $pet_c[0]=0;
                    $pet_d[0]=0;
                    $pet_sum[0]=0;
                    ?>
                    @if(!empty($prefecture))
                    {{-- @if(count($prefecture)>0) --}}
                    @foreach ($query as $diag)
                    @for($i = 0; $i < count($prefecture); $i++)
                    @if($diag->prefecture == $prefecture[$i])
                    <?php

                        $acc_query = \App\Accounting::where('diagnosis_id', $diag->id)->first();

                        $a=$diag->price;
                        $sum_a+=$a;
                        $b=$acc_query['medical_expenses'];
                        $sum_b+=$b;
                        $c=$acc_query['fee'];
                        $sum_c+=$c;
                        $d=$acc_query['delivery_cost'];//730;
                        $sum_d+=$d;
                        $e=300;
                        $sum_e+=$e;
                        $hos_sum=$acc_query['sum'];//$a+$b+$c+$d;
                        $sum_hos+=$hos_sum;

                        for($i=0;$i<count($petID);$i++){
                            if($diag->user_id == $petID[$i]){
                            $flag=1;
                            break;
                            }
                        }
                        if($flag==0){
                            $petID[$count] = $diag->user_id;
                            $petName[$count] = \App\User::find($diag->user_id)->last_name.\App\User::find($diag->user_id)->first_name;
                            $ownerName[$count]=\App\Pet::find($diag->pet_id)->name;
                            $clinicName[$count]=$diag->name;
                            $pet_a[$count]=$a;
                            $pet_b[$count]=$b;
                            $pet_c[$count]=$c;
                            $pet_d[$count]=$d;
                            $pet_sum[$count]=$hos_sum;
                            $count++;
                        }else{
                            $flag=0;
                            $pet_a[$count-1]+=$a;
                            $pet_b[$count-1]+=$b;
                            $pet_c[$count-1]+=$c;
                            $pet_d[$count-1]+=$d;
                            $pet_sum[$count-1]+=$hos_sum;
                        }
                    ?>
                    @break
                @endif
                @endfor

                @endforeach
                @for($i=0;$i<$count;$i++)
                <tr>
                    <td>
                            @if($petID[$i]<10)
                            P{{ date('ym').'000'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="P{{ date('ym').'000'.$petID[$i] }}">
                            @elseif($petID[$i]<100)
                            P{{ date('ym').'00'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="P{{ date('ym').'000'.$petID[$i] }}">
                            @elseif($petID[$i]<1000)
                            P{{ date('ym').'0'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="P{{ date('ym').'000'.$petID[$i] }}">
                            @else
                            P{{ date('ym').''.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="P{{ date('ym').'000'.$petID[$i] }}">
                            @endif
                    </td>
                    <td>{{ $petName[$i] }}</td>
                    <td>{{ $ownerName[$i] }}</td>
                    <td>{{ $clinicName[$i] }}</td>
                    <td>¥{{ $pet_a[$i] }}</td>
                    <td>¥{{ $pet_b[$i] }}</td>
                    <td>¥{{ $pet_c[$i] }}</td>
                    <td>¥{{ $pet_d[$i] }}</td>
                    <td>¥{{ $pet_sum[$i] }}</td>
                    <input type="hidden" name="petName[]" value="{{ $petName[$i] }}">
                    <input type="hidden" name="ownerName[]" value="{{ $ownerName[$i] }}">
                    <input type="hidden" name="clinicName[]" value="{{ $clinicName[$i] }}">
                    <input type="hidden" name="pet_a[]" value="{{ $pet_a[$i] }}">
                    <input type="hidden" name="pet_b[]" value="{{ $pet_b[$i] }}">
                    <input type="hidden" name="pet_c[]" value="{{ $pet_c[$i] }}">
                    <input type="hidden" name="pet_d[]" value="{{ $pet_d[$i] }}">
                    <input type="hidden" name="pet_sum[]" value="{{ $pet_sum[$i] }}">
                </tr>
                 @endfor
                @else
                    @foreach ($query as $diag)
                    <?php

                        $acc_query = \App\Accounting::where('diagnosis_id', $diag->id)->first();

                        $a=$diag->price;
                        $sum_a+=$a;
                        $b=$acc_query['medical_expenses'];
                        $sum_b+=$b;
                        $c=$acc_query['fee'];
                        $sum_c+=$c;

                        $d=$acc_query['delivery_cost'];//730;
                        $sum_d+=$d;
                        $e=300;
                        $sum_e+=$e;
                        $hos_sum=$acc_query['sum'];//$a+$b+$c+$d;
                        $sum_hos+=$hos_sum;
                        for($i=0;$i<count($petID);$i++){
                            if($diag->user_id == $petID[$i]){
                            $flag=1;
                            break;
                            }
                        }
                        if($flag==0){
                            $petID[$count] = $diag->user_id;
                            $petName[$count] = \App\User::find($diag->user_id)->last_name.\App\User::find($diag->user_id)->first_name;
                            $ownerName[$count]=\App\Pet::find($diag->pet_id)->name;
                            $clinicName[$count]=$diag->name;
                            $pet_a[$count]=$a;
                            $pet_b[$count]=$b;
                            $pet_c[$count]=$c;
                            $pet_d[$count]=$d;
                            $pet_sum[$count]=$hos_sum;
                            $count++;
                        }else{
                            $flag=0;
                            $pet_a[$count-1]+=$a;
                            $pet_b[$count-1]+=$b;
                            $pet_c[$count-1]+=$c;
                            $pet_d[$count-1]+=$d;
                            $pet_sum[$count-1]+=$hos_sum;
                        }
                    ?>


                @endforeach
                @for($i=0;$i<$count;$i++)
                <tr>
                    <td>
                            @if($petID[$i]<10)
                            P{{ date('ym').'000'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="p{{ date('ym').'000'.$petID[$i] }}">
                            @elseif($petID[$i]<100)
                            P{{ date('ym').'00'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="p{{ date('ym').'000'.$petID[$i] }}">
                            @elseif($petID[$i]<1000)
                            P{{ date('ym').'0'.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="p{{ date('ym').'000'.$petID[$i] }}">
                            @else
                            P{{ date('ym').''.$petID[$i] }}
                            <input type="hidden" name="petID[]" value="p{{ date('ym').'000'.$petID[$i] }}">
                            @endif
                    </td>
                    <td>{{ $petName[$i] }}</td>
                    <td>{{ $ownerName[$i] }}</td>
                    <td>{{ $clinicName[$i] }}</td>
                    <td>¥{{ $pet_a[$i] }}</td>
                    <td>¥{{ $pet_b[$i] }}</td>
                    <td>¥{{ $pet_c[$i] }}</td>
                    <td>¥{{ $pet_d[$i] }}</td>
                    <td>¥{{ $pet_sum[$i] }}</td>

                    <input type="hidden" name="petName[]" value="{{ $petName[$i] }}">
                    <input type="hidden" name="ownerName[]" value="{{ $ownerName[$i] }}">
                    <input type="hidden" name="clinicName[]" value="{{ $clinicName[$i] }}">
                    <input type="hidden" name="pet_a[]" value="{{ $pet_a[$i] }}">
                    <input type="hidden" name="pet_b[]" value="{{ $pet_b[$i] }}">
                    <input type="hidden" name="pet_c[]" value="{{ $pet_c[$i] }}">
                    <input type="hidden" name="pet_d[]" value="{{ $pet_d[$i] }}">
                    <input type="hidden" name="pet_sum[]" value="{{ $pet_sum[$i] }}">
                </tr>
                 @endfor
                @endif
                @endif
            </tbody>

                    @if(count($query)>0)
                            <tr >
                                <td></td>
                                <td></td>
                                <td></td>
                                <td  align="right">合計</td>
                                <td>¥{{ $sum_a }}</td>
                                <td>¥{{ $sum_b }}</td>
                                <td>¥{{ $sum_c }}</td>
                                <td>¥{{ $sum_d }}</td>
                                <td>¥{{ $sum_hos }}</td>
                            </tr>

                        @endif
                </table>
                <div class="form-group col-md-12 text-right">
                        <a onclick="exportTableToCSV('売上飼い主様.csv')" href="#" class="btn btn-danger">CSV出力</a>
                        <button type="submit" class="btn btn-primary" id="">PDF出力</button>
                </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
    <script src="{{URL::to('/')}}/assets/tableHTMLExport.js"></script>
    <script src="{{URL::to('/')}}/assets/pdfassets/dist/jspdf.plugin.autotable.js"></script>
    {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.2.4/jspdf.plugin.autotable.js"></script>  --}}

    <script>
        $('#json').on('click', function() {
            $("#example2").tableHTMLExport({
                type: 'json',
                filename: 'sample.json'
            });
        })
        $('#csv').on('click', function() {
            $("#example2").tableHTMLExport({
                type: 'csv',
                filename: 'sample.csv'
            });
        })
        $('#pdf').on('click', function() {
            $("#example2").tableHTMLExport({
                type: 'pdf',
                filename: '売上飼い主様.pdf'
            });
        })
    </script>
    <script>
        try {
            fetch(new Request("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", {
                method: 'HEAD',
                mode: 'no-cors'
            })).then(function(response) {
                return true;
            }).catch(function(e) {
                var carbonScript = document.createElement("script");
                carbonScript.src = "//cdn.carbonads.com/carbon.js?serve=CK7DKKQU&placement=wwwjqueryscriptnet";
                carbonScript.id = "_carbonads_js";
                document.getElementById("carbon-block").appendChild(carbonScript);
            });
        } catch (error) {
            console.log(error);
        }
    </script>
</body>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<script type="text/javascript">
    function downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV file
        csvFile = new Blob([csv], {type: "text/csv"});

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Hide download link
        downloadLink.style.display = "none";

        // Add the link to DOM
        document.body.appendChild(downloadLink);

        // Click download link
        downloadLink.click();
    }

    function exportTableToCSV(filename) {

        var csv = [];
        var rows = document.querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++)
                row.push(('\uFEFF'+cols[j].innerText));

            csv.push(row.join(","));
        }

        // Download CSV file
        downloadCSV(csv.join("\n"), filename);
    }
    function demoFromHTML() {
        var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#customers')[0];

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function(element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, {// y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },
        function(dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            pdf.save('Test.pdf');
        }
        , margins);
    }
</script>
<script>
  $(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
