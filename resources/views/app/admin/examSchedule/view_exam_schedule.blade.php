
@extends('layouts.app.admin.header')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>診察一覧</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/exam_schedule_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>年</label>
                            <select name="year" class="form-control select2" style="width: 100%;">
                                <option value="0">----</option>
                                <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                @foreach($years as $year)
                                    <option
                                        value="{{ $year }}"
                                        {{ $data['year'] == $year ? 'selected' : '' }}
                                    >{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>月</label>
                            <select name="month" class="form-control select2" style="width: 100%;">
                                <option value="0">--</option>
                                @foreach(range(1, 12) as $month)
                                    <option
                                        value="{{ $month }}"
                                        {{ $data['month'] == $month ? 'selected' : '' }}
                                    >{{ $month }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>日</label>
                            <select name="day" class="form-control select2" style="width: 100%;">
                                <option value="0">--</option>+
                                @foreach(range(1, 31) as $day)
                                    <option
                                        value="{{ $day }}"
                                        {{ $data['day'] == $day ? 'selected' : '' }}
                                    >{{ $day }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="hospital_name">動物病院番号</label>
                            <input id="hospital_name" value="{{ $data['hos_no'] }}" name="hos_no" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">動物病院名</label>
                            <input id="hospital_name" value="{{ $data['hos_name'] }}" name="hos_name" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">電話番号</label>
                            <input id="hospital_name" value="{{ $data['tel'] }}" name="tel" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">都道府県</label>
                            <input id="hospital_name" value="{{ $data['city'] }}" name="city" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">区市</label>
                            <input id="hospital_name" value="{{ $data['district'] }}" name="district" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="row col-md-12  text-right">
                            <div class="col-md-11"></div>
                            <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                        </div>
            </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">

                    {{-- <table class="table table-bordered table-hove table-head-fixed" border="1" width=100%>
                        <tr align="center" style="background-color: #ffeee3;font-size:14px;height:50px">
                            <td align="center" >
                            <strong> 診察内容</strong>
                            </td>
                            <td align="center" >
                            <strong> 病院へのお支払い明細</strong>
                            </td>
                            <td align="center" >
                            <strong> 患者様請求額</strong>
                            </td>
                        </tr>
                    </table> --}}
                    <div class="card-body table-responsive p-1" style="height: 800px;">
                        <table id="example2" class="table table-bordered table-hove table-head-fixed" style="font-size:12px">
                            <thead>
                                    <tr align="center" style="background-color: #ffeee3;font-size:14px;height:50px">
                                            <td colspan="10" align="center" >
                                            <strong> 診察内容</strong>
                                            </td>
                                            <td colspan="5" align="center" >
                                            <strong> 病院へのお支払い明細</strong>
                                            </td>
                                            <td colspan="5" align="center" >
                                            <strong> 患者様請求額</strong>
                                            </td>
                                    </tr>
                                    <tr>
                                            <th >ステータス</th>
                                            <th >日付</th>
                                            <th >動物病院No</th>
                                            <th >動物病院名</th>
                                            <th >カルテNo</th>
                                            <th >飼い主様名</th>
                                            <th >患者名</th>
                                            <th >担当医</th>
                                            <th >目的</th>
                                            <th >配送</th>
                                            <th >予約料</th>
                                            <th >診察料</th>
                                            <th >システム利用料</th>
                                            <th >配送資材料</th>
                                            <th >振込金額</th>
                                            <th >予約料</th>
                                            <th >診察料</th>
                                            <th >システム利用料</th>
                                            <th >配送料</th>
                                            <th >請求金額</th>
                                     </tr>
                            </thead>
                            {{--  <thead>
                            <tr>
                                <th >ステータス</th>
                                <th >日付</th>
                                <th >動物病院No</th>
                                <th >動物病院名</th>
                                <th >カルテNo</th>
                                <th >飼い主様名</th>
                                <th >患者名</th>
                                <th >担当医</th>
                                <th >目的</th>
                                <th >配送</th>
                                <th >予約料</th>
                                <th >診察料</th>
                                <th >システム利用料</th>
                                <th >配送資材料</th>
                                <th >振込金額</th>
                                <th >予約料</th>
                                <th >診察料</th>
                                <th >システム利用料</th>
                                <th >配送料</th>
                                <th >請求金額</th>
                            </tr>
                            </thead>  --}}
                            <tbody>
                                @if(count($query)>0)
                                <?php $sum_a=0;$sum_b=0;$sum_c=0;$sum_d=0;$sum_hos=0;$sum_e=0;$sum_f=0;$sum_p=0;$auto_num=0;$curr_datetime = date("Y-m-d");?>
                                @foreach($query as $diag)
                                <tr>
                                    <?php
                                    $acc_query = \App\Accounting::where('diagnosis_id', $diag->cata_no)->first();
                                    $exam="";
                                    $edatetime="";
                                    $auto_num++;
                                    $diag_query = \App\Diagnosis::where('cata_no', $diag->cata_no)->first();
                                    if($diag_query->status==0){
                                        //$exam="진찰대기";
                                        $exam="診察待ち";
                                        $edatetime=$diag->reserve_datetime;
                                    }
                                    if($diag_query->status==1 or $diag_query->status==5){
                                        if((!empty($acc_query) && $acc_query->status != 2) || empty($acc_query)){
                                        //$exam="회계대기";
                                        $exam="会計待機";
                                        $edatetime=$diag_query->reserve_datetime;
                                        }
                                    }
                                    //if(($diag_query->status==0 OR $diag_query->status==1 OR $diag_query->status==3 ) and (!empty($acc_query) and $acc_query->next_remind_date!=null) ){
                                    if($diag_query->status==1 && !empty($acc_query) && $acc_query->status==2 &&  $acc_query->next_remind_date != null){
                                        //$exam="진찰완료";
                                        $exam="診察完了";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    //if(($diag_query->status==1 or $diag_query->status==2 or $diag_query->status==5) and (!empty($acc_query) and $acc_query->delivery_cost!=0) ){
                                    if($diag_query->status==1 && !empty($acc_query) && $acc_query->delivery_cost != 0 && $acc_query->status == 2){
                                    //  $exam="발송대기";
                                        $exam="発送待ち";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    if($diag_query->status==4 or $diag_query->status==3){
                                        //$exam="진찰완료";
                                        $exam="診察完了";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    if($diag_query->status==10){
                                        //$exam="취소완료";
                                        $exam="キャンセル完了";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    if($diag_query->status==2){
                                        //$exam="진찰완료";
                                        $exam="診察完了";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    if($diag_query->status==6){
                                        //$exam="취소";
                                        $exam="キャンセル";
                                        $edatetime=$diag_query->reserve_datetime;
                                    }
                                    $edatetime=$diag_query->reserve_datetime;
                                    if((!empty($acc_query) and $acc_query->delivery_cost!=0)){
                                        $delivery="有り";
                                    }else{
                                        $delivery="無い";
                                    }
                                    ?>
                                    <td>{{ $exam }}</td>
                                    <td>{{ $edatetime }}</td>
                                    <td>
                                        @if(\App\Clinic::find($diag_query->clinic_id)->id<10)
                                        V{{ date('ym').'00'.\App\Clinic::find($diag_query->clinic_id)->id }}
                                        @elseif(\App\Clinic::find($diag_query->clinic_id)->id<100)
                                        V{{ date('ym').'0'.\App\Clinic::find($diag_query->clinic_id)->id }}
                                        @else
                                        V{{ date('ym').''.\App\Clinic::find($diag_query->clinic_id)->id }}
                                        @endif
                                    </td>
                                    <td>{{\App\Clinic::find($diag_query->clinic_id)->name}}</td>
                                    <td>
                                        @if($diag_query->user_id<10)
                                        P{{ date('ym').'000'.$diag_query->user_id }}
                                        @elseif($diag_query->user_id<100)
                                        P{{ date('ym').'00'.$diag_query->user_id }}
                                        @elseif($diag_query->user_id<1000)
                                        P{{ date('ym').'0'.$diag_query->user_id }}
                                        @else
                                        P{{ date('ym').''.$diag_query->user_id }}
                                        @endif
                                    </td>
                                    <td>{{\App\User::find($diag_query->user_id)->last_name}}</td>
                                    <td>{{\App\Pet::find($diag_query->pet_id)->name}}</td>
                                    <td>{{\App\Clinic::find($diag_query->clinic_id)->manager_name}}</td>
                                    <td>{{ $diag_query->content }}</td>
                                    <td>{{ $delivery }}</td>
                                    <?php
                                    $system_fee = \App\SystemFee::first();
                                    $a=$diag->price;
                                    $sum_a+=$a;
                                    if(!empty($acc_query)){
                                        $b=$acc_query->medical_expenses;
                                        $d=$acc_query->delivery_cost;
                                        $f=$acc_query->delivery_cost;
                                    }
                                    else
                                    {
                                        $b=0;
                                        $d=0;
                                        $f=0;
                                    }

                                    $sum_b+=$b;
                                    $c=($a+$b)*($acc_query['clinic_fee'])/100;
                                    $sum_c+=$c;

                                    $sum_d+=$d;
                                    $hos_sum=($a+$b-$c+$d);
                                    $sum_hos+=$hos_sum;
                                    $e=$acc_query['fee'];
                                    $sum_e+=$e;

                                    $sum_f+=$f;
                                    $p_sum=$acc_query['sum'];//$a+$b+$e+$f;
                                    $sum_p+=$p_sum;
                                    ?>
                                    <td>¥{{ $a }}</td>
                                    <td>¥{{ $b }}</td>
                                    <td>-¥{{ $c }}</td>
                                    <td>¥{{ $d }}</td>
                                    <td>¥{{ $hos_sum }}</td>

                                    <td>¥{{ $a }}</td>
                                    <td>¥{{ $b }}</td>
                                    <td>¥{{ $e }}</td>
                                    <td>¥{{ $f }}</td>
                                    <td>¥{{ $p_sum }}</td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                            @if(count($query)>0)
                            <tr>
                                <td align="right" colspan="10">
                                    合計
                                </td>
                                <td>¥{{ $sum_a }}</td>
                                <td>¥{{ $sum_b }}</td>
                                <td>-¥{{ $sum_c }}</td>
                                <td>¥{{ $sum_d }}</td>
                                <td>¥{{ $sum_hos }}</td>
                                <td>¥{{ $sum_a }}</td>
                                <td>¥{{ $sum_b }}</td>
                                <td>¥{{ $sum_e }}</td>
                                <td>¥{{ $sum_f }}</td>
                                <td>¥{{ $sum_p }}</td>
                            </tr>
                            @endif
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->


    <script>
            $(function () {
              $("#example1").DataTable();
              $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
              });
            });
</script>
@endsection
