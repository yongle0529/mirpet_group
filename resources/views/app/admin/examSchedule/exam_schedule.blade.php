
@extends('layouts.app.admin.header')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>診察一覧</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/exam_schedule_search')}}" method="POST">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>年</label>
                            <select name="year" class="form-control select2" style="width: 100%;">
                                <option value="0">----</option>
                                <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                @foreach($years as $year)
                                    <option
                                        value="{{ $year }}"
                                        {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                    >{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>月</label>
                            <select name="month" class="form-control select2" style="width: 100%;">
                                <option value="0">--</option>
                                @foreach(range(1, 12) as $month)
                                    <option
                                        value="{{ $month }}"
                                        {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                    >{{ $month }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>日</label>
                            <select name="day" class="form-control select2" style="width: 100%;">
                                <option value="0">--</option>
                                @foreach(range(1, 31) as $day)
                                    <option
                                        value="{{ $day }}"
                                        {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                    >{{ $day }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="hospital_name">動物病院番号</label>
                            <input id="hospital_name" name="hos_no" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">動物病院名</label>
                            <input id="hospital_name" name="hos_name" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">電話番号</label>
                            <input id="hospital_name" name="tel" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">都道府県</label>
                            <input id="hospital_name" name="city" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="hospital_name">区市</label>
                            <input id="hospital_name" name="district" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="row col-md-12  text-right">
                            <div class="col-md-11"></div>
                            <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                        </div>
            </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">

                <table id="example2" class="table table-bordered table-hover" style="font-size:12px">
                    <thead>
                        <tr style="font-size:14px">
                            <td align="center" colspan="10">
                            <strong> 診察内容</strong>
                            </td>
                            <td align="center" colspan="5">
                            <strong> 病院へのお支払い明細</strong>
                            </td>
                            <td align="center" colspan="5">
                            <strong> 患者様請求額</strong>
                            </td>
                        </tr>
                    <tr>
                    <th>ステータス</th>
                    <th>日付</th>
                    <th>動物病院No</th>
                    <th>動物病院名</th>
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>担当医</th>
                    <th>目的</th>
                    <th>配送</th>
                    <th>予約料</th>
                    <th>診察料</th>
                    <th>システム利用料</th>
                    <th>配送資材料</th>
                    <th>振込金額</th>
                    <th>予約料</th>
                    <th>診察料</th>
                    <th>システム利用料</th>
                    <th>配送料</th>
                    <th>請求金額</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="20"></td>
                    </tr>
                    </tbody>
                </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>
@endsection
