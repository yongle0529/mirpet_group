
@extends('layouts.app.admin.header')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <h1>プロフィール設定</h1>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <form method="POST" action="{{ URL::to('/app/admin/account_new_setting/') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-success">


                        <div class="card-header">
                            <h3 class="card-title"><b><p></p></b></h3>

                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-group row">
                                <label id="fee" for="fee" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                                <div class="col-md-2">
                                    <input type="email" value="{{ $adminuser->email }}" placeholder="" required class="form-control" id="new_email" name="new_email" >
                                </div>
                                <label id="fee" for="fee" class="col-md-1 col-form-label text-md-right">パスワード</label>

                                    <div class="col-md-2 text-md-right">
                                        <input type="password" value="" required class="form-control" id="new_password" name="new_password" >
                                    </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="col-md-12 text-md-center">
                                        <button type="submit" class="btn btn-danger" ><i class="fas fa-user-edit"></i> 変更
                                        </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
