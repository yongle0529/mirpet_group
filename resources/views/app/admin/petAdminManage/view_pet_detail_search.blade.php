@extends('layouts.app.admin.header')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ペットオーナー管理-詳細</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header text-muted border-bottom-0">
                 <h4 class=""><b> 基本情報 </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">飼い主様番号: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">
                                        @if($user_query->id<10)
                                        P{{ date('ym').'000'.$user_query->id }}
                                        @elseif($user_query->id<100)
                                        P{{ date('ym').'00'.$user_query->id }}
                                        @elseif($user_query->id<1000)
                                        P{{ date('ym').'0'.$user_query->id }}
                                        @else
                                        P{{ date('ym').''.$user_query->id }}
                                        @endif
                                </span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">飼い主様名　姓: </label>
                                <label for="pet" class="col-md-2 col-form-label text-left"><span class="small">{{ $user_query->last_name }}</span></label>
                                <label for="pet" class="col-md-1 col-form-label text-right">名: </label>
                                <label for="pet" class="col-md-3 col-form-label text-left"><span class="small">{{ $user_query->first_name }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">郵便番号: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">{{ $user_query->zip_code }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right"><i class="fas fa-lg fa-building"></i>住所: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">
                                        {{ $user_query->prefecture }} {{ $user_query->city }} {{ $user_query->address }}

                                </span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right"><i class="fas fa-lg fa-phone"></i>電話番号: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">{{ $user_query->tel }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">メールアドレス: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">{{ $user_query->email }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right"><h5 class=""><b>同居動物</b></h5></label>
                        </div>
                        <?php
                            $animal_type[0]=0;
                            $animal_type[1]=0;
                            $animal_type[2]=0;
                            $pet_query = App\Pet::where('user_id',$user_query->id)->get();
                            if(!empty($pet_query)){
                                foreach($pet_query as $row){
                                    if($row->type==1)$animal_type[0]++;
                                    elseif($row->type==2)$animal_type[1]++;
                                    elseif($row->type==3)$animal_type[2]++;
                                }
                            }
                        ?>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">犬: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">{{ $animal_type[0] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">猫: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small">{{ $animal_type[1] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right">その他: </label>
                                <label for="pet" class="col-md-8 col-form-label text-left"><span class="small"></span>{{ $animal_type[2] }}</label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-4 col-form-label text-right"><h5 class=""><b>登録動物</b></h5></label>
                        </div>
                        <div class="form-group row">
                                <div class="card-body table-responsive p-0" style="height: 200px;">
                                    <table class="table table-head-fixed table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    患者名
                                                </th>
                                                <th>
                                                    動物種
                                                </th>
                                                <th>
                                                    品種
                                                </th>
                                                <th>
                                                    詳細
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if(!empty($pet_query)){
                                                foreach($pet_query as $row){

                                            ?>
                                                    <tr>
                                                        <td>{{ $row->name }}</td>
                                                        <td>
                                                            @if($row->type==1) 犬
                                                            @elseif($row->type==2) 猫
                                                            @elseif($row->type==3) その他
                                                            @endif
                                                        </td>
                                                        <td>{{ $row->genre }}</td>
                                                        <td><a href="{{URL::to('/app/admin/animal_reg_detail_search/'.$row->id)}}" class="btn bg-gradient-warning">詳細</a></td>
                                                    </tr>
                                            <?php
                                                }}
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header text-muted border-bottom-0">
                  <h4 class=""><b> 診察履歴 </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th>日付</th>
                                    <th>患者番号</th>
                                    <th>患者名</th>
                                    <th>動物病院名</th>
                                    <th>金額</th>
                                    <th>ステータス</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($user_query)){
                                        foreach($pet_query as $pets)
                                        {
                                            $diags_query=App\Diagnosis::where('pet_id',$pets->id)->get();
                                            if(count($diags_query)>0){
                                                foreach($diags_query as $diags)
                                                {
                                                    $accounting = App\Accounting::where('diagnosis_id',$diags->id)->first();
                                                    ?>
                                                    <tr>
                                                        <td>{{ $diags->reserve_datetime }}</td>
                                                        <td>
                                                                {{--  @if($diags->pet_id<10)
                                                                V{{ date('ym').'00'.$diags->pet_id }}
                                                                @elseif($diags->id<100)
                                                                V{{ date('ym').'0'.$diags->pet_id }}
                                                                @else
                                                                V{{ date('ym').''.$diags->pet_id }}
                                                                @endif  --}}

                                                                @if($user_query->id<10)
                                                                P{{ date('ym').'000'.$user_query->id.'-0'.$diags->pet_id }}
                                                                @elseif($user_query->id<100)
                                                                P{{ date('ym').'00'.$user_query->id.'-0'.$diags->pet_id }}
                                                                @elseif($user_query->id<1000)
                                                                P{{ date('ym').'0'.$user_query->id.'-0'.$diags->pet_id }}
                                                                @else
                                                                P{{ date('ym').''.$user_query->id.'-0'.$diags->pet_id }}
                                                                @endif

                                                                {{--  @if($diags->pet_id<10)
                                                                P{{ date('ym').'000'.$diags->pet_id }}
                                                                @elseif($diags->pet_id<100)
                                                                P{{ date('ym').'00'.$diags->pet_id }}
                                                                @elseif($diags->pet_id<1000)
                                                                P{{ date('ym').'0'.$diags->pet_id }}
                                                                @else
                                                                P{{ date('ym').''.$diags->pet_id }}
                                                                @endif  --}}
                                                        </td>
                                                        <td>{{ $pets->name }}</td>
                                                        <td>{{ App\Clinic::find($diags->clinic_id)->name }}</td>
                                                        <td>{{ $accounting['sum'] }}</td>
                                                        <td>
                                                            <?php
                                                                //if($diags->status == 0 and $accounting['next_remind_date'] == null) echo "診察待ち";
                                                                //elseif($diags->status == 1 and $accounting['next_remind_date'] == null and $accounting['delivery_cost'] == null) echo "会計待機";
                                                                //elseif($diags->status == 1 and $accounting['delivery_cost'] != null) echo "発送待ち";
                                                                //elseif($diags->status == 1 and $accounting['next_remind_date'] != null) echo "再真理マインド";
                                                                //elseif($diags->status == 4) echo "診察完了";
                                                                //elseif($diags->status == 3) echo "発送";
                                                                //elseif($diags->status == 2) echo "リマインド";
                                                                $curr_datetime = date("Y-m-d");
                                                                if($diags->status==0){
                                                                    //$exam="진찰대기";
                                                                    $exam="診察待ち";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                if($diags->status==1 or $diags->status==5){
                                                                    if((!empty($accounting) && $accounting->status != 2) || empty($accounting)){
                                                                    //$exam="회계대기";
                                                                    $exam="会計待機";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                    }
                                                                }
                                                                //if(($diags->status==0 OR $diags->status==1 OR $diags->status==3 ) and (!empty($accounting) and $accounting->next_remind_date!=null) ){

                                                                if($diags->status==4 or $diags->status==3){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                if($diags->status==1 && !empty($accounting) && $accounting->status==2 &&  $accounting->next_remind_date != null){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                //if(($diags->status==1 or $diags->status==2 or $diags->status==5) and (!empty($accounting) and $accounting->delivery_cost!=0) ){
                                                                if($diags->status==1 && !empty($accounting) && $accounting->delivery_cost != 0 && $accounting->status == 2){
                                                                //  $exam="발송대기";
                                                                    $exam="発送待ち";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                if($diags->status==10){
                                                                    //$exam="취소완료";
                                                                    $exam="キャンセル完了";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                if($diags->status==2){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                if($diags->status==6){
                                                                    //$exam="취소";
                                                                    $exam="キャンセル";
                                                                    $edatetime=$diags->reserve_datetime;
                                                                }
                                                                echo $exam;
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }

                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
 <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
