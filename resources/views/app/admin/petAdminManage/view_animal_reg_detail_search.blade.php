@extends('layouts.app.admin.header')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ペットの飼い主管理-登録動物</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <div class="col-md-6">
              <div class="card">
                  <div class="card-header text-muted border-bottom-0">
                  <h4 class=""><b> </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">患者様番号: </label>
                                <label for="pet" class="col-md-2 col-form-label text-left"><span class="small">
                                        {{--  @if($pet_query->id<10)
                                        V{{ date('ym').'00'.$pet_query->id }}
                                        @elseif($pet_query->id<100)
                                        V{{ date('ym').'0'.$pet_query->id }}
                                        @else
                                        V{{ date('ym').''.$pet_query->id }}
                                        @endif  --}}

                                        @if($pet_query->user_id<10)
                                        P{{ date('ym').'000'.$pet_query->user_id.'-0'.$pet_query->id }}
                                        @elseif($pet_query->user_id<100)
                                        P{{ date('ym').'00'.$pet_query->user_id.'-0'.$pet_query->id }}
                                        @elseif($pet_query->user_id<1000)
                                        P{{ date('ym').'0'.$pet_query->user_id.'-0'.$pet_query->id }}
                                        @else
                                        P{{ date('ym').''.$pet_query->user_id.'-0'.$pet_query->id }}
                                        @endif
                                </span></label>
                                <label for="pet" class="col-md-2 col-form-label text-right">動物写真: </label>
                                <div class="col-md-6">
                                    <img class="img-fluid mb-3" src="{{URL::to('/')}}/images/insurance/{{ $pet_query->profile_picture_path }}" alt="Photo">
                                </div>
                        </div>
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">患者様名: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $pet_query->name }}</span></label>
                        </div>
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">動物種: </label>
                            <label for="pet" class="col-md-2 col-form-label text-left"><span class="small">

                            @if($pet_query->type == 1)
                            イヌ
                            @elseif($pet_query->type == 2)
                            ネコ
                            @elseif($pet_query->type == 4)
                            ハムスター
                            @elseif($pet_query->type == 5)
                            フェレット
                            @elseif($pet_query->type == 6)
                            爬虫類
                            @elseif($pet_query->type == 7)
                            ウサギ
                            @elseif($pet_query->type == 8)
                            鳥
                            @else
                            その他
                            @endif
                            </span></label>
                            <label for="pet" class="col-md-2 col-form-label text-right">品種: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $pet_query->genre }}</span></label>
                        </div>
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">性別: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">
                                @if($pet_query->gender==1) 男の子
                                @else 女の子
                                @endif
                            </span></label>
                        </div>
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">生年月日: </label>
                            <label for="pet" class="col-md-2 col-form-label text-left"><span class="small">{{ $pet_query->birthday }}</span></label>
                            <label for="pet" class="col-md-2 col-form-label text-right">年齢: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ date("Y")-substr($pet_query->birthday,0,4) }}</span></label>
                        </div>
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">保険加入: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left">
                                    <span class="small">
                                    @if($pet_query->insurance_option == 1)〇あり
                                    @else 〇なし
                                    @endif
                                    </span>
                            </label>
                        </div>
                        <div class="form-group row">

                            @if($pet_query->insurance_option == 1)
                            <label for="pet" class="col-md-5 col-form-label text-center"><span class="small">会社名: {{$pet_query->insurance_name}}</span></label>
                            <label for="pet" class="col-md-5 col-form-label text-center"><span class="small">保険証番号: {{$pet_query->insurance_no}}</span></label>
                            <label for="pet" class="col-md-5 col-form-label text-center"><span class="small">保険証期限:
                                @if($pet_query->insurance_from_date != null && $pet_query->insurance_from_date != "0000-00-00")
                                {{$pet_query->insurance_from_date . " ~ " . $pet_query->insurance_to_date}}
                                @endif
                                </span></label>
                            @endif
                        </div>
                        @if($pet_query->insurance_option == 1)
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">保険証の写真: </label>

                        </div>
                        <div class="form-group row">
                                <div class="col-md-6">
                                        <img class="img-fluid" src="{{URL::to('/')}}/images/insurance/{{ $pet_query->insurance_front }}" alt="Photo">
                                    </div>
                                <div class="col-md-6">
                                        <img class="img-fluid" src="{{URL::to('/')}}/images/insurance/{{ $pet_query->insurance_back }}" alt="Photo">
                                </div>
                        </div>
                        {{--  <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">保険期間: </label>
                                <label for="pet" class="col-md-10 col-form-label text-left"><span class="small">
                                    {{ $pet_query->insurance_from_date }} ~ {{ $pet_query->insurance_to_date }}
                                </span></label>
                        </div>  --}}
                        @endif
                        <div class="form-group row">
                            <label for="pet" class="col-md-2 col-form-label text-right">飼い主様名: </label>
                            <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ App\User::find($pet_query->user_id)->last_name }}
                             {{ App\User::find($pet_query->user_id)->first_name }}
                            </span></label>
                        </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header text-muted border-bottom-0">
                  <h4 class=""><b> 診察履歴、予約状況 </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                             <div class="card-body table-responsive p-0" style="height: 600px;">
                                    <table id="example2" class="table table-head-fixed table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    日付
                                                </th>
                                                <th>
                                                    時間
                                                </th>
                                                <th>
                                                    目的
                                                </th>
                                                <th>
                                                    動物病院名
                                                </th>
                                                <th>
                                                    発送
                                                </th>
                                                <th>
                                                    金額
                                                </th>
                                                <th>
                                                    診察ステータス
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $diag = App\Diagnosis::where('pet_id',$pet_query->id)->get();
                                            if(count($diag)>0){
                                                foreach($diag as $row)
                                                {
                                                    ?>
                                                    <tr style="font-size:12px">
                                                        <td>
                                                            {{ substr($row->reserve_datetime,0,10) }}
                                                        </td>
                                                        <td>
                                                            {{ substr($row->reserve_datetime,11,8) }}
                                                        </td>
                                                        <td>
                                                            {{ $row->content }}
                                                        </td>
                                                        <td>
                                                            {{ App\Clinic::find($row->clinic_id)->name }}
                                                        </td>
                                                        <td align="center">
                                                            <?php
                                                            $accounting = App\Accounting::where('diagnosis_id',$row->id)->first();

                                                            //$diags = App\Diagnosis::find($accounting->diagnosis_id)->first();
                                                            if(!empty($accounting) and $accounting->delivery_cost != 0) echo "〇";
                                                            else echo "×";
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if(!empty($accounting))
                                                             {
                                                                $diags = App\Diagnosis::find($accounting->diagnosis_id)->first();
                                                                $system_fee = \App\SystemFee::first();
                                                                $a=$diags->price;
                                                                $b=$accounting->medical_expenses;
                                                                $e=$system_fee->owner_fee;
                                                                $f=$accounting->delivery_cost;
                                                                echo $accounting->sum;//$a+$b+$e+$f;
                                                             }
                                                            else echo "";
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                                //if($row->status == 0 and $accounting['next_remind_date'] == null) echo "診察待ち";
                                                                //elseif($row->status == 1 and $accounting['next_remind_date'] == null and $accounting['delivery_cost'] == null) echo "会計待機";
                                                                //elseif($row->status == 1 and $accounting['delivery_cost'] != null) echo "発送待ち";
                                                                //elseif($row->status == 1 and $accounting['next_remind_date'] != null) echo "再真理マインド";
                                                                //elseif($row->status == 4) echo "診察完了";
                                                                //elseif($row->status == 3) echo "発送";
                                                                //elseif($row->status == 2) echo "リマインド";
                                                                $exam="";
                                                                $curr_datetime = date("Y-m-d");
                                                                if($row->status==0){
                                                                    //$exam="진찰대기";
                                                                    $exam="診察待ち";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                if($row->status==1 or $row->status==5){
                                                                    if((!empty($accounting) && $accounting->status != 2) || empty($accounting)){
                                                                    //$exam="회계대기";
                                                                    $exam="会計待機";
                                                                    $edatetime=$row->reserve_datetime;
                                                                    }
                                                                }
                                                                //if(($row->status==0 OR $row->status==1 OR $row->status==3 ) and (!empty($accounting) and $accounting->next_remind_date!=null) ){

                                                                if($row->status==4 or $row->status==3){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                if($row->status==1 && !empty($accounting) && $accounting->status==2 &&  $accounting->next_remind_date != null){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                //if(($row->status==1 or $row->status==2 or $row->status==5) and (!empty($accounting) and $accounting->delivery_cost!=0) ){
                                                                if($row->status==1 && !empty($accounting) && $accounting->delivery_cost != 0 && $accounting->status == 2){
                                                                //  $exam="발송대기";
                                                                    $exam="発送待ち";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                if($row->status==10){
                                                                    //$exam="취소완료";
                                                                    $exam="キャンセル完了";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                if($row->status==2){
                                                                    //$exam="진찰완료";
                                                                    $exam="診察完了";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                if($row->status==6){
                                                                    //$exam="취소";
                                                                    $exam="キャンセル";
                                                                    $edatetime=$row->reserve_datetime;
                                                                }
                                                                echo $exam;
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                             </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
    <script>
            $(function() {
                $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                    event.preventDefault();
                    $(this).ekkoLightbox({
                        alwaysShowClose: true
                    });
                });

                $('.filter-container').filterizr({
                    gutterPixels: 3
                });
                $('.btn[data-filter]').on('click', function() {
                    $('.btn[data-filter]').removeClass('active');
                    $(this).addClass('active');
                });
            })
        </script>
 <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
