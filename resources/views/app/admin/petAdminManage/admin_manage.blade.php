
@extends('layouts.app.admin.header')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>飼い主様管理</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form name="exam_frm" action="{{URL::to('/app/admin/pet_manage_search')}}" method="GET">
        @csrf
        <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">飼い主様番号</label>

                                <div class="col-md-1">
                                    <input type="number" class="form-control" name="hos_num">
                                </div>
                                <label for="pet" class="col-md-2 col-form-label text-right">飼い主様名</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control"  name="hos_name">
                                </div>
                                <label for="pet" class="col-md-2 col-form-label text-right">電話番号</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control"  name="hos_tel">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-2 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-1">

                                       <div class="form-group">
                                        <select class="select2" name="prefecture" data-placeholder="" style="width: 100%;">
                                            <option value="0">---</option>
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            {{-- @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else --}}
                                                <option
                                                    value="{{ $pref }}"
                                                    {{-- {{ $pref == '東京都' ? 'selected' : '' }} --}}
                                                >{{ $pref }}</option>
                                            {{-- @endif --}}
                                        @endforeach
                                        </select>
                                    </div>

                                </div>
                                <label for="pet" class="col-md-2 col-form-label text-right">区、市</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control"  name="city">
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1"><button type="submit" class="btn btn-block bg-gradient-primary">検索</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
                </div> --}}
                <!-- /.card-header -->
                <div class="card-body">
                <div class="card-body table-responsive p-1" style="height: 300px;">
                    <table id="example2" class="table table-head-fixed" style="font-size:12px">
                        <thead>
                        <tr style="height:80px">
                            <th>飼い主様番号</th>
                            <th>飼い主様名</th>
                            <th>電話番号</th>
                            <th>都道府県</th>
                            <th>区、市</th>
                            <th>詳細</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6"></td>
                        </tr>
                        </tbody>
                    </table>
                 </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
    </form>
    <!-- /.content -->

<script>
  $(function () {
    //Initialize Select2 Elements

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true,
    });
  });
</script>
@endsection
