
@extends('layouts.app.admin.header')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>動物病院管理-詳細</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header text-muted border-bottom-0">
                 <h4 class=""><b> 基本情報 </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <?php $bankinfo = json_decode($clinic_query->bank_info,true); ?>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">動物病院番号: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">
                                    @if($clinic_query->id<10)
                                    V{{ date('ym').'00'. $clinic_query->id }}
                                    @elseif($clinic_query->id<100)
                                    V{{ date('ym').'0'.$clinic_query->id }}
                                    @else
                                    V{{ date('ym').''. $clinic_query->id }}
                                    @endif
                                </span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right"></label>
                                <label for="pet" class="col-md-4 col-form-label text-left"></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">動物病院名: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->name }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right"><h5 class=""><b>取引口座</b></h5></label>
                                <label for="pet" class="col-md-4 col-form-label text-left"></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">カナ: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->name_kana }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right">銀行名: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $bankinfo['bank_name'] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">院長名: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->manager_name }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right">口座種別: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $bankinfo['bank_type'] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">郵便番号: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->zip_code }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right">店番: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $bankinfo['branch_number'] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right"><i class="fas fa-lg fa-building"></i>住所: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">
                                    {{ $clinic_query->prefecture }} {{ $clinic_query->city }} {{ $clinic_query->address }}
                                </span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right">口座番号: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $bankinfo['bank_account_number'] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right"><i class="fas fa-lg fa-phone"></i>電話番号: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->tel }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right">口座名: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $bankinfo['bank_account_name'] }}</span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">メールアドレス: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->email }}</span></label>

                                <label for="pet" class="col-md-2 col-form-label text-right"><h5 class=""><b>予約料設定</b></h5></label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small"></span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">診察対象動物: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">
                                    <?php
                                    $clinic_menu = App\ClinicMenu::where('clinic_id', $clinic_query->id)->get();
                                    ?>
                                    @php($optionPet = json_decode($clinic_query->option_pet,true))
                                    <div class="row">
                                            @if($optionPet != null)
                                            @foreach($optionPet as $pet=>$val)
                                                @if($val=='true')
                                                    <div class="col-6 col-md-2">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                                            style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                            @endif
                                        </div>
                                </span></label>
                                <div class="col-md-6">
                                    <div class="card-body table-responsive p-0" style="height: 150px;">
                                    <table class="table table-head-fixed table-bordered table-hover">
                                        <thead>
                                            <tr style="background-color: azure;">
                                                <th>
                                                        メニュー
                                                </th>
                                                <th>
                                                        予約料
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $base_clinic_menu = App\ClinicMenu::where('clinic_id', 0)->get();
                                            if(count($base_clinic_menu)>0){
                                            foreach ($base_clinic_menu as $row){
                                                $priceList = json_decode($row->price_list,true);
                                                foreach($priceList as $item);
                                                ?>
                                                <tr>
                                                    <td>
                                                        {{ $row->name }}
                                                    </td>
                                                    <td>
                                                        	￥{{ $item['price'] }}
                                                    </td>
                                                </tr>
                                            <?php }}?>
                                            <?php
                                                if(count($clinic_menu)>0){
                                                    foreach($clinic_menu as $row){
                                                        $priceList = json_decode($row->price_list,true);
                                                        foreach($priceList as $item);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                {{ $row->name }}
                                                            </td>
                                                            <td>
                                                                ￥{{ $item['price'] }}
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>


                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">ホームページURL: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">病院ホームページURL</span></label>


                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">登録獣医師: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->manager_name }}
                                <?php $doctornames=App\ClinicDoctorName::where('clinic_id', $clinic_query->id)->get();?>
                                @if(!empty($doctornames))
                                @foreach ($doctornames as $doctorname)
                                    ,{{ $doctorname->clinic_name }}
                                @endforeach
                                @endif
                                </span></label>



                                </span></label>
                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">対応保険会社: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->insurance }}</span></label>


                        </div>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">病院説明: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">{{ $clinic_query->clinic_description }}</span></label>

                        </div>
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                                <div class="col-lg-6 col-12">
                                        <!-- small box -->
                                        <div class="small-box bg-warning">
                                          <div class="inner">
                                            <h4>
                                                    <?php
                                                    $diags = \App\Diagnosis::where('clinic_id',$clinic_query->id)->get();
                                                    $flag=0;
                                                    $petNumber[0]=0;
                                                    $count=0;
                                                    foreach($diags as $diag)
                                                    {
                                                        for($i=0;$i<count($petNumber);$i++){
                                                            if($diag->user_id == $petNumber[$i]){
                                                                $flag=1;
                                                                break;
                                                            }
                                                        }
                                                        if($flag==0)
                                                        {
                                                            $petNumber[$count] = $diag->user_id;
                                                            $count++;
                                                        }else{
                                                            $flag=0;
                                                        }
                                                    }
                                                    echo $count."件";
                                                    ?>
                                            </h4>

                                            <p>この病院登録飼い主様数</p>
                                          </div>
                                          <div class="icon">
                                            <i class="fa fa-user"></i>
                                          </div>
                                          <a href="#" class="small-box-footer"><p></p></a>
                                </div>
                                <div class="col-md-3"></div>
                              </div>

                        </div>
                    </div>

                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <div class="col-12 text-right">
                        @if($clinic_query->eyecatchimage != null)
                        <img src="{{ URL::to('/assets/img/clinics/'.$clinic_query->id.'/'.$clinic_query->eyecatchimage) }}" alt="" width="200" height="150" class="img-fluid">
                        @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header text-muted border-bottom-0">
                  <h4 class=""><b> 予約可能日 </b></h4>
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                        <?php
                        $clinicInfo = json_decode($clinic_query->clinic_info,true);
                        $timeslote = json_decode($clinic_query->menu_time_option,true);
                        ?>
                        <div class="form-group row">
                                <label for="pet" class="col-md-2 col-form-label text-right">予約単位: </label>
                                <label for="pet" class="col-md-4 col-form-label text-left"><span class="small">
                                    <?php
                                        if(isset($timeslote['timeSlotType']))
                                        {
                                            if($timeslote['timeSlotType']==0)
                                            {
                                                echo "10分";
                                            }elseif($timeslote['timeSlotType']==1){
                                                echo "15分";
                                            }elseif($timeslote['timeSlotType']==2){
                                                echo "20分";
                                            }elseif($timeslote['timeSlotType']==3){
                                                echo "30分";
                                            }elseif($timeslote['timeSlotType']==4){
                                                echo "60分";
                                            }
                                        }
                                    ?>
                                間隔</span></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 table-responsive text-center">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th>月</th>
                                        @if(isset($clinicInfo['businessHour'][1]) && !empty($clinicInfo['businessHour'][1]))
                                            @php($count = count($clinicInfo['businessHour'][1]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][1][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][1][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][1][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][1][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][1][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][1][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][1][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][1][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][1][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>火</th>
                                        @if(isset($clinicInfo['businessHour'][2]) && !empty($clinicInfo['businessHour'][2]))
                                            @php($count = count($clinicInfo['businessHour'][2]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][2][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][2][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][2][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][2][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][2][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][2][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][2][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][2][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][2][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>水</th>
                                        @if(isset($clinicInfo['businessHour'][3]) && !empty($clinicInfo['businessHour'][3]))
                                            @php($count = count($clinicInfo['businessHour'][3]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][3][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][3][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][3][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][3][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][3][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][3][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][3][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][3][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][3][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>木</th>
                                        @if(isset($clinicInfo['businessHour'][4]) && !empty($clinicInfo['businessHour'][4]))
                                            @php($count = count($clinicInfo['businessHour'][4]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][4][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][4][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][4][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][4][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][4][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][4][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][4][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][4][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][4][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>金</th>
                                        @if(isset($clinicInfo['businessHour'][5]) && !empty($clinicInfo['businessHour'][5]))
                                            @php($count = count($clinicInfo['businessHour'][5]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][5][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][5][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][5][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][5][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][5][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][5][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][5][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][5][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][5][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th style="color: blue">土</th>
                                        @if(isset($clinicInfo['businessHour'][6]) && !empty($clinicInfo['businessHour'][6]))
                                            @php($count = count($clinicInfo['businessHour'][6]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][6][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][6][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][6][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][6][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][6][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][6][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][6][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][6][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][6][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif

                                    </tr>
                                    <tr>
                                        <th style="color: red">日</th>
                                        @if(isset($clinicInfo['businessHour'][7]) && !empty($clinicInfo['businessHour'][7]))
                                            @php($count = count($clinicInfo['businessHour'][7]))
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @php($a_start = "--")
                                            @php($a_end = "--")
                                            @if($clinicInfo['businessHour'][7][0]['start'] <= "12:00")
                                                @php($m_start = $clinicInfo['businessHour'][7][0]['start'])
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][7][$i]['start'] <= "12:00")
                                                        @php($m_end = $clinicInfo['businessHour'][7][$i]['start'])
                                                    {{-- @else
                                                        @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                    @endif
                                                @endfor
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][7][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][7][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][7][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][7][$i]['start'])
                                                    @endif
                                                @endfor
                                            @else
                                                @php($m_start = "--")
                                                @php($m_end = "--")
                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][7][$i]['start'] > "12:00")
                                                        @php($a_start = $clinicInfo['businessHour'][7][$i]['start'])
                                                        @break
                                                    @endif
                                                @endfor

                                                @for($i = 0; $i < $count; $i++)
                                                    @if($clinicInfo['businessHour'][7][$i]['start'] > "12:00")
                                                        @php($a_end = $clinicInfo['businessHour'][7][$i]['start'])
                                                    @endif
                                                @endfor
                                            @endif
                                            <td>{{$m_start . "~" . $m_end}}</td>
                                            <td>{{$a_start . "~" . $a_end}}</td>
                                        @else
                                            <td>--</td>
                                            <td>--</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th style="color: red">祝</th>
                                        <td>{{isset($clinicInfo['businessHour']['holiday'][0]['start'])?$clinicInfo['businessHour']['holiday'][0]['start']." ~ ".$clinicInfo['businessHour']['holiday'][0]['end']:"--"}}</td>
                                        <td>{{isset($clinicInfo['businessHour']['holiday'][1]['start'])?$clinicInfo['businessHour']['holiday'][1]['start']." ~ ".$clinicInfo['businessHour']['holiday'][1]['end']:"--"}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            {{-- <div class="col-md-2"><a class="btn btn-danger" href="#">診察一覧</a></div> --}}
                        </div>

                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <div class="col-12 text-right">
                        @if($clinic_query->eyecatchimage != null)
                        <img src="{{ URL::to('/assets/img/clinics/'.$clinic_query->id.'/'.$clinic_query->eyecatchimage) }}" alt="" width="200" height="150" class="img-fluid">
                        @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
@endsection
