
@extends('layouts.app.admin.header')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <h1>&nbsp;価格</h1>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <form method="POST" action="{{ URL::to("/app/admin/updateFee") }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title"><b>動物病院</b></h3>

                            {{-- <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div> --}}
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-group row">
                                <label id="fee" for="fee" class="col-md-6 col-form-label text-md-right">システム利用料</label>

                                <div class="col-md-2">
                                    <input type="number" class="form-control" id="clinic_fee" name="clinic_fee" min="0" style="text-align:right;" step="1" value="{{$fee->clinic_fee}}">
                                </div>
                                <label id="fee" for="fee" class="col-md-1 col-form-label text-md-left">%</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title"><b>飼い主様</b></h3>

                            {{-- <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div> --}}
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-group row">
                                <label id="fee" for="fee" class="col-md-6 col-form-label text-md-right">システム利用料</label>

                                <div class="col-md-2">
                                    <input type="number" class="form-control" id="owner_fee" name="owner_fee" min="0" style="text-align:right;" step="10" value="{{$fee->owner_fee}}">
                                </div>
                                <label id="fee" for="fee" class="col-md-1 col-form-label text-md-left">円</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label id="fee" for="fee" class="col-md-5 col-form-label text-md-left"></label>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-block btn-outline-primary btn-flat">設定</button>
                        </div>
                        <label id="fee" for="fee" class="col-md-2 col-form-label text-md-left"></label>
                        <label id="fee" for="fee" class="col-md-3 col-form-label text-md-left"></label>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
