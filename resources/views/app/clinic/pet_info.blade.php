@extends('layouts.app.clinic')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
@endsection

@section('content')
<script>
 function dispChange(id)
{
    if(id == 1)
    {
        $("#seltb1").css("display", "block");
        $("#seltb2").css("display","none");
        $("#div2").css("display","block");
        $("#div1").css("display","none");
        $("#div3").css("display","block");
        $("#div4").css("display","none");
    }else{
        $("#seltb1").css("display","none");
        $("#seltb2").css("display","block");
    }

}
function search_back()
{
    window.history.back();
}
</script>
<div class="content" style="padding-left: 100px;padding-right: 100px;">
          <div class="row">
            <div class="col-md-4">
              <h3 class="font-weight-bold mb-4">患者情報</h3>
              <!-- Nav tabs -->
              <div class="card">
                <div class="card-body">
                    <!-- Tab panes -->
                    @if($pet->profile_picture_path != null)
                    <div>
                        <img src="{{URL::to('images/insurance/'.$pet->profile_picture_path)}}"/>
                    </div>
                    @endif
                    {{--  @if(!empty($diag))  --}}
                    <div>
                        <label  class="col-md-12 col-form-label text-md-left"><strong>カルテNo</strong> :
                            <?php
                            $ownerflag =0;
                            $petflag=0;
                            $ownercatano = \App\OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first();
                            if(!empty($ownercatano))$ownerflag = 1;
                            else $ownerflag = 0;

                            $petcatano = \App\PetCatano::where('pet_id', $pet->id)->where('clinic_id', $diag->clinic_id)->first();
                            if(!empty($petcatano))$petflag = 1;
                            else $petflag = 0;
                            ?>
                            @if($ownerflag==1)
                            {{$ownercatano->cata_no}}
                            @else
                            {{--  {!! $diag->user_id !!}  --}}

                            @endif

                             &nbsp;&nbsp;&nbsp;
                             <strong>動物No</strong> :
                             @if($petflag==1)
                            {{$petcatano->cata_no}}
                            @endif

                            </label>
                    </div>
                    {{--  @endif  --}}
                    <div>
                        <label  class="col-md-12 col-form-label text-md-left"><strong>患者様名</strong> : {!! $pet->name !!}</label>
                    </div>
                    <div>
                        <label  class="col-md-12 col-form-label text-md-left">
                            <strong>性別</strong> :
                            @if($pet['gender'] == 1)
                            &nbsp;男の子
                            @else
                            &nbsp;女の子
                            @endif
                        </label>
                    </div>
                    <!-- <div>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>カルテ番号​</strong> : {!! $pet->name !!}</label>
                    </div> -->
                    <div>
                    <label  class="col-md-12 col-form-label text-md-left">
                        <strong>動物種</strong> :
                        @if($pet->type == 1)
                        イヌ
                        @elseif($pet->type == 2)
                        ネコ
                        @elseif($pet->type == 4)
                        ハムスター
                        @elseif($pet->type == 5)
                        フェレット
                        @elseif($pet->type == 6)
                        爬虫類
                        @elseif($pet->type == 7)
                        ウサギ
                        @elseif($pet->type == 8)
                        鳥
                        @endif
                        &nbsp;&nbsp;
                        <strong>種類</strong> : {!! $pet->genre !!}
                    </label>
                    </div>
                    <div>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>生年月日</strong> : {!! $pet->birthday !!} &nbsp;&nbsp;&nbsp; <strong>年齢</strong> : {!! $age !!}</label>
                    </div>
                    <div>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>保険加入</strong>
                    @if($pet->insurance_option == 0)
                    &nbsp;&nbsp;&nbsp;ㅇ なし
                    @else
                    &nbsp;&nbsp;&nbsp;ㅇ あり<br/>
                    @endif
                    </label>
                    @if($pet->insurance_option == 1)
                    @if($pet->insurance_front != null)
                    <label  class="col-md-12 col-form-label text-md-left">
                        <img id="img-pet_reg_insurance_front" src="{{URL::to('images/insurance/'.$pet->insurance_front)}}">
                    </label>
                    @endif
                    @if($pet->insurance_back != null)
                    <label  class="col-md-12 col-form-label text-md-left">
                        <img id="img-pet_reg_insurance_back" src="{{URL::to('images/insurance/'.$pet->insurance_back)}}">
                    </label>
                    @endif
                    <label  class="col-md-12 col-form-label text-md-left"><strong>保険会社名 : </strong>
                    {{$pet->insurance_name}}
                    </label>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>保険証番号 : </strong>
                    {{$pet->insurance_no}}
                    </label>
                    @if($pet->insurance_from_date != null && $pet->insurance_from_date != '0000-00-00')
                    <label  class="col-md-12 col-form-label text-md-left"><strong>保険証の期限 : </strong>

                    {{$pet->insurance_from_date . " ~ " . $pet->insurance_to_date}}

                    </label>
                    @endif
                    @endif
                    {{-- <label  class="col-md-12 col-form-label text-md-left"><strong>会社名</strong> : 001 &nbsp;&nbsp;&nbsp;<strong>保険証番号</strong> : 123</label> --}}
                    </div>
                    <div>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>飼い主様名</strong> : {!! \App\User::find($pet->user_id)->last_name . " " . \App\User::find($pet->user_id)->first_name !!}&nbsp;&nbsp;<a href="{!! URL::to('/app/clinic/ownerinfo/'.$pet->user_id.'/'.$diagId) !!}"><button class="sub-menu-btn btn btn-warning">詳細</button></a></label>
                    </div>
                    <div>
                    <label  class="col-md-12 col-form-label text-md-left">
                        <button id="" type="button" class="btn btn-default" onclick = "search_back()">戻る</button>
                        {{-- <button class="sub-menu-btn btn btn-blue">編集</button>
                        <button class="sub-menu-btn btn btn-primary">保存</button> --}}
                    </label>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
                <!-- <div class="col-md-12 text-md-left">
                    <button id="div1" class="sub-menu-btn btn btn-defalult" onclick = "dispChange(1)">診療履歴​</button>
                    <button id="div2" style="display:block" class="sub-menu-btn btn btn-defalult" onclick = "dispChange(1)">診療履歴​</button>
                </div> -->
                <div class="col-md-12 text-md-left">
                <p><br><br>
                </div>
            <div class="col-md-10 ml-auto col-xl-12 mr-auto">
              <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist" data-background-color="orange">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">診療履歴​</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">予約状況​</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <!-- Tab panes -->
                  <div class="tab-content text-center">
                    <div class="tab-pane active" id="home1" role="tabpanel">
                      <p>
                        <div class="table-responsive">
                            <table id = "seltb1" width="100%" class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                <thead>
                                <tr class="bg-warning">
                                    <th>日時</th>
                                    <th>担当医</th>
                                    <th>処方</th>
                                    <th>発送</th>
                                    <th>請求金額（税込）</th>
                                    {{--  <th>発送</th>
                                    <th>金額</th>  --}}
                                    <th>アクション</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($diag))
                                    @php($owner_diags = \App\Diagnosis::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->where('pet_id', $pet->id)->get())
                                    <?php
                                    foreach ($owner_diags as $owner_diag) {
                                        $accountings = \App\Accounting::where('diagnosis_id',$owner_diag->id)->where('clinic_id',$owner_diag->clinic_id)->get();
                                        if(!empty($accountings)){

                                            foreach ($accountings as $accounting){
                                                //echo $accounting->diagnosis_id.',';
                                    ?>
                                            <tr>
                                                <td>
                                                    @if($owner_diag->status==5 || $owner_diag->status==10)
                                                    キャンセル
                                                    @else
                                                    {!! substr($owner_diag->end_datetime,0,16) !!}
                                                    @endif

                                                </td>
                                                <td>
                                                    {!! $owner_diag->clinic_name !!}
                                                </td>
                                                <td>
                                                    @if(!empty(\App\Accounting::where('diagnosis_id',$owner_diag->id)->first()) && \App\Accounting::where('diagnosis_id',$owner_diag->id)->first()->comment != "")
                                                    あり
                                                    @else
                                                    なし
                                                    @endif
                                                </td>
                                                <td>
                                                        @if(!empty(\App\Accounting::where('diagnosis_id',$owner_diag->id)->first()) && \App\Accounting::where('diagnosis_id',$owner_diag->id)->first()->delivery_cost != 0)
                                                        〇
                                                        @else
                                                        ✕
                                                        @endif
                                                </td>

                                                <td>
                                                    @if(!empty(\App\Accounting::where('diagnosis_id',$owner_diag->id)->first()))
                                                    ￥{{ \App\Accounting::where('diagnosis_id',$owner_diag->id)->first()->sum }}
                                                    @endif
                                                </td>

                                                <td><a class="sub-menu-btn btn btn-warning" href="{!! URL::to('/app/clinic/reservedetail/'.$pet->id."/".$owner_diag->id) !!}">詳細</a> </td>
                                            </tr>
                                    <?php
                                            }
                                        }
                                    }
                                    ?>
                                    @endif
                                </tbody>
                            </table>
                            </div>
                      </p>
                    </div>
                    <div class="tab-pane" id="profile1" role="tabpanel">
                      <p>
                      <div class="table-responsive">
                      <table id = "seltb2" width="100%" class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                        <thead>
                        <tr class="bg-warning">
                            <th>予約日</th>
                            <th>時間</th>
                            {{--  <th>目的</th>  --}}
                            <th>担当医</th>
                            <th>アクション</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($diag))
                            @foreach ($owner_diags as $owner_diag)
                            @if($owner_diag->status==0)
                            <tr>
                                <?php
                                $datetime = explode(" ", $owner_diag->reserve_datetime);
                                $date1 = $datetime[0];
                                $time1 = $datetime[1];
                                ?>
                                <td>{!! $date1 !!}</td>
                                <td>{!! substr($time1,0,strlen($time1)-3) !!}</td>
                                {{--  <td><a href="{{URL::to('/app/clinic/purpose/'.$owner_diag['id'])}}">詳細</a></td>  --}}
                                <td>{!! $owner_diag['clinic_name'] !!}</td>
                                <td><a class="sub-menu-btn btn btn-warning" href="{{URL::to('/app/clinic/purpose/'.$owner_diag['id'])}}">詳細</a> </td>
                            </tr>
                            @endif
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                      </div>
                       </p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Tabs on plain Card -->
            </div>
            </div>
          </div>
</div>
@endsection
