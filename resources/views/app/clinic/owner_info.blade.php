@extends('layouts.app.clinic')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
@endsection

@section('content')
<script>
function search_back()
{
    history.go(-1);
}
</script>

        <div class="content" style="padding-left: 100px;padding-right: 100px;">
          <div class="row">
            <div class="col-md-6">
              <h3 class="category">飼い主様情報</h3>
              <!-- Nav tabs -->
              <div class="card">
                <div class="card-body">
                  <!-- Tab panes -->
                  <?php
                    $ownerflag =0;
                    $ownercatano = \App\OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first();
                    if(!empty($ownercatano))$ownerflag = 1;
                    else $ownerflag = 0;
                  ?>
                  <div>
                @if($ownerflag == 0)
                  {{--  <label  class="col-md-12 col-form-label text-md-left"><strong>カルテ番号</strong> : {{ $diag->user_id }}</label>  --}}
                  <label  class="col-md-12 col-form-label text-md-left"><strong>カルテ番号</strong> : </label>
                @else
                  <label  class="col-md-12 col-form-label text-md-left"><strong>カルテ番号</strong> : {{ \App\OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first()->cata_no }}</label>
                @endif
                </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>飼い主様名</strong> : {{$user->last_name . " " . $user->first_name}} </label>
                </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>電話番号</strong> : {{$user->tel}}</label>
                </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>郵便番号</strong> : {{ $user->zip_code }}</label>
                </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>住所</strong> : {{$user->prefecture . " " . $user->city . " " . $user->address}}</label>
                </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>同居動物</strong></label>
                </div>
                <div style="margin-left:10px">
                <label  class="col-md-12 col-form-label text-md-left">
                    <?php

                    $pet_array['dog']=0;
                    $pet_array['cat']=0;
                    $pet_array['hamster']=0;
                    $pet_array['ferret']=0;
                    $pet_array['rabbit']=0;
                    $pet_array['bird']=0;
                    $pet_array['reptile']=0;
                    $pet_array['others']=0;

                    if(count($pet) > 0){
                        foreach ($pet as $row){
                            if($row->type==1)$pet_array['dog']+=1;
                            if($row->type==2)$pet_array['cat']+=1;
                            if($row->type==4)$pet_array['hamster']+=1;
                            if($row->type==5)$pet_array['ferret']+=1;
                            if($row->type==6)$pet_array['rabbit']+=1;
                            if($row->type==7)$pet_array['bird']+=1;
                            if($row->type==8)$pet_array['reptile']+=1;
                            if($row->type==3)$pet_array['others']+=1;
                        }
                    ?>
                        @if($pet_array['dog']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/dog_icon.png" style="width: 1.2rem;" alt="イヌ">
                        イヌ:  {{ $pet_array['dog'] }}
                        @endif
                        @if($pet_array['cat']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/cat_icon.png" style="width: 1.2rem;" alt="イヌ">
                        ネコ:  {{ $pet_array['cat'] }}
                        @endif
                        @if($pet_array['hamster']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/hamster_icon.png" style="width: 1.2rem;" alt="イヌ">
                        ハムスター:  {{ $pet_array['hamster'] }}
                        @endif
                        @if($pet_array['ferret']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/ferret_icon.png" style="width: 1.2rem;" alt="イヌ">
                        フェレット:  {{ $pet_array['ferret'] }}
                        @endif
                        @if($pet_array['rabbit']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/rabbit_icon.png" style="width: 1.2rem;" alt="イヌ">
                        ウサギ:  {{ $pet_array['rabbit'] }}
                        @endif
                        @if($pet_array['bird']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/bird_icon.png" style="width: 1.2rem;" alt="イヌ">
                        鳥:  {{ $pet_array['bird'] }}
                        @endif
                        @if($pet_array['reptile']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/reptile_icon.png" style="width: 1.2rem;" alt="イヌ">
                        爬虫類:  {{ $pet_array['reptile'] }}
                        @endif
                        @if($pet_array['others']!=0)
                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/others_icon.png" style="width: 1.2rem;" alt="イヌ">
                        その他:  {{ $pet_array['others'] }}
                        @endif
                    <?php
                    }
                    ?>
                </label>
                </div>
                <div>
                    <label  class="col-md-12 col-form-label text-md-left"><strong>動物一覧</strong></label>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                            <thead>
                            <tr>
                                <th>動物番号</th>
                                <th>患者様名</th>
                                <th>動物種</th>
                                <th>種類 </th>
                                <th>詳細</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(!empty($pet))
                                <?php $animal_type='';?>
                                    @foreach ($pet as $row)
                                        <tr>
                                            <td>
                                                <?php
                                                $petcatano = \App\PetCatano::where('pet_id', $row->id)->where('clinic_id', $diag->clinic_id)->first();
                                                if(!empty($petcatano)){
                                                    echo $petcatano->cata_no;
                                                }
                                                ?>
                                            </td>
                                            <td>{{ $row->name }}</td>
                                            <?php
                                                if($row->type==1)$animal_type='イヌ';
                                                elseif($row->type==2)$animal_type='ネコ';
                                                elseif($row->type==3)$animal_type='その他';
                                                elseif($row->type==4)$animal_type='ハムスター';
                                                elseif($row->type==5)$animal_type='フェレット';
                                                elseif($row->type==6)$animal_type='ウサギ';
                                                elseif($row->type==7)$animal_type='鳥';
                                                elseif($row->type==8)$animal_type='爬虫類';
                                            ?>

                                            <td>{{ $animal_type }}</td>
                                            <td>{{ $row->genre }}</td>
                                            <td>
                                                <a href="{{URL::to('/app/clinic/petinfo/'.$row->id.'/'.$diag['id'])}}" class="sub-menu-btn btn btn-warning">詳細</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
                <br/>
                {{--  <div>
                <label  class="col-md-12 col-form-label text-md-left"><strong>動物番号</strong> : {{$diag->id}}</label>
                </div>  --}}

                {{--  <div>
                <label  class="col-md-12 col-form-label text-md-left"><a href="{{URL::to('/app/clinic/petinfo/'.$diag['pet_id'].'/'.$diag['id'])}}" class="sub-menu-btn btn btn-warning">詳細</a></label>
                </div>  --}}
                <div class="form-group row">
                <div  class="col-md-2 ">
                    <button id="" type="button" class="btn btn-default" onclick = "search_back()">戻る</button>

                </div>
                <div class="col-md-2">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                編集
                        </button>
                </div>
                </div>
            </div>
              </div>
            </div>
            <div class="col-md-6">
              <h3 class="font-weight-bold mb-4">会計履歴</h3>
              <!-- Nav tabs -->
              <div class="card">
                <div class="card-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                <thead>
                <tr class="bg-warning">
                    <th>日時</th>
                    <th>カルテ番号</th>
                    <th>患者名</th>
                    <th>請求金額（税込）</th>
                </tr>
                </thead>
                @php($owner_diags = \App\Diagnosis::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->get())
                <tbody>
                    <?php
                    foreach ($owner_diags as $owner_diag) {
                        $accountings = \App\Accounting::where('diagnosis_id',$owner_diag->id)->where('clinic_id',$owner_diag->clinic_id)->get();
                        if(!empty($accountings)){

                            foreach ($accountings as $accounting){
                                //echo $accounting->diagnosis_id.',';
                    ?>
                            <tr>
                                <td>
                                    @if($owner_diag->status==5)
                                    キャンセル
                                    @else
                                    {!! substr($accounting->updated_at,0,16) !!}
                                    @endif</td>
                                <?php
                                    $ownercatano = \App\OwnerCatano::where('user_id', $owner_diag->user_id)->where('clinic_id', $accounting->clinic_id)->first();
                                ?>
                                @if(!empty($ownercatano))
                                <td>{{ \App\OwnerCatano::where('user_id', $owner_diag->user_id)->where('clinic_id', $accounting->clinic_id)->first()->cata_no }}</td>
                                @else
                                {{--  <td>{{ $owner_diag->user_id }}</td>  --}}
                                <td></td>
                                @endif
                                <td>{{ \App\Pet::find($owner_diag->pet_id)->name }}</td>
                                <td>¥{{ $accounting->sum }}</td>
                            </tr>
                    <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header justify-content-center">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                          </button>
                          <h4 class="title title-up">飼い主様情報 編集</h4>
                        </div>
                        <form name="frm" id="frm" method="POST" action="{{URL::to('/app/clinic/ownerPetNo_update')}}">
                            @csrf
                            <div class="modal-body">
                            <p>
                                <div class="form-group row">
                                        <label  class="col-md-4 col-form-label text-md-right"><strong>カルテ番号</strong> : </label>
                                        <div class="col-md-5">
                                            @if($ownerflag == 1)
                                            <input type="text" class="form-control" name="owner_catano" value={{ \App\OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first()->cata_no }}>
                                            @else
                                            {{--  <input type="text" class="form-control" name="owner_catano" value={{ $diag->user_id }}>  --}}
                                            <input type="text" class="form-control" name="owner_catano">

                                            @endif
                                            <input type="hidden" class="form-control" name="diagid" value="{{$diag->id}}">
                                        </div>
                                </div>
                                <div>
                                        <label  class="col-md-12 col-form-label text-md-left"><strong>動物一覧</strong></label>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                                <thead>
                                                <tr>
                                                    <th>動物番号</th>
                                                    <th>患者様名</th>
                                                    <th>動物種</th>
                                                    <th>種類 </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($pet))
                                                    <?php $animal_type='';?>
                                                        @foreach ($pet as $row)
                                                            <tr>
                                                                <td>
                                                                    <?php
                                                                    $petcatano = \App\PetCatano::where('pet_id', $row->id)->where('clinic_id', $diag->clinic_id)->first();
                                                                    if(!empty($petcatano)){

                                                                    ?>
                                                                    <input type="text" name="pet_no[]" class="form-control" value="{{   $petcatano->cata_no }}">
                                                                    <?php
                                                                    }else{
                                                                    ?>
                                                                    <input type="text" name="pet_no[]" class="form-control">
                                                                    <?php
                                                                    }
                                                                     ?>

                                                                    <input type="hidden" name="petid[]" value="{{ $row->id }}">
                                                                </td>
                                                                <td>{{ $row->name }}</td>
                                                                <?php
                                                                    if($row->type==1)$animal_type='イヌ';
                                                                    elseif($row->type==2)$animal_type='ネコ';
                                                                    elseif($row->type==3)$animal_type='その他';
                                                                    elseif($row->type==4)$animal_type='ハムスター';
                                                                    elseif($row->type==5)$animal_type='フェレット';
                                                                    elseif($row->type==6)$animal_type='ウサギ';
                                                                    elseif($row->type==7)$animal_type='鳥';
                                                                    elseif($row->type==8)$animal_type='爬虫類';
                                                                ?>

                                                                <td>{{ $animal_type }}</td>
                                                                <td>{{ $row->genre }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                        </table>
                                    </div>

                            </p>
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">保存</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                            </div>
                         </form>
                      </div>
                    </div>
                  </div>
          </div>
        </div>
@endsection
