@extends('layouts.app.clinic')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <script>
    function search_back()
    {
        history.go(-1);
    }
    </script>
    <div class="container">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{URL::to('/app/clinic')}}">マイページ</a></li>
                <li class="breadcrumb-item active" aria-current="page">目的</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-12">
                <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">問診内容</h5>

                <h5 class="description" style="">
                    {!! nl2br($diag->content) !!}
                </h5>
            </div>
            <div class="col-md-12">
                <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">写真・動画データ​</h5>
                <div class="text-left">
                    <?php
                    $file_count = 0;
                    foreach($diag_files as $file) {
                        if($file->type == 2) {
                            $file_count++;
                        }
                    }
                    ?>
                    @foreach($diag_files as $file)
                        @if($file->type == 1)
                            <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                @if($file_count > 0)
                                <img src="{{URL::to('/images/insurance/'.$file->filename)}}" width="300px" style="margin-top: -160px">
                                @else
                                <img src="{{URL::to('/images/insurance/'.$file->filename)}}" width="300px">
                                @endif
                            </a>
                        @elseif($file->type == 2)
                            <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                <video width="300px" style="max-height: 200px; max-width: 300px" controls autoplay>
                                    <source src="{{URL::to('/images/insurance/'.$file->filename)}}" type="video/mp4">
                                </video>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <button id="" type="button" class="btn btn-default" onclick = "search_back()">戻る</button>
            </div>
        </div>
    </div>
@endsection
