@extends('layouts.app.clinic')

@section('content')
<script>
function search_back()
{
    history.go(-1);
}

</script>
<div class="container">
    <div class="content">
    <div class="col-md-12 border-bottom border-danger mb-4">
        <h3 class="title">検索結果</h3>
    </div>
        <div class="row">
        <div class="col-md-12">
            <!-- Nav tabs -->
            <div class="card">
            <div class="card-body">
            <div class="row col-md -12">
            <strong>該当件数　{{count($query)}} 件</strong>
            </div>
            <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                <thead>
                <tr class="bg-warning">
                    <th>カルテ番号</th>
                    <th>飼い主様名​</th>
                    <th>患者様名</th>
                    <th>動物種</th>
                    <th>種類</th>
                    <th>性別</th>
                    <th>最終診療日</th>
                    <th>次回予定日</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        if(!empty($query)){
                            foreach($query as $row){
                                if ($row->status != 0) {
                    ?>
                    <tr>
                    <td>
                        <?php
                            $ownercatano = \App\OwnerCatano::where('user_id', $row->user_id)->where('clinic_id', $row->clinic_id)->first();
                            if(!empty($ownercatano))
                            {
                                echo $ownercatano->cata_no;
                            }

                            else
                            {
                                echo '';//$row->user_id;
                            }

                        ?>
                    </td>
                    <td><a href="{{URL::to('/app/clinic/ownerinfo/'.$row->user_id.'/'.$row->cata_no)}}"><?php echo  $row->last_name.' '.$row->first_name?></a></td>
                    <td><a href="{{URL::to('/app/clinic/petinfo/'.$row->pet_id.'/'.$row->cata_no)}}"><?php echo $row->name?></a></td>
                    <td>
                        @if($row->type == 1)
                        イヌ
                        @elseif($row->type == 2)
                        ネコ
                        @elseif($row->type == 4)
                        ハムスター
                        @elseif($row->type == 5)
                        フェレット
                        @elseif($row->type == 6)
                        爬虫類
                        @elseif($row->type == 7)
                        ウサギ
                        @elseif($row->type == 8)
                        鳥
                        @endif
                    </td>
                    <td><?php echo $row->genre?></td>
                    <td><?php
                    if($row->gender==1)echo "男の子";
                    else echo "女の子";
                    ?></td>
                    <td>
                        @if($row->status == 5 || $row->status == 10)
                        キャンセル
                        @else
                        {{$row->end_datetime}}
                        @endif
                    </td>
                    <td>
                        @php($accounting = \App\Accounting::where('diagnosis_id', $row->id)->first())
                        @if($accounting['next_remind_date'] != null)
                        {{$accounting['next_remind_date']}}
                        @endif
                    </td>
                    </tr>
                    <?php }}}else{
                        ?>
                        <tr><td colspa = '8'>no search</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        <div class ="row">
            </div>
            <div class="col-md-12" align="center">
                <a id="" type="button"  class="btn btn-primary btn-lg" href="{{URL::to('app/clinic/search')}}"> 検索画面へ戻る</a>
            </div>
        </div>
        </div>
        </div>
            </div>
            </div>
        </div>

    </div>
</div>
@endsection
