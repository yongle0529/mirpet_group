@extends('layouts.app.clinic')

@section('content')
<script>
function mount_view(id)
{
    if(id ==1){
        $("#seldiv_mount").css("display","block");
        // $("#div_sum2").css("display","block");
        // $("#div_sum1").css("display","none");
        var price = 0;
        if(document.getElementById('medical-expenses').value != "") {
            price = parseInt(document.getElementById('medical-expenses').value);
        }
        var sum = price + parseInt(document.getElementById('reserve_price').value) + parseInt(document.getElementById('delivery_cost').value) + parseInt(document.getElementById('fee').value);
        document.getElementById('sum').value = sum;
        if(sum < 50) {
            document.getElementById('alert_sum').textContent = "※ 合計金額が￥５０以上になるようにご入力ください";
        } else {
            document.getElementById('alert_sum').textContent = "";
        }
    }
    else {
        // $("#div_sum2").css("display","none");
        // $("#div_sum1").css("display","block");
        $("#seldiv_mount").css("display","none");
        var price = 0;
        if(document.getElementById('medical-expenses').value != "") {
            price = parseInt(document.getElementById('medical-expenses').value);
        }
        var sum = price + parseInt(document.getElementById('reserve_price').value) + parseInt(document.getElementById('fee').value);
        document.getElementById('sum').value = sum;
        if(sum < 50) {
            document.getElementById('alert_sum').textContent = "※ 合計金額が￥５０以上になるようにご入力ください";
        } else {
            document.getElementById('alert_sum').textContent = "";
        }
    }
}
function changeSum() {
    const element = document.getElementById('seldiv_mount');
    if (element.style.display == 'none') {
        var price = 0;
        if(document.getElementById('medical-expenses').value != "") {
            price = parseInt(document.getElementById('medical-expenses').value);
        }
        var sum = price + parseInt(document.getElementById('reserve_price').value) + parseInt(document.getElementById('fee').value);
        document.getElementById('sum').value = sum;
        if(sum < 50) {
            document.getElementById('alert_sum').textContent = "※ 合計金額が￥５０以上になるようにご入力ください";
        } else {
            document.getElementById('alert_sum').textContent = "";
        }
    } else {
        var price = 0;
        if(document.getElementById('medical-expenses').value != "") {
            price = parseInt(document.getElementById('medical-expenses').value);
        }
        var sum = price + parseInt(document.getElementById('reserve_price').value) + parseInt(document.getElementById('delivery_cost').value) + parseInt(document.getElementById('fee').value);
        document.getElementById('sum').value = sum;
        if(sum < 50) {
            document.getElementById('alert_sum').textContent = "※ 合計金額が￥５０以上になるようにご入力ください";
        } else {
            document.getElementById('alert_sum').textContent = "";
        }
    }

}
function reserve_back()
{
    history.go(-1);
}
</script>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
        </h3>
        <form id="reserveForm" name="frm" method="POST" enctype="multipart/form-data" action="{{ URL::to('/app/clinic/accounting_update/'.$accounting->id) }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                        <div class="card-heading" style="margin-top:5px">
                        <h5 align="center"><strong>会計情報変更</strong></h5>
                        </div>

                                <label for="last_name" class="col-md-4 text-md-left">ファイル選択(pdf)</label>
                                <input type="file" id ="file" name="file_upload" accept=".pdf"/>
                                <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right"><font color =red >請求金額変更</font></label>
                                <label for="last_name" class="col-md-6 col-form-label text-md-right"></label>

                                @if($query->status!=5)

                                <label for="last_name" class="col-md-4 col-form-label text-md-right">予約料（税込）</label>
                                <label for="last_name" class="col-md-1 col-form-label text-md-left" >¥</label>
                                <div class="col-md-5">
                                    <input id="reserve_price" type="text" class="form-control" onchange="changeSum()" readonly name="reserve_price" value="{{ $query->price }}">
                                </div>

                                <div class="col-md-12"><br></div>
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">診療費（税込）</label>
                                <label for="last_name" class="col-md-1 col-form-label text-md-left">¥</label>
                                <div class="col-md-2">
                                    <input required id="medical-expenses" type="number" onchange="changeSum()" class="form-control" name="medical-expenses" value="{{$accounting->medical_expenses}}" min="0">
                                </div>

                                <label for="last_name" class="col-md-1 col-form-label text-md-left"><font color =blue >薬発送:</font></label>
                                <div class="col-md-1 text-md-left">
                                <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                <input class="form-check-input" type="radio" onclick="mount_view(1)" name="exampleRadios"  id="exampleRadios1" <?php if($accounting->delivery_cost != 0) echo 'checked'?> value="1">
                                <span class="form-check-sign"></span>
                                有り
                                </label>
                                </div>
                                </div>
                                <div class="col-md-1 text-md-left">
                                <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                <input class="form-check-input" type="radio" onclick="mount_view(0)" name="exampleRadios" <?php if($accounting->delivery_cost == 0) echo 'checked'?> id="exampleRadios1" value="2">
                                <span class="form-check-sign"></span>
                                無し
                                </label>
                                </div>
                                </div>
                                <div class="col-md-12" >
                                <div class="form-group row">


                                    <div class="col-md-12"><br></div>

                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">システム手数料</label>
                                    <label for="last_name" class="col-md-1 col-form-label text-md-left" >¥</label>
                                    <div class="col-md-5">
                                        <input id="fee" type="text" class="form-control" onchange="changeSum()" readonly name="fee" value="{{ $accounting->fee }}">
                                    </div>


                                    <div class="col-md-12"><br></div>
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">配達費</label>
                                    <label for="last_name" class="col-md-1 col-form-label text-md-left">¥</label>
                                    <div class="col-md-5" id ="seldiv_mount" style="<?php if($accounting->delivery_cost == 0) echo 'display:none' ?>">
                                        <input id="delivery_cost" type="text" onchange="changeSum()" class="form-control" name="delivery_cost"  value="<?php if($accounting->delivery_cost != 0) echo $accounting->delivery_cost; else echo '100' ?>">
                                    </div>


                                    <div class="col-md-12"><br></div>
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">合計</label>
                                    <label for="last_name" class="col-md-1 col-form-label text-md-left">¥</label>
                                    <div class="col-md-5" id ="div_sum">
                                        <input id="sum" type="text" class="form-control" name="sum" value="{{$accounting->sum}}" readonly>
                                        <span id="alert_sum" style="color: red;"></span>
                                    </div>
                                    {{-- <div class="col-md-5" id ="div_sum2" style="display:none">
                                        <input id="sum" type="text" class="form-control" name="sum" value="{{$accounting->sum+$query->price}}" readonly>
                                    </div> --}}

                                </div>

                                </div>
                                @else
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">予約料（税込）</label>
                                <label for="last_name" class="col-md-1 col-form-label text-md-left">¥</label>

                                <div class="col-md-2">
                                    <input required id="medical-expenses" type="text" onchange="changeSum()" readonly class="form-control" name="reserve_price" value="{{$query->price}}">
                                </div>

                                <div class="col-md-12" >
                                <div class="form-group row">


                                    <div class="col-md-12"><br></div>

                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">システム手数料</label>
                                    <label for="last_name" class="col-md-1 col-form-label text-md-left" >¥</label>
                                    <div class="col-md-5">
                                        <input id="fee" type="text" class="form-control" onchange="changeSum()" readonly name="fee" value="{{ $systemfee->owner_fee }}">
                                    </div>




                                    <div class="col-md-12"><br></div>
                                    <label for="last_name" class="col-md-4 col-form-label text-md-right">合計</label>
                                    <label for="last_name" class="col-md-1 col-form-label text-md-left">¥</label>
                                    <div class="col-md-5" id ="div_sum">
                                        <input id="sum" type="text" class="form-control" name="sum" value="{{$accounting->sum}}" readonly>
                                        <span id="alert_sum" style="color: red;"></span>
                                    </div>
                                    {{-- <div class="col-md-5" id ="div_sum2" style="display:none">
                                        <input id="sum" type="text" class="form-control" name="sum" value="{{$accounting->sum+$query->price}}" readonly>
                                    </div> --}}

                                </div>

                                </div>
                                @endif
                                <div class="col-md-12"><br></div>
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">次回再診予約日</label>
                                <div class="col-md-5">
                                <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="pet_reg_birth_year" class="form-control" name="pet_year" placeholder="年">
                                                        @if($accounting->next_remind_date == null)
                                                            <option value="0">----</option>
                                                            <?php $years = array_reverse(range(today()->year - 30, today()->year+3)); ?>
                                                            @foreach($years as $year)
                                                                <option
                                                                    value="{{ $year }}"
                                                                    {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                                                >{{ $year }}</option>
                                                            @endforeach
                                                        @else
                                                            <?php
                                                            $sql_year = explode("-", $accounting->next_remind_date);
                                                            $real_year = $sql_year[0];
                                                            $years = array_reverse(range(today()->year - 30, today()->year+3));
                                                            ?>
                                                            <option value="0">--</option>
                                                            @foreach($years as $year)
                                                                <option value="{{ $year }}" <?php if ($year == $real_year) echo "selected"; ?>>{{ $year }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="pet_reg_birth_month" class="form-control" name="pet_month" placeholder="月">
                                                        @if($accounting->next_remind_date == null)
                                                            <option value="0">--</option>
                                                            @foreach(range(1, 12) as $month)
                                                                <option
                                                                    value="{{ $month }}"
                                                                    {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                                                >{{ $month }}</option>
                                                            @endforeach
                                                        @else
                                                            <?php
                                                            $sql_year = explode("-", $accounting->next_remind_date);
                                                            $real_year = $sql_year[1];
                                                            $months = range(1, 12);
                                                            ?>
                                                            <option value="0">--</option>
                                                            @foreach($months as $month)
                                                                <option value="{{ $month }}" <?php if ($month == $real_year) echo "selected"; ?>>{{ $month }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select  id="pet_reg_birth_date" class="form-control" name="pet_day" placeholder="日">
                                                        @if($accounting->next_remind_date == null)
                                                            <option value="0">--</option>
                                                            @foreach(range(1, 31) as $day)
                                                                <option
                                                                    value="{{ $day }}"
                                                                    {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                                                >{{ $day }}</option>
                                                            @endforeach
                                                        @else
                                                            <?php
                                                            $sql_year = explode("-", $accounting->next_remind_date);
                                                            $real_year = $sql_year[2];
                                                            $days = range(1, 31);
                                                            ?>
                                                            <option value="0">--</option>
                                                            @foreach($days as $day)
                                                                <option value="{{ $day }}" <?php if ($day == $real_year) echo "selected"; ?>>{{ $day }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                <div class="col-md-12"><br></div>
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">コメント</label>
                                <div class="col-md-5">
                                <div class="datepicker-container">
                                    <div class="textarea-container">
                                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。">{{$accounting->comment}}</textarea>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-12"><br></div>
                                <label for="last_name" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-5">
                                <div class="datepicker-container">
                                    <div class="textarea-container">
                                    <button class="btn btn-primary" type="submit" onclick="">完了</button>
                                    <input type ="hidden" name ="diagID" value = {{$data['diagId']}} >
                                    <input type ="hidden" name ="pet_id" value = {{$data['pet_id']}} >
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <script>
        var flag = parseInt(document.getElementById("sum").value);
        if(flag < 50) {
            document.getElementById('alert_sum').textContent = "※ 合計金額が￥５０以上になるようにご入力ください";
        }
    </script>
@endsection
