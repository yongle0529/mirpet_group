@extends('layouts.app.clinic')

@section('content')
    <div class="container">
        <div id="inquiry" class="row mb-4 justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">お問い合わせ</h3>
            </div>
            <form id="inquiry_form" method="POST" action="{{ URL::to('/clinic/inquiry') }}" class="row col-md-8">
                @csrf
                <div class="col-md-12">
                    <h5 class="font-weight-bold">お名前</h5>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname-kanji">姓</label>
                        <input id="lastname-kanji" name="lastname-kanji" type="text" value="" placeholder="山本" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname-kanji">名</label>
                        <input id="firstname-kanji" name="firstname-kanji" type="text" value="" placeholder="太郎" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold">フリガナ</h5>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname-kana">セイ</label>
                        <input id="lastname-kana" name="lastname-kana" type="text" value="" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="ヤマモト" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname-kana">メイ</label>
                        <input id="firstname-kana" name="firstname-kana" type="text" value="" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="タロウ" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">医院名</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-name" name="clinic-name" type="text" value="" placeholder="みるペット動物病院" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">役職</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="user-role" name="user-role" type="text" value="" placeholder="院長" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">年代</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="user-age" name="user-age" required>
                            <option value="">選択して下さい</option>
                            @for ($i = 1; $i < 9; $i++)
                                <option
                                    value="{{$i}}0代"
                                    {{ $i."0代" == old('user-age') ? 'selected' : '' }}>{{$i}}0代
                                </option>
                            @endfor
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">メールアドレス</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-mail" name="clinic-mail" type="email" value="" placeholder="info@mirpet.co.jp" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">電話番号(ハイフンなし)</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-tel" name="clinic-tel" type="tel" value="" pattern="^[0-9]+$" title="※数字のみ入力できます" placeholder="0369103242" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">ご希望の連絡方法</h5>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" checked>電話
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" checked>メール
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                </div>

                <div class="col-md-12 contact_method_tel_area">
                    <h5 class="font-weight-bold">電話連絡希望日時</h5>
                </div>

                <div class="col-md-12 contact_method_tel_area">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="contact-method-tel-date" class="col-form-label">連絡希望日時</label>
                            <select class="form-control" id="contact-method-tel-date" name="contact-method-tel-date">
                                <option value="指定なし" {{ empty(old('contact-method-tel-date')) ? 'selected' : '' }}>指定なし</option>
                                @php(setlocale(LC_ALL, 'ja_JP.UTF-8'))
                                @for ($i = 1; $i <= 7; $i++)
                                    @php($targetDate = \Carbon\Carbon::now()->addDay($i)->formatLocalized('%Y年%m月%d日(%a)'))
                                    <option value="{{$targetDate}}" {{ $targetDate == old('contact-method-tel-date') ? 'selected' : '' }}>{{$targetDate}}
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="contact-method-tel-date" class="col-form-label">連絡希望時間帯</label>
                            <select class="form-control" id="contact-method-tel-timezone" name="contact-method-tel-timezone">
                                @foreach(config('const.INQUIRY_TEL_DESIRED_TIMEZONE') as $time)
                                    <option value="{{$time}}" {{ $time == old('contact-method-tel-timezone') ? 'selected' : '' }}>{{$time}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">みるペットを知ったきっかけ</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="referer" name="referer" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.INQUIRY_REFERER') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('referer') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ内容</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.INQUIRY_TYPE') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('type') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ詳細</h5>
                </div>
                <div class="col-md-12">
                    <div class="textarea-container">
                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                    </div>
                </div>


                <div class="col-md-12 text-center mt-4">
                    <button id="confirm" type="button" class="btn btn-primary btn-lg">入力内容を確認</button>
                </div>


            </form>

        </div>
    </div>
    <script>
        $(function () {
            $('input[name="contact-method[]"]').change(function () {
                $('.contact_method_tel_area').hide();
                $('.modal_contact_method_tel_area').hide();

                $('input[name="contact-method[]"]:checked').each(function () {
                    var val = $(this).val();
                    if (val == "tel") {
                        $('.contact_method_tel_area').show();
                        $('.modal_contact_method_tel_area').show();
                    }
                })
            });

            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-lastname-kanji').text($('input[name="lastname-kanji"]').val());
                    $('#confirm-firstname-kanji').text($('input[name="firstname-kanji"]').val());
                    $('#confirm-lastname-kana').text($('input[name="lastname-kana"]').val());
                    $('#confirm-firstname-kana').text($('input[name="firstname-kana"]').val());
                    $('#confirm-clinic-name').text($('input[name="clinic-name"]').val());
                    $('#confirm-user-role').text($('input[name="user-role"]').val());
                    $('#confirm-user-age').text($('[name="user-age"]').val());
                    $('#confirm-clinic-mail').text($('input[name="clinic-mail"]').val());
                    $('#confirm-clinic-tel').text($('input[name="clinic-tel"]').val());

                    var contactMethod = [];
                    $('input[name="contact-method[]"]:checked').each(function () {
                        contactMethod.push($(this).data('label'));
                    });
                    $('#confirm-contact-method').text(contactMethod.length == 0 ? '選択されていません' : contactMethod);
                    $('#confirm-contact-method-tel-date').text($('[name="contact-method-tel-date"]').val());
                    $('#confirm-contact-method-tel-timezone').text($('[name="contact-method-tel-timezone"]').val());
                    $('#confirm-referer').text($('[name="referer"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection
