@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            請求ミステーク
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            ペットオーナー様がクレジットカードを登録していないため、お支払いができません。<br/>
                            <a href="{{URL::to('/app/clinic/reserve_insert')}}" class="btn btn-primary btn-lg">戻る</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
