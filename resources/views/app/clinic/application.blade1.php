@extends('layouts.app.clinic')

@section('content')
    <div class="row">
        <h3 class="font-weight-bold mb-4">受付状況</h3>
    </div>

    <div class="row mb-3">
        <style>
            .sub-menu-btn {
                min-width: 10vw;
                font-size: 1rem;
            }
        </style>
        <script>

            $(function () {
                var current = "{{$mode}}";
                $('.sub-menu-btn').each(function (index, element) {
                    var rel = $(this).attr('rel');
                    if (current == rel) {
                        $(element).addClass('btn-info');
                        $(element).removeClass('btn-default');
                    }

                })

                $('.sub-menu-btn').click(function () {
                    var mode = $(this).attr('rel');
                    if (current != mode) {
                        location.href = "{{URL::to('/app/clinic')}}/application?mode=" + mode;
                    }
                });
            });
        </script>
        <button class="sub-menu-btn btn btn-default" rel="exam">診察待ち({{$statusCount['exam']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="bill">会計待ち({{$statusCount['bill']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="shipping">発送待ち({{$statusCount['shipping']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="remind">再診リマインド({{$statusCount['remind']}}件)</button>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                <thead>
                <tr class="bg-warning">
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物種</th>
                    <th>種類</th>
                    <th>予約時間</th>
                    <th>目的</th>
                    <th>担当医</th>
                    <th>アクション</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{$item['karteNumber']}}</td>
                        <td>{{$item['ownerName']}}</td>
                        <td>{{$item['petName']}}</td>
                        <td>{{$item['petType']}}</td>
                        <td>{{$item['petSpecifiedTyoe']}}</td>
                        <td>{{$item['reserveDatetime']}}</td>
                        <td>{{$item['purpose']}}</td>
                        <td>{{$item['pic']}}</td>
                        <td class="text-center text-white">
                            <a class="btn btn-sm btn-primary mt-1" href="{{URL::to('/app/clinic/chat/'.$item['reserveId'])}}">診察する</a>
                            <a class="btn btn-sm btn-default mt-1" data-toggle="modal" data-target="#exampleModal">取り消し</a>
                        </td>
                    </tr>
                @endforeach
                @if(count($data) == 0)
                    <tr>
                        <td colspan="9">対象のデータがありません</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">予約を取り消しますか？</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    操作は取り消すことはできません。実行しますか？
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">実行</button>
                </div>
            </div>
        </div>
    </div>



@endsection
