@extends('layouts.app.clinic')

@section('header')
    <style>
        #navigation {
            display: none !important;
        }
    </style>
@endsection

@section('content')
<script>
function search_back()
{
    history.go(-1);
}
</script>
<div class="content" style="padding-left: 5%;padding-right: 5%">
    <div class="content">
    <div class="col-md-12 border-bottom border-danger mb-4">
        <h3 class="title">診療詳細</h3>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <!-- Tab panes -->
                <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr class="bg-warning">
                            <th>カルテ番号</th>
                            <th>動物番号​</th>
                            <th>患者様名</th>
                            <th>動物種</th>
                            <th>種類</th>
                            <th>生年月日</th>
                            <th>年齢​</th>
                            <th>診療日</th>
                            <th>目的​</th>
                            <th>担当医</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr style="
    background-color: aliceblue;
    height: 73px;
" valign="middle">
                            <td>
                                <?php
                                                $ownercatano = \App\OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first();
                                                if(!empty($ownercatano)){
                                                    echo $ownercatano->cata_no;
                                                }
                                                ?>
                            </td>
                            <td>
                                <?php
                                $petcatano = \App\PetCatano::where('pet_id', $pet->id)->where('clinic_id', $diag->clinic_id)->first();
                                if(!empty($petcatano)){
                                    echo $petcatano->cata_no;
                                }
                                ?>
                            </td>
                            <td>{!! $pet->name !!}</td>
                            <td>
                                @if($pet->type == 1)
                                イヌ
                                @elseif($pet->type == 2)
                                ネコ
                                @elseif($pet->type == 4)
                                ハムスター
                                @elseif($pet->type == 5)
                                フェレット
                                @elseif($pet->type == 6)
                                爬虫類
                                @elseif($pet->type == 7)
                                ウサギ
                                @elseif($pet->type == 8)
                                鳥
                                @else
                                その他
                                @endif
                            </td>
                            <td>{!! $pet->genre !!}</td>
                            <td>{!! $pet->birthday !!}</td>
                            <td>{!! $age !!}</td>
                            <td>
                                @if($diag->status == 5 || $diag->status == 10)
                                キャンセル
                                @else
                                {!! substr($diag->end_datetime, 0, 10) !!}
                                @endif
                            </td>
                            <td></td>
                            <td>{!! $diag->clinic_name/* . " " . \App\Clinic::find($diag->id)->manager_name_kana*/ !!}</td>

                        </tr>
                        </tbody>
                    </table>
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">問診内容</h5>
                    {!! $diag->content !!}

                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">写真・動画データ</h5>
                    <div class="row col-md-12">
                    <?php
                    if(!empty($diagfile)){
                        foreach($diagfile as $row)
                        {
                            if($row->type == 1){
                    ?>


                        <div class="col-sm-4" id="image_preview">
                            <a href="{{URL::to('/images/insurance/'.$row->filename)}}" target="_bank">
                            <img src=<?php echo "/images/insurance/".$row->filename?> width="300px" class="rounded img-raised">
                            </a>
                        </div>


                    <?php }}?>
                    </div>
                    <br/>
                    <div>

                    <?php
                    foreach($diagfile as $row)
                    {
                if($row->type==2){
                        ?>
                                <div class="col-sm-12">
                                    <a href="{{URL::to('/images/insurance/'.$row->filename)}}" target="_bank">
                            <video width="300px" style="max-height: 200px; max-width: 300px" controls autoplay>
                            <source src="{{URL::to('/images/insurance/'.$row->filename)}}" type="video/mp4">
                        </video>
                            </a>
                        </div>
                    </div>
                        <?php }?>
                   <?php } }?>
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">会計データ​</h5>

                    @if(!empty($accounting))
                    <a target="_blank" href=<?php echo "/file/pdf/".$accounting->save_filename.'.'.$accounting->extension;?>>{!! $accounting->origi_filename !!}</a>
                    @endif
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">薬の処方</h5>

                    @if($accounting->delivery_cost!=0)
                    薬物処方あり
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療後のコメント</h5>
                    @endif
                    @if(!empty($accounting))
                    {{$accounting->comment}}
                    @endif
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">次回再診予定日​</h5>
                    @if(!empty($accounting))
                    {{$accounting->next_remind_date}}
                    @endif
                </div>
                </div>
            </div>
                <div>
                <label  class="col-md-12 col-form-label text-md-center">
                <button id="" type="button" class="btn btn-default" onclick = "search_back()">戻る</button>
                </label>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
