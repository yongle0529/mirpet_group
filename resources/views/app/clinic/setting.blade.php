@extends('layouts.app.clinic')

@section('content')
<div class ="container">
    <div class="row">
        <h3 class=" mb-4">各種設定</h3>
    </div>

    <div class="row mb-3">
        <style>
            .sub-menu-btn {
                min-width: 10vw;
                font-size: 1rem;
            }
        </style>
        <script>

            $(function () {
                var current = "{{$mode}}";
                $('.sub-menu-btn').each(function (index, element) {
                    var rel = $(this).attr('rel');
                    if (current == rel) {
                        $(element).addClass('btn-info');
                        $(element).removeClass('btn-default');
                    }

                })

                $('.sub-menu-btn').click(function () {
                    var mode = $(this).attr('rel');
                    if (current != mode) {
                        location.href = "{{URL::to('/app/clinic')}}/setting?mode=" + mode;
                    }
                });
            });
        </script>
        <button class="sub-menu-btn btn btn-default" rel="info">病院情報設定</button>
        <button class="sub-menu-btn btn btn-default" rel="rever">予約設定</button>
        <button class="sub-menu-btn btn btn-default" rel="pwd">暗号設定</button>
        <button class="sub-menu-btn btn btn-default" rel="account">口座設定</button>
    </div>
</div>
<div class ="content">
    <?php if($mode == "info"){ ?>
        <div class="row">
                <div class="col-md-10 ml-auto col-xl-8 mr-auto">
                    <div class="card">
                    <div class="card-body">
                    <div id="inquiry" class="row mb-4 justify-content-center">
                <div class="col-md-12 border-bottom border-danger mb-4">
                    <h3 class="title">病院基本設定</h3>
                </div>
                <form id="frm" method="POST" action="" class="row col-md-8">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="userid">ユーザーID</label>
                            <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hospital_name">病原名</label>
                            <input id="hospital_name" name="hospital_name" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kana">ひらがな表記</label>
                            <input id="" name="kana" type="text" value="" title="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Principal name">郵便番号</label>
                            <input id="" name="Principal name" type="text" value="" title="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="">郵便番号L</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input id="clinic-tel" name="clinic-tel" type="tel" value=""  title="" placeholder="" class="form-control" required="">
                        </div>
                    </div>


                    <div class="col-md-12">
                        <h5 class="">住所</h5>
                    </div>
                    <div class="col-md-12">

                    </div>
                    <div class="col-md-12">
                    <div class="form-group row">
                    <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                    <div class="col-md-6">
                        <select class="form-control" id="prefecture" name="prefecture">
                            <option value="北海道">北海道</option>
                            <option value="青森県">青森県</option>
                            <option value="岩手県">岩手県</option>
                            <option value="宮城県">宮城県</option>
                            <option value="秋田県">秋田県</option>
                            <option value="山形県">山形県</option>
                            <option value="福島県">福島県</option>
                            <option value="茨城県">茨城県</option>
                            <option value="栃木県">栃木県</option>
                            <option value="群馬県">群馬県</option>
                            <option value="埼玉県">埼玉県</option>
                            <option value="千葉県">千葉県</option>
                            <option value="東京都" selected="">東京都</option>
                            <option value="神奈川県">神奈川県</option>
                            <option value="新潟県">新潟県</option>
                            <option value="富山県">富山県</option>
                            <option value="石川県">石川県</option>
                            <option value="福井県">福井県</option>
                            <option value="山梨県">山梨県</option>
                            <option value="長野県">長野県</option>
                            <option value="岐阜県">岐阜県</option>
                            <option value="静岡県">静岡県</option>
                            <option value="愛知県">愛知県</option>
                            <option value="三重県">三重県</option>
                            <option value="滋賀県">滋賀県</option>
                            <option value="京都府">京都府</option>
                            <option value="大阪府">大阪府</option>
                            <option value="兵庫県">兵庫県</option>
                            <option value="奈良県">奈良県</option>
                            <option value="和歌山県">和歌山県</option>
                            <option value="鳥取県">鳥取県</option>
                            <option value="島根県">島根県</option>
                            <option value="岡山県">岡山県</option>
                            <option value="広島県">広島県</option>
                            <option value="山口県">山口県</option>
                            <option value="徳島県">徳島県</option>
                            <option value="香川県">香川県</option>
                            <option value="愛媛県">愛媛県</option>
                            <option value="高知県">高知県</option>
                            <option value="福岡県">福岡県</option>
                            <option value="佐賀県">佐賀県</option>
                            <option value="長崎県">長崎県</option>
                            <option value="熊本県">熊本県</option>
                            <option value="大分県">大分県</option>
                            <option value="宮崎県">宮崎県</option>
                            <option value="鹿児島県">鹿児島県</option>
                            <option value="沖縄県">沖縄県</option>
                        </select>
                    </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">九九鼎村</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">距離名</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">建物名</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="">医院名</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input id="hospital-mail" name="hospital-mail" type="email" value="" placeholder="info@mirpet.co.jp" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="">診療科目</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input id="hospital-mail" name="hospital-mail" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="">ホームページURL</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input id="clinic-tel" name="clinic-tel" type="tel" value=""  title="" placeholder="" class="form-control" required="">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h5 class="">病院説明</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="textarea-container">
                            <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="userid">担当医登録</label>
                            <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <label for="userid"><br></label>
                            <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <label for="userid"><br></label>
                            <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-primary btn-lg">追加ボタン</button>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                        <label for="userid">対応保険会社</label>
                            <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                    </div>


                </form>

                </div>
                </div>
            </div>
            </div>
        </div>

    <?php }else if($mode == "rever"){ ?>
        <div class="row">
            <div class="col-md-11 ml-auto col-xl-8 mr-auto">
              <div class="card">
                <div class="card-body">
                <div id="inquiry" class="row mb-4 justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">予約可能枠設定</h3>
            </div>
            <form id="frm" method="POST" action="" class="row col-md-8">
                <div class="col-md-12">
                <h5 class="">予約可能曜日</h5>
                </div>
                <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="" data-label="メール">月
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]"  data-label="電話" >火
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >水
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >木
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >金
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >土
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >日
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >予約単位
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="">郵便番号L</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                    <div class="table-responsive">
        <table id = "seltb1"  class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
            <thead>
            <tr class="bg-warning">
                <th>時間</th>
                <th><?php echo  "(日)" ?></th>
                <th><?php echo  "(月)" ?></th>
                <th><?php echo  "(火)" ?></th>
                <th><?php echo  "(木)" ?></th>
                <th><?php echo  "(水)" ?></th>
                <th><?php echo  "(金)" ?></th>
                <th><?php echo  "(土)" ?></th>
            </tr>
            </thead>
        <tbody>
        <?php for($i = 8; $i < 20 ; $i++){?>
        <tr >
            <td>
            <?php echo $i."時"; ?>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td   style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]"  data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
            <td  style = "cursor: pointer">
            <div class="col-md-12 mb-3">

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input  type="checkbox" >10
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >15
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" >20
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >30
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" >60
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>

            </div>
            </td>
        </tr>
        <?php }?>
        </tbody>
                </table>
                </div>
                                    </div>
                    </div>


                    <div class="col-md-12">
                        <h5 class="">料金設定</h5>
                    </div>
                    <div class="col-md-12">

                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">相談（初回）　　　　　　　　￥</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">相談（セカンドオピニオン）　￥</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">相談（再診）　　　　　　　　￥</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">その他　　　　　　　　　　　￥</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="text" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>


                    <div class="col-md-12" align="center">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-primary btn-lg">追加ボタン</button>
                        </div>
                    </div>


                </form>

            </div>
                    </div>
                </div>
                </div>
            </div>
    <?php }else if($mode == "pwd"){ ?>
        <div class="row">
                <div class="col-md-10 ml-auto col-xl-8 mr-auto">
                    <div class="card">
                    <div class="card-body">
                    <div id="inquiry" class="row mb-4 justify-content-center">
                <div class="col-md-12 border-bottom border-danger mb-4">
                    <h3 class="title">パスワード変更</h3>
                </div>
                <form id="frm" method="POST" action="" class="row col-md-8">

                    <div class="col-md-12">
                        <h5 class="">現在のパスワード</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">現在のパスワード</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">新しいパスワード</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">新しいパスワード（確認)</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right"><br></label>

                        </div>
                    </div>
                    <div class="col-md-12" align = "center">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-primary btn-lg">パスワードを変更するボタン</button>
                        </div>
                    </div>


                </form>

                </div>
                </div>
            </div>
            </div>
        </div>
    <?php }else if($mode == "account"){ ?>
        <div class="row">
                <div class="col-md-10 ml-auto col-xl-8 mr-auto">
                    <div class="card">
                    <div class="card-body">
                    <div id="inquiry" class="row mb-4 justify-content-center">
                <div class="col-md-12 border-bottom border-danger mb-4">
                    <h3 class="title">口座設定</h3>
                </div>
                <form id="frm" method="POST" action="" class="row col-md-8">

                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">口座種類</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">口座種類</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">点名</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">口座番号</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">口座名人(かたがな名)</label>
                        <div class="col-md-6">
                            <input id="city" name="city" type="password" value="" placeholder="" class="form-control" required="">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">備考</label>
                        <div class="col-md-6">
                        <div class="textarea-container">
                            <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                        </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                        <label for="prefecture" class="col-md-4 col-form-label text-md-right"><br></label>

                        </div>
                    </div>
                    <div class="col-md-4" align = "center">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-warning btn-lg">辺境</button>
                        </div>
                    </div>
                    <div class="col-md-4" align = "center">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-primary btn-lg">保存</button>
                        </div>
                    </div>
                    <div class="col-md-4" align = "center">
                        <div class="form-group">
                        <button id="confirm" type="button" class="btn btn-default btn-lg">キャンセル</button>
                        </div>
                    </div>

                </form>

                </div>
                </div>
            </div>
            </div>
        </div>
    <?php } ?>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">予約を取り消しますか？</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    操作は取り消すことはできません。実行しますか？
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">実行</button>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

