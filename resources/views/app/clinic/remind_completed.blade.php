@extends('layouts.app.clinic')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>再診予約が<b>{{ $user->last_name }} {{$pet->name}}</b>にリマインドされました.</h4><br/>
                            <a href="{{URL::to('/app/clinic/reserve_remind')}}" class="btn btn-primary btn-lg">閉じる</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
