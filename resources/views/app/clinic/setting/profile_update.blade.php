@extends('layouts.app.clinic')

@section('content')
    <div class="container">
    <div align="right">
        <form action="{{ route('clinic.delete', $user['id']) }}" class="clinic_delete" method="post">
            @csrf
            @method('DELETE')
            @php($diag_mine = \App\Diagnosis::where('clinic_id', $user['id'])->where('status', '!=', 6)->get())
            @if(!empty($diag_mine) && count($diag_mine) > 0)
            <button class="btn btn-primary" type="submit" onclick="" disabled><i class="fa fa-trash-o"></i>アカウント解約</button>
            @else
            <button class="btn btn-primary" type="submit" onclick=""><i class="fa fa-trash-o"></i>アカウント解約</button>
            @endif
        </form>
    </div>
    <h3 class="font-weight-bold mb-4 border-bottom border-dark">各種設定 - 病院情報設定</h3>
    <form method="POST" enctype="multipart/form-data" action="{{URL::to('/app/clinic/setting/profile')}}">
         @csrf
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-heading" style="margin-top:5px">
                        <h5 align="center"><strong>病院基本情報</strong></h5>
                    </div>
                    <div class="card-body">
                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">ユーザーID​</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" disabled class="form-control" name="id" value="{{ $user_id }}" required
                                           autofocus>

                                    @if ($errors->has('id'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">病院名​</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="みるぺっとクリニック" value="{{ old('name',$user->name) }}" required
                                           autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name_kana" class="col-md-4 col-form-label text-md-right">カナ</label>

                                <div class="col-md-6">
                                    <input id="last_name_kana" type="text" class="form-control{{ $errors->has('name_kana') ? ' is-invalid' : '' }}" name="name_kana" placeholder="ミルペットクリニック"
                                           value="{{ old('name_kana',$user->name_kana) }}"
                                           required autofocus>

                                    @if ($errors->has('name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name_kana" class="col-md-4 col-form-label text-md-right">院長名​</label>

                                <div class="col-md-6">
                                    <input id="first_name_kana" type="text" class="form-control{{ $errors->has('manager_name') ? ' is-invalid' : '' }}" name="manager_name" placeholder="山下"
                                           value="{{ old('manager_name',$user->manager_name) }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('manager_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('manager_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="taro.yamamoto@mirpet.co.jp" value="{{ old('email',$user->email) }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">住所</label>
                            </div>

                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('prefecture') ? ' is-invalid' : '' }}" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            <option
                                                value="{{ $pref }}"
                                                {{ $pref == old('prefecture',$user->prefecture) ? 'selected' : $pref == $user->prefecture ? 'selected' : '' }}
                                            >{{ $pref }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('prefecture'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prefecture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">郡市区(島)</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="中央区" value="{{ old('city',$user->city) }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">それ以降の住所</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="銀座7丁目18-13 クオリア銀座504" value="{{ old('address',$user->address) }}"
                                            required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">電話番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" placeholder="09012345678" value="{{ old('tel',$user->tel) }}" required autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <?php
                            $medical_option = $user->option_medical_course;
                            $pet_option = $user->option_pet;
                            if($medical_option != null){
                                $data = json_decode($user->option_medical_course,true);
                            }else {
                                $data = ([
                                    'internal' => null, //内科
                                    'orthopedic' => null,//整形外科
                                    'neurosurgery' => null,//神経外科
                                    'dermatology' => null,//皮膚科
                                    'cardiology' => null,//循環器科
                                    'urology' => null,//泌尿器科
                                    'ophthalmology' => null,//眼科
                                    'dentistry' => null,//歯科
                                    'rehabilitation' => null,//リハビリテーション
                                    'others' => null,
                                ]);
                            }
                            if($pet_option != null){
                                $data_pet = json_decode($user->option_pet,true);
                            }else {
                                $data_pet = ([
                                    'dog' => null, //内科
                                    'cat' => null,//整形外科
                                    'hamster' => null,//神経外科
                                    'ferret' => null,//皮膚科
                                    'rabbit' => null,//循環器科
                                    'bird' => null,//泌尿器科
                                    'reptile' => null,//眼科
                                    'others' => null,//歯科
                                ]);
                            }
                            ?>


                            <div class="form-group row">
                                    <label for="zip_code" class="col-md-12 col-form-label text-md-left">診察対象動物</label>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/dog_icon.png" style="width: 1.2rem;" alt="イヌ">
                                                <input class="form-check-input" type="checkbox" id="dog" <?php if($data_pet['dog'] == 'true')echo "checked" ?> name="dog" value="agree" data-label="" autocomplete="off" >

                                                イヌ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/cat_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="cats" name="cat" <?php if($data_pet['cat'] == 'true')echo "checked" ?>  value="agree" data-label="" autocomplete="off" >

                                                ネコ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/hamster_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="hamsters" name="hamster" <?php if($data_pet['hamster'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                ハムスター
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/ferret_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="ferret" <?php if($data_pet['ferret'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                フェレット
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/reptile_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="reptile"  <?php if($data_pet['reptile'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                爬虫類
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/rabbit_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="rabbit" <?php if($data_pet['rabbit'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                ウサギ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/bird_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                <input class="form-check-input" type="checkbox" id="agree" name="bird" <?php if($data_pet['bird'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                鳥
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/other_icon.png" style="width: 1.2rem;" alt="ネコ">
                                                    <input class="form-check-input" type="checkbox" id="agree" name="others" <?php if($data_pet['others'] == 'true')echo "checked" ?> value="agree" data-label="" autocomplete="off" >

                                                    その他
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    <div class="col-md-12"><hr></div>
                            </div>
                            <div class="form-group row">
                                    <label for="zip_code" class="col-md-8 col-form-label text-md-left">担当医師登録</label>
                                    <div for="zip_code" class="col-md-4 col-form-label text-md-left"><a data-toggle="modal" data-target="#myModal" class="btn btn-warning">追加</a></div>
                                    <div class="col-md-12">
                                        <label class="col-md-12 col-form-label text-md-left">
                                            @if(!empty($chargeDoctor))
                                                @foreach ($chargeDoctor as $chargeRow)
                                                    <a style="color:black" title="削除" href="{{URL::to('/app/clinic/setting/clinic_name_del/'.$chargeRow->id)}}">{{ $chargeRow->clinic_name.' ' }}</a>
                                                @endforeach
                                            @endif
                                        </label>
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">ホームページURL​</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" placeholder="https://mirpet.co.jp" value="{{ old('url', $user->url) }}" >

                                    @if ($errors->has('url'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    {{-- <a href="{{URL::to('/app/owner')}}" class="btn btn-default btn-lg">戻る</a> --}}
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-heading" style="margin-top:5px">
                        <h5 align="center"><strong>病院説明</strong></h5>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="textarea-container">
                            <?php
                            if(count($clinic_menus)>0)foreach($clinic_menus as $row)
                                ?>
                                <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。">{{ old('url',$user->clinic_description) }}</textarea>
                            </div>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="form-group">
                                <label for="userid">担当医登録</label>
                                <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <label for="userid"><br></label>
                                <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <label for="userid"><br></label>
                                <input id="userid" name="userid" type="text" value="" placeholder="" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button id="confirm" type="button" class="btn btn-primary">追加ボタン</button>
                            </div>
                        </div> -->
                        <div class="col-md-12">
                        <div class="form-group row">
                                    <label class="col-md-12 col-form-label text-md-left">病院紹介写真

                                    </label>
                                    <div class="col-md-12">
                                        <div id="image_preview"></div>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail img-raised">
                                                        @if(strlen($user->clinic_info_photos)>0)
                                                            <img id="img-pet_profile_image2" src="{{URL::to('assets/img/clinics/'.$user->id.'/'.$user->clinic_info_photos)}}">
                                                        @else

                                                        @endif
                                                    </div>
                                            <div class="fileinput-new thumbnail img-raised">
                                                <img id="img-pet_reg_insurance_front">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-default btn-file">
                                                    <span class="fileinput-new">ファイル選択</span>
                                                    <input type="file" name="clinic_info_photos" id="photos" accept=".png, .jpg, .jpeg">
                                                </span>
                                                @if(strlen($user->clinic_info_photos)>0)
                                                <a href="{{ URL::to('/app/clinic/setting/clinic_info_remove') }}" class="btn btn-danger btn-round" data-dismiss="fileinput">削除</a>


                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group row">
                                    <label class="col-md-12 col-form-label text-md-left">病院マークイメージ

                                    </label>
                                    <div class="col-md-12">
                                        <div id="image_preview1"></div>

                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                                @if(strlen($user->eyecatchimage)>0)
                                                <img id="img-pet_profile_image1" src="{{URL::to('assets/img/clinics/'.$user->id.'/'.$user->eyecatchimage)}}">
                                                @else

                                                @endif
                                            <div class="fileinput-new thumbnail img-raised">
                                                <img id="img-pet_reg_insurance_front">

                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-default btn-file">
                                                    <span class="fileinput-new">ファイル選択</span>
                                                    <input type="file" name="eyecatchimage" id="photos1" accept=".png, .jpg, .jpeg">
                                                </span>
                                                @if(strlen($user->eyecatchimage)>0)
                                                <a href="{{ URL::to('/app/clinic/setting/clinic_eye_remove') }}" class="btn btn-danger btn-round" data-dismiss="fileinput">削除</a>

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="userid">対応保険会社</label>
                                <input id="userid" name="insurance_company" type="text" value="{{ old('url',$user->insurance) }}" placeholder="" class="form-control">
                            </div>
                        </div>
                    </div>

                  </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header justify-content-center">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                  </button>
                  <h4 class="title title-up">担当医師登録</h4>
                </div>
                <form name="frm" method="POST" action="{{URL::to('/app/clinic/clinic_name_insert')}}">
                    @csrf
                    <div class="modal-body">
                    <p>
                            <div class="form-group row">
                                    <label for="zip_code" class="col-md-4 col-form-label text-md-right">担当医師登録</label>
                                    <div id="clinicname" class="col-md-6">
                                            <input class="form-control" required type="text" name="clinic_name">
                                            <input type="hidden" name="clinic_id" value="{{ $user->id }}">
                                    </div>
                            </div>
                    </p>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                    </div>
                 </form>
              </div>
            </div>
    </div>
    </div>
    <script type="text/javascript">
    // $(document).ready(function () {
    //     //DatePicker Example
    //     $('#datetimepicker').datetimepicker();
    // });
    function editReserve(date, time) {
        document.getElementById('reserve_datetime').value = date + " " + time + ":00";
    }
    // $('td').click(function() {
    //     $(this).css('backgroundColor', '#000');
    // });
    $("#photos").change(function(){

        $('#image_preview').html("");

        var total_file=document.getElementById("photos").files.length;

        for(var i=0;i<total_file;i++)
        {
            var name = event.target.files[i].name;
            //console.log("asdfgfhg"+name);
            var lastDot = name.lastIndexOf('.');
            var ext = name.substring(lastDot + 1);
            //console.log("asdfgfhg"+ext);
            if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif') {
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
            } else {
                $('#image_preview').append("<video><source src='"+URL.createObjectURL(event.target.files[i])+"' type='video/mp4'></video>");
            }

        }

    });
    $("#photos1").change(function(){

$('#image_preview1').html("");

var total_file=document.getElementById("photos1").files.length;

for(var i=0;i<total_file;i++)
{
    var name = event.target.files[i].name;
    //console.log("asdfgfhg"+name);
    var lastDot = name.lastIndexOf('.');
    var ext = name.substring(lastDot + 1);
    //console.log("asdfgfhg"+ext);
    if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif') {
        $('#image_preview1').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
    } else {
        $('#image_preview1').append("<video><source src='"+URL.createObjectURL(event.target.files[i])+"' type='video/mp4'></video>");
    }

}

});
jQuery(document).ready(function($){
    $('.clinic_delete').on('submit',function(e){
        if(!confirm('アカウントに保存されているデータはすべて削除され、\n以後、みるペットの機能は一切利用することができなくなります。\n本当に解約されますか？')){
            e.preventDefault();
        }
    });
});
</script>
@endsection
