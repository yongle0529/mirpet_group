@extends('layouts.app.clinic')

@section('content')
@php($bankInfo = json_decode($user['bank_info'], true))
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定 - 取引口座設定​</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                         <form method="POST" action="{{ URL::to('/app/clinic/setting/payment_save/') }}">
                            @csrf 

                            <div class="form-group row">
                                <label for="bank_name" class="col-md-4 col-form-label text-md-right">銀行名</label>

                                <div class="col-md-6">
                                    <input id="bank_name" type="text" class="form-control{{ $errors->has('bank_name') ? ' is-invalid' : '' }}" name="bank_name" placeholder="三井住友銀行" value="{{ $bankInfo['bank_name'] }}" required autofocus>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_branch_name" class="col-md-4 col-form-label text-md-right">支店名</label>

                                <div class="col-md-6">
                                    <input id="bank_branch_name" type="text" class="form-control{{ $errors->has('bank_branch_name') ? ' is-invalid' : '' }}" name="bank_branch_name" placeholder="新橋支店" value="{{ $bankInfo['bank_branch_name'] }}" required autofocus>

                                    @if ($errors->has('bank_branch_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_branch_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_type" class="col-md-4 col-form-label text-md-right">口座種別</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('bank_type') ? ' is-invalid' : '' }}" id="bank_type" name="bank_type">

                                        <option value="普通" {{ "普通" == $bankInfo['bank_type'] ? 'selected' : '' }}>普通</option>
                                        <option value="当座" {{ "当座" == $bankInfo['bank_type'] ? 'selected' : '' }}>当座</option>
                                        <option value="貯蓄" {{ "貯蓄" == $bankInfo['bank_type'] ? 'selected' : '' }}>貯蓄</option>
                                        <option value="その他" {{ "その他" == $bankInfo['bank_type'] ? 'selected' : '' }}>その他</option>

                                    </select>
                                    @if ($errors->has('bank_type'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="branch_number" class="col-md-4 col-form-label text-md-right">店番</label>

                                <div class="col-md-6">
                                    <input id="branch_number" type="text" class="form-control{{ $errors->has('branch_number') ? ' is-invalid' : '' }}" name="branch_number" placeholder="001"
                                            value="{{ $bankInfo['branch_number'] }}" required autofocus>

                                    @if ($errors->has('branch_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('branch_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_account_number" class="col-md-4 col-form-label text-md-right">口座番号</label>

                                <div class="col-md-6">
                                    <input id="bank_account_number" type="text" class="form-control{{ $errors->has('bank_account_number') ? ' is-invalid' : '' }}" name="bank_account_number" placeholder="0123456" value="{{ $bankInfo['bank_account_number'] }}" required autofocus>

                                    @if ($errors->has('bank_account_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_account_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_account_name" class="col-md-4 col-form-label text-md-right">口座名義(カタカナ)</label>

                                <div class="col-md-6">
                                    <input id="bank_account_name" type="text" class="form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" name="bank_account_name" placeholder="カブシキガイシャミルペットクリニック" value="{{ $bankInfo['bank_account_name'] }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bank_account_name" class="col-md-4 col-form-label text-md-right">備考</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="comment"><?php if(count($bankInfo)>6) echo $bankInfo['comment'];else echo ''?></textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                            
                                <div class="col-md-8 offset-md-6">
                                   <button type="submit" class="btn btn-primary">
                                   保 存
                                    </button> 
                                </div>
                            </div>
                         </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
