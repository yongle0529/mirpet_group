@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">           

        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                                <h4>相談メニューを削除しました。</h4>                            
                            <a href="{{URL::to('/app/clinic/setting/menu')}}" class="btn btn-primary btn-lg">戻る</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
