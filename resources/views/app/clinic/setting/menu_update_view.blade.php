@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">

        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="col-md-12 border-bottom border-danger mb-4">
                    <h3 class="title">相談・診療メニュー設定</h3>
                </div>
                    <div class="row justify-content-md-center text-center">
                    <?php
                        if(!empty($query)){
                            if($query->clinic_id==0){

                        ?>
                        <div class="col-md-12">
                        <form id="frm_menu_update" name="frm_menu_update" method="POST" action="{{URL::to('/app/clinic/setting/oldmenu_update/'.$query->id)}}">
                        @csrf
                        <?php $data = json_decode($query->price_list,true);
                           foreach($data as $item){
                            ?>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right">題目</label>
                                    <div class="col-md-6">
                                        <input id="first_title" name="first_title" type="text" value="{{$query->name}}" readdisable placeholder="" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right">詳細内容</label>
                                    <div class="col-md-6">
                                        <textarea id="description" name="description"  class='form-control' required>{{$query->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">相談時間(分)</label>
                                <div class="col-md-6">
                                    <input id="toke_time" value="{{$item['title']}}" name="toke_time[]" value="" type="number" value=""  placeholder="" class="form-control" >
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right">料金</label>
                                    <div class="col-md-6">
                                        <input id="toke_price"  value="{{$item['price']}}" name="toke_price[]" type="number" value=""  placeholder="" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                           <?php }?>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right"></label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                        <?php if($query->is_insurance_applicable==1){?>
                                            保険証適用 <input class="form-check-input" checked type="checkbox" name ="Insurance" value="1" data-label="電話" >
                                        <?php }else{?>
                                        保険証適用 <input class="form-check-input" type="checkbox" name ="Insurance" value="1" data-label="電話" >
                                        <?php }?>
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" align="center">
                                <div class="form-group">
                                    <button id="confirm" onclick="" type="submit" class="btn btn-warning btn-lg">変更</button>
                                    <a href="{{URL::to('/app/clinic/setting/menu')}}" class="btn btn-primary btn-lg">戻る</a>
                                    </div>
                                </div>

                            </div>
                        </form>
                        </div>
                        <?php }else{?>
                            <div class="col-md-12">
                        <form id="frm_menu_update" name="frm_menu_update" method="POST" action="{{URL::to('/app/clinic/setting/newmenu_update/'.$query->id)}}">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">題目</label>
                                <div class="col-md-6">
                                    <input id="first_title" name="first_title" type="text" value="{{$query->name}}" readdisable placeholder="" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">詳細内容</label>
                                <div class="col-md-6">
                                    <textarea id="description" name="description"  class='form-control' required>{{$query->description}}</textarea>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">
                            <div class="form-group row">
                            <?php $data = json_decode($query->price_list,true);
                           foreach($data as $item)
                            ?>
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">相談時間(分)</label>
                                <div class="col-md-6">
                                    <input id="toke_time" value="{{$item['title']}}" name="toke_time" value="" type="number" value="" readdisable placeholder="" class="form-control" >
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right">料金</label>
                                    <div class="col-md-6">
                                        <input id="toke_price"  value="{{$item['price']}}" name="toke_price" type="number" value="" readdisable placeholder="" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="prefecture" class="col-md-4 col-form-label text-md-right"></label>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                        <?php if($query->is_insurance_applicable==1){?>
                                            保険証適用 <input class="form-check-input" checked type="checkbox" name ="Insurance" value="1" data-label="電話" >
                                        <?php }else{?>
                                        保険証適用 <input class="form-check-input" type="checkbox" name ="Insurance" value="1" data-label="電話" >
                                        <?php }?>
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" align="center">
                                <div class="form-group">
                                    <button id="confirm" onclick="" type="submit" class="btn btn-warning btn-lg">変更</button>
                                    <a href="{{URL::to('/app/clinic/setting/menu')}}" class="btn btn-primary btn-lg">戻る</a>
                                    </div>
                                </div>

                            </div>
                        </form>
                        </div>

                            <?php }?>
                       <?php }
                    ?>

                </div>
            </div>
        </div>
    </div>
@endsection
