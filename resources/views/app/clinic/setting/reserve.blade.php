@extends('layouts.app.clinic')

@section('content')
<style>
        .tableFixHead { overflow-y: auto; height: 600px; }
        .tableFixHead thead th { position: sticky; top: 0; }

        /* Just common table stuff. Really. */
        table  { border-collapse: collapse; width: 100%; }
        th, td { padding: 8px 16px; }
        th     { background:#eee; }
    </style>
    <script>
        $(function () {
            $("#code_flag").on('onchange', function () {
                $("#hospital_code").disabled = $("#code_flag").checked;
            });
        });
    </script>
<script>
function timelineChecked()
{
    if ( $("#time_line").prop('checked') == true) {
        if ( $("#week1").prop('checked') == true) {
            $("#week_sel_1_2").css("display","none");
            $("#week_sel_1_1").css("display","none");
            $("#week_sel_1_3").css("display","block");
            //
            $("#p_week_sel_1_2").css("display","none");
            $("#p_week_sel_1_1").css("display","none");
            $("#p_week_sel_1_3").css("display","block");
        }else{
            $("#week_sel_1_2").css("display","none");
            $("#week_sel_1_1").css("display","block");
            $("#week_sel_1_3").css("display","none");
            //
            $("#p_week_sel_1_2").css("display","none");
            $("#p_week_sel_1_1").css("display","block");
            $("#p_week_sel_1_3").css("display","none");
        }
        if ( $("#week2").prop('checked') == true) {
            $("#week_sel_2_2").css("display","none");
            $("#week_sel_2_1").css("display","none");
            $("#week_sel_2_3").css("display","block");
            //
            $("#p_week_sel_2_2").css("display","none");
            $("#p_week_sel_2_1").css("display","none");
            $("#p_week_sel_2_3").css("display","block");
        }else{
            $("#week_sel_2_2").css("display","none");
            $("#week_sel_2_1").css("display","block");
            $("#week_sel_2_3").css("display","none");
            //
            $("#p_week_sel_2_2").css("display","none");
            $("#p_week_sel_2_1").css("display","block");
            $("#p_week_sel_2_3").css("display","none");
        }
        //
        if ( $("#week3").prop('checked') == true) {
            $("#week_sel_3_2").css("display","none");
            $("#week_sel_3_1").css("display","none");
            $("#week_sel_3_3").css("display","block");
            //
            $("#p_week_sel_3_2").css("display","none");
            $("#p_week_sel_3_1").css("display","none");
            $("#p_week_sel_3_3").css("display","block");
        }else{
            $("#week_sel_3_2").css("display","none");
            $("#week_sel_3_1").css("display","block");
            $("#week_sel_3_3").css("display","none");
            //
            $("#p_week_sel_3_2").css("display","none");
            $("#p_week_sel_3_1").css("display","block");
            $("#p_week_sel_3_3").css("display","none");
        }
        //
        if ( $("#week4").prop('checked') == true) {
            $("#week_sel_4_2").css("display","none");
            $("#week_sel_4_1").css("display","none");
            $("#week_sel_4_3").css("display","block");
            //
            $("#p_week_sel_4_2").css("display","none");
            $("#p_week_sel_4_1").css("display","none");
            $("#p_week_sel_4_3").css("display","block");
        }else{
            $("#week_sel_4_2").css("display","none");
            $("#week_sel_4_1").css("display","block");
            $("#week_sel_4_3").css("display","none");
            //
            $("#p_week_sel_4_2").css("display","none");
            $("#p_week_sel_4_1").css("display","block");
            $("#p_week_sel_4_3").css("display","none");
        }
        //
        if ( $("#week5").prop('checked') == true) {
            $("#week_sel_5_2").css("display","none");
            $("#week_sel_5_1").css("display","none");
            $("#week_sel_5_3").css("display","block");
            //
            $("#p_week_sel_5_2").css("display","none");
            $("#p_week_sel_5_1").css("display","none");
            $("#p_week_sel_5_3").css("display","block");
        }else{
            $("#week_sel_5_2").css("display","none");
            $("#week_sel_5_1").css("display","block");
            $("#week_sel_5_3").css("display","none");
            //
            $("#p_week_sel_5_2").css("display","none");
            $("#p_week_sel_5_1").css("display","block");
            $("#p_week_sel_5_3").css("display","none");
        }
        //
        if ( $("#week6").prop('checked') == true) {
            $("#week_sel_6_2").css("display","none");
            $("#week_sel_6_1").css("display","none");
            $("#week_sel_6_3").css("display","block");
            //
            $("#p_week_sel_6_2").css("display","none");
            $("#p_week_sel_6_1").css("display","none");
            $("#p_week_sel_6_3").css("display","block");
        }else{
            $("#week_sel_6_2").css("display","none");
            $("#week_sel_6_1").css("display","block");
            $("#week_sel_6_3").css("display","none");
            //
            $("#p_week_sel_6_2").css("display","none");
            $("#p_week_sel_6_1").css("display","block");
            $("#p_week_sel_6_3").css("display","none");
        }
        //
        if ( $("#week7").prop('checked') == true) {
            $("#week_sel_7_2").css("display","none");
            $("#week_sel_7_1").css("display","none");
            $("#week_sel_7_3").css("display","block");
            //
            $("#p_week_sel_7_2").css("display","none");
            $("#p_week_sel_7_1").css("display","none");
            $("#p_week_sel_7_3").css("display","block");
        }else{
            $("#week_sel_7_2").css("display","none");
            $("#week_sel_7_1").css("display","block");
            $("#week_sel_7_3").css("display","none");
            //
            $("#p_week_sel_7_2").css("display","none");
            $("#p_week_sel_7_1").css("display","block");
            $("#p_week_sel_7_3").css("display","none");
        }
    }else{
        if ( $("#week1").prop('checked') == true) {
            $("#week_sel_1_2").css("display","block");
            $("#week_sel_1_1").css("display","none");
            $("#week_sel_1_3").css("display","none");
            //
            $("#p_week_sel_1_2").css("display","block");
            $("#p_week_sel_1_1").css("display","none");
            $("#p_week_sel_1_3").css("display","none");
        }else{
            $("#week_sel_1_2").css("display","none");
            $("#week_sel_1_1").css("display","block");
            $("#week_sel_1_3").css("display","none");
            //
            $("#p_week_sel_1_2").css("display","none");
            $("#p_week_sel_1_1").css("display","block");
            $("#p_week_sel_1_3").css("display","none");
        }
        if ( $("#week2").prop('checked') == true) {
            $("#week_sel_2_2").css("display","block");
            $("#week_sel_2_1").css("display","none");
            $("#week_sel_2_3").css("display","none");
            //
            $("#p_week_sel_2_2").css("display","block");
            $("#p_week_sel_2_1").css("display","none");
            $("#p_week_sel_2_3").css("display","none");
        }else{
            $("#week_sel_2_2").css("display","none");
            $("#week_sel_2_1").css("display","block");
            $("#week_sel_2_3").css("display","none");
            //
            $("#p_week_sel_2_2").css("display","none");
            $("#p_week_sel_2_1").css("display","block");
            $("#p_week_sel_2_3").css("display","none");
        }
        //
        if ( $("#week3").prop('checked') == true) {
            $("#week_sel_3_2").css("display","block");
            $("#week_sel_3_1").css("display","none");
            $("#week_sel_3_3").css("display","none");
            //
            $("#p_week_sel_3_2").css("display","block");
            $("#p_week_sel_3_1").css("display","none");
            $("#p_week_sel_3_3").css("display","none");
        }else{
            $("#week_sel_3_2").css("display","none");
            $("#week_sel_3_1").css("display","block");
            $("#week_sel_3_3").css("display","none");
            //
            $("#p_week_sel_3_2").css("display","none");
            $("#p_week_sel_3_1").css("display","block");
            $("#p_week_sel_3_3").css("display","none");
        }
        //
        if ( $("#week4").prop('checked') == true) {
            $("#week_sel_4_2").css("display","block");
            $("#week_sel_4_1").css("display","none");
            $("#week_sel_4_3").css("display","none");
            //
            $("#p_week_sel_4_2").css("display","block");
            $("#p_week_sel_4_1").css("display","none");
            $("#p_week_sel_4_3").css("display","none");
        }else{
            $("#week_sel_4_2").css("display","none");
            $("#week_sel_4_1").css("display","block");
            $("#week_sel_4_3").css("display","none");
            //
            $("#p_week_sel_4_2").css("display","none");
            $("#p_week_sel_4_1").css("display","block");
            $("#p_week_sel_4_3").css("display","none");
        }
        //
        if ( $("#week5").prop('checked') == true) {
            $("#week_sel_5_2").css("display","block");
            $("#week_sel_5_1").css("display","none");
            $("#week_sel_5_3").css("display","none");
            //
            $("#p_week_sel_5_2").css("display","block");
            $("#p_week_sel_5_1").css("display","none");
            $("#p_week_sel_5_3").css("display","none");
        }else{
            $("#week_sel_5_2").css("display","none");
            $("#week_sel_5_1").css("display","block");
            $("#week_sel_5_3").css("display","none");
            //
            $("#p_week_sel_5_2").css("display","none");
            $("#p_week_sel_5_1").css("display","block");
            $("#p_week_sel_5_3").css("display","none");
        }
        //
        if ( $("#week6").prop('checked') == true) {
            $("#week_sel_6_2").css("display","block");
            $("#week_sel_6_1").css("display","none");
            $("#week_sel_6_3").css("display","none");
            //
            $("#p_week_sel_6_2").css("display","block");
            $("#p_week_sel_6_1").css("display","none");
            $("#p_week_sel_6_3").css("display","none");
        }else{
            $("#week_sel_6_2").css("display","none");
            $("#week_sel_6_1").css("display","block");
            $("#week_sel_6_3").css("display","none");
            //
            $("#p_week_sel_6_2").css("display","none");
            $("#p_week_sel_6_1").css("display","block");
            $("#p_week_sel_6_3").css("display","none");
        }
        //
        if ( $("#week7").prop('checked') == true) {
            $("#week_sel_7_2").css("display","block");
            $("#week_sel_7_1").css("display","none");
            $("#week_sel_7_3").css("display","none");
            //
            $("#p_week_sel_7_2").css("display","block");
            $("#p_week_sel_7_1").css("display","none");
            $("#p_week_sel_7_3").css("display","none");
        }else{
            $("#week_sel_7_2").css("display","none");
            $("#week_sel_7_1").css("display","block");
            $("#week_sel_7_3").css("display","none");
            //
            $("#p_week_sel_7_2").css("display","none");
            $("#p_week_sel_7_1").css("display","block");
            $("#p_week_sel_7_3").css("display","none");
        }
    }
}
function timeslotechange()
{
    {{-- $("#week1").prop('checked',false);
    $("#week2").prop('checked',false);
    $("#week3").prop('checked',false);
    $("#week4").prop('checked',false);
    $("#week5").prop('checked',false);
    $("#week6").prop('checked',false);
    $("#week7").prop('checked',false); --}}
    if(document.getElementById("time_option").value==0){
        $("#timeslote15").css("display","none");
        $("#timeslote20").css("display","none");
        $("#timeslote30").css("display","none");
        $("#timeslote0").css("display","block");
        $("#timeslote60").css("display","none");
        if ( $("#week1").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","none");
                    $("#monday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","block");
                    $("#monday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week2").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","none");
                    $("#tuesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","block");
                    $("#tuesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week3").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","none");
                    $("#wednesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","block");
                    $("#wednesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week4").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","none");
                    $("#thursday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","block");
                    $("#thursday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week5").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","none");
                    $("#friday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","block");
                    $("#friday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week6").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","none");
                    $("#saturday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","block");
                    $("#saturday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week7").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","none");
                    $("#sunday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","block");
                    $("#sunday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }

        }
    }else if(document.getElementById("time_option").value==1){
        $("#timeslote15").css("display","block");
        $("#timeslote20").css("display","none");
        $("#timeslote30").css("display","none");
        $("#timeslote0").css("display","none");
        $("#timeslote60").css("display","none");
        if ( $("#week1").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","none");
                    $("#monday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","block");
                    $("#monday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week2").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","none");
                    $("#tuesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","block");
                    $("#tuesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week3").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","none");
                    $("#wednesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","block");
                    $("#wednesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week4").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","none");
                    $("#thursday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","block");
                    $("#thursday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week5").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","none");
                    $("#friday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","block");
                    $("#friday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week6").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","none");
                    $("#saturday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","block");
                    $("#saturday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week7").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","none");
                    $("#sunday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","block");
                    $("#sunday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }

        }
    }else if(document.getElementById("time_option").value==2){
        $("#timeslote15").css("display","none");
        $("#timeslote20").css("display","block");
        $("#timeslote30").css("display","none");
        $("#timeslote0").css("display","none");
        $("#timeslote60").css("display","none");
        if ( $("#week1").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","none");
                    $("#monday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","block");
                    $("#monday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week2").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","none");
                    $("#tuesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","block");
                    $("#tuesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week3").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","none");
                    $("#wednesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","block");
                    $("#wednesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week4").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","none");
                    $("#thursday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","block");
                    $("#thursday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week5").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","none");
                    $("#friday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","block");
                    $("#friday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week6").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","none");
                    $("#saturday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","block");
                    $("#saturday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week7").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","none");
                    $("#sunday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","block");
                    $("#sunday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }

        }
    }else if(document.getElementById("time_option").value==3){
        $("#timeslote15").css("display","none");
        $("#timeslote20").css("display","none");
        $("#timeslote30").css("display","block");
        $("#timeslote0").css("display","none");
        $("#timeslote60").css("display","none");
        if ( $("#week1").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","none");
                    $("#monday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","block");
                    $("#monday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week2").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","none");
                    $("#tuesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","block");
                    $("#tuesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week3").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","none");
                    $("#wednesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","block");
                    $("#wednesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week4").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","none");
                    $("#thursday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","block");
                    $("#thursday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week5").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","none");
                    $("#friday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","block");
                    $("#friday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week6").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","none");
                    $("#saturday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","block");
                    $("#saturday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week7").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","none");
                    $("#sunday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","block");
                    $("#sunday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }

        }
    }else if(document.getElementById("time_option").value==4){
        $("#timeslote15").css("display","none");
        $("#timeslote20").css("display","none");
        $("#timeslote30").css("display","none");
        $("#timeslote0").css("display","none");
        $("#timeslote60").css("display","block");
        if ( $("#week1").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","none");
                    $("#monday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","none");
                        $("#monday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#monday"+i).css("display","block");
                    $("#monday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#monday"+count).css("display","block");
                        $("#monday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week2").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","none");
                    $("#tuesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","none");
                        $("#tuesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#tuesday"+i).css("display","block");
                    $("#tuesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#tuesday"+count).css("display","block");
                        $("#tuesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week3").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","none");
                    $("#wednesday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","none");
                        $("#wednesday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#wednesday"+i).css("display","block");
                    $("#wednesday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#wednesday"+count).css("display","block");
                        $("#wednesday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week4").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","none");
                    $("#thursday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","none");
                        $("#thursday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#thursday"+i).css("display","block");
                    $("#thursday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#thursday"+count).css("display","block");
                        $("#thursday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week5").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","none");
                    $("#friday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","none");
                        $("#friday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#friday"+i).css("display","block");
                    $("#friday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#friday"+count).css("display","block");
                        $("#friday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week6").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","none");
                    $("#saturday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","none");
                        $("#saturday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#saturday"+i).css("display","block");
                    $("#saturday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#saturday"+count).css("display","block");
                        $("#saturday_view"+count).css("display","none");
                    }
                }
            }

        }
        //
        if ( $("#week7").prop('checked') == true) {
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","none");
                    $("#sunday_view"+i).css("display","block");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","none");
                        $("#sunday_view"+count).css("display","block");
                    }
                }
            }

        }else{
            if(document.getElementById("time_option").value==4){
                for(i=0;i<25;i++){
                    $("#sunday"+i).css("display","block");
                    $("#sunday_view"+i).css("display","none");
                }
            }else if(document.getElementById("time_option").value==0){
                var count=1000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=10){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==1){
                var count=2000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=15){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==2){
                var count=3000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=20){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }else if(document.getElementById("time_option").value==3){
                var count=4000;
                for(i=0;i<25;i++){
                    for(j=0;j<60;j+=30){
                        count++;
                        $("#sunday"+count).css("display","block");
                        $("#sunday_view"+count).css("display","none");
                    }
                }
            }

        }
    }
}
function weekChecked()
{

    if ( $("#week1").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#monday"+i).css("display","none");
                $("#monday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#monday"+count).css("display","none");
                    $("#monday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#monday"+count).css("display","none");
                    $("#monday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#monday"+count).css("display","none");
                    $("#monday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#monday"+count).css("display","none");
                    $("#monday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#monday"+i).css("display","block");
                $("#monday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#monday"+count).css("display","block");
                    $("#monday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#monday"+count).css("display","block");
                    $("#monday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#monday"+count).css("display","block");
                    $("#monday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#monday"+count).css("display","block");
                    $("#monday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week2").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#tuesday"+i).css("display","none");
                $("#tuesday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#tuesday"+count).css("display","none");
                    $("#tuesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#tuesday"+count).css("display","none");
                    $("#tuesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#tuesday"+count).css("display","none");
                    $("#tuesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#tuesday"+count).css("display","none");
                    $("#tuesday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#tuesday"+i).css("display","block");
                $("#tuesday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#tuesday"+count).css("display","block");
                    $("#tuesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#tuesday"+count).css("display","block");
                    $("#tuesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#tuesday"+count).css("display","block");
                    $("#tuesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#tuesday"+count).css("display","block");
                    $("#tuesday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week3").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#wednesday"+i).css("display","none");
                $("#wednesday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#wednesday"+count).css("display","none");
                    $("#wednesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#wednesday"+count).css("display","none");
                    $("#wednesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#wednesday"+count).css("display","none");
                    $("#wednesday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#wednesday"+count).css("display","none");
                    $("#wednesday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#wednesday"+i).css("display","block");
                $("#wednesday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#wednesday"+count).css("display","block");
                    $("#wednesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#wednesday"+count).css("display","block");
                    $("#wednesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#wednesday"+count).css("display","block");
                    $("#wednesday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#wednesday"+count).css("display","block");
                    $("#wednesday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week4").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#thursday"+i).css("display","none");
                $("#thursday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#thursday"+count).css("display","none");
                    $("#thursday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#thursday"+count).css("display","none");
                    $("#thursday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#thursday"+count).css("display","none");
                    $("#thursday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#thursday"+count).css("display","none");
                    $("#thursday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#thursday"+i).css("display","block");
                $("#thursday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#thursday"+count).css("display","block");
                    $("#thursday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#thursday"+count).css("display","block");
                    $("#thursday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#thursday"+count).css("display","block");
                    $("#thursday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#thursday"+count).css("display","block");
                    $("#thursday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week5").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#friday"+i).css("display","none");
                $("#friday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#friday"+count).css("display","none");
                    $("#friday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#friday"+count).css("display","none");
                    $("#friday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#friday"+count).css("display","none");
                    $("#friday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#friday"+count).css("display","none");
                    $("#friday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#friday"+i).css("display","block");
                $("#friday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#friday"+count).css("display","block");
                    $("#friday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#friday"+count).css("display","block");
                    $("#friday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#friday"+count).css("display","block");
                    $("#friday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#friday"+count).css("display","block");
                    $("#friday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week6").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#saturday"+i).css("display","none");
                $("#saturday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#saturday"+count).css("display","none");
                    $("#saturday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#saturday"+count).css("display","none");
                    $("#saturday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#saturday"+count).css("display","none");
                    $("#saturday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#saturday"+count).css("display","none");
                    $("#saturday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#saturday"+i).css("display","block");
                $("#saturday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#saturday"+count).css("display","block");
                    $("#saturday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#saturday"+count).css("display","block");
                    $("#saturday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#saturday"+count).css("display","block");
                    $("#saturday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#saturday"+count).css("display","block");
                    $("#saturday_view"+count).css("display","none");
                }
            }
        }

    }
    //
    if ( $("#week7").prop('checked') == true) {
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#sunday"+i).css("display","none");
                $("#sunday_view"+i).css("display","block");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#sunday"+count).css("display","none");
                    $("#sunday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#sunday"+count).css("display","none");
                    $("#sunday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#sunday"+count).css("display","none");
                    $("#sunday_view"+count).css("display","block");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#sunday"+count).css("display","none");
                    $("#sunday_view"+count).css("display","block");
                }
            }
        }

    }else{
        if(document.getElementById("time_option").value==4){
            for(i=0;i<25;i++){
                $("#sunday"+i).css("display","block");
                $("#sunday_view"+i).css("display","none");
            }
        }else if(document.getElementById("time_option").value==0){
            var count=1000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=10){
                    count++;
                    $("#sunday"+count).css("display","block");
                    $("#sunday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==1){
            var count=2000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=15){
                    count++;
                    $("#sunday"+count).css("display","block");
                    $("#sunday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==2){
            var count=3000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=20){
                    count++;
                    $("#sunday"+count).css("display","block");
                    $("#sunday_view"+count).css("display","none");
                }
            }
        }else if(document.getElementById("time_option").value==3){
            var count=4000;
            for(i=0;i<25;i++){
                for(j=0;j<60;j+=30){
                    count++;
                    $("#sunday"+count).css("display","block");
                    $("#sunday_view"+count).css("display","none");
                }
            }
        }

    }

}
function menuCheck()
{

}
</script>

    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">各種設定 - 予約設定</h3>
        <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                    <div id="inquiry" class="row mb-4 justify-content-center">
                <div class="col-md-12 border-bottom border-danger mb-4">
                    <h3 class="title">予約可能枠設定</h3>
                </div>
                <form id="frm" method="POST" action="{{URL::to('/app/clinic/setting/reseve_edit')}}">
                @csrf
                    <div class="col-md-12">
                    <h5 class="">予約可能曜日</h5>
                    </div>
                    <div class="col-md-12 mb-3">

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week1" name="contact_method[]" value="1" data-label="メール">月
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week2" name="contact_method[]" value="2" data-label="電話" >火
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week3" name="contact_method[]" value="3" data-label="メール" >水
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week4" name="contact_method[]" value="4" data-label="電話" >木
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week5" name="contact_method[]" value="5" data-label="メール" >金
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week6" name="contact_method[]" value="6" data-label="メール" >土
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" onclick="weekChecked()" id = "week7" name="contact_method[]" value="0" data-label="電話" >日
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">

                            <div class="form-group row">
                            <label for="prefecture" class="col-md-1 col-form-label text-md-left">予約単位</label>
                            <div class="col-md-2">
                            <select onchange="timeslotechange()" class="form-control"  id = "time_option" name="time_option">
                                <option value = '4'>60分</option>
                                <option value = '0'>10分</option>
                                <option value = '1'>15分</option>
                                <option value = '2'>20分</option>
                                <option value = '3'>30分</option>
                             </select>
                            </div>
                            </div>
                        </div>
                    <div class="col-md-6">
                    <label for="prefecture" class="col-md-3 col-form-label text-md-right"></label>

                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                        <div id="timeslote60"  class="tableFixHead">
                            <table class="table table-bordered  text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                <thead>
                                <tr class="bg-warning">
                                    <th>時間</th>
                                    <th><?php echo  "(月)" ?></th>
                                    <th><?php echo  "(火)" ?></th>
                                    <th><?php echo  "(水)" ?></th>
                                    <th><?php echo  "(木)" ?></th>
                                    <th><?php echo  "(金)" ?></th>
                                    <th><?php echo  "(土)" ?></th>
                                    <th><?php echo  "(日)" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                        @for($t=0;$t<25;$t++)
                                            <tr>
                                                <td>
                                                    @if(strlen($t)==1)
                                                    {{ '0'.$t.':00' }}
                                                    @else
                                                    {{ $t.':00' }}
                                                    @endif
                                                </td>
                                                {{--  //mondyay  --}}
                                                <td>
                                                    <div id="monday{{ $t }}">✕</div>
                                                    <div id="monday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="monday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //tuesday  --}}
                                                <td>
                                                    <div id="tuesday{{ $t }}">✕</div>
                                                    <div id="tuesday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="tuesday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //wednesday  --}}
                                                <td>
                                                    <div id="wednesday{{ $t }}">✕</div>
                                                    <div id="wednesday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="wednesday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //Thursday  --}}
                                                <td>
                                                    <div id="thursday{{ $t }}">✕</div>
                                                    <div id="thursday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="thursday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //Friday  --}}
                                                <td>
                                                    <div id="friday{{ $t }}">✕</div>
                                                    <div id="friday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="friday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //Saturday  --}}
                                                <td>
                                                    <div id="saturday{{ $t }}">✕</div>
                                                    <div id="saturday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="saturday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                                {{--  //Sunday  --}}
                                                <td>
                                                    <div id="sunday{{ $t }}">✕</div>
                                                    <div id="sunday_view{{ $t }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="sunday_chk[]" value="{{ $t.':00' }}">
                                                    </div>
                                                </td>
                                            </tr>
                                        @endfor
                                </tbody>
                            </table>
                        </div>
                        <div id="timeslote0" style="display:none" class="tableFixHead">
                                <table class="table table-bordered  text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                    <thead>
                                    <tr class="bg-warning">
                                        <th>時間</th>
                                        <th><?php echo  "(月)" ?></th>
                                        <th><?php echo  "(火)" ?></th>
                                        <th><?php echo  "(水)" ?></th>
                                        <th><?php echo  "(木)" ?></th>
                                        <th><?php echo  "(金)" ?></th>
                                        <th><?php echo  "(土)" ?></th>
                                        <th><?php echo  "(日)" ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                            <?php $timeend=0;$count=1000;?>
                                            @for($t=0;$t<25;$t++)
                                                @for($m=0;$m<60;$m+=10)
                                                <?php $count++;?>
                                                @if($timeend==1)
                                                    <?php $timeend=0;?>
                                                    @break
                                                @endif
                                                <tr>
                                                    <td>
                                                        @if($t!=24)
                                                            @if(strlen($t)==1)
                                                            @if($m==0)
                                                            {{ '0'.$t.':'.$m.'0' }}
                                                            @else
                                                            {{ '0'.$t.':'.$m }}
                                                            @endif
                                                            @else
                                                            @if($m==0)
                                                            {{ $t.':'.$m.'0'  }}
                                                            @else
                                                            {{ $t.':'.$m  }}
                                                            @endif
                                                            @endif
                                                        @else
                                                            <?php $timeend++;?>
                                                            @if($timeend==2)
                                                                @break
                                                                @else
                                                                {{ $t.':00'  }}
                                                            @endif
                                                        @endif
                                                    </td>
                                                    {{--  //mondyay  --}}
                                                    <td>
                                                        <div id="monday{{ $count  }}">✕</div>
                                                        <div id="monday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="monday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //tuesday  --}}
                                                    <td>
                                                        <div id="tuesday{{ $count  }}">✕</div>
                                                        <div id="tuesday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="tuesday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //wednesday  --}}
                                                    <td>
                                                        <div id="wednesday{{ $count  }}">✕</div>
                                                        <div id="wednesday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="wednesday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //Thursday  --}}
                                                    <td>
                                                        <div id="thursday{{ $count  }}">✕</div>
                                                        <div id="thursday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="thursday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //Friday  --}}
                                                    <td>
                                                        <div id="friday{{ $count  }}">✕</div>
                                                        <div id="friday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="friday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //Saturday  --}}
                                                    <td>
                                                        <div id="saturday{{ $count  }}">✕</div>
                                                        <div id="saturday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="saturday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                    {{--  //Sunday  --}}
                                                    <td>
                                                        <div id="sunday{{ $count  }}">✕</div>
                                                        <div id="sunday_view{{ $count  }}" style="display:none">
                                                            <input type="checkbox" style="width:20px;height:20px" name="sunday_chk[]" value="{{ $t.':'.$m }}">
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endfor
                                            @endfor
                                    </tbody>
                                </table>
                        </div>
                        <div id="timeslote15" style="display:none" class="tableFixHead">
                            <table class="table table-bordered  text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                <thead>
                                <tr class="bg-warning">
                                    <th>時間</th>
                                    <th><?php echo  "(月)" ?></th>
                                    <th><?php echo  "(火)" ?></th>
                                    <th><?php echo  "(水)" ?></th>
                                    <th><?php echo  "(木)" ?></th>
                                    <th><?php echo  "(金)" ?></th>
                                    <th><?php echo  "(土)" ?></th>
                                    <th><?php echo  "(日)" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                        <?php $timeend=0;$count=2000;?>
                                        @for($t=0;$t<25;$t++)
                                            @for($m=0;$m<60;$m+=15)
                                            <?php $count++;?>
                                            @if($timeend==1)
                                                <?php $timeend=0;?>
                                                @break
                                            @endif
                                            <tr>
                                                <td>
                                                    @if($t!=24)
                                                        @if(strlen($t)==1)
                                                        @if($m==0)
                                                        {{ '0'.$t.':'.$m.'0' }}
                                                        @else
                                                        {{ '0'.$t.':'.$m }}
                                                        @endif
                                                        @else
                                                        @if($m==0)
                                                        {{ $t.':'.$m.'0'  }}
                                                        @else
                                                        {{ $t.':'.$m  }}
                                                        @endif
                                                        @endif
                                                    @else
                                                        <?php $timeend++;?>
                                                        @if($timeend==2)
                                                            @break
                                                            @else
                                                            {{ $t.':00'  }}
                                                        @endif
                                                    @endif
                                                </td>
                                                {{--  //mondyay  --}}
                                                <td>
                                                    <div id="monday{{ $count  }}">✕</div>
                                                    <div id="monday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="monday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //tuesday  --}}
                                                <td>
                                                    <div id="tuesday{{ $count  }}">✕</div>
                                                    <div id="tuesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="tuesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //wednesday  --}}
                                                <td>
                                                    <div id="wednesday{{ $count  }}">✕</div>
                                                    <div id="wednesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="wednesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Thursday  --}}
                                                <td>
                                                    <div id="thursday{{ $count  }}">✕</div>
                                                    <div id="thursday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="thursday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Friday  --}}
                                                <td>
                                                    <div id="friday{{ $count  }}">✕</div>
                                                    <div id="friday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="friday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Saturday  --}}
                                                <td>
                                                    <div id="saturday{{ $count  }}">✕</div>
                                                    <div id="saturday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="saturday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Sunday  --}}
                                                <td>
                                                    <div id="sunday{{ $count  }}">✕</div>
                                                    <div id="sunday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="sunday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                            </tr>
                                            @endfor
                                        @endfor
                                </tbody>
                            </table>
                        </div>
                        <div id="timeslote20" style="display:none" class="tableFixHead">
                            <table class="table table-bordered  text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                <thead>
                                <tr class="bg-warning">
                                    <th>時間</th>
                                    <th><?php echo  "(月)" ?></th>
                                    <th><?php echo  "(火)" ?></th>
                                    <th><?php echo  "(水)" ?></th>
                                    <th><?php echo  "(木)" ?></th>
                                    <th><?php echo  "(金)" ?></th>
                                    <th><?php echo  "(土)" ?></th>
                                    <th><?php echo  "(日)" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                        <?php $timeend=0;$count=3000;?>
                                        @for($t=0;$t<25;$t++)
                                            @for($m=0;$m<60;$m+=20)
                                            <?php $count++;?>
                                            @if($timeend==1)
                                                <?php $timeend=0;?>
                                                @break
                                            @endif
                                            <tr>
                                                <td>
                                                    @if($t!=24)
                                                        @if(strlen($t)==1)
                                                        @if($m==0)
                                                        {{ '0'.$t.':'.$m.'0' }}
                                                        @else
                                                        {{ '0'.$t.':'.$m }}
                                                        @endif
                                                        @else
                                                        @if($m==0)
                                                        {{ $t.':'.$m.'0'  }}
                                                        @else
                                                        {{ $t.':'.$m  }}
                                                        @endif
                                                        @endif
                                                    @else
                                                        <?php $timeend++;?>
                                                        @if($timeend==2)
                                                            @break
                                                            @else
                                                            {{ $t.':00'  }}
                                                        @endif
                                                    @endif
                                                </td>
                                                {{--  //mondyay  --}}
                                                <td>
                                                    <div id="monday{{ $count  }}">✕</div>
                                                    <div id="monday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="monday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //tuesday  --}}
                                                <td>
                                                    <div id="tuesday{{ $count  }}">✕</div>
                                                    <div id="tuesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="tuesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //wednesday  --}}
                                                <td>
                                                    <div id="wednesday{{ $count  }}">✕</div>
                                                    <div id="wednesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="wednesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Thursday  --}}
                                                <td>
                                                    <div id="thursday{{ $count  }}">✕</div>
                                                    <div id="thursday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="thursday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Friday  --}}
                                                <td>
                                                    <div id="friday{{ $count  }}">✕</div>
                                                    <div id="friday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="friday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Saturday  --}}
                                                <td>
                                                    <div id="saturday{{ $count  }}">✕</div>
                                                    <div id="saturday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="saturday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Sunday  --}}
                                                <td>
                                                    <div id="sunday{{ $count  }}">✕</div>
                                                    <div id="sunday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="sunday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                            </tr>
                                            @endfor
                                        @endfor
                                </tbody>
                            </table>
                        </div>
                        <div id="timeslote30" style="display:none" class="tableFixHead">
                            <table class="table table-bordered  text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
                                <thead>
                                <tr class="bg-warning">
                                    <th>時間</th>
                                    <th><?php echo  "(月)" ?></th>
                                    <th><?php echo  "(火)" ?></th>
                                    <th><?php echo  "(水)" ?></th>
                                    <th><?php echo  "(木)" ?></th>
                                    <th><?php echo  "(金)" ?></th>
                                    <th><?php echo  "(土)" ?></th>
                                    <th><?php echo  "(日)" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                        <?php $timeend=0;$count=4000;?>
                                        @for($t=0;$t<25;$t++)
                                            @for($m=0;$m<60;$m+=30)
                                            <?php $count++;?>
                                            @if($timeend==1)
                                                <?php $timeend=0;?>
                                                @break
                                            @endif
                                            <tr>
                                                <td>
                                                    @if($t!=24)
                                                        @if(strlen($t)==1)
                                                        @if($m==0)
                                                        {{ '0'.$t.':'.$m.'0' }}
                                                        @else
                                                        {{ '0'.$t.':'.$m }}
                                                        @endif
                                                        @else
                                                        @if($m==0)
                                                        {{ $t.':'.$m.'0'  }}
                                                        @else
                                                        {{ $t.':'.$m  }}
                                                        @endif
                                                        @endif
                                                    @else
                                                        <?php $timeend++;?>
                                                        @if($timeend==2)
                                                            @break
                                                            @else
                                                            {{ $t.':00'  }}
                                                        @endif
                                                    @endif
                                                </td>
                                                {{--  //mondyay  --}}
                                                <td>
                                                    <div id="monday{{ $count  }}">✕</div>
                                                    <div id="monday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="monday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //tuesday  --}}
                                                <td>
                                                    <div id="tuesday{{ $count  }}">✕</div>
                                                    <div id="tuesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="tuesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //wednesday  --}}
                                                <td>
                                                    <div id="wednesday{{ $count  }}">✕</div>
                                                    <div id="wednesday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="wednesday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Thursday  --}}
                                                <td>
                                                    <div id="thursday{{ $count  }}">✕</div>
                                                    <div id="thursday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="thursday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Friday  --}}
                                                <td>
                                                    <div id="friday{{ $count  }}">✕</div>
                                                    <div id="friday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="friday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Saturday  --}}
                                                <td>
                                                    <div id="saturday{{ $count  }}">✕</div>
                                                    <div id="saturday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="saturday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                                {{--  //Sunday  --}}
                                                <td>
                                                    <div id="sunday{{ $count  }}">✕</div>
                                                    <div id="sunday_view{{ $count  }}" style="display:none">
                                                        <input type="checkbox" style="width:20px;height:20px" name="sunday_chk[]" value="{{ $t.':'.$m }}">
                                                    </div>
                                                </td>
                                            </tr>
                                            @endfor
                                        @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
     <div class="col-md-12" align="center">
        <div class="form-group">
            <button id="confirm" onclick="" type="submit" class="btn btn-primary btn-lg">保存</button>
            <!-- <a href="{{ URL::to('/app/clinic/setting/reserve_completed') }}" class="btn btn-primary btn-lg">追加ボタン</a> -->
        </div>
    </div>
</form>

</div>
        </div>
    </div>
    </div>
</div>
    </div>
@endsection
