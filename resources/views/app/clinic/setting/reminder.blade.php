@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定 - リマインダー設定</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="padding: 30px;">
                    <div class="card-title" style="/* padding: 20px; */">事前に診察情報のお知らせをいたします。 ご希望のものをお送りください。（複数可）
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{URL::to('/app/owner/setting/reminder')}}">
                            @csrf
                            <div class="form-check row">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="oneday">
                                    <span class="form-check-sign"> 1日前</span>
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="datetime">
                                    <span class="form-check-sign"> 当日</span>
                                    <input type="number" max="23" min="0" class="form-control" style="width: 70px;" name="datetime1"> 時
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="hour">
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="23" min="0" class="form-control" style="width: 70px;" name="hour1"> 時間前
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="minute" checked>
                                    <span class="form-check-sign"></span>
                                    <input type="number" class="form-control" max="59" min="0" style="width: 70px;" value="10" name="minute">分前
                                </label>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
