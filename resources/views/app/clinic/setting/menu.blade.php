@extends('layouts.app.clinic')

@section('content')
<div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">各種設定 - 予約設定</h3>
        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div id="inquiry" class="row mb-4 justify-content-center">
                    <div class="col-md-12">
                            <form id="frm_menu_add" name="frm_menu_add" method="POST" action="{{URL::to('/app/clinic/setting/menu_edit')}}">
                            @csrf
                                <div class="col-md-12 border-bottom border-danger mb-4">
                                    <h3 class="title">相談・診療メニューの追加</h3>
                                </div>
                                    <div class="col-md-10 ml-auto col-xl-10 mr-auto">
                                    <!-- Tabs with Background on Card -->
                                    <div class="card">
                                        <div class="card-header">
                                        {{-- <ul class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist" data-background-color="orange">
                                            <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#home1" role="tab" aria-selected="false">
                                            <i class="now-ui-icons  "></i>メニュー追加</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#settings1" role="tab" aria-selected="true">
                                            <i class="now-ui-icons "></i>メニューキャンセル</a>
                                            </li>
                                        </ul> --}}
                                        </div>
                                        <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active" id="home1" role="tabpanel">
                                            <p>
                                            <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">題目</label>
                                                        <div class="col-md-6">
                                                            <input id="first_title" name="first_title" type="text" value="" readdisable placeholder="" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">詳細内容</label>
                                                        <div class="col-md-6">
                                                            <textarea id="description" name="description"  class='form-control' required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                            <div class="col-md-4">

                                                            </div>
                                                            <label class="col-md-7">
                                                                ※メニューの内容、料金目安、注意事項などご記入ください。

                                                            </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">目安時間(分)</label>
                                                        <div class="col-md-6">
                                                            <input id="toke_time" name="toke_time" type="number" value="" readdisable placeholder="" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="prefecture" class="col-md-4 col-form-label text-md-right">予約料金（税込）</label>
                                                        <div class="col-md-6">
                                                            <input id="toke_price" name="toke_price" type="number" value="" readdisable placeholder="" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                            <div class="col-md-4">

                                                            </div>
                                                            <label class="col-md-7">
                                                                ※予約料金は予約が当日キャンセルされた場合に<br>
                                                                キャンセル料として飼い主様より支払われます。
                                                            </label>
                                                    </div>

                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="prefecture" class="col-md-4 col-form-label text-md-right"></label>
                                                        <div class="form-check form-check-inline">
                                                            <label class="form-check-label">
                                                            保険証適用 <input class="form-check-input" type="checkbox" name ="Insurance" value="1" data-label="電話" >
                                                                <span class="form-check-sign">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" align="center">
                                                    <div class="form-group">
                                                        <button id="confirm" onclick="" type="submit" class="btn btn-primary btn-lg">保存</button>
                                                        {{-- <a href="{{ URL::to('/app/clinic/setting/reserve_completed') }}" class="btn btn-primary btn-lg">追加ボタン</a> --}}
                                                        </div>
                                                    </div>
                                            </p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- End Tabs on plain Card -->
                                    </div>

                            </form>
                            <form id="frm_old_menu" name="frm_old_menu" method="POST" action="{{URL::to('/app/clinic/setting/reseve_edit')}}">
                            @csrf
                                <div class="col-md-12 border-bottom border-danger mb-4">
                                    <h3 class="title">既定メニュー</h3>
                                </div>
                                    <div class="row">
                                    <div class="col-md-10 ml-auto col-xl-10 mr-auto">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <div class="card-header">
                                        <ul class="nav nav-tabs justify-content-center" role="tablist">
                                            <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-selected="true">
                                                <i class="now-ui-icons business_badge"></i> メニュー-1
                                            </a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false">
                                                <i class="now-ui-icons business_badge"></i> メニュー-2
                                            </a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-selected="false">
                                                <i class="now-ui-icons business_badge"></i> メニュー-3
                                            </a>
                                            </li>
                                        </ul>
                                        </div>
                                        <?php
                                            if(count($old_clinic_menus)>0){
                                                $num=0;
                                                foreach($old_clinic_menus as $row)
                                                {
                                                    if($num==3)break;
                                                    $priceList = json_decode($row->price_list,true);
                                                    $n=0;
                                                    foreach($priceList as $item)
                                                    {
                                                        $title_list[$num][$n] = $item['title'];
                                                        $price_list[$num][$n] = $item['price'];
                                                        $n++;
                                                    }
                                                    if($row->is_insurance_applicable==1){
                                                        $insure[$num]=1;
                                                        $ho_insurance[$num] = "保険適用可能";
                                                    }else{
                                                        $insure[$num]=0;
                                                        $ho_insurance[$num] = "保険適用なし";
                                                    }
                                                    $ho_title[$num]=$row->name;
                                                    $ho_description[$num]=$row->description;
                                                    $ho_id[$num]=$row->id;
                                                    $num++;
                                                }
                                            }
                                            ?>
                                        <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-left">
                                            <div class="tab-pane active" id="home" role="tabpanel">
                                                    <p>
                                                    <?php
                                                    if(count($old_clinic_menus)>0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5 class="p-2 font-weight-bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                            {{$ho_title[0]}}
                                                            </font></font></h5>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="p-2"><font style="vertical-align: inherit;">
                                                            {{$ho_description[0]}}
                                                            </font></div>
                                                        </div>
                                                        <div class="col-12">
                                                        <div class="col-md-12">
                                                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></h5>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                            <tr class="bg-warning">
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目安時間</font></font></th>
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php if(count($price_list)>0){
                                                            for($i=0;$i<count($price_list[0]);$i++){
                                                            ?>
                                                                <tr style="background-color: antiquewhite;">
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                                        @if($title_list[0][$i]!='')
                                                                        {{ $title_list[0][$i] }}
                                                                        分 程度
                                                                        @endif
                                                                    </font></font></td>
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¥ {{$price_list[0][$i]}}</font></font></td>
                                                                </tr>
                                                            <?php }}?>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-3 text-center">
                                                    <?php if($insure[0]==1){ ?><h5><span class="badge badge-success">{{$ho_insurance[0]}}</span></h5>
                                                    <?php }else{?><h5><span class="badge badge-default">{{$ho_insurance[0]}}</span></h5><?php }?>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <a class="btn btn-warning" href="{{URL::to('/app/clinic/setting/menu_update/'.$ho_id[0])}}">変更</a>
                                                    <!-- <a class="btn btn-defalult" href="{{URL::to('/app/clinic/menu_delete/'.$ho_id[0]['id'])}}">削除</a> -->
                                                </div>
                                                </div>
                                                    <?php
                                                    }else{
                                                    echo ("相談メニューが登録されていません。");
                                                    }
                                                    ?>
                                                    </p>
                                            </div>
                                            <div class="tab-pane" id="profile" role="tabpanel">
                                            <p>
                                                    <?php
                                                    if(count($old_clinic_menus)>0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5 class="p-2 font-weight-bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                            @if(isset($ho_title[1]))
                                                            {{$ho_title[1]}}
                                                            @endif
                                                            </font></font></h5>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="p-2"><font style="vertical-align: inherit;">
                                                            @if(isset($ho_title[1]))
                                                            {{$ho_description[1]}}
                                                            @endif
                                                            </font></div>
                                                        </div>
                                                        <div class="col-12">
                                                        <div class="col-md-12">
                                                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></h5>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                            <tr class="bg-warning">
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目安時間</font></font></th>
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(isset($price_list[1]))
                                                            <?php if(count($price_list)>0){
                                                            for($i=0;$i<count($price_list[1]);$i++){
                                                            ?>
                                                                <tr style="background-color: antiquewhite;">
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$title_list[1][$i]}}分 程度</font></font></td>
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¥ {{$price_list[1][$i]}}</font></font></td>
                                                                </tr>
                                                            <?php }}?>
                                                            @endif
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-3 text-center">
                                                    @if(isset($ho_insurance[1]))
                                                    <?php if($insure[1]==1){ ?><h5><span class="badge badge-success">{{$ho_insurance[1]}}</span></h5>
                                                    <?php }else{?><h5><span class="badge badge-default">{{$ho_insurance[1]}}</span></h5><?php }?>
                                                    @endif
                                                </div>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    @if(isset($ho_id[1]))
                                                    <a class="btn btn-warning" href="{{URL::to('/app/clinic/setting/menu_update/'.$ho_id[1])}}">変更</a>
                                                    <!-- <a class="btn btn-defalult" href="{{URL::to('/app/clinic/menu_delete/'.$ho_id[0]['id'])}}">削除</a> -->
                                                    @endif
                                                </div>
                                                    <?php
                                                    }else{
                                                    echo ("相談メニューが登録されていません。");
                                                    }
                                                    ?>
                                                    </p>
                                            </div>
                                            <div class="tab-pane" id="messages" role="tabpanel">
                                            <p>
                                                    <?php
                                                    if(count($old_clinic_menus)>0){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5 class="p-2 font-weight-bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                            @if(isset($hos_title[2]))
                                                                {{$ho_title[2]}}
                                                            @endif
                                                            </font></font></h5>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="p-2"><font style="vertical-align: inherit;">
                                                            @if(isset($ho_description[2]))
                                                            {{$ho_description[2]}}
                                                            @endif
                                                            </font></div>
                                                        </div>
                                                        <div class="col-12">
                                                        <div class="col-md-12">
                                                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></h5>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                            <tr class="bg-warning">
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目安時間</font></font></th>
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(isset($price_list[2]))
                                                            <?php if(count($price_list)>0){

                                                            for($i=0;$i<count($price_list[2]);$i++){
                                                            ?>
                                                                <tr style="background-color: antiquewhite;">
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$title_list[2][$i]}}分 程度</font></font></td>
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¥ {{$price_list[2][$i]}}</font></font></td>
                                                                </tr>
                                                            <?php }}?>
                                                            @endif
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-3 text-center">
                                                    @if(isset($insure[2]))
                                                    <?php if($insure[2]==1){ ?><h5><span class="badge badge-success">{{$ho_insurance[2]}}</span></h5>
                                                    <?php }else{?><h5><span class="badge badge-default">{{$ho_insurance[2]}}</span></h5><?php }?>
                                                    @endif
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    @if(isset($ho_id[2]))
                                                    <a class="btn btn-warning" href="{{URL::to('/app/clinic/setting/menu_update/'.$ho_id[2])}}">変更</a>
                                                    <!-- <a class="btn btn-defalult" href="{{URL::to('/app/clinic/menu_delete/'.$ho_id[0]['id'])}}">削除</a> -->
                                                    @endif
                                                </div>
                                                </div>
                                                    <?php
                                                    }else{
                                                    echo ("相談メニューが登録されていません。");
                                                    }
                                                    ?>
                                                    </p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                   </div>
                                </div></div>
                            </form>
                            </div>
                            <div class="col-md-12 border-bottom border-danger mb-4">
                                    <h3 class="title">追加メニュー</h3>
                                </div>
                                <form id="frm_menu_add" name = "frm_new_menu" method="POST" action="{{URL::to('/app/clinic/setting/reseve_edit')}}">
                            @csrf
                            <div class="col-md-10 ml-auto col-xl-10 mr-auto">
                                <div class="card">
                                 <?php if(!empty($clinic_menus)>0){?>
                                    <?php
                                        foreach($clinic_menus as $row1){
                                    ?>





                                    <div class="card-header">
                                        <ul class="nav nav-tabs justify-content-center" role="tablist">
                                            <li class="nav-item">

                                            </li>
                                        </ul>
                                        </div>
                                        <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home1" role="tabpanel">
                                            <p>
                                                    <?php

                                                        $new_priceList = json_decode($row1->price_list,true);
                                                        $n=0;
                                                        foreach($new_priceList as $item)
                                                        {
                                                            $new_title_list[$n] = $item['title'];
                                                            $new_price_list[$n] = $item['price'];
                                                            $n++;
                                                        }
                                                        if($row1->is_insurance_applicable==1){
                                                            $new_insure=1;
                                                            $new_insurance = "保険適用可能";
                                                        }else{
                                                            $new_insure=0;
                                                            $new_insurance = "保険適用なし";
                                                        }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h5 class="p-2 font-weight-bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                            {{$row1->name}}
                                                            </font></font></h5>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="p-2"><font style="vertical-align: inherit;">
                                                            {{$row1->description}}
                                                            </font></div>
                                                        </div>
                                                        <div class="col-12">
                                                        <div class="col-md-12">
                                                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></h5>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                            <tr class="bg-warning">
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目安時間</font></font></th>
                                                                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">予約料（税込)</font></font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php if(count($new_price_list)>0){
                                                            for($i=0;$i<count($new_price_list);$i++){
                                                            ?>
                                                                <tr style="background-color: antiquewhite;">
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                                        @if($new_title_list[$i]!='')
                                                                        {{ $new_title_list[$i] }}
                                                                        分 程度
                                                                        @endif
                                                                    </font></font></td>
                                                                    <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¥ {{$new_price_list[$i]}}</font></font></td>
                                                                </tr>
                                                            <?php }}?>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-3 text-center">
                                                    <?php if($new_insure==1){ ?><h5><span class="badge badge-success">{{$new_insurance}}</span></h5>
                                                    <?php }else{?><h5><span class="badge badge-default">{{$new_insurance}}</span></h5><?php }?>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <a class="btn btn-warning"  href=<?php echo "/app/clinic/setting/menu_update/".$row1->id ?>>変更</a>
                                                    <a class="btn btn-defalult"  href=<?php echo "/app/clinic/setting/reseve_delete/".$row1->id ?>>削除</a>
                                                </div>
                                                </div>

                                            </p>
                                            </div>

                                        </div>
                                        </div>


                                    <?php
                                                    }
                                                    ?>
                                                     <?php }?>
                                    </div>
                            </form>
                                    <!-- End Tabs on plain Card -->
                                    </div>

                            </div>

                            </div>

                                    <div class="row col-md-12">
                                    <p></p><p><br><br><br>
                                    <div>



                                            </div>
                                        </div>
                                    </div>
                            </div>
    </div>
</div>
@endsection
