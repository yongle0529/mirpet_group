@extends('layouts.app.clinic')

@section('content')
<style>
        .tableFixHead { overflow-y: auto; height: 600px; }
        .tableFixHead thead th { position: sticky; top: 0; }

        /* Just common table stuff. Really. */
        table  { border-collapse: collapse; width: 100%; }
        th, td { padding: 8px 16px; }
        th     { background:#eee; }
    </style>
    <script>
        $(function () {
            $("#code_flag").on('onchange', function () {
                $("#hospital_code").disabled = $("#code_flag").checked;
            });
        });
    </script>

<div class="container">
<h3 class="font-weight-bold mb-4 border-bottom border-dark">各種設定 - 予約設定</h3>
<div class="row">
<div class="col-md-12">
<div class="card">
<div class="card-body">
<div id="inquiry" class="row mb-4 justify-content-center">
<div class="col-md-12 border-bottom border-danger mb-4">
<h3 class="title">
        <a href="{{ URL::to('/app/clinic/setting/reserve_edit') }}"  class="btn btn-danger">予約変更</a>
</h3>
</div>
@php($weekOffset = app('request')->input('weekOffset',0))
<?php

    foreach($query as $clinic);
    $clinicInfo = json_decode($clinic['clinic_info'],true);
    $timeslote = json_decode($clinic['menu_time_option'],true);
?>




<div class="col-md-12 tableFixHead">

    @if(!empty($clinicMenu))
        <div class="row">
            <div class="col-6 text-left">
                @if($weekOffset > 0)
                    <a href="{{Request::url()}}?weekOffset={{$weekOffset - 1}}" class="btn btn-warning text-black">＜ 前の一週間</a>
                @endif
            </div>
            <div class="col-6 text-right">
                <a href="{{Request::url()}}?weekOffset={{$weekOffset + 1}}" class="btn btn-warning text-black">次の一週間 ＞</a>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
                <?php
                $clinicInfo = $clinic->clinic_info;
                $clinicMenuOption = $clinicMenu->option;
                if (isset($clinicMenuOption)) {
                    $clinicMenuOption = json_decode($clinicMenuOption, true);
                }
                $hourArray = [];

                if (isset($clinicInfo)) {
                    $decodedClinicInfo = json_decode($clinicInfo, true);

                    if (isset($decodedClinicInfo['businessHour'])) {
                        foreach ($decodedClinicInfo['businessHour'] as $weekday => $hours) {
                            if (isset($hours)) {
                                foreach ($hours as $hour) {
                                    $hourArray[] = \Carbon\Carbon::parse($hour['start'])->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($hour['end'])->format('H:i');
                                }
                            }
                        }
                        $minHour = min($hourArray);
                        $maxHour = max($hourArray);
                        $hourArray = [];
                        for ($i = \Carbon\Carbon::parse($minHour)->hour; $i <= \Carbon\Carbon::parse($maxHour)->hour; $i++) {

                            switch ($clinicMenuOption['timeSlotType']) {
                                case 0:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':10')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':50')->format('H:i');
                                    break;
                                case 1:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':15')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':45')->format('H:i');
                                    break;
                                case 2:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                    break;
                                case 3:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    break;
                                case 4:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    break;
                            }
                        }
                    } else {
                        \Illuminate\Support\Facades\Log::error('not set businessHour');
                    }
                } else {
                    \Illuminate\Support\Facades\Log::error('not set clinic_info');
                }
                ?>

                <?php
                setlocale(LC_ALL, 'ja_JP.UTF-8');
                $offsetDate = \Carbon\Carbon::now();
                if ($weekOffset > 0) {
                    $addDays = $weekOffset * 7;
                    $offsetDate = $offsetDate->addDays($addDays);
                }

                $monthList = [];
                $dateList = [];
                $searchDateList = [];
                for ($i = 0; $i < 7; $i++) {
                    if ($i > 0) {
                        $offsetDate = $offsetDate->addDay();
                    }
                    if (isset($monthList[$offsetDate->format('Y年m月')])) {
                        $monthList[$offsetDate->format('Y年m月')] = $monthList[$offsetDate->format('Y年m月')] + 1;
                    } else {
                        $monthList[$offsetDate->format('Y年m月')] = 1;
                    }

                    $dateList[] = [
                        'day' => $offsetDate->day,
                        'dayOfWeek' => $offsetDate->formatLocalized('(%a)'),
                        'dayOfWeekNum' => $offsetDate->dayOfWeek
                    ];
                    $searchDateList[] = $offsetDate->format('Y-m-d');
                }

                $reserveTimetables = \App\ReserveTimetable::where('clinic_id', $clinic->id)
                    ->whereBetween('date', [min($searchDateList), max($searchDateList)])
                    ->get();
                ?>
                <tr class="text-center">
                    <th rowspan="2" class="align-middle">日時</th>

                    @foreach($monthList as $month => $col)
                        <th colspan="{{$col}}">{{$month}}</th>
                    @endforeach
                </tr>
                <tr class="text-center small">
                    @foreach($dateList as $day)
                        <th class="{{$day['dayOfWeekNum'] == 0 ? 'table-danger':''}}{{$day['dayOfWeekNum'] == 6 ? 'table-info':''}}">
                            <span class="d-block">{{$day['day']}}</span>
                            {{$day['dayOfWeek']}}
                        </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
            <?php
            if ($clinicMenuOption['timeSlotType'] == 0) {
                $timeSlotList = config('const.TIME_SLOT')['10_MIN'];
            }elseif ($clinicMenuOption['timeSlotType'] == 1) {
                $timeSlotList = config('const.TIME_SLOT')['15_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 2) {
                $timeSlotList = config('const.TIME_SLOT')['20_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 3) {
                $timeSlotList = config('const.TIME_SLOT')['30_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 4) {
                $timeSlotList = config('const.TIME_SLOT')['60_MIN'];
            }
            ?>

            @foreach($hourArray as $hour)
                <tr class="text-center">
                    <td>{{$hour}}</td>
                    @php($targetTimeSlot = array_search($hour, $timeSlotList))
                    @foreach($searchDateList as $i => $date)
                        <?php
                            $isMatch = 0;
                            if(array_key_exists($dateList[$i]['dayOfWeekNum'], $decodedClinicInfo['businessHour'])) {
                                $openHours = $decodedClinicInfo['businessHour'][$dateList[$i]['dayOfWeekNum']];
                                foreach((array)$openHours as $openHour) {
                                    if(\Carbon\Carbon::parse($openHour['start'])->format('H:i') <= $hour && $hour < \Carbon\Carbon::parse($openHour['end'])->format('H:i')) {
                                        $isMatch = 1;
                                        foreach($reserveTimetables as $timetable) {
                                            if($date == $timetable->date && $targetTimeSlot == $timetable->time_slot) {
                                                $isMatch = 2;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        ?>
                        @if($isMatch == 1)
                            <td><a href="#">◯</a></td>
                        @elseif($isMatch == 2)
                            <td>{{--<a href="{{URL::to('/app/owner/clinic/reserve/register/'.$clinic->id.'/'.$clinicMenu->id.'?date='.\Carbon\Carbon::parse($date)->format('Y-m-d').'&slot='.$targetTimeSlot.'&time='.$hour)}}" class="text-success font-weight-bold">--}}<b>済</b>{{--</a>--}}</td>
                        @else
                            <td>✕</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>
    @else
    <div class="row">
            <div class="col-6 text-left">
                @if($weekOffset > 0)
                    <a href="{{Request::url()}}?weekOffset={{$weekOffset - 1}}" class="btn btn-warning text-black">＜ 前の一週間</a>
                @endif
            </div>
            <div class="col-6 text-right">
                <a href="{{Request::url()}}?weekOffset={{$weekOffset + 1}}" class="btn btn-warning text-black">次の一週間 ＞</a>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
                <?php
                $clinicInfo = $clinic->clinic_info;
                $clinicMenuOption = $old_clinic_menus[0]->option;
                if (isset($clinicMenuOption)) {
                    $clinicMenuOption = json_decode($clinicMenuOption, true);
                }
                $hourArray = [];

                if (isset($clinicInfo)) {
                    $decodedClinicInfo = json_decode($clinicInfo, true);

                    if (isset($decodedClinicInfo['businessHour'])) {
                        foreach ($decodedClinicInfo['businessHour'] as $weekday => $hours) {
                            if (isset($hours)) {
                                foreach ($hours as $hour) {
                                    $hourArray[] = \Carbon\Carbon::parse($hour['start'])->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($hour['end'])->format('H:i');
                                }
                            }
                        }
                        $minHour = min($hourArray);
                        $maxHour = max($hourArray);
                        $hourArray = [];
                        for ($i = \Carbon\Carbon::parse($minHour)->hour; $i <= \Carbon\Carbon::parse($maxHour)->hour; $i++) {

                            switch ($clinicMenuOption['timeSlotType']) {
                                case 0:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':10')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':50')->format('H:i');
                                    break;
                                case 1:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':15')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':45')->format('H:i');
                                    break;
                                case 2:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                    break;
                                case 3:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                    break;
                                case 4:
                                    $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                    break;
                            }
                        }
                    } else {
                        \Illuminate\Support\Facades\Log::error('not set businessHour');
                    }
                } else {
                    \Illuminate\Support\Facades\Log::error('not set clinic_info');
                }
                ?>

                <?php
                setlocale(LC_ALL, 'ja_JP.UTF-8');
                $offsetDate = \Carbon\Carbon::now();
                if ($weekOffset > 0) {
                    $addDays = $weekOffset * 7;
                    $offsetDate = $offsetDate->addDays($addDays);
                }

                $monthList = [];
                $dateList = [];
                $searchDateList = [];
                for ($i = 0; $i < 7; $i++) {
                    if ($i > 0) {
                        $offsetDate = $offsetDate->addDay();
                    }
                    if (isset($monthList[$offsetDate->format('Y年m月')])) {
                        $monthList[$offsetDate->format('Y年m月')] = $monthList[$offsetDate->format('Y年m月')] + 1;
                    } else {
                        $monthList[$offsetDate->format('Y年m月')] = 1;
                    }

                    $dateList[] = [
                        'day' => $offsetDate->day,
                        'dayOfWeek' => $offsetDate->formatLocalized('(%a)'),
                        'dayOfWeekNum' => $offsetDate->dayOfWeek
                    ];
                    $searchDateList[] = $offsetDate->format('Y-m-d');
                }

                $reserveTimetables = \App\ReserveTimetable::where('clinic_id', $clinic->id)
                    ->whereBetween('date', [min($searchDateList), max($searchDateList)])
                    ->get();
                ?>
                <tr class="text-center">
                    <th rowspan="2" class="align-middle">日時</th>

                    @foreach($monthList as $month => $col)
                        <th colspan="{{$col}}">{{$month}}</th>
                    @endforeach
                </tr>
                <tr class="text-center small">
                    @foreach($dateList as $day)
                        <th class="{{$day['dayOfWeekNum'] == 0 ? 'table-danger':''}}{{$day['dayOfWeekNum'] == 6 ? 'table-info':''}}">
                            <span class="d-block">{{$day['day']}}</span>
                            {{$day['dayOfWeek']}}
                        </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
            <?php
            if ($clinicMenuOption['timeSlotType'] == 0) {
                $timeSlotList = config('const.TIME_SLOT')['10_MIN'];
            }elseif ($clinicMenuOption['timeSlotType'] == 1) {
                $timeSlotList = config('const.TIME_SLOT')['15_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 2) {
                $timeSlotList = config('const.TIME_SLOT')['20_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 3) {
                $timeSlotList = config('const.TIME_SLOT')['30_MIN'];
            } else if ($clinicMenuOption['timeSlotType'] == 4) {
                $timeSlotList = config('const.TIME_SLOT')['60_MIN'];
            }
            ?>

            @foreach($hourArray as $hour)
                <tr class="text-center">
                    <td>{{$hour}}</td>
                    @php($targetTimeSlot = array_search($hour, $timeSlotList))
                    @foreach($searchDateList as $i => $date)
                        <?php
                            $isMatch = 0;
                            if(array_key_exists($dateList[$i]['dayOfWeekNum'], $decodedClinicInfo['businessHour'])) {
                                $openHours = $decodedClinicInfo['businessHour'][$dateList[$i]['dayOfWeekNum']];
                                foreach((array)$openHours as $openHour) {
                                    if(\Carbon\Carbon::parse($openHour['start'])->format('H:i') <= $hour && $hour < \Carbon\Carbon::parse($openHour['end'])->format('H:i')) {
                                        $isMatch = 1;
                                        foreach($reserveTimetables as $timetable) {
                                            if($date == $timetable->date && $targetTimeSlot == $timetable->time_slot) {
                                                $isMatch = 2;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        ?>
                        @if($isMatch == 1)
                            <td><a href="#">◯</a></td>
                        @elseif($isMatch == 2)
                            <td>{{--<a href="{{URL::to('/app/owner/clinic/reserve/register/'.$clinic->id.'/'.$clinicMenu->id.'?date='.\Carbon\Carbon::parse($date)->format('Y-m-d').'&slot='.$targetTimeSlot.'&time='.$hour)}}" class="text-success font-weight-bold">--}}<b>済</b>{{--</a>--}}</td>
                        @else
                            <td>✕</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>
    @endif
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
