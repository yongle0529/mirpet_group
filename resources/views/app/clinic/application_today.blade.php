@extends('layouts.app.clinic')

@section('content')
<div class="container">
    <h3 class="font-weight-bold mb-4">受付状況</h3>

    <div class="row">
        <style>
            .sub-menu-btn {
                min-width: 10vw;
                font-size: 1rem;
            }
        </style>
        <script>

            $(function () {
                var current = "{{$mode}}";
                $('.sub-menu-btn').each(function (index, element) {
                    var rel = $(this).attr('rel');
                    if (current == rel) {
                        $(element).addClass('btn-info');
                        $(element).removeClass('btn-default');
                    }

                })

                $('.sub-menu-btn').click(function () {
                    var mode = $(this).attr('rel');
                    if (current != mode) {
                        location.href = "{{URL::to('/app/clinic')}}/application?mode=" + mode;
                    }
                });
            });
        </script>
        <button class="sub-menu-btn btn btn-default" rel="exam">診察待ち({{$statusCount['exam']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="bill">会計待ち({{$statusCount['bill']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="shipping">発送待ち({{$statusCount['shipping']}}件)</button>
        <button class="sub-menu-btn btn btn-default" rel="remind">再診リマインド({{$statusCount['remind']}}件)</button>
    </div>
    <?php if($mode == "exam"){ ?>
    <div class="row">
        <div><a class="btn btn-warning" href="{{URL::to('/app/clinic')}}/application">すべての予約表示</a></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物種</th>
                    <th>種類</th>
                    <th>予約時間</th>
                    <th>目的</th>
                    <th>担当医</th>
                    <th>アクション</th>
                </tr>
                </thead>
                <tbody>
                @foreach($diags as $item)
                    <tr>
                        <td>
                            <?php

                            $ownercatano = \App\OwnerCatano::where('user_id', $item->user_id)->where('clinic_id', $item->clinic_id)->first();
                            if(!empty($ownercatano))echo $ownercatano->cata_no;
                            //else echo $item['user_id'];

                            ?>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/ownerinfo/'.$item['user_id'].'/'.$item['id'])}}">
                                {{\App\User::find($item['user_id'])->last_name . " " . \App\User::find($item['user_id'])->first_name}}
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/petinfo/'.$item['pet_id'].'/'.$item['id'])}}">
                                {{\App\Pet::find($item['pet_id'])->name}}
                            </a>
                        </td>
                        <td>
                            @if(\App\Pet::find($item['pet_id'])->type == 1)
                            イヌ
                            @elseif(\App\Pet::find($item['pet_id'])->type == 2)
                            ネコ
                            @elseif(\App\Pet::find($item['pet_id'])->type == 4)
                            ハムスター
                            @elseif(\App\Pet::find($item['pet_id'])->type == 5)
                            フェレット
                            @elseif(\App\Pet::find($item['pet_id'])->type == 6)
                            爬虫類
                            @elseif(\App\Pet::find($item['pet_id'])->type == 7)
                            ウサギ
                            @elseif(\App\Pet::find($item['pet_id'])->type == 8)
                            鳥
                            @else
                            その他
                            @endif
                        </td>
                        <td>{{\App\Pet::find($item['pet_id'])->genre}}</td>
                        <td>{{ substr($item['reserve_datetime'],0,16) }}</td>
                        <td><a href="{{URL::to('/app/clinic/purpose/'.$item['id'])}}">詳細</a></td>
                        <td>{{ $item['clinic_name'] }}</td>
                        <td class="text-center text-white">
                            {{-- <form action="{{ route('application.delete', $item['id']) }}" class="reserve_delete" method="post">
                                @csrf
                                @method('DELETE') --}}
                                <a class="btn btn-sm btn-primary" href="{{route('app.clinic.chat', $item['id'])}}">診察する</a>
                                {{-- @if(substr($item->reserve_datetime,0,10)==date("Y")."-".date("m"."-".date("d"))) --}}
                                <a class="btn btn-sm btn-warning" href="{{URL::to('/app/clinic')}}/bill_send/{{ $item['id'] }}">キャンセル（課金有り）</a>
                                {{-- @endif --}}
                                <a class="btn btn-sm btn-default" href="{{URL::to('/app/clinic')}}/delete_diag/{{ $item['id'] }}">取り消し</a>

                                {{--<a class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal"><button class="btn btn-sm btn-default" type="submit">取り消し</button></a>--}}
                            {{-- </form> --}}
                        </td>
                    </tr>
                @endforeach
                @if(count($diags) == 0)
                    <tr>
                        <td colspan="9">対象のデータがありません</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <?php }else if($mode == "bill"){ ?>
        <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物種</th>
                    <th>種類</th>
                    <th>を診察完了時間</th>
                    <th>アクション</th>
                </tr>
                </thead>
                <tbody>
                @foreach($finishedDiags as $item)
                @if($item->status==1 or $item->status==5)
                <?php $accounting = \App\Accounting::where('diagnosis_id', $item->id)->first(); ?>

                    @if(((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting))
                    <tr>
                        <td>
                                <?php

                                $ownercatano = \App\OwnerCatano::where('diag_id', $item->id)->where('clinic_id', $item->clinic_id)->first();
                                if(!empty($ownercatano))echo $ownercatano->cata_no;
                               // else echo $item['id'];

                                ?>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/ownerinfo/'.$item['user_id'].'/'.$item['id'])}}">
                                {{\App\User::find($item['user_id'])->last_name . " " . \App\User::find($item['user_id'])->first_name}}
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/petinfo/'.$item['pet_id'].'/'.$item['id'])}}">
                                {{\App\Pet::find($item['pet_id'])->name}}
                            </a>
                        </td>
                        <td>
                            @if(\App\Pet::find($item['pet_id'])->type == 1)
                            犬
                            @elseif(\App\Pet::find($item['pet_id'])->type == 2)
                            猫
                            @else
                            その他
                            @endif
                        </td>
                        <td>{{\App\Pet::find($item['pet_id'])->genre}}</td>
                        <td align="center">
                                @if($item['status']==5)
                                キャンセル
                                @else
                                {{ substr($item['end_datetime'],0,16) }}
                                @endif
                        </td>
                        <?php $accounting = \App\Accounting::where('diagnosis_id', $item->id)->first(); ?>
                        <td class="text-center text-white">
                            @if(!empty($accounting))
                                <a class="btn btn-sm btn-warning" href="{{URL::to('/app/clinic/accounting_update/'.$item['id'].'/'.$item['pet_id'].'/'.$accounting['id'])}}">会計変更</a>
                                {{-- @if($accounting->status == 0 || $accounting->status == 1)
                                <a class="btn btn-sm btn-warning" href="{{URL::to('/app/clinic/accounting_update/'.$item['id'].'/'.$item['pet_id'].'/'.$accounting['id'])}}">会計変更</a>
                                @else
                                <a class="btn btn-sm btn-warning" href="{{URL::to('/app/clinic/accounting_completed/'.$item['id'].'/'.$item['pet_id'])}}">会計入力</a>
                                @endif --}}
                            @else
                                <a class="btn btn-sm btn-warning" href="{{URL::to('/app/clinic/accounting_completed/'.$item['id'].'/'.$item['pet_id'])}}">会計入力</a>
                            @endif

                            @if(!empty($accounting))
                                @if($accounting->status == 0 || $accounting->status == 1)
                                <a href="{{URL::to('/app/clinic/send_accounting_mail/'.$item->id.'/'.$accounting->id)}}"><button class="btn btn-sm btn-primary">送信</button></a>
                                @else
                                <button class="btn btn-sm btn-primary" disabled>送信完了</button>
                                @endif
                            @else
                            <button class="btn btn-sm btn-primary" disabled>送信</button>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endif
                @endforeach

                @if(count($finishedDiags) == 0)
                    <tr>
                        <td colspan="7">対象のデータがありません</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <?php }else if($mode == "shipping"){ ?>
        <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物種</th>
                    <th>種類</th>
                    <th>診察完了時間</th>
                    <th>アクション</th>
                </tr>
                </thead>
                <tbody>
                @foreach($delayedDiags as $item)
                    @php($accounting = \App\Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first())
                    @if(!empty($accounting) && $accounting->delivery_cost != 0 && $accounting->status == 2)
                    <tr>
                        <td>
                                <?php

                                $ownercatano = \App\OwnerCatano::where('diag_id', $item->id)->where('clinic_id', $item->clinic_id)->first();
                                if(!empty($ownercatano))echo $ownercatano->cata_no;
                               // else echo $item['id'];

                                ?>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/ownerinfo/'.$item['user_id'].'/'.$item['id'])}}">
                                {{\App\User::find($item['user_id'])->last_name . " " . \App\User::find($item['user_id'])->first_name}}
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::to('/app/clinic/petinfo/'.$item['pet_id'].'/'.$item['id'])}}">
                                {{\App\Pet::find($item['pet_id'])->name}}
                            </a>
                        </td>
                        <td>
                            @if(\App\Pet::find($item['pet_id'])->type == 1)
                            犬
                            @elseif(\App\Pet::find($item['pet_id'])->type == 2)
                            猫
                            @else
                            その他
                            @endif
                        </td>
                        <td>{{\App\Pet::find($item['pet_id'])->genre}}</td>
                        <td>{{ substr($item['end_datetime'],0,16) }}</td>
                        <td class="text-center text-white">
                            <a class="btn btn-sm btn-primary" href="{{URL::to('/app/clinic/parcel_completed/'.$item->id)}}">発送</a>
                            <a class="btn btn-sm btn-warning" {{--data-toggle="modal" data-target="#exampleModal"--}}>発送住所印刷</a>
                        </td>
                    </tr>
                    @endif
                @endforeach
                {{-- @if(count($diags) == 0)
                    <tr>
                        <td colspan="7">対象のデータがありません</td>
                    </tr>
                @endif --}}
                </tbody>
            </table>
        </div>
    </div>
    <?php }else if($mode == "remind"){ ?>
        <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr class="bg-warning">
                    <th>カルテNo</th>
                    <th>飼い主様名</th>
                    <th>患者名</th>
                    <th>動物種</th>
                    <th>再診予定日</th>
                    <th>アクション</th>
                </tr>
                </thead>
                <tbody>
                    @php($d = strtotime("+1 day"))
                    @php($curr_datetime = date("Y-m-d", $d))
                    @foreach($nextDiags as $item)
                    @if($item->status==0 or $item->status==1 or $item->status==3 or $item->status==5)
                        @php($accounting = \App\Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first())
                        @if(!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime)
                        <tr>
                            <td>
                                    <?php

                                    $ownercatano = \App\OwnerCatano::where('user_id', $item->user_id)->where('clinic_id', $item->clinic_id)->first();
                                    if(!empty($ownercatano))echo $ownercatano->cata_no;
                                    //else echo $item['user_id'];

                                    ?>
                            </td>
                            <td>
                                <a href="{{URL::to('/app/clinic/ownerinfo/'.$item['user_id'].'/'.$item['id'])}}">
                                    {{\App\User::find($item['user_id'])->last_name . " " . \App\User::find($item['user_id'])->first_name}}
                                </a>
                            </td>
                            <td>
                                <a href="{{URL::to('/app/clinic/petinfo/'.$item['pet_id'].'/'.$item['id'])}}">
                                    {{\App\Pet::find($item['pet_id'])->name}}
                                </a>
                            </td>
                            <td>
                                @if(\App\Pet::find($item['pet_id'])->type == 1)
                                犬
                                @elseif(\App\Pet::find($item['pet_id'])->type == 2)
                                猫
                                @else
                                その他
                                @endif
                            </td>
                            <td>{{$accounting['next_remind_date']}}</td>
                            <td class="text-center text-white">
                                {{-- <form action="{{ route('application.delete', $item['id']) }}" class="reserve_delete" method="post">
                                    @csrf
                                    @method('DELETE') --}}
                                    <a class="btn btn-sm btn-primary" href="{{URL::to('/app/clinic/remind_completed/'.$item->id)}}">リマインド</a>
                                    <a class="btn btn-sm btn-default" href="{{URL::to('/app/clinic/remind_updated/'.$accounting->id)}}">取り消し</a>
                                {{-- </form> --}}
                                {{-- <a class="btn btn-sm btn-primary" href="{{URL::to('/app/clinic/remind_completed')}}">リマインド</a>
                                <a class="btn btn-sm btn-default" data-toggle="modal" data-target="#exampleModal">取り消し</a> --}}
                            </td>
                        </tr>
                        @endif
                    @endif
                    @endforeach
                    @if(count($nextDiags) == 0)
                        <tr>
                            <td colspan="6">対象のデータがありません</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <?php } ?>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">予約を取り消しますか？</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    操作は取り消すことはできません。実行しますか？
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">実行</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
