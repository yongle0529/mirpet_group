@extends('layouts.app.clinic')

@section('content')
<!DOCTYPE html>
<!-- saved from url=(0031)#####clinics.medley.life/faq -->
<html class=" wf-kozuka-gothic-pro-n3-active wf-kozuka-gothic-pro-n5-active wf-kozuka-gothic-pro-n4-active wf-ryo-gothic-plusn-n4-active wf-ryo-gothic-plusn-n2-active wf-active"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width">
<link rel="prefetch" href="{{URL::to('/')}}/assets/faq/runtime_www-3f4bac85795a1e964d3e.js.download" as="script"><link rel="prefetch" href="{{URL::to('/')}}/assets/faq/3-f38ce0058429a4b4d873.js.download" as="script"><link rel="prefetch" href="{{URL::to('/')}}/assets/faq/vendors_ariza_www-1deed95146589bd72f67.js.download" as="script"><link rel="prefetch" href="{{URL::to('/')}}/assets/faq/www-290f06037a8f18cfb238.js.download" as="script"><link rel="prefetch" href="{{URL::to('/')}}/assets/faq/vendors_ariza_www-33ff9f0bf161ff7b308b.css" as="style" media="all"><link rel="prefetch" href="{{URL::to('/')}}/assets/faq/www-10e07543c41d12d0b1e6.css" as="style" media="all">
<link rel="preload" href="#####d18fktiuhhpgn2.cloudfront.net/assets/runtime~www_clinic_search-a1a20a3fda54b0b2304b.js" as="script"><link rel="preload" href="{{URL::to('/')}}/assets/faq/3-f38ce0058429a4b4d873.js.download" as="script"><link rel="preload" href="#####d18fktiuhhpgn2.cloudfront.net/assets/4-52e4006029f54bfbef4e.js" as="script"><link rel="preload" href="#####d18fktiuhhpgn2.cloudfront.net/assets/www_clinic_search-0fc8ac94e821e0f0a669.js" as="script"><link rel="preload" href="#####d18fktiuhhpgn2.cloudfront.net/assets/www_clinic_search-cc209bca9bf1878bbfd2.css" as="style" media="all">
<link as="script" href="{{URL::to('/')}}/assets/faq/gtm.js.download" rel="preload">
<script type="text/javascript" src="{{URL::to('/')}}/assets/faq/pd.js.download"></script><script async="" src="{{URL::to('/')}}/assets/faq/saved_resource"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/faq/f.txt"></script><script type="text/javascript" async="" id="fjssync" src="{{URL::to('/')}}/assets/faq/mieruca-hm.js.download"></script><script src="{{URL::to('/')}}/assets/faq/442759219474091" async=""></script><script src="{{URL::to('/')}}/assets/faq/sdk.js.download" async="" crossorigin="anonymous"></script><script async="" src="{{URL::to('/')}}/assets/faq/fbevents.js.download"></script><script src="{{URL::to('/')}}/assets/faq/wjz8plk.js.download" async=""></script><script async="" src="{{URL::to('/')}}/assets/faq/branch-latest.min.js.download"></script><script async="" src="{{URL::to('/')}}/assets/faq/analytics.js.download"></script><script id="facebook-jssdk" src="{{URL::to('/')}}/assets/faq/sdk.js(1).download"></script><script async="" src="{{URL::to('/')}}/assets/faq/gtm.js.download"></script><script>
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '#####www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-53FKFW');
</script>

<link as="script" href="{{URL::to('/')}}/assets/faq/analytics.js.download" rel="prefetch">

<link as="script" href="{{URL::to('/')}}/assets/faq/branch-latest.min.js.download" rel="prefetch">

<link as="script" href="{{URL::to('/')}}/assets/faq/wjz8plk.js.download" rel="prefetch">

<link as="script" href="{{URL::to('/')}}/assets/faq/sdk.js(1).download" rel="preload">
<script>
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8&appId=674246976088868";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<link as="script" href="{{URL::to('/')}}/assets/faq/fbevents.js.download" rel="prefetch">



    <!--[if lte IE 8]>
  <link rel="stylesheet" href="#####cdnjs.cloudflare.com/ajax/libs/pure/0.6.0/grids-responsive-old-ie-min.css">
<![endif]-->
<!--[if lte IE 8]>
  <script src="#####oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<![endif]-->

    <link rel="manifest" href="#####clinics.medley.life/manifest.json">

    <script>
//<![CDATA[

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

//]]>
</script>
  <script>
//<![CDATA[

    ga('create', 'UA-72842088-1', {'allowLinker':true});
    ga('require', 'linker');
    ga('linker:autoLink', ['shinroppongi-clinic.com', 'clinics.medley.life']);
    ga('send', 'pageview');

//]]>
</script>

  <script>
//<![CDATA[

    if (window.location.search !== '?app=1') {
      (function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="#####cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode banner closeBanner creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode".split(" "), 0);
      branch.init('key_live_lop3Y8JUQE6F9MsR3uIoFoforBbAkP6p');
    }

//]]>
</script>
    <script>
//<![CDATA[

  (function(d) {var config={kitId:'wjz8plk',scriptTimeout:3000,async:true},h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='#####use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)})(document);

//]]>
</script>
      <script>
//<![CDATA[

    !function(f,b,e,v,n,t,s) {if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window,document,'script', '//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '442759219474091');
    fbq('track', 'PageView');

//]]>
</script>
    <script id="mierucajs">
//<![CDATA[

  window.__fid = window.__fid || [];__fid.push([347468794]);
  (function() {
    function mieruca(){if(typeof window.__fjsld != "undefined") return; window.__fjsld = 1; var fjs = document.createElement('script'); fjs.type = 'text/javascript'; fjs.async = true; fjs.id = "fjssync"; var timestamp = new Date;fjs.src = ('#####' == document.location.protocol ? 'https' : 'http') + '://hm.mieru-ca.com/service/js/mieruca-hm.js?v='+ timestamp.getTime(); var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(fjs, x); };
    setTimeout(mieruca, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent("onload", mieruca) : window.addEventListener("load", mieruca, false)) : mieruca();
  })();

//]]>
</script>
      <link rel="stylesheet" media="all" href="{{URL::to('/')}}/assets/faq/vendors_ariza_www-33ff9f0bf161ff7b308b.css"><link rel="stylesheet" media="all" href="{{URL::to('/')}}/assets/faq/www-10e07543c41d12d0b1e6.css"><script src="{{URL::to('/')}}/assets/faq/runtime_www-3f4bac85795a1e964d3e.js.download" defer="defer"></script><script src="{{URL::to('/')}}/assets/faq/3-f38ce0058429a4b4d873.js.download" defer="defer"></script><script src="{{URL::to('/')}}/assets/faq/vendors_ariza_www-1deed95146589bd72f67.js.download" defer="defer"></script><script src="{{URL::to('/')}}/assets/faq/www-290f06037a8f18cfb238.js.download" defer="defer"></script>

    <title><font color="blue">よくある質問 | CLINICS(クリニクス)</font></title>
<meta name="description" content="オンライン診療アプリ「CLINICS(クリニクス)」に関するよくある質問。CLINICSは、病院に行くための移動時間や待ち時間を気にすることなく、スマホやPCで診察を受けて、薬や処方せんも自宅で受け取れるサービスです。サービスご利用の前に本ページをご一読ください。">
<meta name="keywords" content="スマホ通院, 遠隔診療, オンライン診療, オンライン診察">
<meta property="og:type" content="website">
<meta property="og:title" content="よくある質問">
<meta property="og:description" content="オンライン診療アプリ「CLINICS(クリニクス)」に関するよくある質問。CLINICSは、病院に行くための移動時間や待ち時間を気にすることなく、スマホやPCで診察を受けて、薬や処方せんも自宅で受け取れるサービスです。サービスご利用の前に本ページをご一読ください。">
<meta property="og:image" content="#####d18fktiuhhpgn2.cloudfront.net/assets/www/og-73a9aced300f9f7cdc2253b78b06bcf31bb00d929fcf1d54d61076dd47300b4c.png">
<meta property="og:locale" content="ja_JP">
<meta property="og:site_name" content="CLINICS(クリニクス)">
    <link rel="shortcut icon" type="image/x-icon" href="#####d18fktiuhhpgn2.cloudfront.net/assets/favicon-8701ac82f2e83ce7ef1c823ce67c354d84f895cba1f9d134aba1734f574bb54b.ico">
    <link rel="apple-touch-icon" type="image/png" href="#####d18fktiuhhpgn2.cloudfront.net/assets/www/apple-touch-icon-3c09deeced999d73a7f645de7b25ec0e4e1231f4a3ec36dc32cfe784457f24fe.png">
    <meta name="google-site-verification" content="uuYcWvMesHdj9PNHNPP5a6sgPNeb3-YDVmvfP1lk1Pc">


  <style>.tk-kozuka-gothic-pro{font-family:"kozuka-gothic-pro",sans-serif;}.tk-ryo-gothic-plusn{font-family:"ryo-gothic-plusn",sans-serif;}</style><script src="{{URL::to('/')}}/assets/faq/f(1).txt"></script><style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, "hiragino kaku gothic pro",meiryo,"ms pgothic",sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_dialog_advanced{border-radius:8px;padding:10px}.fb_dialog_content{background:#fff;color:#373737}.fb_dialog_close_icon{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{left:5px;right:auto;top:5px}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{height:100%;left:0;margin:0;overflow:visible;position:absolute;top:-10000px;transform:none;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{background:none;height:auto;min-height:initial;min-width:initial;width:auto}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{clear:both;color:#fff;display:block;font-size:18px;padding-top:20px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .4);bottom:0;left:0;min-height:100%;position:absolute;right:0;top:0;width:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_mobile .fb_dialog_iframe{position:sticky;top:0}.fb_dialog_content .dialog_header{background:linear-gradient(from(#738aba), to(#2c4987));border-bottom:1px solid;border-color:#043b87;box-shadow:white 0 1px 1px -1px inset;color:#fff;font:bold 14px Helvetica, sans-serif;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:linear-gradient(from(#4267B2), to(#2a4887));background-clip:padding-box;border:1px solid #29487d;border-radius:3px;display:inline-block;line-height:18px;margin-top:3px;max-width:85px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{background:none;border:none;color:#fff;font:bold 12px Helvetica, sans-serif;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(#####static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #4a4a4a;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f5f6f7;border:1px solid #4a4a4a;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(#####static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-position:50% 50%;background-repeat:no-repeat;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}</style><script type="text/javascript" src="{{URL::to('/')}}/assets/faq/analytics"></script><script type="text/javascript" src="{{URL::to('/')}}/assets/faq/analytics(1)"></script></head>
  <body>
      <noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-53FKFW" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>

<div class="header-inner">
<div class="header-logo">
<a href="#####clinics.medley.life/"><img class="logo" alt="オンライン診療アプリ CLINICS(クリニクス)" src="{{URL::to('/')}}/assets/faq/clinics-logo-e582529b8e4be427351eac7c4468fa5c1e9ac4a728a778e9b7a5f837d8f4e55f.svg">
</a></div>
<div class="header-menu">
<div class="header-menu-icon"></div>
</div>
</div>

<div class="l-cover">
  <div class="cover">
    <div class="cover-inner">
      <div class="cover-nav breadcrumbs"><span itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="#####clinics.medley.life/"><span itemprop="title">CLINICS</span></a></span> › <span itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><span class="current" itemprop="title">ご利用ガイド</span></span></div>

      <div class="cover-content">
        <div class="cover-symbol">
            <img src="{{URL::to('/')}}/assets/faq/icon-clinic-e4b6febad6b0b0ac82a745ac2b95ceac30bf3a923c32f0cdc26c03b890ee612a.png" alt="Icon clinic">
        </div>
        <div class="cover-item">
          <h1 class="cover-item-title">よくある質問</h1>
          <div class="cover-item-description"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="section-box">
<div class="section-box-inner">
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
どの病院・クリニックでも利用できますか？
</dt>
<dd class="faq-answer">
CLINICSを使ったオンライン診療に対応している病院・クリニックでのみご利用可能です。対応している医療機関についてはCLINICSアプリやWebサイトにてご確認いただけます。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
どんな症状・病気でもオンライン診療できますか？
</dt>
<dd class="faq-answer">
すべての症状・病気でオンライン診療を受けられるわけではありません。
<br>
原則的に来院して対面診療を受けたうえ、医師が利用可能と判断した場合のみ、オンライン診療を受けていただくことができます。
<br>
詳細は病院・クリニックへお問い合わせください。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
特別な料金は掛かりますか？
<br>
また、決済はどのような仕組みですか？
</dt>
<dd class="faq-answer">
CLINICSのアプリはダウンロード・利用料いずれも無料です。
<br>
通常の来院時の診療と同様、診察費が掛かります。診療内容によっては予約料・薬代等が掛かることがあります。決済には各種クレジットカードをご利用いただけます（VISA、Mastercard、American Express、JCB、Diners Club、Discoverに対応）。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
保険は適用されますか？
</dt>
<dd class="faq-answer">
病院・クリニックに来院して診療を受ける場合と同様に、保険診療では保険が適用されます。オンライン診療でも保険証の掲示が必要となる場合もございますのでお手元にご準備ください。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
アカウント登録ができたら、どのように予約を進めればよいですか？
</dt>
<dd class="faq-answer">
<a href="#####clinics.medley.life/clinics">病院・クリニックを探す</a>
より受診する病院・クリニックを検索します。病院・クリニックごとの詳細ページにて診療メニューを選ぶと診察日時を指定できます。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
再診コード（またはQRコード）を受け取った方へ
</dt>
<dd class="faq-answer">
医師がオンライン診療可能と判断した場合、8桁の「再診コード（またはQRコード）」を渡される場合がございます。診察予約で必要となりますのでアプリまたはWebサイトで再診コードを入力または読み取りいただくと、診察予約がスムーズに行えます。
</dd>
</dl>
<dl class="faq">
<dt class="faq-question">
<span>Q</span>
アプリがないと利用できませんか？
</dt>
<dd class="faq-answer">
スマートフォンのアプリはもちろん、ブラウザや、パソコンでもご利用になれます。
<br>
詳しくは
<a href="#####clinics.medley.life/guide#support">サポート環境について</a>
をご確認ください。
</dd>
</dl>
</div>
</section>
{{-- <footer class="l-footer footer">
<div class="footer-nav">
<div class="pure-g">
<div class="pure-u-1 pure-u-md-8-24">
<div class="footer-logo">
<a href="#####clinics.medley.life/"><img alt="オンライン診療アプリ CLINICS(クリニクス)" src="{{URL::to('/')}}/assets/faq/clinics-logo-e582529b8e4be427351eac7c4468fa5c1e9ac4a728a778e9b7a5f837d8f4e55f.svg">
</a></div>
</div>
<div class="pure-u-1 pure-u-md-5-24 footer-nav-panel">
<div class="footer-nav-panel-title">
一般の方
</div>
<ul class="pure-menu-list footer-nav-panel-list">
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/clinics">病院・クリニックを探す</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/guide">ご利用ガイド</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/guide#support">サポート環境</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/requirements">PCをご利用の方</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/apps">アプリ</a>
</li>
</ul>
</div>
<div class="pure-u-1 pure-u-md-5-24 footer-nav-panel">
<div class="footer-nav-panel-title">
医療機関の方
</div>
<ul class="pure-menu-list footer-nav-panel-list">
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####clinics-cloud.com/">クラウド診療支援システムCLINICS</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####clinics-cloud.com/reservation/">CLINICS予約</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####clinics-cloud.com/online/">CLINICSオンライン診療</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####clinics-cloud.com/karte/">CLINICSカルテ</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/safety">安心安全への取り組みについて</a>
</li>
</ul>
</div>
<div class="pure-u-1 pure-u-md-5-24 footer-nav-panel">
<div class="footer-nav-panel-title">
共通
</div>
<ul class="pure-menu-list footer-nav-panel-list">
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/terms">利用規約</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/terms#transaction">特定商取引法に基づく表記</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a href="#####clinics.medley.life/privacy">プライバシーポリシー</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####www.medley.jp/">運営会社</a>
</li>
<li class="pure-menu-item footer-nav-panel-list-item">
<a target="_blank" href="#####www.medley.jp/about/brand-resource.html">ロゴ利用ガイドライン</a>
</li>
<p class="isms-logo">
<a href="#####clinics.medley.life/safety/#cloud"><img src="{{URL::to('/')}}/assets/faq/img-isms_iso27001-d34c3d4c9f682ac2ef7ec9126ffd573ff4845a846b5e6e99ca2c33f46feeb806.png" alt="Img isms iso27001">
<img src="{{URL::to('/')}}/assets/faq/img-isms_iso27017-752fc2607523600e35f065a92179bebfc3da337fa20b142f6fcd58a645b10f7d.png" alt="Img isms iso27017">
</a></p>
</ul>
</div>
</div>
</div>
<div class="footer-global">
<ul class="footer-global-menu">
<li class="pure-menu-item">
<a href="#####medley.life/"><div class="footer-global-service">
<div class="footer-global-service-title">
医師たちがつくるオンライン医療事典
</div>
<div class="footer-global-service-logo medley">
<img alt="MEDLEY(メドレー)" src="{{URL::to('/')}}/assets/faq/logo-medley-a0725b25fdfa9ee6b168d7f77d760beea37fa56055db3195b23d29d9e8747b5a.svg">
</div>
</div>
</a></li>
<li class="pure-menu-item">
<a href="#####job-medley.com/"><div class="footer-global-service">
<div class="footer-global-service-title">
日本最大級の医療介護求人サイト
</div>
<div class="footer-global-service-logo jobmedley">
<img alt="ジョブメドレー" src="{{URL::to('/')}}/assets/faq/logo-jobmedley-0b96405f99e5c203eb611847e822e8fa4395bf2787f379ded0814d178c9e7ad8.svg">
</div>
</div>
</a></li>
<li class="pure-menu-item">
<a href="#####www.kaigonohonne.com/"><div class="footer-global-service">
<div class="footer-global-service-title">
医療につよい老人ホーム検索サイト
</div>
<div class="footer-global-service-logo honne">
<img alt="介護のほんね" src="{{URL::to('/')}}/assets/faq/logo-honne-9aa67cb25339f84abf3c6f0dfa71bfb99e9953987c49ccf50e01620b077557d1.svg">
</div>
</div>
</a></li>
</ul>
<div class="footer-global-copy">
Copyright © Medley, Inc. All rights reserved.
</div>
</div>
</footer> --}}


    <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div><iframe name="fb_xdm_frame_https" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="{{URL::to('/')}}/assets/faq/xd_arbiter.html" style="border: none;"></iframe></div><div></div></div></div>


</div>
<script type="text/javascript" id="">var yahoo_retargeting_id="4K4C0DE3X3",yahoo_retargeting_label="",yahoo_retargeting_page_type="",yahoo_retargeting_items=[{item_id:"",category_id:"",price:"",quantity:""}];</script>
<script type="text/javascript" id="" src="{{URL::to('/')}}/assets/faq/s_retargeting.js.download"></script><script type="text/javascript" id="">piAId="173482";piCId="1280";(function(){function a(){var b=document.createElement("script");b.type="text/javascript";b.src=("#####"==document.location.protocol?"#####pi":"http://cdn")+".pardot.com/pd.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(b,a)}window.attachEvent?window.attachEvent("onload",a):window.addEventListener("load",a,!1)})();</script><iframe src="{{URL::to('/')}}/assets/faq/pixel.html" style="display: none;"></iframe></body></html>
@endsection
