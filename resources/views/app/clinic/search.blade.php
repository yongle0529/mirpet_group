@extends('layouts.app.clinic')

@section('content')
<script>
function search_back()
{
    history.go(-1);
}
</script>
<form method="GET" action="{{ URL::to('/app/clinic/search_result') }}">
@csrf
<div class="container">
<div class="content">
    <div class="col-md-12 border-bottom border-danger mb-4">
        <h3 class="title">飼い主様・動物検索</h3>
    </div>
        <div class="row">
        <div class="col-md-6">
            <p class="category"><h3>患者様情報</h3></p>
            <div class="card">
                <div class="card-body">
                    <div class = "row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="userid">カルテ番号</label>
                                <input id="userid" name="pet_diagId" type="text" placeholder="101" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hospital_name">動物番号</label>
                                <input id="hospital_name" name="petId" type="text" placeholder="101" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hospital_name">患者様名</label>
                                <input id="hospital_name" name="pet_name" type="text" placeholder="たろう" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hospital_name">動物種</label>
                                <select name ="pet_type" class ="form-control">
                                    <option value =""></option>
                                    <option value="1">イヌ</option>
                                    <option value="2">ネコ</option>
                                    <option value="4">ハムスター</option>
                                    <option value="5">フェレット</option>
                                    <option value="6">爬虫類</option>
                                    <option value="7">ウサギ</option>
                                    <option value="8">鳥</option>
                                    <option value="3">その他</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hospital_name">種類</label>
                                <input id="hospital_name" name="pet_other_type" type="text" placeholder="ポメラニアン" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hospital_name">年齢</label>
                                <input id="hospital_name" name="pet_age" type="text" placeholder="1歳2カ月" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <p class="category"><h3>飼い主様情報</h3></p>
            <!-- Nav tabs -->
            <div class="card">
            <div class="card-body">
            <div class = "row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="userid">カルテ番号</label>
                    <input name="user_diagId" type="text" placeholder="101" class="form-control">
                </div>
            </div>
            <div class="col-md-3 text-md-right">
                <div class="form-group">
                    <label for="hospital_name">飼い主様</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="hospital_name">姓</label>
                    <input id="hospital_name" name="user_name" type="text" placeholder="山下" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="hospital_name">名</label>
                    <input id="hospital_name" name="user_name_kana" type="text" placeholder="佐藤" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="hospital_name">電話番号</label>
                    <input id="hospital_name" name="tel" type="text" placeholder="09012345678" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="hospital_name">都道府県</label>
                    <select class="form-control" id="prefecture" name="prefecture">
                    <option value=""></option>
                        @foreach(config('const.PREFECTURE_LIST') as $pref)

                                <option
                                    value="{{ $pref }}"
                                    {{ $pref == '東京都' ? '' : '' }}
                                >{{ $pref }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="hospital_name">区市町村</label>
                    <input id="hospital_name" name="city" type="text" placeholder="世田谷区" class="form-control">
                </div>
            </div>
            </div>

            </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class ="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="button" class="btn btn-default btn-lg" onclick = "search_back()" value="戻る">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="reset" class="btn btn-warning btn-lg" value ="条件クリア">
                    </div>
                </div>
                <div class="col-md-4">
                    <input type = "submit" class="btn btn-primary btn-lg" value = "検索">
                </div>
            </div>
        </div>
    </div>
</div>
</div>
 </form>
@endsection
