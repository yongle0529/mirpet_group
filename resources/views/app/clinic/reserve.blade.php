@extends('layouts.app.clinic')

@section('content')
<script>
 function dispChange(id_day,id_time,id)
{

    // var sel_id="sel"+2+3;
    // alert(id_time);
    for(i=1;i<32;i++){
        for(j=8;j<20;j++){

            if(id_day == i){
                if(id_time == j){
                    $("#seldiv"+i+j).css("display","block");
                }else{
                    $("#seldiv"+i+j).css("display","none");
                }
            }else{

                $("#seldiv"+i+j).css("display","none");
            }
        }
    }
    // $("#seldiv"+id).css("display","block");



}

</script>
<div class="content">
    <div class="content">
    <!-- @if(!empty($query))
        @foreach($query as $row)
            {{$row->reserve_datetime}}
        @endforeach
    @endif -->
    <?php
    $r_day = array();
    $r_day[0]='';
    $r_time = array();
    $r_time[0] = '';
    $r_date = array();
    $r_date[0] = '';
    $r_datetime = array();
    $r_datetime[0] = '';
    $doctor[0] = '';
    $cat_no[0] = '';
    $pet_name[0] = '';
    $pet_last_name[0] = '';
    $num=0;
    if(!empty($query)){
        foreach($query as $row)
        {
            $r_datetime[$num] = $row->reserve_datetime;
            $r_day[$num] = substr($row->reserve_datetime, 8,2);
            $r_date[$num] = substr($row->reserve_datetime, 0,10);
            $r_time[$num] = substr($row->reserve_datetime, 11,5);
            // $user_name[$num] = $row->firstname;
            $pet_name[$num] = $row->name;
            $pet_last_name[$num] = $row->last_name;
            $ownercatano = \App\OwnerCatano::where('user_id', $row->user_id)->where('clinic_id', $row->clinic_id)->first();
            if(!empty($ownercatano))$cat_no[$num] = $ownercatano->cata_no;
            else $cat_no[$num] = '';//$row->user_id;

            $doctor[$num] = $row->clinic_name;
            $num++;
        }
    }

    ?>
          <div class="row">
          <div class="col-md-1"></div>
            <div class="col-md-3">
              <h3>予約状況</h3>
              <!-- Nav tabs -->
              <div class="card">
                <div class="card-body">
                  <!-- Tab panes -->
                  <center>
    <?php
    // $year = date("Y");
    // $month = date("n");
    // $day = date("j");

    $year = $data['year'];
    $month = $data['month'];
    $day = $data['day'];
    $next_day_num = $data['week_day_num'];
    $cata_view = $data['cata_view'];


    $wt = array('日', '月', '火', '水', '木', '金', '土');
    $week = date("w");

    $firstdate_timestamp = mktime(0,0,0,$month,1,$year);

    $d = -idate("w", $firstdate_timestamp);
    $last_day = idate("t", $firstdate_timestamp);
    ?>

    <form name ="frm" method="POST">
    <div style="height: 30%">
        <div>
            <table width="80%" style="border-collapse: collapse" border=0 cellspacing=0 cellpadding=0 align="center">
                <tr >
                    <td  valign="middle" align="center">
                        <div style="padding-top: 10px;">
                        <font size="6">
                        <a href=<?php echo "/app/clinic/reserve_update/$year/$month/$day/1/$next_day_num"?> style= "cursor:pointer" class="datechange-btn " rel="l_year">«</a>
                        <a href=<?php echo "/app/clinic/reserve_update/$year/$month/$day/2/$next_day_num"?> style= "cursor:pointer"  class="datechange-btn " rel="l_month">﹤</a>

                        </font> <b> <font style="font-size: 20px"><?php echo $year?>年</font></b>
                        <b><font style="font-size: 20px"><?php echo $month?>月</font></b>
                        <font size="6">

                        <a href=<?php echo "/app/clinic/reserve_update/$year/$month/$day/3/$next_day_num"?> style= "cursor:pointer"  class="datechange-btn " rel="f_month">﹥</a>
                        <a href=<?php echo "/app/clinic/reserve_update/$year/$month/$day/4/$next_day_num"?> style= "cursor:pointer"  class="datechange-btn " rel="f_year"> »</a>

                    </td>
        </div>
                </tr>
                <tr>
                    <td  width="100%" style="height: 100%">
                        <table style="width: 100%;border:1px solid #ccc; "  cellspacing=0 cellpadding=0>
                            <tr style="height: 30px"><td style="color: red" align="center">日</td>
                            <td align="center">月</td>
                            <td align="center">火</td>
                            <td align="center">水</td>
                            <td align="center">木</td>
                            <td align="center">金</td>
                            <td style="color: blue" align="center">土</td></tr>
                            <?php
                            for($i = 0; $i < 6; $i++)
                            {
                                echo "<tr>\n";

                                for($j = 0; $j < 7; $j++)
                                {
                                    $d++;

                                    echo '<td style="height: 30px; border:1px dotted #ddd;font-size:14px;"';
                                    for($ii = 0; $ii < count($r_day); $ii++){
                                        if($d == $r_day[$ii] and $r_day[$ii]!=0)
                                        {
                                            echo " bgColor='#d1f2ff'";
                                        }
                                    }


                                    echo ' align="center">';

                                    if($d > 0 && $d <= $last_day)
                                    {
                                        if($d == $day) {
                                            echo "<a href='/app/clinic/reserve_day_update/$year/$month/$d/$next_day_num/' style= 'cursor:pointer;color:green' >
                                            <font color='green'>$d</font>
                                            </a>";
                                        } else {
                                            if($j == 0) echo "<a href='/app/clinic/reserve_day_update/$year/$month/$d/$next_day_num/' style= 'cursor:pointer;color:green' >
                                            <font color='red'>$d</font>
                                            </a>";
                                            elseif($j == 6) echo "<a href='/app/clinic/reserve_day_update/$year/$month/$d/$next_day_num/' style= 'cursor:pointer;color:green' >
                                            <font color='blue'>$d</font>
                                            </a>";
                                            else echo "<a href='/app/clinic/reserve_day_update/$year/$month/$d/$next_day_num/' style= 'cursor:pointer;color:green' >
                                            $d</a>";
                                        }
                                    }

                                    echo "</td>";
                                }

                                echo "</tr>\n";
                            }
                            ?>
                        </table></td></tr>
            </table>
        </div>
    </div>

</center>
<?php
/////////// week Calc//////////////////
// date('w',mktime(0,0,0,$i,$j,$year));
$wday=$year.'-'.$month.'-'.$day;

$currentWeekNumber = date('w',strtotime($wday));
// echo $currentWeekNumber;
$WeekNumber= array(7);
$n=0;
for($i = $currentWeekNumber; $i > 0; $i--)
{
    if($day-$i<1){
        $WeekNumber[$n] = "";
    }else{
        $WeekNumber[$n] = $day-$i;
    }
    $n++;
}

for($i = 0; $i < 7- $currentWeekNumber; $i++)
{
    if($day+$i > $last_day){
        $WeekNumber[$n] = " ";
    }else{
        $WeekNumber[$n] = $day+$i;
    }
    $n++;
}
$wt = array('日', '月', '火', '水', '木', '金', '土');
///////////////////////////////////////////////
// $wwwk = array();
// $wwwk=yearToWeek($year,true);
// echo $wwwk[0];
?>
<?php if($cata_view == 1){
    if(!empty($query_view)){
        $view_num =0;
        foreach($query_view as $row){
            $view_day = substr($row->reserve_datetime, 0,10);
            $view_time = substr($row->reserve_datetime, 11,5);
            $view_num++;
    ?>
        <div id =  style = "display:none">
        <p>
            <div>
            <label  class="col-md-12 col-form-label text-md-left"> <b> 予約情報詳細 <?php if(count($query_view)>1)
            echo ("<span class='badge badge-info'>$view_num </span>");
            ?>
            </b></label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">カルテNo. :
                    <?php

                    $ownercatano = \App\OwnerCatano::where('user_id', $row->user_id)->where('clinic_id', $row->clinic_id)->first();
                    if(!empty($ownercatano))echo $ownercatano->cata_no;
                    else echo '';//$row->user_id;

                    ?>
            </label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">飼い主様名 : {{$row->last_name}}</label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">患者様名 : {{$row->name}}</label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">予約日 : {{$view_day}}</label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">予約時間 : {{$view_time}}</label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">目的 : {{$row->content}}</label>
            </div>
            <div>
            <label  class="col-md-12 col-form-label text-md-left">担当医 : {{$row->clinic_name}}</label>
            </div>
        </div>
<?php }}}?>
</div>
</div>
</div>
<div class="col-md-7">
<p class="category">
<label  class="col-md-12 col-form-label text-md-right">
<?php
// echo "<a href='/app/clinic/reserve_next_week/$year/$month/$day/$last_day/1/$next_day_num' style= 'cursor:pointer;color:green' >
// <font color='green'>←先週&nbsp;&nbsp;</font>
// </a>";
echo "<button class='btn btn-primary' id='beforeweek'>←先週</button>";
?>

<?php
// echo "<a href='/app/clinic/reserve_next_week/$year/$month/$day/$last_day/2/$next_day_num' style= 'cursor:pointer;color:green' >
// <font color='red'>今週</font>
// </a>";
echo "<button class='btn btn-primary' id='thisweek'>今週</button>";
?>
<?php
// echo "<a href='/app/clinic/reserve_next_week/$year/$month/$day/$last_day/3/$next_day_num' style= 'cursor:pointer;color:green' >
// <font color='green'>&nbsp;&nbsp;来週→</font>
// </a>";
echo "<button class='btn btn-primary' id='nextweek'>来週→</button>";
?>

</label></p>
<input type="hidden" name="year" id ="year" value={{$year}}>
<input type="hidden" name="month"  id ="month"value={{$month}}>
<input type="hidden" name="day"  id ="day"value={{$day}}>
<input type="hidden" name="last_day" id ="last_day" value={{$last_day}}>
<input type="hidden" name="next_day_num"  id ="next_day_num"value={{$next_day_num}}>
<input type="hidden" name="type1"  id ="type1" value='1'>
<!-- Nav tabs -->
<div class="card">
<div class="card-body">
<div class="table-responsive">
<table id = "seltb1"  class="table table-bordered table-hover table-striped text-center" style="width: 100%; table-layout: fixed; word-wrap: break-word;">
    <thead>
    <tr class="bg-warning">
        <th>時間</th>
        <th><?php echo  $WeekNumber[0]."(日)" ?></th>
        <th><?php echo  $WeekNumber[1]."(月)" ?></th>
        <th><?php echo  $WeekNumber[2]."(火)" ?></th>
        <th><?php echo  $WeekNumber[3]."(水)" ?></th>
        <th><?php echo  $WeekNumber[4]."(木)" ?></th>
        <th><?php echo  $WeekNumber[5]."(金)" ?></th>
        <th><?php echo  $WeekNumber[6]."(土)" ?></th>
    </tr>
    </thead>
    <tbody>
    <?php for($i = 0; $i < 24 ; $i++){?>
        <tr >
        <td>
            <?php echo $i."時"; ?>
            </td>
            <?php for($t =0; $t < 7;$t++){?>

            <?php

                for($ii = 0; $ii < count($r_day);$ii++){

                    if($WeekNumber[$t] == $r_day[$ii]){

                        if(substr($r_time[$ii],0,2)==$i){

                            $sel_day = $r_day[$ii];
                            if(substr($sel_day,0,1) == 0)$sel_day=substr($sel_day,1,1);
                            $sel_time = $i;
                            $r_no = $sel_day.$sel_time;
                            echo ("<td onclick = '' style = 'cursor: pointer'>");
                            $viewtime = $r_time[$ii];
                            echo "<a href='/app/clinic/reserve_state_view/$year/$month/$next_day_num/$r_datetime[$ii]/' style= 'cursor:pointer;color:green' >
                                    <font color='green'>$viewtime<br>$doctor[$ii]<br>$pet_last_name[$ii]<br>$pet_name[$ii]<br>$cat_no[$ii]</font>
                                    </a>";
                            echo("</td>");
                            break;
                        }else{
                            $viewtime = "";
                        }
                    }else{
                        $viewtime = "";
                    }
                }
                if($viewtime ==""){
                    echo("<td>");
                    echo $viewtime;
                    echo("</td>");
                }

            ?>

            <?php }?>
        </tr>
    <?php }?>
    </tbody>
</table>
<div id ="seltb2" ></div>
</div>
</div>
</div>
</div>
</div>
</div>

</form>
</div>
<?php
function yearToWeek($year,$overlap=true){

    $k = 0;

    $allWeek = array();

    for($i=1; $i<=12;$i++)

    {

        $sa = mktime(0,0,0,$i,1,$year);



        $ea = mktime(23,59,59,$i,date("t",$sa),$year);

        $a = date("d",$ea);



        for($j=1; $j<=$a;$j++)

        {

            //해당날짜의 요일

            $dayOfTheWeek = date('w',mktime(0,0,0,$i,$j,$year));



            //주차의 시작일

            $sd = mktime(0,0,0,$i,$j-$dayOfTheWeek,$year);

            $sd1 = date("Y-m-d",$sd);

            $sdm = sprintf("%2d",date("m",$sd));

            $sdd = sprintf("%2d",date("d",$sd));



            //주차의 종료일

            $ed = mktime(23,59,59,$i,$j+(6-$dayOfTheWeek),$year);

            $ed1 = date("Y-m-d",$ed);

            $edm = sprintf("%2d",date("m",$ed));

            $edd = sprintf("%2d",date("d",$ed));
            $aa =0;


            if($aa != $sd1)

            {

                $k++;



                $allWeek[$k]['syear'] = $year;

                $allWeek[$k]['smonth'] = $i;

                $allWeek[$k]['sday'] = $j;

                $allWeek[$k]['eyear'] = $year;

                $allWeek[$k]['emonth'] = $edm;

                $allWeek[$k]['eday'] = $edd;



                //달이 틀리면? 두번을 넣어야 하는데???

                if($overlap)

                {

                    if($i != $edm)

                    {

                        //해당달의 시작일

                        $msd = mktime(23,59,59,$i,1,$year);

                        //해당월의 마지막 날짜

                        $med = mktime(23,59,59,$i,date("t",$msd),$year);

                        $med1 = date("Y-m-d",$med);

                        $medd = date("d",$med);



                        $allWeek[$k]['syear'] = $year;

                        $allWeek[$k]['smonth'] = $i;

                        $allWeek[$k]['sday'] = $j;

                        $allWeek[$k]['eyear'] = $year;

                        $allWeek[$k]['emonth'] = $i;

                        $allWeek[$k]['eday'] = $medd;



                        $k++;



                        $allWeek[$k]['syear'] = $year;

                        $allWeek[$k]['smonth'] = $edm;

                        $allWeek[$k]['sday'] = 1;

                        $allWeek[$k]['eyear'] = $year;

                        $allWeek[$k]['emonth'] = $edm;

                        $allWeek[$k]['eday'] = $edd;

                    }

                }



                //echo($k."~~~~~~~".$year."년".$i."월".$j."일====".$sd1."~~".$ed1."<br/>");

            }



            $aa = $sd1;

        }

    }

    Return $allWeek;

}


?>
<script type='text/javascript'>


jQuery(document).ready(function(){
           jQuery('#beforeweek').click(function(e){
              e.preventDefault();
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                 }
             });
            //  $.get('/app/clinic/reserve_next_week/' + )
              jQuery.ajax({
                 url: ("/app/clinic/reserve_next_week1/"+jQuery('#year').val()+'/'+jQuery('#month').val()+'/'+jQuery('#day').val()+'/'+jQuery('#last_day').val()+'/'+1+'/'+jQuery('#next_day_num').val()),
                 method: 'get',
                //  data: {
                //     year: jQuery('#year').val(),
                //     // type: jQuery('#month').val(),
                //     // month: jQuery('#day').val(),
                //     // month: jQuery('#last_day').val(),
                //     // month: jQuery('#next_day_num').val(),
                //     // month: jQuery('#type').val()
                //  },
                 success: function(response){
                    //  alert(9);
                    document.getElementById("year").value=response.data.year;
                    document.getElementById("month").value=response.data.month;
                    document.getElementById("day").value=response.data.day;
                    // document.getElementById("last_day").value=response.data.year;
                    document.getElementById("next_day_num").value=response.data.week_day_num;
                    $('#seltb1').hide();
                    $('#seltb2').show();
                //    var show_content = "";
                //     msg_list.forEach(function(message){
                //         show_content = message['year'];
                //     });
                    var i;
                    var ii;
                    var t;
                    var r_datetime = new Array();
                    var r_day = new Array();
                    var r_date = new Array();
                    var r_time = new Array();
                    var pet_name = new Array();
                    var pet_last_name = new Array();
                    var cat_no = new Array();
                    var doctor = new Array();
                    // for(i=8;i<20;i++){

                    // }
                    // const r_datetime = [1, 29, 47];
                    // const copy = [];
                    var num=0;
                    var ownerflag=0;
                    response.query.forEach(function(row){
                    // copy.push(item*item);
                        r_datetime[num] = row['reserve_datetime'];
                        r_day[num] = row['reserve_datetime'].substr(8,2);
                        r_date[num] = row['reserve_datetime'].substr(0,10);
                        r_time[num] = row['reserve_datetime'].substr(11,5);
                        // // $user_name[$num] = $row->firstname;
                        pet_name[num] = row['name'];
                        pet_last_name[num] = row['last_name'];
                        response.owner_query.forEach(function(row1){
                            if(row['user_id'] == row1['user_id'])
                            {
                                cat_no[num] = row1['cata_no'];
                                ownerflag=1;
                                //continue;
                            }

                        });
                        if(ownerflag==0){
                            cat_no[num] = '';//row['user_id'];
                        }else{
                            ownerflag=0;
                        }
                        doctor[num] = row['genre'];
                        num++;
                    });
                    var tr_div="";
                    for(i = 0; i < 24 ; i++){
                    tr_div+="<tr >";
                    tr_div+="<td>";
                    tr_div+=i+"時";
                    tr_div+="</td>";
                       for(t =0; t < 7;t++){

                        var viewtime = "";

                            for(ii = 0; ii < num;ii++){
                                // tr_div+="<td onclick = '' style = 'cursor: pointer'>"+r_day[5]+"</td>";
                                if(response.WeekNumber[t] == r_day[ii]){

                                    if(r_time[ii].substr(0,2)==i){

                                        var sel_day = r_day[ii];
                                        if(sel_day.substr(0,1) == 0)sel_day=sel_day.substr(sel_day,1,1);
                                        var sel_time = i;
                                        var r_no = sel_day+sel_time;
                                        tr_div+="<td onclick = '' style = 'cursor: pointer'>";
                                        var viewtime = r_time[ii];
                                        tr_div+= "<a href='/app/clinic/reserve_state_view/"+response.data.year+'/'+response.data.month+'/'+response.data.day+'/'+r_datetime[ii]+" style= 'cursor:pointer;color:green' >"+
                                                "<font color='green'>"+viewtime+"<br>"+doctor[ii]+"<br>"+pet_last_name[ii]+"<br>"+pet_name[ii]+"<br>"+cat_no[ii]+"</font>"+
                                                "</a>";
                                                tr_div+="</td>";
                                        break;
                                    }else{
                                        var viewtime = "";
                                    }
                                }else{
                                    var viewtime = "";
                                }
                            }
                            if(viewtime ==""){
                                tr_div+="<td>"+
                                 viewtime+
                                "</td>";
                            }
                       }


                        tr_div+="</tr>"
                 }
                    // tr_div = "<tr><td>5</td></tr>"
                    // print(copy);
                    $('#seltb2').html(
                        // response.data.year
                        // response
                        // response.WeekNumber[0]+','+response.WeekNumber[6]
                        "<table class='table table-bordered table-hover table-striped text-center' style='width: 100%; table-layout: fixed; word-wrap: break-word;'><thead><tr class='bg-warning'><th>時間</th><th>"+
                        response.WeekNumber[0]+
                        "(日)</th>"+
                        "<th>"+response.WeekNumber[1]+"(月)</th>"+
                        "<th>"+response.WeekNumber[2]+"(火)</th>"+
                        "<th>"+response.WeekNumber[3]+"(水)</th>"+
                        "<th>"+response.WeekNumber[4]+"(木)</th>"+
                        "<th>"+response.WeekNumber[5]+"(金)</th>"+
                        "<th>"+response.WeekNumber[6]+"(土)</th>"+
                        "</thead></tr>"+
                       tr_div+

                        "</table>"
                    );
                 }});
              });

              jQuery('#thisweek').click(function(e){
              e.preventDefault();
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                 }
             });
            //  $.get('/app/clinic/reserve_next_week/' + )
              jQuery.ajax({
                 url: ("/app/clinic/reserve_next_week1/"+jQuery('#year').val()+'/'+jQuery('#month').val()+'/'+jQuery('#day').val()+'/'+jQuery('#last_day').val()+'/'+2+'/'+jQuery('#next_day_num').val()),
                 method: 'get',
                //  data: {
                //     year: jQuery('#year').val(),
                //     // type: jQuery('#month').val(),
                //     // month: jQuery('#day').val(),
                //     // month: jQuery('#last_day').val(),
                //     // month: jQuery('#next_day_num').val(),
                //     // month: jQuery('#type').val()
                //  },
                 success: function(response){
                    //  alert(9);
                    document.getElementById("year").value=response.data.year;
                    document.getElementById("month").value=response.data.month;
                    document.getElementById("day").value=response.data.day;
                    // document.getElementById("last_day").value=response.data.year;
                    document.getElementById("next_day_num").value=response.data.week_day_num;
                    $('#seltb1').hide();
                    $('#seltb2').show();
                //    var show_content = "";
                //     msg_list.forEach(function(message){
                //         show_content = message['year'];
                //     });
                    var i;
                    var ii;
                    var t;
                    var r_datetime = new Array();
                    var r_day = new Array();
                    var r_date = new Array();
                    var r_time = new Array();
                    var pet_name = new Array();
                    var pet_last_name = new Array();
                    var cat_no = new Array();
                    var doctor = new Array();
                    // for(i=8;i<20;i++){

                    // }
                    // const r_datetime = [1, 29, 47];
                    // const copy = [];
                    var num=0;
                    var ownerflag=0;
                    response.query.forEach(function(row){
                    // copy.push(item*item);
                        r_datetime[num] = row['reserve_datetime'];
                        r_day[num] = row['reserve_datetime'].substr(8,2);
                        r_date[num] = row['reserve_datetime'].substr(0,10);
                        r_time[num] = row['reserve_datetime'].substr(11,5);
                        // // $user_name[$num] = $row->firstname;
                        pet_name[num] = row['name'];
                        pet_last_name[num] = row['last_name'];
                        response.owner_query.forEach(function(row1){
                            if(row['user_id'] == row1['user_id'])
                            {
                                cat_no[num] = row1['cata_no'];
                                ownerflag=1;
                                //continue;
                            }

                        });
                        if(ownerflag==0){
                            cat_no[num] = '';//row['user_id'];
                        }else{
                            ownerflag=0;
                        }
                        doctor[num] = row['genre'];
                        num++;
                    });
                    var tr_div="";
                    for(i = 0; i < 24 ; i++){
                    tr_div+="<tr >";
                    tr_div+="<td>";
                    tr_div+=i+"時";
                    tr_div+="</td>";
                       for(t =0; t < 7;t++){

                        var viewtime = "";

                            for(ii = 0; ii < num;ii++){
                                // tr_div+="<td onclick = '' style = 'cursor: pointer'>"+r_day[5]+"</td>";
                                if(response.WeekNumber[t] == r_day[ii]){

                                    if(r_time[ii].substr(0,2)==i){

                                        var sel_day = r_day[ii];
                                        if(sel_day.substr(0,1) == 0)sel_day=sel_day.substr(sel_day,1,1);
                                        var sel_time = i;
                                        var r_no = sel_day+sel_time;
                                        tr_div+="<td onclick = '' style = 'cursor: pointer'>";
                                        var viewtime = r_time[ii];
                                        tr_div+= "<a href='/app/clinic/reserve_state_view/"+response.data.year+'/'+response.data.month+'/'+response.data.day+'/'+r_datetime[ii]+" style= 'cursor:pointer;color:green' >"+
                                            "<font color='green'>"+viewtime+"<br>"+doctor[ii]+"<br>"+pet_last_name[ii]+"<br>"+pet_name[ii]+"<br>"+cat_no[ii]+"</font>"+
                                                "</a>";
                                                tr_div+="</td>";
                                        break;
                                    }else{
                                        var viewtime = "";
                                    }
                                }else{
                                    var viewtime = "";
                                }
                            }
                            if(viewtime ==""){
                                tr_div+="<td>"+
                                 viewtime+
                                "</td>";
                            }
                       }


                        tr_div+="</tr>"
                 }
                    // tr_div = "<tr><td>5</td></tr>"
                    // print(copy);
                    $('#seltb2').html(
                        // response.data.year
                        // response
                        // response.WeekNumber[0]+','+response.WeekNumber[6]
                        "<table class='table table-bordered table-hover table-striped text-center' style='width: 100%; table-layout: fixed; word-wrap: break-word;'><thead><tr class='bg-warning'><th>時間</th><th>"+
                        response.WeekNumber[0]+
                        "(日)</th>"+
                        "<th>"+response.WeekNumber[1]+"(月)</th>"+
                        "<th>"+response.WeekNumber[2]+"(火)</th>"+
                        "<th>"+response.WeekNumber[3]+"(水)</th>"+
                        "<th>"+response.WeekNumber[4]+"(木)</th>"+
                        "<th>"+response.WeekNumber[5]+"(金)</th>"+
                        "<th>"+response.WeekNumber[6]+"(土)</th>"+
                        "</thead></tr>"+
                       tr_div+

                        "</table>"
                    );
                 }});
              });

              jQuery('#nextweek').click(function(e){
              e.preventDefault();
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                 }
             });
            //  $.get('/app/clinic/reserve_next_week/' + )
              jQuery.ajax({
                 url: ("/app/clinic/reserve_next_week1/"+jQuery('#year').val()+'/'+jQuery('#month').val()+'/'+jQuery('#day').val()+'/'+jQuery('#last_day').val()+'/'+3+'/'+jQuery('#next_day_num').val()),
                 method: 'get',
                //  data: {
                //     year: jQuery('#year').val(),
                //     // type: jQuery('#month').val(),
                //     // month: jQuery('#day').val(),
                //     // month: jQuery('#last_day').val(),
                //     // month: jQuery('#next_day_num').val(),
                //     // month: jQuery('#type').val()
                //  },
                 success: function(response){
                    //  alert(9);
                    document.getElementById("year").value=response.data.year;
                    document.getElementById("month").value=response.data.month;
                    document.getElementById("day").value=response.data.day;
                    // document.getElementById("last_day").value=response.data.year;
                    document.getElementById("next_day_num").value=response.data.week_day_num;
                    $('#seltb1').hide();
                    $('#seltb2').show();
                //    var show_content = "";
                //     msg_list.forEach(function(message){
                //         show_content = message['year'];
                //     });
                    var i;
                    var ii;
                    var t;
                    var r_datetime = new Array();
                    var r_day = new Array();
                    var r_date = new Array();
                    var r_time = new Array();
                    var pet_name = new Array();
                    var pet_last_name = new Array();
                    var cat_no = new Array();
                    var doctor = new Array();
                    // for(i=8;i<20;i++){

                    // }
                    // const r_datetime = [1, 29, 47];
                    // const copy = [];
                    var num=0;
                    var ownerflag=0;
                    response.query.forEach(function(row){
                    // copy.push(item*item);
                        r_datetime[num] = row['reserve_datetime'];
                        r_day[num] = row['reserve_datetime'].substr(8,2);
                        r_date[num] = row['reserve_datetime'].substr(0,10);
                        r_time[num] = row['reserve_datetime'].substr(11,5);
                        //document.write(r_date[num]);
                        // // $user_name[$num] = $row->firstname;
                        pet_name[num] = row['name'];
                        pet_last_name[num] = row['last_name'];
                        response.owner_query.forEach(function(row1){
                            if(row['user_id'] == row1['user_id'])
                            {
                                cat_no[num] = row1['cata_no'];
                                ownerflag=1;
                                //continue;
                            }

                        });
                        if(ownerflag==0){
                            cat_no[num] = '';//row['user_id'];
                        }else{
                            ownerflag=0;
                        }
                        doctor[num] = row['genre'];
                        num++;
                    });
                    var tr_div="";
                    var a='';
                    for(i = 0; i < 24 ; i++){
                    tr_div+="<tr >";
                    tr_div+="<td>";
                    tr_div+=i+"時";
                    tr_div+="</td>";
                       for(t =0; t < 7;t++){

                        var viewtime = "";
                            for(ii = 0; ii < num;ii++){
                                // tr_div+="<td onclick = '' style = 'cursor: pointer'>"+r_day[5]+"</td>";

                                if(response.WeekNumber[t] == r_day[ii]){

                                    if(r_time[ii].substr(0,2)==i){

                                        var sel_day = r_day[ii];
                                        if(sel_day.substr(0,1) == 0)sel_day=sel_day.substr(sel_day,1,1);
                                        var sel_time = i;
                                        var r_no = sel_day+sel_time;
                                        tr_div+="<td onclick = '' style = 'cursor: pointer'>";
                                        var viewtime = r_time[ii];
                                        tr_div+= "<a href='/app/clinic/reserve_state_view/"+response.data.year+'/'+response.data.month+'/'+response.data.day+'/'+r_datetime[ii]+" style= 'cursor:pointer;color:green' >"+
                                            "<font color='green'>"+viewtime+"<br>"+doctor[ii]+"<br>"+pet_last_name[ii]+"<br>"+pet_name[ii]+"<br>"+cat_no[ii]+"</font>"+
                                                "</a>";
                                                tr_div+="</td>";
                                        break;
                                    }else{
                                        var viewtime = "";
                                    }
                                }else{
                                    var viewtime = "";
                                }
                            }
                            if(viewtime ==""){
                                tr_div+="<td>"+
                                 viewtime+
                                "</td>";
                            }
                       }


                        tr_div+="</tr>"
                 }
                    // tr_div = "<tr><td>5</td></tr>"
                    // print(copy);
                    $('#seltb2').html(
                        // response.data.year
                        // response
                        // response.WeekNumber[0]+','+response.WeekNumber[6]
                        "<table class='table table-bordered table-hover table-striped text-center' style='width: 100%; table-layout: fixed; word-wrap: break-word;'><thead><tr class='bg-warning'><th>時間</th><th>"+
                        response.WeekNumber[0]+
                        "(日)</th>"+
                        "<th>"+response.WeekNumber[1]+"(月)</th>"+
                        "<th>"+response.WeekNumber[2]+"(火)</th>"+
                        "<th>"+response.WeekNumber[3]+"(水)</th>"+
                        "<th>"+response.WeekNumber[4]+"(木)</th>"+
                        "<th>"+response.WeekNumber[5]+"(金)</th>"+
                        "<th>"+response.WeekNumber[6]+"(土)</th>"+
                        "</thead></tr>"+
                       tr_div+

                        "</table>"
                    );
                 }});
              });

});
        //    jQuery('#thisweek').click(function(e){
        //       e.preventDefault();
        //       $.ajaxSetup({
        //          headers: {
        //              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        //          }
        //      });
        //     //  $.get('/app/clinic/reserve_next_week/' + )
        //       jQuery.ajax({
        //          url: ("/app/clinic/reserve_next_week1/"+jQuery('#year').val()+'/'+jQuery('#month').val()+'/'+jQuery('#day').val()+'/'+jQuery('#last_day').val()+'/'+2+'/'+jQuery('#next_day_num').val()),
        //          method: 'get',
        //         //  data: {
        //         //     year: jQuery('#year').val(),
        //         //     // type: jQuery('#month').val(),
        //         //     // month: jQuery('#day').val(),
        //         //     // month: jQuery('#last_day').val(),
        //         //     // month: jQuery('#next_day_num').val(),
        //         //     // month: jQuery('#type').val()
        //         //  },
        //          success: function(response){
        //             //  alert(9);
        //             document.getElementById("year").value=response.data.year;
        //             document.getElementById("month").value=response.data.month;
        //             document.getElementById("day").value=response.data.day;
        //             // document.getElementById("last_day").value=response.data.year;
        //             document.getElementById("next_day_num").value=response.data.week_day_num;
        //             $('#seltb1').hide();
        //             $('#seltb2').show();
        //         //    var show_content = "";
        //         //     msg_list.forEach(function(message){
        //         //         show_content = message['year'];
        //         //     });
        //             var i;
        //             var ii;
        //             var t;
        //             var r_datetime = new Array();
        //             var r_day = new Array();
        //             var r_date = new Array();
        //             var r_time = new Array();
        //             var pet_name = new Array();
        //             var cat_no = new Array();
        //             var doctor = new Array();
        //             // for(i=8;i<20;i++){

        //             // }
        //             // const r_datetime = [1, 29, 47];
        //             // const copy = [];
        //             var num=0;
        //             response.query.forEach(function(row){
        //             // copy.push(item*item);
        //                 r_datetime[num] = row['reserve_datetime'];
        //                 r_day[num] = row['reserve_datetime'].substring(8,10);
        //                 r_date[num] = row['reserve_datetime'].substring(0,10);
        //                 r_time[num] = row['reserve_datetime'].substring(11,19);
        //                 // // $user_name[$num] = $row->firstname;
        //                 pet_name[num] = row['name'];
        //                 cat_no[num] = row['cata_no'];
        //                 doctor[num] = row['genre'];
        //                 num++;
        //             });
        //             var tr_div="";
        //             for(i = 8; i < 20 ; i++){
        //             tr_div+="<tr >";
        //             tr_div+="<td>";
        //             tr_div+=i+"時";
        //             tr_div+="</td>";
        //                for(t =0; t < 7;t++){

        //                 var viewtime = "";

        //                     for(ii = 0; ii < num;ii++){
        //                         // tr_div+="<td onclick = '' style = 'cursor: pointer'>"+r_day[5]+"</td>";
        //                         if(response.WeekNumber[t] == r_day[ii]){

        //                             if(r_time[ii].substring(0,2)==i){

        //                                 var sel_day = r_day[ii];
        //                                 if(sel_day.substring(0,1) == 0)sel_day=sel_day.substring(sel_day,1,1);
        //                                 var sel_time = i;
        //                                 var r_no = sel_day+sel_time;
        //                                 tr_div+="<td onclick = '' style = 'cursor: pointer'>";
        //                                 var viewtime = r_time[ii];
        //                                 tr_div+= "<a href='/app/clinic/reserve_state_view/"+response.data.year+'/'+response.data.month+'/'+response.data.day+'/'+r_datetime[ii]+" style= 'cursor:pointer;color:green' >"+
        //                                         "<font color='green'>"+viewtime+"<br>"+doctor[ii]+"<br>"+cat_no[ii]+"<br>"+pet_name[ii]+"</font>"+
        //                                         "</a>";
        //                                         tr_div+="</td>";
        //                                 break;
        //                             }else{
        //                                 var viewtime = "";
        //                             }
        //                         }else{
        //                             var viewtime = "";
        //                         }
        //                     }
        //                     if(viewtime ==""){
        //                         tr_div+="<td>"+
        //                          viewtime+
        //                         "</td>";
        //                     }
        //                }


        //                 tr_div+="</tr>"
        //          }
        //             // tr_div = "<tr><td>5</td></tr>"
        //             // print(copy);
        //             $('#seltb2').html(
        //                 // response.data.year
        //                 // response
        //                 // response.WeekNumber[0]+','+response.WeekNumber[6]
        //                 "<table class='table table-bordered table-hover table-striped text-center' style='width: 100%; table-layout: fixed; word-wrap: break-word;'><thead><tr class='bg-warning'><th>時間</th><th>"+
        //                 response.WeekNumber[0]+
        //                 "(日)</th>"+
        //                 "<th>"+response.WeekNumber[1]+"(月)</th>"+
        //                 "<th>"+response.WeekNumber[2]+"(火)</th>"+
        //                 "<th>"+response.WeekNumber[3]+"(木)</th>"+
        //                 "<th>"+response.WeekNumber[4]+"(水)</th>"+
        //                 "<th>"+response.WeekNumber[5]+"(金)</th>"+
        //                 "<th>"+response.WeekNumber[6]+"(土)</th>"+
        //                 "</thead></tr>"+
        //                tr_div+

        //                 "</table>"
        //             );
        //          }});
        //       });
        //    });
        //    jQuery('#nextweek').click(function(e){
        //       e.preventDefault();
        //       $.ajaxSetup({
        //          headers: {
        //              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        //          }
        //      });
        //     //  $.get('/app/clinic/reserve_next_week/' + )
        //       jQuery.ajax({
        //          url: ("/app/clinic/reserve_next_week1/"+jQuery('#year').val()+'/'+jQuery('#month').val()+'/'+jQuery('#day').val()+'/'+jQuery('#last_day').val()+'/'+3+'/'+jQuery('#next_day_num').val()),
        //          method: 'get',
        //         //  data: {
        //         //     year: jQuery('#year').val(),
        //         //     // type: jQuery('#month').val(),
        //         //     // month: jQuery('#day').val(),
        //         //     // month: jQuery('#last_day').val(),
        //         //     // month: jQuery('#next_day_num').val(),
        //         //     // month: jQuery('#type').val()
        //         //  },
        //          success: function(response){
        //             //  alert(9);
        //             document.getElementById("year").value=response.data.year;
        //             document.getElementById("month").value=response.data.month;
        //             document.getElementById("day").value=response.data.day;
        //             // document.getElementById("last_day").value=response.data.year;
        //             document.getElementById("next_day_num").value=response.data.week_day_num;
        //             $('#seltb1').hide();
        //             $('#seltb2').show();
        //         //    var show_content = "";
        //         //     msg_list.forEach(function(message){
        //         //         show_content = message['year'];
        //         //     });
        //             var i;
        //             var ii;
        //             var t;
        //             var r_datetime = new Array();
        //             var r_day = new Array();
        //             var r_date = new Array();
        //             var r_time = new Array();
        //             var pet_name = new Array();
        //             var cat_no = new Array();
        //             var doctor = new Array();
        //             // for(i=8;i<20;i++){

        //             // }
        //             // const r_datetime = [1, 29, 47];
        //             // const copy = [];
        //             var num=0;
        //             response.query.forEach(function(row){
        //             // copy.push(item*item);
        //                 r_datetime[num] = row['reserve_datetime'];
        //                 r_day[num] = row['reserve_datetime'].substring(8,10);
        //                 r_date[num] = row['reserve_datetime'].substring(0,10);
        //                 r_time[num] = row['reserve_datetime'].substring(11,19);
        //                 // // $user_name[$num] = $row->firstname;
        //                 pet_name[num] = row['name'];
        //                 cat_no[num] = row['cata_no'];
        //                 doctor[num] = row['genre'];
        //                 num++;
        //             });
        //             var tr_div="";
        //             for(i = 8; i < 20 ; i++){
        //             tr_div+="<tr >";
        //             tr_div+="<td>";
        //             tr_div+=i+"時";
        //             tr_div+="</td>";
        //                for(t =0; t < 7;t++){

        //                 var viewtime = "";

        //                     for(ii = 0; ii < num;ii++){
        //                         // tr_div+="<td onclick = '' style = 'cursor: pointer'>"+r_day[5]+"</td>";
        //                         if(response.WeekNumber[t] == r_day[ii]){

        //                             if(r_time[ii].substring(0,2)==i){

        //                                 var sel_day = r_day[ii];
        //                                 if(sel_day.substring(0,1) == 0)sel_day=sel_day.substring(sel_day,1,1);
        //                                 var sel_time = i;
        //                                 var r_no = sel_day+sel_time;
        //                                 tr_div+="<td onclick = '' style = 'cursor: pointer'>";
        //                                 var viewtime = r_time[ii];
        //                                 tr_div+= "<a href='/app/clinic/reserve_state_view/"+response.data.year+'/'+response.data.month+'/'+response.data.day+'/'+r_datetime[ii]+" style= 'cursor:pointer;color:green' >"+
        //                                         "<font color='green'>"+viewtime+"<br>"+doctor[ii]+"<br>"+cat_no[ii]+"<br>"+pet_name[ii]+"</font>"+
        //                                         "</a>";
        //                                         tr_div+="</td>";
        //                                 break;
        //                             }else{
        //                                 var viewtime = "";
        //                             }
        //                         }else{
        //                             var viewtime = "";
        //                         }
        //                     }
        //                     if(viewtime ==""){
        //                         tr_div+="<td>"+
        //                          viewtime+
        //                         "</td>";
        //                     }
        //                }


        //                 tr_div+="</tr>"
        //          }
        //             // tr_div = "<tr><td>5</td></tr>"
        //             // print(copy);
        //             $('#seltb2').html(
        //                 // response.data.year
        //                 // response
        //                 // response.WeekNumber[0]+','+response.WeekNumber[6]
        //                 "<table class='table table-bordered table-hover table-striped text-center' style='width: 100%; table-layout: fixed; word-wrap: break-word;'><thead><tr class='bg-warning'><th>時間</th><th>"+
        //                 response.WeekNumber[0]+
        //                 "(日)</th>"+
        //                 "<th>"+response.WeekNumber[1]+"(月)</th>"+
        //                 "<th>"+response.WeekNumber[2]+"(火)</th>"+
        //                 "<th>"+response.WeekNumber[3]+"(木)</th>"+
        //                 "<th>"+response.WeekNumber[4]+"(水)</th>"+
        //                 "<th>"+response.WeekNumber[5]+"(金)</th>"+
        //                 "<th>"+response.WeekNumber[6]+"(土)</th>"+
        //                 "</thead></tr>"+
        //                tr_div+

        //                 "</table>"
        //             );
        //          }});
        //       });
        //    });

</script>

@endsection
