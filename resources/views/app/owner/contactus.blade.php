@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <div id="inquiry" class="row mb-4 justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">お問い合わせ</h3>
            </div>
            <form method="POST" action="{{ URL::to('/app/owner/inquiry') }}" class="row col-md-8">
                @csrf
                <div class="col-md-12">
                    <h5 class="font-weight-bold">お名前</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="name" name="name" type="text" value="" placeholder="山本" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">メールアドレス</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="mail" name="mail" type="email" value="" placeholder="info@mirpet.co.jp" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">電話番号(ハイフンなし)</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="tel" name="tel" type="tel" value="" pattern="^[0-9]+$" title="※数字のみ入力できます" placeholder="0369103242" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ内容</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.OWNER_INQUIRY_TYPE') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('type') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ詳細</h5>
                </div>
                <div class="col-md-12">
                    <div class="textarea-container">
                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                    </div>
                </div>

                <div class="col-md-12 text-center mt-4">
                    <button id="confirm" type="submit" class="btn btn-primary btn-lg">入力内容を確認</button>
                </div>
            </form>
        </div>
    </div>
@endsection
