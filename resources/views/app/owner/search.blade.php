@extends('layouts.app.owner')

@section('content')
    <script>
        $(function () {
            $(".fav-add").click(function () {
                var clinicId = $(this).data('clinic-id');
                var obj = $(this);
                $.ajax({
                    url: "{{ URL::to("/app/owner/clinic/fav") }}/" + clinicId,
                    type: 'GET',
                    dataType: 'html',
                    success: function (data) {
                        var data = JSON.parse(data);
                        if (data.status == true) {
                            obj.text('マイリストに登録済み');
                            obj.removeClass('btn-info');
                            obj.addClass('btn-default');
                        } else {
                            obj.text('マイリストに登録する');
                            obj.removeClass('btn-default');
                            obj.addClass('btn-info');
                        }
                    },
                    error: function (data) {
                        alert('error');
                    }
                });
            });

            $('.card-link').click(function () {
                // alert('click');
            });


        });
    </script>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">動物病院検索</h3>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title pt-2 mb-2">検索条件</h3>
                <div class="card">
                    <div class="card-body">
                        <form method="GET" action="{{ URL::to('/app/owner/clinic/search_result') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="pet_reg_name" class="col-md-2 col-form-label text-md-right">都道府県</label>
                                <div class="col-md-2">
                                    <select class="form-control" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            @if(isset($selectedPref))
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == $selectedPref ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @else
                                                <option
                                                    value="{{ $pref }}"
                                                    {{ $pref == '東京都' ? 'selected' : '' }}
                                                >{{ $pref }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <label for="pet_reg_name" class="col-md-2 col-form-label text-md-right">市区町村</label>
                                <div class="col-md-2">
                                    <input id="city" type="text" class="form-control" name="city" placeholder="世田谷区" value="{{isset($inputCity)?$inputCity:""}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pet_reg_name" class="col-md-2 col-form-label text-md-right">対象動物</label>
                                <div class="col-md-2">
                                    <select class="form-control" id="pet_type" name="pet_type">
                                        <option value="" <?php if($selectedPet == '') echo 'selected'; ?>></option>
                                        <option value="dog" <?php if($selectedPet == 'dog') echo 'selected'; ?>>イヌ</option>
                                        <option value="cat" <?php if($selectedPet == 'cat') echo 'selected'; ?>>ネコ</option>
                                        <option value="hamster" <?php if($selectedPet == 'hamster') echo 'selected'; ?>>ハムスター</option>
                                        <option value="ferret" <?php if($selectedPet == 'ferret') echo 'selected'; ?>>フェレット</option>
                                        <option value="rabbit" <?php if($selectedPet == 'rabbit') echo 'selected'; ?>>ウサギ</option>
                                        <option value="bird" <?php if($selectedPet == 'bird') echo 'selected'; ?>>鳥</option>
                                        <option value="reptile" <?php if($selectedPet == 'reptile') echo 'selected'; ?>>爬虫類</option>
                                        <option value="others" <?php if($selectedPet == 'others') echo 'selected'; ?>>その他</option>
                                    </select>
                                </div>
                                <label for="pet_reg_name" class="col-md-2 col-form-label text-md-right">病院名</label>
                                <div class="col-md-2">
                                    <input id="hospital_name" type="text" class="form-control" name="hospital_name" placeholder="みるぺっとクリニック" value="{{isset($hospital_name)?$hospital_name:""}}">
                                </div>
                            </div>
                            <div class="form-group row mb-0 justify-content-md-center text-center">
                                <div class="col-md-12">
                                    @if(\Auth::check())
                                    <a href="{{URL::to('/app/owner')}}" class="btn btn-dark btn-lg">戻る</a>
                                    @endif
                                    <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                        検索
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title mb-2">検索結果</h3>
                <div class="card">
                    @php($count = 0)
                    @if(isset($clinics) && count($clinics))
                        @if($selectedPet == "")
                            @foreach($clinics as $clinic)
                                <div class="card mb-1">
                                    <div class="card-body">
                                        <div class="row">
                                            @isset($clinic['eyecatchimage'])
                                                <div class="col-md-3 mb-2">
                                                    @if($clinic['eyecatchimage'] != null)
                                                    <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}" style="max-height: 200px;">
                                                    @endif
                                                </div>
                                            @endisset
                                            <div class="col-md-9">
                                                <h4 class="mt-0 mb-1">{{$clinic['name']}}</h4>
                                                <div class="row">
                                                    <h6 class="mb-1 col-md-2">
                                                        所在地
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['prefecture'].$clinic['city'].$clinic['address']}}
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        電話番号
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['tel']}}
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        公式HP
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        <a href="{{$clinic['url']}}" target="_blank">{{$clinic['url']}}</a>
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        対応保険会社
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['insurance']}}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        @php($optionPet = json_decode($clinic['option_pet'],true))
                                        <p class="card-text">
                                            <span class="font-weight-bold small">診療対象</span><br>
                                        <div class="row">
                                            @if($optionPet != null)
                                            @foreach($optionPet as $pet=>$val)
                                                @if($val=='true')
                                                    @if($pet != "others")
                                                    <div class="col-6 col-md-2">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                                            style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                                    </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                            @foreach($optionPet as $pet=>$val)
                                                @if($val=='true')
                                                    @if($pet == "others")
                                                    <div class="col-6 col-md-2">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                                            style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                                    </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                            @endif
                                        </div>
                                        </p>
                                        <div>
                                            @if(Auth::check())
                                                <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'])}}" class="btn btn-warning">詳細</a>
                                                @if($clinic->isFavorite(Auth::id()) == 1)
                                                    <a href="javascript:void(0)" class="btn btn-default fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録済み</a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn btn-info fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録する</a>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        @else
                            @foreach($clinics as $clinic)
                                @php($clinicPet = json_decode($clinic['option_pet'], true))
                                @if($clinicPet != null && $clinicPet[$selectedPet] == true)
                                @php($count++)
                                <div class="card mb-1">
                                    <div class="card-body">
                                        <div class="row">
                                            @isset($clinic['eyecatchimage'])
                                                <div class="col-md-4 mb-2">
                                                    @if($clinic['eyecatchimage'] != null)
                                                    <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}" style="max-height: 50px;">
                                                    @endif
                                                </div>
                                            @endisset
                                            <div class="col-md-8">
                                                <h4 class="mt-0 mb-1">{{$clinic['name']}}</h4>
                                                <div class="row">
                                                    <h6 class="mb-1 col-md-2">
                                                        所在地
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['prefecture'].$clinic['city'].$clinic['address']}}
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        電話番号
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['tel']}}
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        公式HP
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        <a href="{{$clinic['url']}}" target="_blank">{{$clinic['url']}}</a>
                                                    </span>
                                                    <h6 class="mb-1 col-md-2">
                                                        保険会社
                                                    </h6>
                                                    <span class="mb-1 col-md-10 font-weight-normal">
                                                        {{$clinic['insurance']}}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        @php($optionPet = json_decode($clinic['option_pet'], true))
                                        <p class="card-text">
                                            <span class="font-weight-bold small">診療対象</span><br>
                                        <div class="row">
                                            @if($optionPet != null)
                                            @foreach($optionPet as $pet=>$val)
                                                @if($val)
                                                    <div class="col-6 col-md-2">
                                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                                            style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                                    </div>
                                                @endif
                                            @endforeach
                                            @endif
                                        </div>
                                        </p>
                                        <div>
                                            @if(Auth::check())
                                                <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'])}}" class="btn btn-warning">詳細</a>
                                                @if($clinic->isFavorite(Auth::id()) == 1)
                                                    <a href="javascript:void(0)" class="btn btn-default fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録済み</a>
                                                @else
                                                    <a href="javascript:void(0)" class="btn btn-info fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録する</a>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                @endif
                            @endforeach
                            @if($count == 0)
                                <div class="row justify-content-md-center text-center">
                                    <div class="col-md-12 m-5">
                                        <h4>該当する動物病院はありません</h4>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @else
                        <div class="row justify-content-md-center text-center">
                            <div class="col-md-12 m-5">
                                <h4>該当する動物病院はありません</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
