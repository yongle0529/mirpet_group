@extends('layouts.app.owner')

@section('content')
    <style>
        .table th {
            background-color: #F3EDE3;
            color: #BB8639;
        }
    </style>
    <div class="container">
    <h3 class="font-weight-bold mb-4 border-bottom border-dark">私の支払い履歴</h3>
    <div class="row">
        <div class="col-md-12">
            @if(count($payments))

                <div class="card mb-1">
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>支払日付</th>
                                        <th>動物</th>
                                        <th>対象（動物病院/担当医）</th>
                                        <th>支払金額（税込）</th>
                                    </tr>
                                    @foreach($payments as $diag)
                                    <tr>
                                        <td>{{$diag['created_at']}}</td>
                                        <td>{{\App\Pet::find($diag['pet_id'])->name}}</td>
                                        <td>{{\App\Clinic::find($diag['clinic_id'])->name}} / {{\App\Clinic::find($diag['clinic_id'])->manager_name}}</td>
                                        <td>{{$diag['price']}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            @else
                <div class="card mb-1">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>支払い履歴はありません。</h4>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
