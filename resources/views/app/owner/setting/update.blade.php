@extends('layouts.app.owner')

@section('content')
    <script>
        $(function () {
            $("#delete").click(function () {
                location.href = "{{URL::to('/app/owner/setting/delcc')}}";
            });
        });
    </script>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{URL::to('/app/owner/setting/profile')}}">
                            @csrf

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">氏名：姓</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="山本" value="{{ old('last_name',$user->last_name) }}" required
                                           autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">氏名：名</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="太郎" value="{{ old('first_name',$user->first_name) }}" required
                                           autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：セイ</label>

                                <div class="col-md-6">
                                    <input id="last_name_kana" type="text" class="form-control{{ $errors->has('last_name_kana') ? ' is-invalid' : '' }}" name="last_name_kana" placeholder="ヤマモト"
                                           value="{{ old('last_name_kana',$user->last_name_kana) }}"
                                           required autofocus>

                                    @if ($errors->has('last_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：メイ</label>

                                <div class="col-md-6">
                                    <input id="first_name_kana" type="text" class="form-control{{ $errors->has('first_name_kana') ? ' is-invalid' : '' }}" name="first_name_kana" placeholder="タロウ"
                                           value="{{ old('first_name_kana',$user->first_name_kana) }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('first_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="taro.yamamoto@mirpet.co.jp" value="{{ old('email',$user->email) }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">電話番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" placeholder="09012345678" value="{{ old('tel',$user->tel) }}" required autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="zip_code" class="col-md-4 col-form-label text-md-right">郵便番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" placeholder="1040061" value="{{ old('zip_code',$user->zip_code) }}" required
                                           autofocus>

                                    @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('prefecture') ? ' is-invalid' : '' }}" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            <option
                                                value="{{ $pref }}"
                                                {{ $pref == old('prefecture',$user->prefecture) ? 'selected' : $pref == '東京都' ? 'selected' : '' }}
                                            >{{ $pref }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('prefecture'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prefecture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">郡市区(島)</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="中央区" value="{{ old('city',$user->city) }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">それ以降の住所</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="銀座7丁目18-13 クオリア銀座504" value="{{ old('address',$user->address) }}"
                                           required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <a href="{{URL::to('/app/owner')}}" class="btn btn-default btn-lg">戻る</a>
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="font-weight-bold mb-4 border-bottom border-dark">支払い設定</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if(!empty($cardInfo))


                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="font-weight-bold mt-2 mb-2 border-bottom border-warning">クレジットカード情報</h5>
                                </div>
                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">カード会社</span>
                                </div>
                                <div class="col-6 text-left">
                                    <span>{{$cardInfo->brand}}</span>
                                </div>
                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">カード番号(下4桁)</span>
                                </div>
                                <div class="col-6 text-left">
                                    <span>XXXX XXXX XXXX {{$cardInfo->last4}}</span>
                                </div>

                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">有効期限</span>
                                </div>
                                <div class="col-6 text-left">
                                    <span>{{$cardInfo->exp_month}}月 / {{$cardInfo->exp_year}}年</span>
                                </div>
                                <div class="col-md-12 mt-3 text-center">
                                    <div class="small">※別のクレジットカードを登録する場合は、削除してから再度登録を行ってください。</div>
                                </div>

                            </div>
                            <div class="form-group row mb-0">

                                <div class="col-md-12 text-center">
                                    <a href="{{URL::to('/app/owner')}}" class="btn btn-default btn-lg">戻る</a>
                                    <a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#deleteModal">削除する</a>
                                </div>
                            </div>

                        @else
                            <div class="text-center mt-3">
                                <h5>クレジットカードが登録されていません</h5>
                                <form action="{{URL::to('/app/owner/payment')}}" method="post">
                                    <script src="https://checkout.pay.jp/" class="payjp-button"
                                            data-key="{{env('APP_ENV') == 'production' ? env('PAY_JP_PUBLIC_KEY') : env('PAY_JP_PUBLIC_KEY_TEST')}}"
                                            data-text="カードを登録する"
                                            data-submit-text="カードを登録する"></script>
                                    <script>
                                        $(function () {
                                            $('#payjp_cardSubmit').val('カードを登録する');
                                        });

                                    </script>
                                    @csrf
                                </form>
                                <a href="{{URL::to('/app/owner')}}" class="btn btn-default mt-3">戻る</a>
                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    クレジットカード登録情報を本当に削除しますか？
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button id="delete" type="button" class="btn btn-primary">削除</button>
                </div>
            </div>
        </div>
    </div>
@endsection
