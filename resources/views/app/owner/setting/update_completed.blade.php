@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            @if(isset($deleteCreditCard))
                クレジットカード登録情報の削除
            @elseif(isset($registerCreditCard))
                クレジットカード情報の登録
            @else
                アカウントの更新
            @endif

        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            @if(isset($deleteCreditCard))
                                <h4>クレジットカード登録情報の削除が完了しました</h4>
                            @elseif(isset($registerCreditCard))
                                <h4>クレジットカード情報の登録が完了しました</h4>
                            @else
                                <h4>アカウントの更新が完了しました</h4>
                            @endif
                            <a href="{{URL::to('/app/owner/setting/profile')}}" class="btn btn-primary btn-lg">戻る</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
