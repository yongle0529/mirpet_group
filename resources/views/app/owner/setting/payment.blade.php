@extends('layouts.app.owner')

@section('content')
    <script>
        $(function () {
            $("#delete").click(function () {
                location.href = "{{route('setting.deleteCreditCard')}}";
            });
        });
    </script>
    <div class="container">
    <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定 - 支払い情報</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    @if(!empty($cardInfo))


                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="font-weight-bold mt-2 mb-2 border-bottom border-warning">クレジットカード情報</h5>
                            </div>
                            <div class="col-6 text-right">
                                <span class="font-weight-bold">カード会社</span>
                            </div>
                            <div class="col-6 text-left">
                                <span>{{$cardInfo->brand}}</span>
                            </div>
                            <div class="col-6 text-right">
                                <span class="font-weight-bold">カード番号(下4桁)</span>
                            </div>
                            <div class="col-6 text-left">
                                <span>XXXX XXXX XXXX {{$cardInfo->last4}}</span>
                            </div>

                            <div class="col-6 text-right">
                                <span class="font-weight-bold">有効期限</span>
                            </div>
                            <div class="col-6 text-left">
                                <span>{{$cardInfo->exp_month}}月 / {{$cardInfo->exp_year}}年</span>
                            </div>
                            <div class="col-md-12 mt-3 text-center">
                                <div class="small">※別のクレジットカードを登録する場合は、削除してから再度登録を行ってください。</div>
                            </div>

                        </div>
                        <div class="form-group row mb-0">

                            <div class="col-md-12 text-center">
                                {{-- <a href="{{URL::to('/app/owner')}}" class="btn btn-default btn-lg">戻る</a> --}}
                                <a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#deleteModal">削除する</a>
                            </div>
                        </div>

                    @else
                        <div class="text-center mt-3">
                            <h5>クレジットカードが登録されていません</h5>
                            <form action="{{URL::to('/app/owner/payment')}}" method="post">
                                <script src="https://checkout.pay.jp/" class="payjp-button"
                                        data-key="{{env('APP_ENV') == 'production' ? env('PAY_JP_PUBLIC_KEY_TEST') : env('PAY_JP_PUBLIC_KEY_TEST')}}"
                                        data-text="カードを登録する"
                                        data-submit-text="カードを登録する"></script>
                                {{-- <script>
                                    $(function () {
                                        $('#payjp_cardSubmit').val('カードを登録する');
                                    });
                                </script> --}}
                                @csrf
                            </form>
                            <a href="{{URL::to('/app/owner')}}" class="btn btn-default mt-3">戻る</a>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    クレジットカード登録情報を本当に削除しますか？
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button id="delete" type="button" class="btn btn-primary">削除</button>
                </div>
            </div>
        </div>
    </div>
@endsection
