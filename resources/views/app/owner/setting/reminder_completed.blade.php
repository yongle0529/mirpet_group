@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            リマインダ変更
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>リマインダが変更されました。</h4>
                            <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
