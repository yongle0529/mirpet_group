@extends('layouts.app.owner')

@section('content')
    <style>
        input[type=number] {
            width: 50px;
            display: inline-block;
            margin-top: -1.5px;
            margin-left: 3px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
    </style>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定 - リマインダー設定</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="padding: 30px;">
                    <div class="card-title" style="/* padding: 20px; */">事前に診察情報のお知らせをいたします。 ご希望のものをお選びください。（複数可）
                    </div>
                    <div class="card-body col-md-4">
                        <form method="POST" action="{{URL::to('/app/owner/setting/reminder')}}">
                            @csrf
                            <div class="form-check row">
                                <label class="form-check-label">
                                    @if($oneday == 1)
                                    <input class="form-check-input" type="checkbox" name="oneday" checked>
                                    @else
                                    <input class="form-check-input" type="checkbox" name="oneday">
                                    @endif
                                    <span class="form-check-sign"> 1日前</span>
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    @if($datetime == 1)
                                    <input class="form-check-input" type="checkbox" name="datetime" checked>
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="50" min="0" name="datetime1" value="{{$datetime_remind->time}}"> 日前
                                    @else
                                    <input class="form-check-input" type="checkbox" name="datetime">
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="50" min="0" name="datetime1"> 日前
                                    @endif
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    @if($hour == 1)
                                    <input class="form-check-input" type="checkbox" name="hour" checked>
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="23" min="0" name="hour1" value="{{$hour_remind->time}}"> 時間前
                                    @else
                                    <input class="form-check-input" type="checkbox" name="hour">
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="23" min="0" name="hour1"> 時間前
                                    @endif
                                </label>
                            </div>
                            <br/>
                            <div class="form-check row">
                                <label class="form-check-label">
                                    @if($minute == 1)
                                    <input class="form-check-input" type="checkbox" name="minute" checked>
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="59" min="0" value="{{$minute_remind->time}}" name="minute1">分前
                                    {{-- @elseif($oneday == 0 && $datetime == 0 && $hour == 0 && $minute == 0)
                                    <input class="form-check-input" type="checkbox" name="minute" checked>
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="59" min="0" value="10" name="minute1">分前 --}}
                                    @else
                                    <input class="form-check-input" type="checkbox" name="minute">
                                    <span class="form-check-sign"></span>
                                    <input type="number" max="59" min="0" name="minute1">分前
                                    @endif
                                </label>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
