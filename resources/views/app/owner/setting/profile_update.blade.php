@extends('layouts.app.owner')

@section('content')
    <div class="container">
    <div align="right">
        <form action="{{ route('profile.delete', $user['id']) }}" class="profile_delete" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-primary" type="submit" onclick=""><i class="fa fa-trash-o"></i>アカウント解約</button>
        </form>
    </div>
    <h3 class="font-weight-bold mb-4 border-bottom border-dark">アカウント設定 - 基本情報</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{URL::to('/app/owner/setting/profile')}}">
                            @csrf

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">氏名：姓</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="山本" value="{{ old('last_name',$user->last_name) }}" required
                                           autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">氏名：名</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="太郎" value="{{ old('first_name',$user->first_name) }}" required
                                           autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：セイ</label>

                                <div class="col-md-6">
                                    <input id="last_name_kana" type="text" class="form-control{{ $errors->has('last_name_kana') ? ' is-invalid' : '' }}" name="last_name_kana" placeholder="ヤマモト"
                                           value="{{ old('last_name_kana',$user->last_name_kana) }}"
                                           required autofocus>

                                    @if ($errors->has('last_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="first_name_kana" class="col-md-4 col-form-label text-md-right">氏名(フリガナ)：メイ</label>

                                <div class="col-md-6">
                                    <input id="first_name_kana" type="text" class="form-control{{ $errors->has('first_name_kana') ? ' is-invalid' : '' }}" name="first_name_kana" placeholder="タロウ"
                                           value="{{ old('first_name_kana',$user->first_name_kana) }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('first_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="taro.yamamoto@mirpet.co.jp" value="{{ old('email',$user->email) }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tel" class="col-md-4 col-form-label text-md-right">電話番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" placeholder="09012345678" value="{{ old('tel',$user->tel) }}" required autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="zip_code" onKeyUp="AjaxZip3.zip2addr('zip_code', '', 'prefecture', 'city');" class="col-md-4 col-form-label text-md-right">郵便番号(ハイフンなし)</label>

                                <div class="col-md-6">
                                    <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" placeholder="1040061" value="{{ old('zip_code',$user->zip_code) }}" required
                                           autofocus>

                                    @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefecture" class="col-md-4 col-form-label text-md-right">都道府県</label>

                                <div class="col-md-6">
                                    <select class="form-control{{ $errors->has('prefecture') ? ' is-invalid' : '' }}" id="prefecture" name="prefecture">
                                        @foreach(config('const.PREFECTURE_LIST') as $pref)
                                            <option
                                                value="{{ $pref }}"
                                                {{ $pref == old('prefecture',$user->prefecture) ? 'selected' : $pref == '東京都' ? 'selected' : '' }}
                                            >{{ $pref }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('prefecture'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prefecture') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">郡市区(島)</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="中央区" value="{{ old('city',$user->city) }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">それ以降の住所</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="銀座7丁目18-13 クオリア銀座504" value="{{ old('address',$user->address) }}"
                                           required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    {{-- <a href="{{URL::to('/app/owner')}}" class="btn btn-default btn-lg">戻る</a> --}}
                                    <button type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    jQuery(document).ready(function($){
        $('.profile_delete').on('submit',function(e){
            if(!confirm('アカウントに保存されているデータはすべて削除され、以後、みるペットの機能は一切利用することができなくなります。\n本当に解約されますか？')){
                e.preventDefault();
            }
        });
    });
</script>
@endsection
