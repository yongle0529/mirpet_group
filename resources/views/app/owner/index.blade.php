@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">マイページ</h3>
        <div class="row">
            <div class="col-md-6">
                <h3 class="title mb-2">登録されているペット</h3>
                <div class="card" style="min-height: 250px;">

                    @if(count($pets))
                        @foreach($pets as $pet)
                            <div class="card mb-1">
                                <div class="card-body">

                                    <h4 class="card-title mt-0">
                                        @if($pet->profile_picture_path != null)
                                        <img class="mr-2" src="{{URL::to('/images/insurance/'.$pet->profile_picture_path)}}" width="40px" height="35px">{{$pet['name']}}
                                        @else
                                        <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{config('const.PET_ICON')[$pet['type']]}}" width="40px">{{$pet['name']}}
                                        @endif
                                    </h4>
                                    <h6 class="card-subtitle mb-2 text-muted">{{config('const.PET_TYPE')[$pet['type']]}} | {{config('const.PET_GENDER')[$pet['gender']]}} | {{$pet['genre']}} | {{str_replace('-','/',$pet['birthday'])}}
                                        生まれ</h6>
                                    <h6 class="card-subtitle mb-2 text-muted">
                                        保険証期限
                                        @if($pet['insurance_from_date'] != null && $pet['insurance_from_date'] != "0000-00-00")
                                        {{$pet['insurance_from_date'] . " ~ " . $pet['insurance_to_date']}}
                                        @endif
                                    </h6>
                                    <a href="{{URL::to('/app/owner/pet/update/'.$pet['id'])}}" class="card-link">編集する</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-12 mt-5 text-center">
                            <h5>ペットが登録されていません</h5>
                        </div>
                    @endif

                    <div class="col-md-12 mb-5 text-center mt-3">
                        <a href="{{URL::to('/app/owner/pet/register')}}" class="btn btn-primary btn-lg">ペットを登録する</a>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <h3 class="title mb-2">マイリスト</h3>
                <div class="card" style="min-height: 250px;">
                    @if(count($favoriteClinics))
                        @foreach($favoriteClinics as $fav)
                            @php($clinic = $fav->clinic)

                            <div class="card mb-1">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            @if($clinic['eyecatchimage'] != null)
                                            <img class="mx-auto d-block pt-2 pb-2" src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}">
                                            @endif
                                        </div>
                                        <div class="col-8">
                                            <h4 class="card-title mt-0">{{$clinic['name']}}</h4>
                                            {{--<h6 class="card-subtitle mb-2 text-muted">〒{{$clinic['zip_code']}}<br>{{$clinic['prefecture'].$clinic['city'].$clinic['address']}}</h6>--}}
                                            <div class="text-right">

                                                <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'].'?ref=mypage')}}" class="btn btn-warning">詳細</a>
                                                <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'].'?ref=mypage')}}" class="btn btn-primary">相談予約する</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-12 mt-5 text-center">
                            <h5>動物病院が登録されていません</h5>
                        </div>
                    @endif
                    <div class="col-md-12 text-center mt-3">
                        <a href="{{URL::to('/app/owner/clinic/search')}}" class="btn btn-primary btn-lg">動物病院を探す</a>
                    </div>
                    <div class="col-md-12 mb-5 text-center">
                        ご利用方法がわからないかたは<a href="#">こちら</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title mb-2">診察予約一覧</h3>
                @if(count($diags))
                    @foreach($diags as $diag)
                        @if($diag->status==0)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h4 class="mt-0 mb-1">予約内容概要</h4>
                                        <div class="row">
                                            <h6 class="mb-1 col-md-2">
                                                日時
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$diag['reserve_datetime']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                病院名
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                @php($clinic=\App\Clinic::find($diag['clinic_id']))
                                                {{$clinic['name']}}
                                            </span>
                                            <h6 class="mb-1 col-md-3">
                                                予約料（税込）
                                            </h6>
                                            <span class="mb-1 col-md-9 font-weight-normal">
                                                ￥{{number_format($diag['price'])}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                ペット名
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{\App\Pet::find($diag['pet_id'])->name}}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <form action="{{ route('reserve.delete', $diag->id) }}" class="reserve_delete" method="post">
                                            @csrf
                                            @method('DELETE')
                                            @if(substr($diag->reserve_datetime, 0, 10) > $curr_datetime)
                                            <button class="btn btn-dark" type="submit" onclick=""><i class="fa fa-trash-o"></i>キャンセル</button>
                                            @endif

                                            <a href="{{URL::to('/app/owner/clinic/update_detail/'.$diag->clinic_id.'/'.$diag['id'].'?ref=mypage')}}" class="btn btn-info">変更</a>

                                            <a href="{{route('reserve.detail', $diag->id)}}" class="btn btn-warning">内容確認</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                @else
                    <div class="card mb-1">
                        <div class="row justify-content-md-center text-center">
                            <div class="col-md-12 m-5">
                                <h4>診療待ちの予約はありません</h4>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title mb-2">診療履歴</h3>
                @if(count($finishedDiags))
                    @foreach($finishedDiags as $diag)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h4 class="mt-0 mb-1">診療履歴</h4>
                                        <div class="row">
                                            <h6 class="mb-1 col-md-2">
                                                ペット名
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{\App\Pet::find($diag['pet_id'])->name}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                内容
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$diag['content']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                確定料金
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                @php($accounting = \App\Accounting::where('clinic_id', $diag->clinic_id)->where('diagnosis_id', $diag->id)->first())
                                                @if(!empty($accounting) && $accounting->status == 2)
                                                {{$accounting['sum']}}
                                                @else
                                                未確定
                                                @endif
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                日時
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                @if($diag->status == 5)
                                                キャンセル
                                                @else
                                                {{$diag['start_datetime'].'~'.$diag['end_datetime']}}
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        @php($accounting = \App\Accounting::where('clinic_id', $diag->clinic_id)->where('diagnosis_id', $diag->id)->first())
                                        @if(!empty($accounting) && $accounting->next_remind_date!=null)
                                        <a href="{{URL::to('/app/owner/clinic/remind_detail/'.$diag->clinic_id.'/'.$diag['id'].'?ref=mypage')}}" class="btn btn-info">再診予約</a>
                                        @endif
                                        <a href="{{URL::to('/app/owner/clinic/reserve/check/'.$diag['id'])}}" class="btn btn-warning">内容確認</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="card mb-1">
                        <div class="row justify-content-md-center text-center">
                            <div class="col-md-12 m-5">
                                <h4>診療履歴はありません</h4>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3 class="title mb-2">予約リマインダ</h3>
                @if(count($finishedDiags))
                    @foreach($finishedDiags as $diag)
                        @php($accounting = \App\Accounting::where('clinic_id', $diag->clinic_id)->where('diagnosis_id', $diag->id)->first())
                        @if(!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_date)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h4 class="mt-0 mb-1">前回診療履歴</h4>
                                        <div class="row">
                                            <h6 class="mb-1 col-md-2">
                                                ペット名
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{\App\Pet::find($diag['pet_id'])->name}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                内容
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$diag['content']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                診察日時
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$diag['start_datetime'].'~'.$diag['end_datetime']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                再診予定日
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$accounting->next_remind_date}}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <a href="{{URL::to('/app/owner/clinic/remind_detail/'.$diag->clinic_id.'/'.$diag['id'].'?ref=mypage')}}" class="btn btn-info">再診予約</a>
                                        <a href="{{URL::to('/app/owner/clinic/reserve/check/'.$diag['id'])}}" class="btn btn-warning">内容確認</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                @else
                    <div class="card mb-1">
                        <div class="row justify-content-md-center text-center">
                            <div class="col-md-12 m-5">
                                <h4>診療履歴はありません</h4>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3 class="title mb-2">受診病院履歴</h3>
                @if(count($finishedClinics))
                    @foreach($finishedClinics as $clinic)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="row">
                                    @isset($clinic['eyecatchimage'])
                                        <div class="col-md-4 mb-2">
                                            @if($clinic['eyecatchimage'] != null)
                                            <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}" style="max-height: 50px;">
                                            @endif
                                        </div>
                                    @endisset
                                    <div class="col-md-8">
                                        <h4 class="mt-0 mb-1">{{$clinic['name']}}</h4>
                                        <div class="row">
                                            <h6 class="mb-1 col-md-2">
                                                所在地
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$clinic['prefecture'].$clinic['city'].$clinic['address']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                電話番号
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                {{$clinic['tel']}}
                                            </span>
                                            <h6 class="mb-1 col-md-2">
                                                公式HP
                                            </h6>
                                            <span class="mb-1 col-md-10 font-weight-normal">
                                                <a href="{{$clinic['url']}}" target="_blank">{{$clinic['url']}}</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @php($optionPet = json_decode($clinic['option_pet'],true))
                                <p class="card-text">
                                    <span class="font-weight-bold small">診療対象</span><br>
                                <div class="row">
                                    @if(!empty($optionPet))
                                    @foreach($optionPet as $pet=>$val)
                                        @if($val)
                                            <div class="col-6 col-md-2">
                                                <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                                        style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                            </div>
                                        @endif
                                    @endforeach
                                    @endif
                                </div>
                                </p>
                                <div>
                                    <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'])}}" class="btn btn-warning">詳細</a>
                                    @if($clinic->isFavorite(Auth::id()) == 1)
                                        <a href="javascript:void(0)" class="btn btn-default fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録済み</a>
                                    @else
                                        <a href="javascript:void(0)" class="btn btn-info fav-add" data-clinic-id="{{$clinic['id']}}">マイリストに登録する</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="card mb-1">
                        <div class="row justify-content-md-center text-center">
                            <div class="col-md-12 m-5">
                                <h4>診療履歴はありません</h4>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($){
            $('.reserve_delete').on('submit',function(e){
                if(!confirm('予約をキャンセルしますか?')){
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
