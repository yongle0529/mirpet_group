@extends('layouts.app.owner')

@php($weekOffset = app('request')->input('weekOffset',0))

@section('content')
    <style>
        .tableFixHead { overflow-y: auto; height: 600px; }
        .tableFixHead thead th { position: sticky; top: 0; }

        /* Just common table stuff. Really. */
        table  { border-collapse: collapse; width: 100%; }
        th, td { padding: 8px 16px; }
        th     { background:#eee; }
    </style>
    <script>
        $(function () {
            $("#code_flag").on('onchange', function () {
                $("#hospital_code").disabled = $("#code_flag").checked;
            });
        });
    </script>
    @if(isset($clinicMenu))
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    {{-- @php($clinic = $clinicMenu->clinic) --}}
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'])}}">{{$clinic['name']}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$clinicMenu['name']}}</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
                    <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">詳細</h5>
                    <h5 class="description" style="">
                        {!! nl2br(e($clinicMenu['description'])) !!}
                    </h5>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">保険適用の有無</h5>
                    <h5 class="description" style="">
                        {{$clinicMenu['is_insurance_applicable']==0?'保険適用不可':'保険適用可'}}
                    </h5>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">予約料（税込）</h5>
                    <div class="table-responsive">
                        @php($priceList = json_decode($clinicMenu['price_list'],true))
                        <table class="table">
                            <thead>
                            <tr class="bg-warning">
                                <th>目安時間</th>
                                <th>予約料（税込）</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($priceList as $item)
                                <tr>
                                    <td>{{$item['title']}}分程度</td>
                                    <td>￥{{number_format($item['price'])}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="col-md-12 tableFixHead">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">予約日時の選択</h5>
                    <div class="row">
                        <div class="col-6 text-left">
                            @if($weekOffset > 0)
                                <a href="{{Request::url()}}?weekOffset={{$weekOffset - 1}}" class="btn btn-warning text-black">＜ 前の一週間</a>
                            @endif
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{Request::url()}}?weekOffset={{$weekOffset + 1}}" class="btn btn-warning text-black">次の一週間 ＞</a>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <?php
                            $clinicInfo = $clinic->clinic_info;
                            $clinicMenuOption = $clinicMenu->option;
                            if (isset($clinicMenuOption)) {
                                $clinicMenuOption = json_decode($clinicMenuOption, true);
                            }
                            $hourArray = [];

                            if (isset($clinicInfo)) {
                                $decodedClinicInfo = json_decode($clinicInfo, true);

                                if (isset($decodedClinicInfo['businessHour'])) {
                                    foreach ($decodedClinicInfo['businessHour'] as $weekday => $hours) {
                                        if (isset($hours)) {
                                            foreach ($hours as $hour) {
                                                $hourArray[] = \Carbon\Carbon::parse($hour['start'])->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($hour['end'])->format('H:i');
                                            }
                                        }
                                    }
                                    $minHour = min($hourArray);
                                    $maxHour = max($hourArray);
                                    $hourArray = [];
                                    for ($i = \Carbon\Carbon::parse($minHour)->hour; $i <= \Carbon\Carbon::parse($maxHour)->hour; $i++) {

                                        switch ($clinicMenuOption['timeSlotType']) {
                                            case 0:
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':10')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':50')->format('H:i');
                                                break;
                                            case 1:
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':15')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':45')->format('H:i');
                                                break;
                                            case 2:
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                break;
                                            case 3:
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                break;
                                            case 4:
                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                break;
                                        }
                                    }
                                } else {
                                    \Illuminate\Support\Facades\Log::error('not set businessHour');
                                }
                            } else {
                                \Illuminate\Support\Facades\Log::error('not set clinic_info');
                            }
                            ?>

                            <?php
                            setlocale(LC_ALL, 'ja_JP.UTF-8');
                            $offsetDate = \Carbon\Carbon::now();
                            if ($weekOffset > 0) {
                                $addDays = $weekOffset * 7;
                                $offsetDate = $offsetDate->addDays($addDays);
                            }

                            $monthList = [];
                            $dateList = [];
                            $searchDateList = [];
                            for ($i = 0; $i < 7; $i++) {
                                if ($i > 0) {
                                    $offsetDate = $offsetDate->addDay();
                                }
                                if (isset($monthList[$offsetDate->format('Y年m月')])) {
                                    $monthList[$offsetDate->format('Y年m月')] = $monthList[$offsetDate->format('Y年m月')] + 1;
                                } else {
                                    $monthList[$offsetDate->format('Y年m月')] = 1;
                                }

                                $dateList[] = [
                                    'day' => $offsetDate->day,
                                    'dayOfWeek' => $offsetDate->formatLocalized('(%a)'),
                                    'dayOfWeekNum' => $offsetDate->dayOfWeek
                                ];
                                $searchDateList[] = $offsetDate->format('Y-m-d');
                            }

                            $reserveTimetables = \App\ReserveTimetable::where('clinic_id', $clinic->id)
                                ->whereBetween('date', [min($searchDateList), max($searchDateList)])
                                ->get();
                            ?>
                            <tr class="text-center">
                                <th rowspan="2" class="align-middle">日時</th>

                                @foreach($monthList as $month => $col)
                                    <th colspan="{{$col}}">{{$month}}</th>
                                @endforeach
                            </tr>
                            <tr class="text-center small">
                                @foreach($dateList as $day)
                                    <th class="{{$day['dayOfWeekNum'] == 0 ? 'table-danger':''}}{{$day['dayOfWeekNum'] == 6 ? 'table-info':''}}">
                                        <span class="d-block">{{$day['day']}}</span>
                                        {{$day['dayOfWeek']}}
                                    </th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($clinicMenuOption['timeSlotType'] == 0) {
                            $timeSlotList = config('const.TIME_SLOT')['10_MIN'];
                        }elseif ($clinicMenuOption['timeSlotType'] == 1) {
                            $timeSlotList = config('const.TIME_SLOT')['15_MIN'];
                        } else if ($clinicMenuOption['timeSlotType'] == 2) {
                            $timeSlotList = config('const.TIME_SLOT')['20_MIN'];
                        } else if ($clinicMenuOption['timeSlotType'] == 3) {
                            $timeSlotList = config('const.TIME_SLOT')['30_MIN'];
                        } else if ($clinicMenuOption['timeSlotType'] == 4) {
                            $timeSlotList = config('const.TIME_SLOT')['60_MIN'];
                        }
                        ?>

                        @foreach($hourArray as $hour)
                            <tr class="text-center">
                                <td>{{$hour}}</td>
                                @php($targetTimeSlot = array_search($hour, $timeSlotList))
                                @php($curr_time = date("H:i"))
                                @php($curr_week = date("w"))
                                @foreach($searchDateList as $i => $date)
                                    <?php
                                        $isMatch = 0;
                                        if(array_key_exists($dateList[$i]['dayOfWeekNum'], $decodedClinicInfo['businessHour'])) {
                                            $openHours = $decodedClinicInfo['businessHour'][$dateList[$i]['dayOfWeekNum']];
                                            foreach((array)$openHours as $openHour) {
                                                if ($dateList[$i]['dayOfWeekNum'] == $curr_week && \Carbon\Carbon::parse($openHour['start'])->format('H:i') < $curr_time) {
                                                    $isMatch = 0;
                                                } else {
                                                    if(\Carbon\Carbon::parse($openHour['start'])->format('H:i') <= $hour && $hour < \Carbon\Carbon::parse($openHour['end'])->format('H:i')) {
                                                        $isMatch = 1;
                                                        foreach($reserveTimetables as $timetable) {
                                                            if($date == $timetable->date && $targetTimeSlot == $timetable->time_slot) {
                                                                $isMatch = 2;
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                    @if($isMatch == 1)
                                        <td><a href="{{URL::to('/app/owner/clinic/reserve/register/'.$clinic->id.'/'.$clinicMenu->id.'?date='.\Carbon\Carbon::parse($date)->format('Y-m-d').'&slot='.$targetTimeSlot.'&time='.$hour)}}">◯</a></td>
                                    @elseif($isMatch == 2)
                                        <td>{{--<a href="{{URL::to('/app/owner/clinic/reserve/register/'.$clinic->id.'/'.$clinicMenu->id.'?date='.\Carbon\Carbon::parse($date)->format('Y-m-d').'&slot='.$targetTimeSlot.'&time='.$hour)}}" class="text-success font-weight-bold">--}}<b>済</b>{{--</a>--}}</td>
                                    @else
                                        <td>✕</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する相談・診療メニューはありません</h2>
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                </div>
            </div>
        </div>
    @endif
@endsection
