@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            @if(isset($isDeleted))
                予約削除
            @elseif(isset($isUpdate))
                予約変更
            @else
                予約登録
            @endif
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            @if(isset($isDeleted))
                                <h4>予約がキャンセルされました。</h4>
                            @elseif(isset($isUpdate))
                                <h4>予約が変更されました。</h4>
                            @else
                                <h4>予約が完了しました。</h4>
                                <h4>マイページから予約確認ができます。</h4>
                            @endif
                            <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
