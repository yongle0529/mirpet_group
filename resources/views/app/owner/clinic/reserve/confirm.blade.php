@extends('layouts.app.owner')

@section('content')
    @if(isset($diag))
        <?php $clinicMenuOption = json_decode($clinicMenu['option'],true); ?>

        <script>
        </script>
        <style>
            .table th {
                background-color: #F3EDE3;
                color: #BB8639;
            }
        </style>

        @if(isset($isUpdate))
        @endif

        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    @php($clinic = $clinicMenu->clinic)
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner')}}">マイページ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">請求金額確定</li>
                </ol>
            </nav>
            <div class="row">
                <!--<div class="col-md-12">
                    <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
                    <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
                </div> -->

                <div class="col-md-12 text-center">
                    <h3 class="mt-4 mb-2">ご請求金額を確定しました。</h3>
                    <h5 class="mt-0 mb-0">クレジットカードにてご請求させていただきます。</h5>
                </div>

                {{-- <div class="col-md-12 mt-4 mb-4">
                    <h5 class="mt-0 mb-0">次回予約目安日 10/21 ()</h5>
                </div> --}}
                <br/>
                <div class="col-md-12 text-center">
                    @if(!empty($accounting) && $accounting->next_remind_date != null)
                    <a href="{{URL::to('/app/owner/clinic/remind_detail/'.$diag->clinic_id.'/'.$diag['id'].'?ref=mypage')}}" class="btn btn-info">次回予約をする</a>
                    @endif
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-warning" style="margin-left: 20px">後で予約する</a>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する診察内容はありません。</h2>
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                </div>
            </div>
        </div>
    @endif
@endsection
