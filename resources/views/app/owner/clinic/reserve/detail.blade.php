@extends('layouts.app.owner')

@section('content')
    @if(isset($diag))
        <?php $clinicMenuOption = json_decode($clinicMenu['option'],true); ?>
        <script>
        </script>
        <style>
            .table th {
                background-color: #F3EDE3;
                color: #BB8639;
            }
        </style>

        @if(isset($isUpdate))
        @endif

        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    {{-- @php($clinic = $clinicMenu->clinic) --}}
                    {{-- @php($diag_files = \App\DiagFile::where('diag_id', $diag->id)) --}}
                    @php($clinicInfo = json_decode($clinic['clinic_info'], true))
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner')}}">マイページ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">予約内容</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
                    <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
                </div>
                <div class="col-md-12 text-right">
                    <a href="{{URL::to('/app/owner/clinic/update_detail/'.$diag->clinic_id.'/'.$diag['id'].'?ref=mypage')}}" class="btn btn-info">変更</a>
                    {{-- <a href="{{URL::to('/app/owner/clinic/reserve/update1/'.$diag['id'].'/'.$clinicMenu['id'])}}" class="btn btn-info">写真や動画をアップロード</a> --}}
                    {{-- <a href="{{URL::to('app/chat/'.$diag['id'])}}" class="btn btn-primary">オンライン診察を受ける</a> --}}
                    <form action="{{ route('app.owner.chat', $diag['id']) }}" class="reserve_delete" method="POST">
                        @csrf
                        <button class="btn btn-primary" <?php if($diag->reserve_datetime > $real_time) echo "disabled"; ?>>オンライン診察を受ける</button>
                    </form>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">予約内容の詳細</h5>

                    <h5 class="description" style="">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>日時</th>
                                    <td>{{$diag->reserve_datetime}}</td>
                                </tr>
                                <tr>
                                    <th>ペット名​</th>
                                    <td>{{\App\Pet::find($diag['pet_id'])->name}}</td>
                                </tr>
                                <tr>
                                    <th>相談・診療メニュー</th>
                                    <td>{{$clinicMenu->name}}</td>
                                </tr>
                                <tr>
                                    <th>病院名</th>
                                    <td>{{$clinic->name}}</td>
                                </tr>
                                <tr>
                                    <th>担当獣医師​</th>
                                    <td>{{$diag->clinic_name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </h5>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">問診内容</h5>

                    <h5 class="description" style="">
                        {!! nl2br($diag->content) !!}
                    </h5>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">データ</h5>
                    <div class="text-left">
                        <?php
                        $file_count = 0;
                        foreach($diag_files as $file) {
                            if($file->type == 2) {
                                $file_count++;
                            }
                        }
                        ?>
                        @foreach($diag_files as $file)
                            @if($file->type == 1)
                                <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                    @if($file_count > 0)
                                    <img src="{{URL::to('/images/insurance/'.$file->filename)}}" width="300px" style="margin-top: -160px">
                                    @else
                                    <img src="{{URL::to('/images/insurance/'.$file->filename)}}" width="300px">
                                    @endif
                                </a>
                            @elseif($file->type == 2)
                                <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                    <video width="300px" controls autoplay>
                                        <source src="{{URL::to('/images/insurance/'.$file->filename)}}" type="video/mp4">
                                    </video>
                                </a>
                            @endif
                        @endforeach
                    {{-- @foreach($diag_files as $file)
                        @if($file->type == 1)
                        <a href="{{URL::to('/app/popup_viewer/'.$file->filename.'/1')}}" target="_bank">
                        <img src="{{URL::to('/images/insurance/'.$file->filename)}}" width="300px" style="margin-top: -160px">
                        </a>
                        @elseif($file->type == 2)
                        <a href="{{URL::to('/app/popup_viewer/'.$file->filename.'/2')}}" target="_bank">
                        <video width="300px" controls autoplay>
                            <source src="{{URL::to('/images/insurance/'.$file->filename)}}" type="video/mp4">
                        </video>
                        </a>
                        @endif
                    @endforeach --}}
                    </div>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">予約料（税込）</h5>

                    <h5 class="description" style="">
                        ￥{{number_format($diag->price)}}<br/>
                        ※別途相談・診療費、システム利用料等がかかります
                    </h5>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">病院データ</h5>

                    @if(!empty($clinicInfo))
                        {{-- @isset($clinicInfo['eyecatchImages'])
                            @foreach($clinicInfo['eyecatchImages'] as $image)
                                <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$image)}}" width="100%">
                            @endforeach
                        @endisset --}}
                        <strong>名前</strong> : {!! nl2br(e($clinic['name'])) !!}
                        <br/>
                        <strong>住所</strong> : {{$clinic['prefecture'] . " " . $clinic['city'] . " " . $clinic['address']}}
                        <br/>
                        <strong>電話番号​</strong> : {!! nl2br(e($clinic['tel'])) !!}
                    @endif
                </div>
                <div class="col-md-12">
                    <form action="{{ route('app.owner.chat', $diag['id']) }}" class="reserve_delete" method="POST">
                        @csrf
                        <button class="btn btn-primary" <?php if($diag->reserve_datetime > $real_time) echo "disabled"; ?>>オンライン診察を受ける</button>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する予約内容はありません。</h2>
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                </div>
            </div>
        </div>
    @endif
@endsection
