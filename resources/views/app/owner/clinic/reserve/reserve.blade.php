<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport"/>

    <!-- styles -->
    <link href="{{URL::to('/')}}/css/jquery.fileuploader.css" media="all" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>

    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="{{URL::to('/')}}/js/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/js/custom.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>

    <style>
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }

        /* @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
        }

        .material-icons {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 24px;
            line-height: 1;
            letter-spacing: normal;
            text-transform: none;
            display: inline-block;
            white-space: nowrap;
            word-wrap: normal;
            direction: ltr;
            -webkit-font-feature-settings: 'liga';
            -webkit-font-smoothing: antialiased;
        }

        .image-uploader {
            min-height: 10rem;
            border: 1px solid #d9d9d9;
            position: relative;
        }
        .image-uploader.drag-over {
            background-color: #f3f3f3;
        }
        .image-uploader input[type="file"] {
            width: 0;
            height: 0;
            position: absolute;
            z-index: -1;
            opacity: 0;
        }
        .image-uploader .upload-text {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
        .image-uploader .upload-text i {
            display: block;
            font-size: 3rem;
            margin-bottom: 0.5rem;
        }
        .image-uploader .upload-text span {
            display: block;
        }
        .image-uploader.has-files .upload-text {
            display: none;
        }
        .image-uploader .uploaded {
            padding: 0.5rem;
            line-height: 0;
        }
        .image-uploader .uploaded .uploaded-image {
            display: inline-block;
            width: calc(16.6666667% - 1rem);
            padding-bottom: calc(16.6666667% - 1rem);
            height: 0;
            position: relative;
            margin: 0.5rem;
            background: #f3f3f3;
            cursor: default;
        }
        .image-uploader .uploaded .uploaded-image img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            position: absolute;
        }
        .image-uploader .uploaded .uploaded-image .delete-image {
            display: none;
            cursor: pointer;
            position: absolute;
            top: 0.2rem;
            right: 0.2rem;
            border-radius: 50%;
            padding: 0.3rem;
            background-color: rgba(0, 0, 0, 0.5);
            -webkit-appearance: none;
            border: none;
        }
        .image-uploader .uploaded .uploaded-image:hover .delete-image {
            display: block;
        }
        .image-uploader .uploaded .uploaded-image .delete-image i {
            color: #fff;
            font-size: 1.4rem;
        } */
    </style>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white;">
    <div class="container">
        <div class="navbar-translate">
            @guest
                <a class="navbar-brand" href="{{URL::to('/')}}" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @else
                <a class="navbar-brand" href="{{URL::to('/')}}/app/owner" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @endguest

            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">

                @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning btn-round" href="{{ route('register') }}">新規会員登録</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary btn-round" href="{{ route('login') }}">会員ログイン</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner')}}" style="color: black;">
                            マイページ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/clinic/search')}}" style="color: black;">
                            医療機関
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            お知らせ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            使い方
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/setting/profile')}}" style="color: black;">
                            アカウント設定
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-default" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">

    <div style="min-height: 80px;">

    </div>
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">

        @if(Request::is('app/owner/setting/*'))
            <div class="container">
                <div class="row mb-3">
                    <style>
                        .menu-btn {
                            min-width: 10vw;
                        }
                    </style>
                    <script>
                        $(function () {
                            var current = "{{Request::path()}}";
                            $('.menu-btn').each(function (index, element) {
                                var rel = $(this).attr('rel');
                                if (current.indexOf(rel) > -1) {
                                    $(element).addClass('btn-primary');
                                    $(element).removeClass('btn-default');
                                }
                            })
                        });
                    </script>

                    <a href="{{route('setting.profile')}}" class="menu-btn btn btn-default" rel="profile">基本情報</a>
                    <a href="{{route('setting.password')}}" class="menu-btn btn btn-default" rel="password">パスワード</a>
                    <a href="{{route('setting.payment')}}" class="menu-btn btn btn-default" rel="payment">支払い情報</a>
                    <a href="{{route('setting.payhistory')}}" class="menu-btn btn btn-default" rel="payhistory">支払い履歴</a>
                    <a href="{{route('setting.reminder')}}" class="menu-btn btn btn-default" rel="reminder">リマインダー設定</a>
                </div>
            </div>
        @endif
        @if(isset($clinicMenu) && isset($datetime))
            <?php $clinicMenuOption = json_decode($clinicMenu['option'],true); ?>
            <div class="container">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        {{-- @php($clinic = $clinicMenu->clinic) --}}
                        <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'])}}">{{$clinic['name']}}</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'].'/'.$clinicMenu['id'])}}">{{$clinicMenu['name']}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$datetime}}</li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
                        <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
                    </div>
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療予約</h5>
                        <div>
                            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('/app/owner/clinic/reserve/register') }}">
                                @csrf
                                <input type="hidden" id="clinic" name="clinic" value="{{$clinic->id}}" />
                                <input type="hidden" id="clinicmenu" name="clinicmenu" value="{{$clinicMenu->id}}" />
                                <input type="hidden" id="datetime" name="datetime" value="{{$datetime}}" />
                                <input type="hidden" id="slot" name="slot" value="{{request()->get('slot')}}" />
                                <input type="hidden" id="date" name="date" value="{{request()->get('date')}}" />
                                <input type="hidden" id="time_slot_type" name="time_slot_type" value="{{$clinicMenuOption['timeSlotType']}}" />

                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">病院コード</label>

                                    <div class="col-md-4">
                                        <input type="text" id="hospital_code" name="hospital_code" class="form-control" required>
                                    </div>
                                    <div class="col-md-2 form-check row" style="margin-left:10px; margin-top:5px">
                                        <label class="form-check-label">
                                            <input id="code_flag" class="form-check-input" type="checkbox" name="code_flag">
                                            <span class="form-check-sign"></span> なし
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="price" class="col-md-4 col-form-label text-md-right">予約料（税込）</label>

                                    <div class="col-md-6">
                                        <?php $priceList = json_decode($clinicMenu['price_list'], true); ?>

                                        <input type="text" class="form-control" id="price" name="price" value="{{ $priceList[0]['price'] }}" readonly/>
                                        <br/>&nbsp;&nbsp;&nbsp;※予約料とは別に、相談・診療費、システム利用料等がかかります。
                                        {{-- <select required id="price" class="form-control" name="price" placeholder="">
                                            @foreach($priceList as $item)
                                                <option
                                                    value="{{ $item['price'] }}"
                                                    {{ old('price') == $item['price'] ? 'selected' : '' }}
                                                >{{ $item['title'].' - ￥'.number_format($item['price']) }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('price'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif --}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">診察方法</label>

                                    <div class="col-md-3 form-check form-check-inline form-check-radio" style="margin-left:10px; margin-top:5px">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="online" value="0" checked>オンライン
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-check form-check-inline form-check-radio" style="margin-top:5px">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="online" value="1" disabled>来院
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">獣医師</label>

                                    <div class="col-md-6">
                                        @php($clinicnames = \App\ClinicDoctorName::where('clinic_id', $clinic->id)->get())
                                        <select class="form-control" id="clinic_name" name="clinic_name">
                                            <option value="{{$clinic->manager_name}}">{{$clinic->manager_name}}</option>
                                            @if(count($clinicnames) > 0)
                                                @foreach ($clinicnames as $clinicname)
                                                    <option value="{{$clinicname->clinic_name}}">{{$clinicname->clinic_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">ペット名（必須）</label>

                                    <div class="col-md-6">
                                        {!!Form::select('pet', $pets, old('pet'), ['class' => 'form-control'])!!}

                                        @if ($errors->has('pet'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('pet') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="content" class="col-md-4 col-form-label text-md-right">問診内容（必須）</label>

                                    <div class="col-md-6">
                                        <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" id="content" name="content" rows="4" placeholder="" required>{{ old('content') }}</textarea>

                                        @if ($errors->has('content'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('content') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">写真・動画の添付（任意）</label>
                                    <div class="col-md-6" id="file_upload_text">
                                        <input type="file" multiple="multiple" name="photos[]" id="photos" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv, .mov">
                                        {{-- <div id="input-images-1" class="input-images-1" style="padding-top: .5rem;"></div> --}}
                                        {{-- <div id="image_preview"></div>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-raised">
                                                <img id="img-pet_reg_insurance_front">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-default btn-file">
                                                    <span class="fileinput-new">ファイル選択</span>
                                                    <input type="file" multiple="multiple" name="photos[]" id="photos" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv">
                                                </span>
                                                <button type='button' id='remove' class='btn btn-danger btn-round'>削除</button>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>

                                <div class="form-group row mb-0 justify-content-md-center text-center">
                                    <div class="col-md-12">
                                        <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                            予約する
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>該当する相談・診療メニューはありません</h2>
                        <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                    </div>
                </div>
            </div>
        @endif
            <footer class="footer mt-3" data-background-color="black">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="{{URL::to('/app/owner/footer/company')}}" >
                                    会社概要
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/rule')}}">
                                    個人情報保護方針
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/transaction')}}">
                                    特定商取引法に基づく表記
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/userprotocal')}}">
                                    利用規約（飼い主様向け）
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Mirpet, Inc.
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script type="text/javascript">
    $('#code_flag').on('change', function() {
        document.getElementById('hospital_code').disabled = this.checked;
    });
</script>
</html>
