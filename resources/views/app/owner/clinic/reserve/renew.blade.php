{{-- @extends('layouts.app.owner')

@section('content') --}}
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport"/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/clinic.css" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <script src="{{URL::to('/')}}/assets/js/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/assets/css/jquery.datetimepicker.min.css"/>
    <script src="{{URL::to('/')}}/assets/js/jquery.datetimepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    {{-- <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script> --}}
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{URL::to('/')}}/assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>


    {{--<link href="{{URL::to('/')}}/assets/demo/demo.css" rel="stylesheet"/>--}}
    <style>
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }

        .tableFixHead { overflow-y: auto; height: 600px; }
        .tableFixHead thead th { position: sticky; top: 0; }

        /* Just common table stuff. Really. */
        table  { border-collapse: collapse; width: 100%; }
        th, td { padding: 8px 16px; }
        th     { background:#eee; }
    </style>

    <style type="text/css">
        #image_preview{

        padding: 10px;

        }

        #image_preview img{

        width: 200px;

        padding: 5px;

        }

        #image_preview video{

        width: 200px;

        padding: 5px;

        }
    </style>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white;">
    <div class="container">
        <div class="navbar-translate">
            @guest
                <a class="navbar-brand" href="{{URL::to('/')}}" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @else
                <a class="navbar-brand" href="{{URL::to('/')}}/app/owner" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @endguest

            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">

                @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning btn-round" href="{{ route('register') }}">新規会員登録</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary btn-round" href="{{ route('login') }}">会員ログイン</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner')}}" style="color: black;">
                            マイページ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/clinic/search')}}" style="color: black;">
                            医療機関
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            お知らせ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            使い方
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/setting/profile')}}" style="color: black;">
                            アカウント設定
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-default" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">

    <div style="min-height: 80px;">

    </div>
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">

        @if(Request::is('app/owner/setting/*'))
            <div class="container">
                <div class="row mb-3">
                    <style>
                        .menu-btn {
                            min-width: 10vw;
                        }
                    </style>
                    <script>
                        $(function () {
                            var current = "{{Request::path()}}";
                            $('.menu-btn').each(function (index, element) {
                                var rel = $(this).attr('rel');
                                if (current.indexOf(rel) > -1) {
                                    $(element).addClass('btn-primary');
                                    $(element).removeClass('btn-default');
                                }
                            })
                        });
                    </script>

                    <a href="{{route('setting.profile')}}" class="menu-btn btn btn-default" rel="profile">基本情報</a>
                    <a href="{{route('setting.password')}}" class="menu-btn btn btn-default" rel="password">パスワード</a>
                    <a href="{{route('setting.payment')}}" class="menu-btn btn btn-default" rel="payment">支払い情報</a>
                    <a href="{{route('setting.payhistory')}}" class="menu-btn btn btn-default" rel="payhistory">支払い履歴</a>
                    <a href="{{route('setting.reminder')}}" class="menu-btn btn btn-default" rel="reminder">リマインダー設定</a>
                </div>
            </div>
        @endif
    @if(isset($clinic))
    <?php
        if ($isUpdate) {
            $oldpet = old('pet');
            $oldclinicmenu = old('clinicmenu');
            $oldoption = old('option');
            $oldprice = old('price');
            $oldcontent = old('content');
        } else if ( $diag ) {
            $oldpet = $diag->pet_id;
            $oldclinicmenu = $diag->clinic_menu_id;
            $oldoption = $diag->option;
            $oldprice = $diag->price;
            $oldcontent = $diag->content;
        } else {
            $oldpet = '';
            $oldclinicmenu = '';
            $oldoption = '';
            $oldprice = '';
            $oldcontent = '';
        }
    ?>

        <style>
            .table th {
                background-color: #F3EDE3;
                color: #BB8639;
            }
        </style>
        @php($weekOffset = app('request')->input('weekOffset',0))
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner')}}">マイページ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">次回予約</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        @isset($clinic['eyecatchimage'])
                            <div class="col-md-4 mb-2">
                                @if($clinic['eyecatchimage'] != null)
                                <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}" style="max-height: 50px;">
                                @endif
                            </div>
                        @endisset
                        <div class="col-md-8">
                            <div class="row ">
                                <h4 class="mt-0 mb-1">{{$clinic['name']}}</h4>
                                <div class="col-md-5 text-right">
                                    <a href="{{URL::to('/app/owner/clinic/detail/'.$clinic['id'])}}" class="btn btn-primary">予約する</a>
                                </div>
                            </div>
                            <div class="row">
                                <h6 class="mb-1 col-md-2">
                                    所在地
                                </h6>
                                <span class="mb-1 col-md-10 font-weight-normal">
                                    {{$clinic['prefecture'].$clinic['city'].$clinic['address']}}
                                </span>
                                <h6 class="mb-1 col-md-2">
                                    電話番号
                                </h6>
                                <span class="mb-1 col-md-10 font-weight-normal">
                                    {{$clinic['tel']}}
                                </span>
                                <h6 class="mb-1 col-md-2">
                                    公式HP
                                </h6>
                                <span class="mb-1 col-md-10 font-weight-normal">
                                    <a href="{{$clinic['url']}}" target="_blank">{{$clinic['url']}}</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    @php($optionPet = json_decode($clinic['option_pet'],true))
                    <p class="card-text">
                        <span class="font-weight-bold small">診療対象</span><br>
                    <div class="row">
                        @if(!empty($optionPet))
                        @foreach($optionPet as $pet=>$val)
                            @if($val)
                                <div class="col-6 col-md-2">
                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                            style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                </div>
                            @endif
                        @endforeach
                        @endif
                        <div class="col-md-12">
                    <hr>
                    <h3 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">{{$clinicMenu['name']}}</h3>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">詳細</h5>
                    <h5 class="description" style="">
                        {!! nl2br(e($clinicMenu['description'])) !!}
                    </h5>
                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">保険適用の有無</h5>
                    <h5 class="description" style="">
                        {{$clinicMenu['is_insurance_applicable']==0?'保険適用不可':'保険適用可'}}
                    </h5>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">予約料（税込）</h5>
                    <div class="table-responsive">
                        @php($priceList = json_decode($clinicMenu['price_list'],true))
                        <table class="table">
                            <thead>
                            <tr class="bg-warning">
                                <th>目安時間</th>
                                <th>予約料（税込）</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($priceList as $item)
                                <tr>
                                    <td>{{$item['title']}}分程度</td>
                                    <td>￥{{number_format($item['price'])}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
                    </div>
                    </p>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療予約</h5>
                    <div>
                        <form method="POST" enctype="multipart/form-data" action="{{URL::to('reserve/renew/'.$diag->id.'/'.$clinicMenu->id)}}">
                            @csrf

                            <div class="form-group row tableFixHead">
                                {{-- <label for="pet" class="col-md-4 col-form-label text-md-right">予約日付</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="datetimepicker" name="reserve_date" value="{{$diag->reserve_datetime}}">
                                </div> --}}
                                <div class="row">
                                    <div class="col-6 text-left">
                                        @if($weekOffset > 0)
                                            <a href="{{Request::url()}}?weekOffset={{$weekOffset - 1}}" class="btn btn-warning text-black">＜ 前の一週間</a>
                                        @endif
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="{{Request::url()}}?weekOffset={{$weekOffset + 1}}" class="btn btn-warning text-black">次の一週間 ＞</a>
                                    </div>
                                </div>

                                <table class="table table-bordered">
                                    <thead>
                                        <?php

                                        $clinicInfo = $clinic->clinic_info;
                                        $clinicMenuOption = $clinicMenu->option;
                                        if (isset($clinicMenuOption)) {
                                            $clinicMenuOption = json_decode($clinicMenuOption, true);
                                        }
                                        $hourArray = [];

                                        if (isset($clinicInfo)) {
                                            $decodedClinicInfo = json_decode($clinicInfo, true);

                                            if (isset($decodedClinicInfo['businessHour'])) {

                                                foreach ($decodedClinicInfo['businessHour'] as $weekday => $hours) {

                                                    if (isset($hours)) {
                                                        foreach ($hours as $hour) {

                                                            $hourArray[] = \Carbon\Carbon::parse($hour['start'])->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($hour['end'])->format('H:i');
                                                        }
                                                    }
                                                }
                                                $minHour = min($hourArray);
                                                $maxHour = max($hourArray);
                                                $hourArray = [];
                                                for ($i = \Carbon\Carbon::parse($minHour)->hour; $i <= \Carbon\Carbon::parse($maxHour)->hour; $i++) {

                                                    switch ($clinicMenuOption['timeSlotType']) {
                                                        case 0:
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':10')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':50')->format('H:i');
                                                            break;
                                                        case 1:
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':15')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':45')->format('H:i');
                                                            break;
                                                        case 2:
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                            break;
                                                        case 3:
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                            break;
                                                        case 4:
                                                            $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                            break;
                                                    }

                                                }
                                            } else {
                                                \Illuminate\Support\Facades\Log::error('not set businessHour');
                                            }
                                        } else {
                                            \Illuminate\Support\Facades\Log::error('not set clinic_info');
                                        }
                                        ?>

                                        <?php
                                        setlocale(LC_ALL, 'ja_JP.UTF-8');
                                        $offsetDate = \Carbon\Carbon::now();
                                        if ($weekOffset > 0) {
                                            $addDays = $weekOffset * 7;
                                            $offsetDate = $offsetDate->addDays($addDays);
                                        }

                                        $monthList = [];
                                        $dateList = [];
                                        $searchDateList = [];
                                        for ($i = 0; $i < 7; $i++) {
                                            if ($i > 0) {
                                                $offsetDate = $offsetDate->addDay();
                                            }
                                            if (isset($monthList[$offsetDate->format('Y年m月')])) {
                                                $monthList[$offsetDate->format('Y年m月')] = $monthList[$offsetDate->format('Y年m月')] + 1;
                                            } else {
                                                $monthList[$offsetDate->format('Y年m月')] = 1;
                                            }

                                            $dateList[] = [
                                                'day' => $offsetDate->day,
                                                'dayOfWeek' => $offsetDate->formatLocalized('(%a)'),
                                                'dayOfWeekNum' => $offsetDate->dayOfWeek
                                            ];
                                            $searchDateList[] = $offsetDate->format('Y-m-d');
                                        }

                                        $reserveTimetables = \App\ReserveTimetable::where('clinic_id', $clinic->id)
                                            ->whereBetween('date', [min($searchDateList), max($searchDateList)])
                                            ->get();
                                        ?>
                                        <tr class="text-center">
                                            <th rowspan="2" class="align-middle">日時</th>

                                            @foreach($monthList as $month => $col)
                                                <th colspan="{{$col}}">{{$month}}</th>
                                            @endforeach
                                        </tr>
                                        <tr class="text-center small">
                                            @foreach($dateList as $day)
                                                <th class="{{$day['dayOfWeekNum'] == 0 ? 'table-danger':''}}{{$day['dayOfWeekNum'] == 6 ? 'table-info':''}}">
                                                    <span class="d-block">{{$day['day']}}</span>
                                                    {{$day['dayOfWeek']}}
                                                </th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($clinicMenuOption['timeSlotType'] == 0) {
                                        $timeSlotList = config('const.TIME_SLOT')['10_MIN'];
                                    }elseif ($clinicMenuOption['timeSlotType'] == 1) {
                                        $timeSlotList = config('const.TIME_SLOT')['15_MIN'];
                                    } else if ($clinicMenuOption['timeSlotType'] == 2) {
                                        $timeSlotList = config('const.TIME_SLOT')['20_MIN'];
                                    } else if ($clinicMenuOption['timeSlotType'] == 3) {
                                        $timeSlotList = config('const.TIME_SLOT')['30_MIN'];
                                    } else if ($clinicMenuOption['timeSlotType'] == 4) {
                                        $timeSlotList = config('const.TIME_SLOT')['60_MIN'];
                                    }

                                    ?>

                                    @foreach($hourArray as $hour)

                                        <tr class="text-center">
                                            <td>{{$hour}}</td>
                                            @php($targetTimeSlot = array_search($hour, $timeSlotList))
                                            @foreach($searchDateList as $i => $date)
                                                <?php
                                                    $isMatch = 0;
                                                    if(array_key_exists($dateList[$i]['dayOfWeekNum'], $decodedClinicInfo['businessHour'])) {
                                                        $openHours = $decodedClinicInfo['businessHour'][$dateList[$i]['dayOfWeekNum']];
                                                        foreach((array)$openHours as $openHour) {
                                                            if(\Carbon\Carbon::parse($openHour['start'])->format('H:i') <= $hour && $hour < \Carbon\Carbon::parse($openHour['end'])->format('H:i')) {
                                                                $isMatch = 1;
                                                                foreach($reserveTimetables as $timetable) {
                                                                    if($date == $accounting->next_remind_date) {
                                                                        $isMatch = 2;
                                                                        break;
                                                                    }
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                @if($isMatch == 1)
                                                    <td>✕</td>
                                                @elseif($isMatch == 2)
                                                    <td><a href="{{URL::to('/app/owner/clinic/reserve/remind_register/'.$clinic->id.'/'.$clinicMenu->id.'/'.$diag->id.'?date='.\Carbon\Carbon::parse($date)->format('Y-m-d').'&slot='.$targetTimeSlot.'&time='.$hour)}}">◯</a></td>
                                                @else
                                                    <td>✕</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する予約内容はありません。</h2>
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                </div>
            </div>
        </div>
    @endif
    <footer class="footer mt-3" data-background-color="black">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="{{URL::to('/app/owner/footer/company')}}" >
                                会社概要
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/rule')}}">
                                個人情報保護方針
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/transaction')}}">
                                特定商取引法に基づく表記
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/userprotocal')}}">
                                利用規約（飼い主様向け）
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright" id="copyright">
                    &copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script>
                    , Mirpet, Inc.
                </div>
            </div>
        </footer>
    </div>
</div>
</body>
<script type="text/javascript">
// $(document).ready(function () {
//     //DatePicker Example
//     $('#datetimepicker').datetimepicker();
// });
function editReserve(date, time) {
    document.getElementById('reserve_datetime').value = date + " " + time + ":00";
}
// $('td').click(function() {
//     $(this).css('backgroundColor', '#000');
// });
$("#photos").change(function(){

    $('#image_preview').html("");

    var total_file=document.getElementById("photos").files.length;
    var video_count = 0;
    for(var i=0;i<total_file;i++)
    {
        var name = event.target.files[i].name;
        //console.log("asdfgfhg"+name);
        var lastDot = name.lastIndexOf('.');
        var ext = name.substring(lastDot + 1);
        //console.log("asdfgfhg"+ext);
        if (ext != 'png' && ext != 'jpg' && ext != 'jpeg' && ext != 'gif') {
            video_count++;
        }

    }

    console.log(video_count);

    for(var i=0;i<total_file;i++)
    {
        var name = event.target.files[i].name;
        //console.log("asdfgfhg"+name);
        var lastDot = name.lastIndexOf('.');
        var ext = name.substring(lastDot + 1);
        //console.log("asdfgfhg"+ext);
        if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif') {
            if (video_count > 0) {
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"' style='margin-top: -110px'>");
            } else {
                $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        } else {
            $('#image_preview').append("<video controls autoplay><source src='"+URL.createObjectURL(event.target.files[i])+"' type='video/mp4'></video>");
        }

    }

});
</script>
</html>
