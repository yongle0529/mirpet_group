@extends('layouts.app.owner')

@section('content')
    @if(isset($diag))
        <?php $clinicMenuOption = json_decode($clinicMenu['option'],true); ?>
        <script>
            $(function () {
                $("#verify").on('click', function () {
                    $("#pay").show();
                });
            });
        </script>
        <style>
            .table th {
                background-color: #F3EDE3;
                color: #BB8639;
            }
        </style>

        @if(isset($isUpdate))
        @endif

        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    {{-- @php($clinic = $clinicMenu->clinic) --}}
                    @php($clinicInfo = json_decode($clinic['clinic_info'], true))
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner')}}">マイページ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">診察内容</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-0 mb-0">診察内容確認</h3>
                </div>
                <div class="col-md-8">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診察内容の詳細</h5>

                    <h5 class="description" style="">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>ペット名​</th>
                                    <td>{{\App\Pet::find($diag['pet_id'])->name}}</td>
                                </tr>
                                <tr>
                                    <th>診療日付​</th>
                                    <td>
                                        @if($diag->status == 5 || $diag->status == 10)
                                        キャンセル
                                        @else
                                        {{$diag->end_datetime}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>経過時間</th>
                                    <td>{{$diag->res_time}}</td>
                                </tr>
                                <tr>
                                    <th>診察項目</th>
                                    <td>{!! nl2br($diag->content) !!}</td>
                                </tr>
                                <tr>
                                    <th>担当獣医師​</th>
                                    <td>{{$diag->clinic_name}}</td>
                                </tr>
                                <tr>
                                    <th>病院データ</th>
                                    <td>
                                        名前 : {!! nl2br(e($clinic['name'])) !!}
                                        <br/>
                                        住所 : {{$clinic['prefecture'] . " " . $clinic['city'] . " " . $clinic['address']}}
                                        <br/>
                                        電話番号​ : {!! nl2br(e($clinic['tel'])) !!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </h5>

                </div>
                {{-- <div class="col-md-8">

                </div> --}}
                <div class="col-md-4">
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診察結果</h5>

                    <h5 class="description" style="">
                        @if(!empty($accounting) && $accounting->status == 2)
                            <strong>医師のコメント : </strong> {{$accounting->comment}}<br/>
                            <strong>再診予約日 : </strong> {{$accounting->next_remind_date}}<br/>
                            <strong>薬処方内容 : </strong>
                        @endif
                    </h5>

                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">確定料金（税込）</h5>

                    <h5 class="description" class="text-center">
                        {{-- <button href="{{URL::to('/customer/print-pdf/'.$diag['id'])}}"><button id="verify" class="btn btn-info" id="verify">明細の確認をする</button></button> --}}

                        {{--  @if(!empty($accounting)  && $accounting->status == 2)  --}}
                        @if(!empty($accounting) && $accounting->status == 2)
                            <strong>予約料 : </strong>{{$diag->price}}<br/>
                            <strong>相談・診療費 : </strong> {{$accounting->medical_expenses}}<br/>
                            <strong>システム利用料 : </strong> {{$systemfee->owner_fee}}<br/>
                            <strong>送料 : </strong> {{$accounting->delivery_cost}}<br/>
                            <strong>合計金額 : </strong> {{$accounting->sum}}<br/>
                        @else
                            未確定
                        @endif
                    </h5>
                    <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">データ</h5>
                    <h5 class="description" class="text-center">
                        @if(!empty($accounting) && $accounting->status == 2)
                            @if($accounting->origi_filename != null && $accounting->save_filename != null && $accounting->extension == 'pdf')
                            <a href="{{URL::to('/file/pdf/'.$accounting->save_filename.'.pdf')}}" target="_blank">{{$accounting->origi_filename}}</a>
                            @endif
                        @endif
                    </h5>
                    {{-- <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">病院データ</h5> --}}

                    {{-- @if(!empty($clinicInfo))
                        <strong>院長</strong> : {!! nl2br(e($clinicInfo['owner'])) !!}
                        &nbsp;&nbsp;&nbsp;
                        <strong>保険</strong> :
                        {!! nl2br(e($clinic['insurance'])) !!}

                        &nbsp;&nbsp;&nbsp;
                        <strong>病院記述</strong> : {!! nl2br(e($clinic['clinic_description'])) !!}
                    @endif--> --}}

                    <h5 class="description" class="text-center">&nbsp;</h5>
                </div>
                {{-- <div class="col-md-12">
                    @if(!empty($accounting) && $accounting->status == 2)
                        <h5 class="description" class="text-center">
                            <a href="{{route('reserve.confirm', $diag->id)}}"><button id="pay" class="btn btn-primary">請求を確定する</button></a>
                        </h5>
                    @else
                        <h5 class="description" class="text-center">
                            <a href="{{route('reserve.confirm', $diag->id)}}"><button id="pay" class="btn btn-primary" disabled>請求を確定する</button></a>
                        </h5>
                    @endif
                </div> --}}
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する予約内容はありません。</h2>
                    <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                </div>
            </div>
        </div>
    @endif
@endsection
