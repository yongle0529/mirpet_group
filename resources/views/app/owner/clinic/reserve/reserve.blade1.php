@extends('layouts.app.owner')

@section('content')
@if(isset($clinicMenu) && isset($datetime))
<?php $clinicMenuOption = json_decode($clinicMenu['option'], true); ?>
<div class="container">
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            {{-- @php($clinic = $clinicMenu->clinic) --}}
            <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'])}}">{{$clinic['name']}}</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'].'/'.$clinicMenu['id'])}}">{{$clinicMenu['name']}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$datetime}}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
            <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
        </div>
        <div class="col-md-12">
            <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療予約</h5>
            <div>
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('/app/owner/clinic/reserve/register') }}">
                    @csrf
                    <input type="hidden" id="clinic" name="clinic" value="{{$clinic->id}}" />
                    <input type="hidden" id="clinicmenu" name="clinicmenu" value="{{$clinicMenu->id}}" />
                    <input type="hidden" id="datetime" name="datetime" value="{{$datetime}}" />
                    <input type="hidden" id="slot" name="slot" value="{{request()->get('slot')}}" />
                    <input type="hidden" id="date" name="date" value="{{request()->get('date')}}" />
                    <input type="hidden" id="time_slot_type" name="time_slot_type" value="{{$clinicMenuOption['timeSlotType']}}" />

                    <div class="form-group row">
                        <label for="pet" class="col-md-4 col-form-label text-md-right">ペット名</label>

                        <div class="col-md-6">
                            {!!Form::select('pet', $pets, old('content'), ['class' => 'form-control'])!!}

                            @if ($errors->has('pet'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('pet') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="price" class="col-md-4 col-form-label text-md-right">料金</label>

                        <div class="col-md-6">
                            <select required id="price" class="form-control" name="price" placeholder="">
                                <?php $priceList = json_decode($clinicMenu['price_list'], true); ?>
                                @foreach($priceList as $item)
                                <option value="{{ $item['price'] }}" {{ old('price') == $item['price'] ? 'selected' : '' }}>{{ $item['title'].' - ￥'.number_format($item['price']) }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('price'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content" class="col-md-4 col-form-label text-md-right">問診内容（任意）</label>

                        <div class="col-md-6">
                            <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" id="content" name="content" rows="4" placeholder="" autofocus>{{ old('content') }}</textarea>

                            @if ($errors->has('content'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">写真、動画の貽付</label>
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                <div class="fileinput-new thumbnail img-raised">
                                    <img id="img-pet_reg_insurance_front">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                <div>
                                    <span class="btn btn-raised btn-round btn-default btn-file">
                                        <span class="fileinput-new">ファイル選択</span>
                                        <input type="file" multiple="multiple" name="photos[]" id="photos" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv">>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0 justify-content-md-center text-center">
                        <div class="col-md-12">
                            <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                予約する
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@else
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>該当する相談・診療メニューはありません</h2>
            <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
        </div>
    </div>
</div>
@endif
@endsection