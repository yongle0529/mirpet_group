@extends('layouts.app.owner')

@section('content')
<style type="text/css">
    #image_preview {

        border: 1px solid black;

        padding: 10px;

    }

    #image_preview img {

        width: 200px;

        padding: 5px;

    }
</style>
<script type="text/javascript">
    // $(document).ready(function () {
    //     //DatePicker Example
    //     $('#datetimepicker').datetimepicker();
    // });

    $("#photos").change(function() {

        $('#image_preview').html("");

        var total_file = document.getElementById("photos").files.length;

        for (var i = 0; i < total_file; i++)

        {

            $('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[i]) + "'>");

        }

    });

    // $('#photos').on('change', function(){ //on file input change
    //     if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
    //     {
    //         var data = $(this)[0].files; //this file data

    //         $.each(data, function(index, file){ //loop though each file
    //             if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
    //                 var fRead = new FileReader(); //new filereader
    //                 fRead.onload = (function(file){ //trigger function on successful read
    //                 return function(e) {
    //                     var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
    //                     $('#image_preview').append(img); //append image to output element
    //                 };
    //                 })(file);
    //                 fRead.readAsDataURL(file); //URL representing the file's data.
    //             }
    //         });

    //     }else{
    //         alert("Your browser doesn't support File API!"); //if File API is absent
    //     }
    // });
</script>
@if(isset($clinicMenu))
<?php $clinicMenuOption = json_decode($clinicMenu['option'], true); ?>
<div class="container">
    {{-- <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    @php($clinic = $clinicMenu->clinic)
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'])}}">{{$clinic['name']}}</a></li>
    <li class="breadcrumb-item" aria-current="page"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'].'/'.$clinicMenu['id'])}}">{{$clinicMenu['name']}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$datetime}}</li>
    </ol>
    </nav> --}}
    <div class="row">
        {{-- <div class="col-md-12">
                    <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
        <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
    </div> --}}
    <div class="col-md-12">
        <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療予約変更</h5>
        <div>
            <form method="POST" enctype="multipart/form-data" action="{{route('reserve.update', $diag->id)}}">
                @csrf
                <div class="form-group row">
                    <label for="pet" class="col-md-4 col-form-label text-md-right">予約日付</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" id="datetimepicker" name="reserve_date" value="{{$diag->reserve_datetime}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pet" class="col-md-4 col-form-label text-md-right">獣医師</label>
                    <div class="col-md-6">
                        <select class="form-control" id="prefecture" name="prefecture">
                            <option>山本</option>
                            <option>ふじだ</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pet" class="col-md-4 col-form-label text-md-right">診察方法</label>
                    <div class="form-check form-check-inline form-check-radio" style="margin-left: 10px; margin-top: 6px">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="option" checked>オンライン
                            <span class="form-check-sign"></span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline form-check-radio" style="margin-left: 10px; margin-top: 6px">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="option">来院
                            <span class="form-check-sign"></span>
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pet" class="col-md-4 col-form-label text-md-right">ペット名</label>

                    <div class="col-md-6">
                        {!!Form::select('pet', $pets, old('content'), ['class' => 'form-control'])!!}

                        @if ($errors->has('pet'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pet') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">料金</label>

                    <div class="col-md-6">
                        <select required id="price" class="form-control" name="price" placeholder="">
                            <?php $priceList = json_decode($clinicMenu['price_list'], true); ?>
                            @foreach($priceList as $item)
                            <option value="{{ $item['price'] }}" {{ old('price') == $item['price'] ? 'selected' : '' }}>{{ $item['title'].' - ￥'.number_format($item['price']) }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('price'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="content" class="col-md-4 col-form-label text-md-right">問診内容（任意）</label>

                    <div class="col-md-6">
                        <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" id="content" name="content" rows="4" placeholder="" autofocus>{{ old('content', $diag->content) }}</textarea>

                        @if ($errors->has('content'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">写真、動画の貽付</label>
                    <div class="col-md-6">
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-raised">
                                <img id="img-pet_reg_insurance_front">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                            <div>
                                <span class="btn btn-raised btn-round btn-default btn-file">
                                    <span class="fileinput-new">ファイル選択</span>
                                    <input type="file" multiple="multiple" name="photos[]" id="photos" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv">>
                                </span>
                            </div>
                        </div>
                        <div id="image_preview"></div>
                    </div>

                </div>

                <div class="form-group row mb-0 justify-content-md-center text-center">
                    <div class="col-md-12">
                        <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                            予約する
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@else
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>該当する相談・診療メニューはありません</h2>
            <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
        </div>
    </div>
</div>
@endif
@endsection