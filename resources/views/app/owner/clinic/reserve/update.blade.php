<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport"/>
    <!--     Fonts and icons     -->
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <!-- CSS Files -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet"> --}}
    <link href="{{URL::to('/')}}/css/font/font-fileuploader.css" rel="stylesheet">

    <!-- styles -->
    <link href="{{URL::to('/')}}/css/jquery.fileuploader.css" media="all" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,700|Montserrat:300,400,500,600,700|Source+Code+Pro&display=swap" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    {{-- <link href="{{URL::to('/')}}/css/image-uploader.min.css" rel="stylesheet"/> --}}
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/assets/css/jquery.datetimepicker.min.css"/>

    <!-- js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js" crossorigin="anonymous"></script>
    <script src="{{URL::to('/')}}/js/jquery.fileuploader.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/js/custom_update.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>


    {{--<link href="{{URL::to('/')}}/assets/demo/demo.css" rel="stylesheet"/>--}}
    <style>
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }

        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
        }

        .material-icons {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 24px;
            line-height: 1;
            letter-spacing: normal;
            text-transform: none;
            display: inline-block;
            white-space: nowrap;
            word-wrap: normal;
            direction: ltr;
            -webkit-font-feature-settings: 'liga';
            -webkit-font-smoothing: antialiased;
        }

        .image-uploader {
            min-height: 10rem;
            border: 1px solid #d9d9d9;
            position: relative;
        }
        .image-uploader.drag-over {
            background-color: #f3f3f3;
        }
        .image-uploader input[type="file"] {
            width: 0;
            height: 0;
            position: absolute;
            z-index: -1;
            opacity: 0;
        }
        .image-uploader .upload-text {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
        .image-uploader .upload-text i {
            display: block;
            font-size: 3rem;
            margin-bottom: 0.5rem;
        }
        .image-uploader .upload-text span {
            display: block;
        }
        .image-uploader.has-files .upload-text {
            display: none;
        }
        .image-uploader .uploaded {
            padding: 0.5rem;
            line-height: 0;
        }
        .image-uploader .uploaded .uploaded-image {
            display: inline-block;
            width: calc(16.6666667% - 1rem);
            padding-bottom: calc(16.6666667% - 1rem);
            height: 0;
            position: relative;
            margin: 0.5rem;
            background: #f3f3f3;
            cursor: default;
        }
        .image-uploader .uploaded .uploaded-image img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            position: absolute;
        }
        .image-uploader .uploaded .uploaded-image .delete-image {
            display: none;
            cursor: pointer;
            position: absolute;
            top: 0.2rem;
            right: 0.2rem;
            border-radius: 50%;
            padding: 0.3rem;
            background-color: rgba(0, 0, 0, 0.5);
            -webkit-appearance: none;
            border: none;
        }
        .image-uploader .uploaded .uploaded-image:hover .delete-image {
            display: block;
        }
        .image-uploader .uploaded .uploaded-image .delete-image i {
            color: #fff;
            font-size: 1.4rem;
        }

        .tableFixHead { overflow-y: auto; height: 600px; }
        .tableFixHead thead th { position: sticky; top: 0; }

        /* Just common table stuff. Really. */
        table  { border-collapse: collapse; width: 100%; }
        th, td { padding: 8px 16px; }
        th     { background:#eee; }
    </style>

    <style type="text/css">
        #image_preview{

        padding: 10px;

        }

        #image_preview img{

        width: 200px;

        padding: 5px;

        }

        #image_preview video{

        width: 200px;

        padding: 5px;

        }

        #image_preview_add{

        padding: 10px;

        }

        #image_preview_add img{

        width: 200px;

        padding: 5px;

        }

        #image_preview_add video{

        width: 200px;

        padding: 5px;

        }
    </style>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white;">
    <div class="container">
        <div class="navbar-translate">
            @guest
                <a class="navbar-brand" href="{{URL::to('/')}}" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @else
                <a class="navbar-brand" href="{{URL::to('/')}}/app/owner" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @endguest

            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">

                @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning btn-round" href="{{ route('register') }}">新規会員登録</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary btn-round" href="{{ route('login') }}">会員ログイン</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner')}}" style="color: black;">
                            マイページ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/clinic/search')}}" style="color: black;">
                            医療機関
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            お知らせ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            使い方
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/setting/profile')}}" style="color: black;">
                            アカウント設定
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-default" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">

    <div style="min-height: 80px;">

    </div>
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">

        @if(Request::is('app/owner/setting/*'))
            <div class="container">
                <div class="row mb-3">
                    <style>
                        .menu-btn {
                            min-width: 10vw;
                        }
                    </style>
                    <script>
                        $(function () {
                            var current = "{{Request::path()}}";
                            $('.menu-btn').each(function (index, element) {
                                var rel = $(this).attr('rel');
                                if (current.indexOf(rel) > -1) {
                                    $(element).addClass('btn-primary');
                                    $(element).removeClass('btn-default');
                                }
                            })
                        });
                    </script>

                    <a href="{{route('setting.profile')}}" class="menu-btn btn btn-default" rel="profile">基本情報</a>
                    <a href="{{route('setting.password')}}" class="menu-btn btn btn-default" rel="password">パスワード</a>
                    <a href="{{route('setting.payment')}}" class="menu-btn btn btn-default" rel="payment">支払い情報</a>
                    <a href="{{route('setting.payhistory')}}" class="menu-btn btn btn-default" rel="payhistory">支払い履歴</a>
                    <a href="{{route('setting.reminder')}}" class="menu-btn btn btn-default" rel="reminder">リマインダー設定</a>
                </div>
            </div>
        @endif
        @if(isset($clinicMenu))
            <?php $clinicMenuOption = json_decode($clinicMenu['option'],true); ?>
            @php($weekOffset = app('request')->input('weekOffset',0))
            <div class="container">
                {{-- <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        @php($clinic = $clinicMenu->clinic)
                        <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'])}}">{{$clinic['name']}}</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{URL::to('/app/owner/clinic/reserve/'.$clinic['id'].'/'.$clinicMenu['id'])}}">{{$clinicMenu['name']}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$datetime}}</li>
                    </ol>
                </nav> --}}
                <div class="row">
                    {{-- <div class="col-md-12">
                        <h5 class="mt-0 mb-0">{{$clinic['name']}}</h5>
                        <h3 class="mt-0 mb-0">{{$clinicMenu['name']}}</h3>
                    </div> --}}
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診療予約変更</h5>
                        <div>
                            <form method="POST" enctype="multipart/form-data" action="{{URL::to('/app/owner/clinic/reserve/update1/'.$diag['id'].'/'.$clinicMenu['id'])}}">
                                @csrf

                                <div class="form-group row tableFixHead">
                                    {{-- <label for="pet" class="col-md-4 col-form-label text-md-right">予約日付</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="datetimepicker" name="reserve_date" value="{{$diag->reserve_datetime}}">
                                    </div> --}}
                                    <div class="row">
                                        <div class="col-6 text-left">
                                            @if($weekOffset > 0)
                                                <a href="{{Request::url()}}?weekOffset={{$weekOffset - 1}}" class="btn btn-warning text-black">＜ 前の一週間</a>
                                            @endif
                                        </div>
                                        <div class="col-6 text-right">
                                            <a href="{{Request::url()}}?weekOffset={{$weekOffset + 1}}" class="btn btn-warning text-black">次の一週間 ＞</a>
                                        </div>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                            <?php
                                            $clinicInfo = $clinic->clinic_info;
                                            $clinicMenuOption = $clinicMenu->option;
                                            if (isset($clinicMenuOption)) {
                                                $clinicMenuOption = json_decode($clinicMenuOption, true);
                                            }
                                            $hourArray = [];

                                            if (isset($clinicInfo)) {
                                                $decodedClinicInfo = json_decode($clinicInfo, true);

                                                if (isset($decodedClinicInfo['businessHour'])) {
                                                    foreach ($decodedClinicInfo['businessHour'] as $weekday => $hours) {
                                                        if (isset($hours)) {
                                                            foreach ($hours as $hour) {
                                                                $hourArray[] = \Carbon\Carbon::parse($hour['start'])->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($hour['end'])->format('H:i');
                                                            }
                                                        }
                                                    }
                                                    $minHour = min($hourArray);
                                                    $maxHour = max($hourArray);
                                                    $hourArray = [];
                                                    for ($i = \Carbon\Carbon::parse($minHour)->hour; $i <= \Carbon\Carbon::parse($maxHour)->hour; $i++) {

                                                        switch ($clinicMenuOption['timeSlotType']) {
                                                            case 0:
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':10')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':50')->format('H:i');
                                                                break;
                                                            case 1:
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':15')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':45')->format('H:i');
                                                                break;
                                                            case 2:
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':20')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':40')->format('H:i');
                                                                break;
                                                            case 3:
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':30')->format('H:i');
                                                                break;
                                                            case 4:
                                                                $hourArray[] = \Carbon\Carbon::parse($i . ':00')->format('H:i');
                                                                break;
                                                        }
                                                    }
                                                } else {
                                                    \Illuminate\Support\Facades\Log::error('not set businessHour');
                                                }
                                            } else {
                                                \Illuminate\Support\Facades\Log::error('not set clinic_info');
                                            }
                                            ?>

                                            <?php
                                            setlocale(LC_ALL, 'ja_JP.UTF-8');
                                            $offsetDate = \Carbon\Carbon::now();
                                            if ($weekOffset > 0) {
                                                $addDays = $weekOffset * 7;
                                                $offsetDate = $offsetDate->addDays($addDays);
                                            }

                                            $monthList = [];
                                            $dateList = [];
                                            $searchDateList = [];
                                            for ($i = 0; $i < 7; $i++) {
                                                if ($i > 0) {
                                                    $offsetDate = $offsetDate->addDay();
                                                }
                                                if (isset($monthList[$offsetDate->format('Y年m月')])) {
                                                    $monthList[$offsetDate->format('Y年m月')] = $monthList[$offsetDate->format('Y年m月')] + 1;
                                                } else {
                                                    $monthList[$offsetDate->format('Y年m月')] = 1;
                                                }

                                                $dateList[] = [
                                                    'day' => $offsetDate->day,
                                                    'dayOfWeek' => $offsetDate->formatLocalized('(%a)'),
                                                    'dayOfWeekNum' => $offsetDate->dayOfWeek
                                                ];
                                                $searchDateList[] = $offsetDate->format('Y-m-d');
                                            }

                                            $reserveTimetables = \App\ReserveTimetable::where('clinic_id', $clinic->id)
                                                ->whereBetween('date', [min($searchDateList), max($searchDateList)])
                                                ->get();
                                            ?>
                                            <tr class="text-center">
                                                <th rowspan="2" class="align-middle">日時</th>

                                                @foreach($monthList as $month => $col)
                                                    <th colspan="{{$col}}">{{$month}}</th>
                                                @endforeach
                                            </tr>
                                            <tr class="text-center small">
                                                @foreach($dateList as $day)
                                                    <th class="{{$day['dayOfWeekNum'] == 0 ? 'table-danger':''}}{{$day['dayOfWeekNum'] == 6 ? 'table-info':''}}">
                                                        <span class="d-block">{{$day['day']}}</span>
                                                        {{$day['dayOfWeek']}}
                                                    </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if ($clinicMenuOption['timeSlotType'] == 0) {
                                            $timeSlotList = config('const.TIME_SLOT')['10_MIN'];
                                        }elseif ($clinicMenuOption['timeSlotType'] == 1) {
                                            $timeSlotList = config('const.TIME_SLOT')['15_MIN'];
                                        } else if ($clinicMenuOption['timeSlotType'] == 2) {
                                            $timeSlotList = config('const.TIME_SLOT')['20_MIN'];
                                        } else if ($clinicMenuOption['timeSlotType'] == 3) {
                                            $timeSlotList = config('const.TIME_SLOT')['30_MIN'];
                                        } else if ($clinicMenuOption['timeSlotType'] == 4) {
                                            $timeSlotList = config('const.TIME_SLOT')['60_MIN'];
                                        }
                                        ?>

                                        @foreach($hourArray as $hour)
                                            <tr class="text-center">
                                                <td>{{$hour}}</td>
                                                @php($targetTimeSlot = array_search($hour, $timeSlotList))
                                                @foreach($searchDateList as $i => $date)
                                                    <?php
                                                        $isMatch = 0;
                                                        if(array_key_exists($dateList[$i]['dayOfWeekNum'], $decodedClinicInfo['businessHour'])) {
                                                            $openHours = $decodedClinicInfo['businessHour'][$dateList[$i]['dayOfWeekNum']];
                                                            foreach((array)$openHours as $openHour) {
                                                                if(\Carbon\Carbon::parse($openHour['start'])->format('H:i') <= $hour && $hour < \Carbon\Carbon::parse($openHour['end'])->format('H:i')) {
                                                                    $isMatch = 1;
                                                                    foreach($reserveTimetables as $timetable) {
                                                                        if($date == $timetable->date && $targetTimeSlot == $timetable->time_slot) {
                                                                            $isMatch = 2;
                                                                            break;
                                                                        }
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                    @if($isMatch == 1)
                                                        <td><a href="javascript:editReserve('{{\Carbon\Carbon::parse($date)->format('Y-m-d')}}', '{{$hour}}', $('#menu'));">◯</a></td>
                                                    @elseif($isMatch == 2)
                                                        <td>済</td>
                                                    @else
                                                        <td>✕</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group row">
                                    <label id="menu" for="pet" class="col-md-4 col-form-label text-md-right">予約日付</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="reserve_datetime" name="reserve_datetime" value="{{$diag->reserve_datetime}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">病院コード</label>

                                    <div class="col-md-4">
                                        <input type="text" id="hospital_code" name="hospital_code" class="form-control" value="{{$diag->hospital_code}}" required/>
                                    </div>
                                    <div class="col-md-2 form-check row" style="margin-left:10px; margin-top:5px">
                                        <label class="form-check-label">
                                            <input id="code_flag" class="form-check-input" type="checkbox" name="code_flag">
                                            <span class="form-check-sign"></span> なし
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="price" class="col-md-4 col-form-label text-md-right">予約料（税込）</label>

                                    <div class="col-md-6">
                                        <?php $priceList = json_decode($clinicMenu['price_list'], true); ?>

                                        <input type="text" class="form-control" id="price" name="price" value="{{ $priceList[0]['price'] }}" readonly/>
                                        {{-- <select required id="price" class="form-control" name="price" placeholder="">
                                            @foreach($priceList as $item)
                                                <option
                                                    value="{{ $item['price'] }}"
                                                    {{ old('price') == $item['price'] ? 'selected' : '' }}
                                                >{{ $item['title'].' - ￥'.number_format($item['price']) }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('price'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">獣医師</label>
                                    <div class="col-md-6">
                                        @php($clinicnames = \App\ClinicDoctorName::where('clinic_id', $clinic->id)->get())
                                        <select class="form-control" id="clinic_name" name="clinic_name">
                                            <option value="{{$clinic->manager_name}}">{{$clinic->manager_name}}</option>
                                            @if(count($clinicnames) > 0)
                                                @foreach ($clinicnames as $clinicname)
                                                    <option value="{{$clinicname->clinic_name}}">{{$clinicname->clinic_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">診察方法</label>
                                    <div class="form-check form-check-inline form-check-radio" style="margin-left: 10px; margin-top: 6px">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="option" value="0" <?php if($diag->reserve_method == 0) echo 'checked'; ?>>オンライン
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio" style="margin-left: 10px; margin-top: 6px">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="option" value="1" <?php if($diag->reserve_method == 1) echo 'checked'; ?> disabled>来院
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pet" class="col-md-4 col-form-label text-md-right">ペット名（必須）</label>

                                    <div class="col-md-6">
                                        {!!Form::select('pet', $pets, old('pet'), ['class' => 'form-control'])!!}

                                        @if ($errors->has('pet'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('pet') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="content" class="col-md-4 col-form-label text-md-right">問診内容（必須）</label>

                                    <div class="col-md-6">
                                        <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" id="content" name="content" rows="4" placeholder="" required>{{ old('content', $diag->content) }}</textarea>

                                        @if ($errors->has('content'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('content') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">写真・動画の添付（任意）</label>
                                    <div class="col-md-6">
                                        <input type="file" multiple="multiple" name="photos[]" id="photos_upload" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv, .mov">
                                        {{-- <div class="input-images-1" style="padding-top: .5rem;"></div> --}}
                                        {{-- <div id="image_preview">
                                            @if(count($diag_files) > 0)
                                                $file_count = 0;
                                                foreach($diag_files as $file) {
                                                    if($file->type == 2) {
                                                        $file_count++;
                                                    }
                                                }
                                                ?>
                                                @foreach($diag_files as $file)
                                                    @if($file->type == 1)
                                                        <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                                            @if($file_count > 0)
                                                            <img src="{{URL::to('/images/insurance/'.$file->filename)}}" style="margin-top: -110px">
                                                            @else
                                                            <img src="{{URL::to('/images/insurance/'.$file->filename)}}" >
                                                            @endif
                                                        </a>
                                                    @elseif($file->type == 2)
                                                        <a href="{{URL::to('/images/insurance/'.$file->filename)}}" target="_bank">
                                                            <video controls autoplay>
                                                                <source src="{{URL::to('/images/insurance/'.$file->filename)}}" type="video/mp4">
                                                            </video>
                                                        </a>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                        <div id="image_preview_add"></div>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-raised">
                                                <img id="img-pet_reg_insurance_front">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-default btn-file">
                                                    <span class="fileinput-new">ファイル選択</span>
                                                    <input type="file" multiple="multiple" name="photos[]" id="photos" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv">>
                                                </span>
                                                <span id="add_photo" class="btn btn-raised btn-round btn-default btn-file">
                                                    <span class="fileinput-new">ファイル追加</span>
                                                    <input type="file" multiple="multiple" name="photos_add[]" id="photos_add" accept=".png, .jpg, .jpeg, .mp4, .avi, .wmv">>
                                                </span>
                                                <button type='button' id='remove' class='btn btn-danger btn-round'>削除</button>
                                            </div>
                                        </div> --}}
                                    </div>

                                </div>

                                <div class="form-group row mb-0 justify-content-md-center text-center">
                                    <div class="col-md-12">
                                        <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                            予約する
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>該当する相談・診療メニューはありません</h2>
                        <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">マイページへ戻る</a>
                    </div>
                </div>
            </div>
        @endif
            <footer class="footer mt-3" data-background-color="black">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="{{URL::to('/app/owner/footer/company')}}" >
                                    会社概要
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/rule')}}">
                                    個人情報保護方針
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/transaction')}}">
                                    特定商取引法に基づく表記
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/owner/footer/userprotocal')}}">
                                    利用規約（飼い主様向け）
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Mirpet, Inc.
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script type="text/javascript">
    var flag = "{{$flag}}";
    var file_url = "{{URL::to('/images/insurance')}}";
    var diag_files = <?php echo json_encode($diag_files); ?>;

    document.getElementById('code_flag').onchange = function() {
        document.getElementById('hospital_code').disabled = this.checked;
    };
    // var flag = "{{$flag}}";
    // var diag_files = <?php echo json_encode($diag_files); ?>;
    // var diagId = "{{$diag->id}}";
    // var clinicId = "{{$clinic->id}}";
    // var menuId = "{{$clinicMenu->id}}";
    // if (flag == 0) {
    //     $('.input-images-1').imageUploader();
    // } else {
    //     let preloaded = [];
    //     for(let i=0; i<flag; i++) {
    //         let src = "{{URL::to('/images/insurance/')}}" + "/" + diag_files[i].filename;
    //         let obj = {id: i, src: src};
    //         preloaded.push(obj);
    //     }
    //     $('.input-images-1').imageUploader({
    //         preloaded: preloaded,
    //         imagesInputName: 'photos',
    //         preloadedInputName: 'old'
    //     });
    // }

    function editReserve(date, time, element) {
        document.getElementById('reserve_datetime').value = date + " " + time + ":00";
        if (element.length != 0) {
            $("html, body").animate({
                scrollTop: element.offset().top - 75
            }, 1000);
        }
    }
    // $('td').click(function() {
    //     $(this).css('backgroundColor', '#000');
    // });
    // $("#photos").change(function(){

    //     $('#image_preview').html("");

    //     var total_file=document.getElementById("photos").files.length;
    //     var video_count = 0;
    //     for(var i=0;i<total_file;i++)
    //     {
    //         var name = event.target.files[i].name;
    //         //console.log("asdfgfhg"+name);
    //         var lastDot = name.lastIndexOf('.');
    //         var ext = name.substring(lastDot + 1);
    //         //console.log("asdfgfhg"+ext);
    //         if (ext != 'png' && ext != 'jpg' && ext != 'jpeg' && ext != 'gif') {
    //             video_count++;
    //         }

    //     }

    //     console.log(video_count);

    //     for(var i=0;i<total_file;i++)
    //     {
    //         var name = event.target.files[i].name;
    //         //console.log("asdfgfhg"+name);
    //         var lastDot = name.lastIndexOf('.');
    //         var ext = name.substring(lastDot + 1);
    //         //console.log("asdfgfhg"+ext);
    //         if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif') {
    //             if (video_count > 0) {
    //                 $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"' style='margin-top: -110px'>");
    //             } else {
    //                 $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
    //             }
    //         } else {
    //             $('#image_preview').append("<video controls autoplay><source src='"+URL.createObjectURL(event.target.files[i])+"' type='video/mp4'></video>");
    //         }

    //     }

    // });

    // $("#photos_add").change(function(){

    //     $('#image_preview_add').html("");

    //     var total_file=document.getElementById("photos_add").files.length;
    //     var video_count = 0;
    //     for(var i=0;i<total_file;i++)
    //     {
    //         var name = event.target.files[i].name;
    //         //console.log("asdfgfhg"+name);
    //         var lastDot = name.lastIndexOf('.');
    //         var ext = name.substring(lastDot + 1);
    //         //console.log("asdfgfhg"+ext);
    //         if (ext != 'png' && ext != 'jpg' && ext != 'jpeg' && ext != 'gif') {
    //             video_count++;
    //         }

    //     }

    //     console.log(video_count);

    //     for(var i=0;i<total_file;i++)
    //     {
    //         var name = event.target.files[i].name;
    //         //console.log("asdfgfhg"+name);
    //         var lastDot = name.lastIndexOf('.');
    //         var ext = name.substring(lastDot + 1);
    //         //console.log("asdfgfhg"+ext);
    //         if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'gif') {
    //             if (video_count > 0) {
    //                 $('#image_preview_add').append("<img src='"+URL.createObjectURL(event.target.files[i])+"' style='margin-top: -110px'>");
    //             } else {
    //                 $('#image_preview_add').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
    //             }
    //         } else {
    //             $('#image_preview_add').append("<video controls autoplay><source src='"+URL.createObjectURL(event.target.files[i])+"' type='video/mp4'></video>");
    //         }

    //     }

    // });

    // $(document).ready(function(){
    //     $("#remove").click(function(){
    //         $("#photos").val("");
    //         $("#image_preview").empty();
    //         $("#photos_add").val("");
    //         $("#image_preview_add").empty();
    //         // $("#add_photo").hide();
    //     });
    // });
</script>
</html>
