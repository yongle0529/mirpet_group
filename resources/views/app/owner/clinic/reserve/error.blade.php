@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            請求ミステーク
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            予約をする前にクレジットカードを登録しなければなりません。<br/>
                            <a href="{{URL::to('/app/owner/setting/payment')}}" class="btn btn-primary btn-lg">カードを登録する</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
