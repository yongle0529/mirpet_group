@extends('layouts.app.owner')

@section('content')
    @if(isset($clinic))
        <script>
            $(function () {
                $(".fav-add").click(function () {
                    $.ajax({
                        url: "{{ URL::to("/app/owner/clinic/fav") }}/{{$clinic['id']}}",
                        type: 'GET',
                        dataType: 'html',
                        success: function (data) {
                            var data = JSON.parse(data);
                            if (data.status == true) {
                                $(".fav-add").text('マイリストに登録済み');
                                $(".fav-add").removeClass('btn-info');
                                $(".fav-add").addClass('btn-default');
                            } else {
                                $(".fav-add").text('マイリストに登録する');
                                $(".fav-add").removeClass('btn-default');
                                $(".fav-add").addClass('btn-info');
                            }
                        },
                        error: function (data) {
                            alert('error');
                        }
                    });
                });
            });

            function scrollToElement(element) {

                if (element.length != 0) {
                    $("html, body").animate({
                        scrollTop: element.offset().top - 75
                    }, 1000);
                }
            }
        </script>
        @php($clinicInfo = json_decode($clinic['clinic_info'],true))
        <style>
            .table {
                text-align: center;
            }

            .table th {
                background-color: #F3EDE3;
                color: #BB8639;
            }
        </style>
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    @if($isRefFromMypage)
                        <li class="breadcrumb-item"><a href="{{URL::to('/app/owner')}}">マイページ</a></li>
                    @else
                        <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/search')}}">動物病院検索</a></li>
                    @endif

                    <li class="breadcrumb-item active" aria-current="page">{{$clinic['name']}}</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-4">
                    @if($clinic['eyecatchimage'] != null)
                    <img class="mx-auto d-block pt-2 pb-2" src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['eyecatchimage'])}}" style="max-width: 300px;">
                    @endif
                </div>
                <div class="col-md-8 text-right">
                    <h4 class="mt-0 mb-0">{{$clinic['name']}}</h4>
                    <div class="mt-0 mb-0 small">({{$clinic['name_kana']}})</div>
                    @if($isFavoriteClinic)
                        <a href="javascript:void(0)" class="btn btn-default fav-add">マイリストに登録済み</a>
                    @else
                        <a href="javascript:void(0)" class="btn btn-info fav-add">マイリストに登録する</a>
                    @endif
                    <a href="javascript:void(0)" onclick='scrollToElement($("#menu"))' class="btn btn-primary">相談予約する</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-2 mb-2 border-bottom border-warning">病院紹介</h5>
                        @if($clinic['clinic_info_photos'] != null)
                        <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$clinic['clinic_info_photos'])}}" width="100%">
                        @endif
                    <h5 class="description" style="">
                        {!! nl2br(e($clinic['clinic_description'])) !!}

                    </h5>

                    <h5 class="font-weight-bold mt-2 mb-2 border-bottom border-warning">対象動物</h5>
                    <div class="row">
                        @php($optionPet = json_decode($clinic['option_pet'],true))
                        @if($optionPet != null)
                        @foreach($optionPet as $pet=>$val)
                            @if($val)
                                <div class="col-6 col-md-4">
                                    <img class="mr-2" src="{{URL::to('/')}}/assets/img/icon/{{$pet}}_icon.png"
                                         style="width: 1.2rem;" alt="{{config('const.PET_TYPE_JP')[$pet]}}"><span style="font-size: 0.8rem;">{{config('const.PET_TYPE_JP')[$pet]}}</span>
                                </div>
                            @endif
                        @endforeach
                        @endif
                    </div>
                    <h5 id="menu" class="font-weight-bold mt-2 mb-2 border-bottom border-warning">相談・診療メニュー</h5>
                    @if(count($clinic_Menus))
                        @foreach($clinic_Menus as $clinic_menu)
                            <div class="card">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="p-2 font-weight-bold">{{$clinic_menu['name']}}</h5>
                                    </div>
                                    <div class="col-4">
                                        <div class="{{$clinic_menu['is_insurance_applicable']==0?'bg-default':'bg-primary'}} text-white p-1 text-center small">{{$clinic_menu['is_insurance_applicable']==0?'保険適用不可':'保険適用可'}}</div>
                                    </div>
                                    <div class="col-12">
                                        <div class="p-2">{!! nl2br(e($clinic_menu['description'])) !!}</div>
                                    </div>
                                    <div class="col-6">
                                        @php($min_Price = min(array_column(json_decode($clinic_menu['price_list'],true),'price')))
                                        <h5 class="mt-2 p-2 font-weight-bold">費用 ¥{{number_format($min_Price)}} 〜</h5>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="{{URL::to('/app/owner/clinic/userupdate_reserve/'.$clinic['id'].'/'.$clinic_menu['id'].'/'.$remind_diagid)}}" class="btn btn-primary mr-3">相談予約</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(count($clinic->clinicMenus))
                        @foreach($clinic->clinicMenus as $menu)
                            <div class="card">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="p-2 font-weight-bold">{{$menu['name']}}</h5>
                                    </div>
                                    <div class="col-4">
                                        <div class="{{$menu['is_insurance_applicable']==0?'bg-default':'bg-primary'}} text-white p-1 text-center small">{{$menu['is_insurance_applicable']==0?'保険適用不可':'保険適用可'}}</div>
                                    </div>
                                    <div class="col-12">
                                        <div class="p-2">{!! nl2br(e($menu['description'])) !!}</div>
                                    </div>
                                    <div class="col-6">
                                        @php($minPrice = min(array_column(json_decode($menu['price_list'],true),'price')))
                                        <h5 class="mt-2 p-2 font-weight-bold">費用 ¥{{number_format($minPrice)}} 〜</h5>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="{{URL::to('/app/owner/clinic/userupdate_reserve/'.$clinic['id'].'/'.$menu['id']).'/'.$remind_diagid}}" class="btn btn-primary mr-3">相談予約</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    {{-- @else
                        <div class="card">
                            <div class="row justify-content-md-center text-center">
                                <div class="col-md-12 m-5">
                                    <h4>利用できる診療メニューは現在ございません</h4>
                                </div>
                            </div>
                        </div> --}}
                    @endif

                </div>
                <div class="col-md-6">
                    <h5 class="font-weight-bold mt-2 mb-2 border-bottom border-warning">病院情報</h5>
                    <div class="row mb-3">
                        <div class="col-md-2 font-weight-bold">
                            所在地
                        </div>
                        <div class="col-md-10">
                            〒{{$clinic['zip_code']}}<br>
                            <a href="https://maps.google.com/maps?q={{$clinic['prefecture'].$clinic['city'].$clinic['address']}}" target="_blank">{{$clinic['prefecture'].$clinic['city'].$clinic['address']}}</a>
                            <iframe src="https://maps.google.co.jp/maps?&output=embed&q={{$clinic['prefecture'].$clinic['city'].$clinic['address']}}&z=15" style="border: none;width: 100%; height: 30vh;"></iframe>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-2 font-weight-bold">
                            電話番号
                        </div>
                        <div class="col-md-10">
                            <a href="tel:{{$clinic['tel']}}">{{$clinic['tel']}}</a>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-2 font-weight-bold">
                            公式HP
                        </div>
                        <div class="col-md-10">
                            <a href="{{$clinic['url']}}" target="_blank">{{$clinic['url']}}</a>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12 font-weight-bold">
                            オンライン相談・診療対応時間
                        </div>
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>月</th>
                                    @if(isset($clinicInfo['businessHour'][1]) && !empty($clinicInfo['businessHour'][1]))
                                        @php($count = count($clinicInfo['businessHour'][1]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][1][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][1][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][1][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][1][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][1][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][1][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][1][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][1][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][1][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][1][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>火</th>
                                    @if(isset($clinicInfo['businessHour'][2]) && !empty($clinicInfo['businessHour'][2]))
                                        @php($count = count($clinicInfo['businessHour'][2]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][2][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][2][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][2][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][2][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][2][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][2][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][2][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][2][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][2][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][2][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>水</th>
                                    @if(isset($clinicInfo['businessHour'][3]) && !empty($clinicInfo['businessHour'][3]))
                                        @php($count = count($clinicInfo['businessHour'][3]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][3][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][3][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][3][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][3][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][3][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][3][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][3][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][3][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][3][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][3][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>木</th>
                                    @if(isset($clinicInfo['businessHour'][4]) && !empty($clinicInfo['businessHour'][4]))
                                        @php($count = count($clinicInfo['businessHour'][4]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][4][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][4][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][4][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][4][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][4][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][4][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][4][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][4][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][4][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][4][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>金</th>
                                    @if(isset($clinicInfo['businessHour'][5]) && !empty($clinicInfo['businessHour'][5]))
                                        @php($count = count($clinicInfo['businessHour'][5]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][5][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][5][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][5][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][5][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][5][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][5][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][5][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][5][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][5][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][5][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th style="color: blue">土</th>
                                    @if(isset($clinicInfo['businessHour'][6]) && !empty($clinicInfo['businessHour'][6]))
                                        @php($count = count($clinicInfo['businessHour'][6]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][6][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][6][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][6][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][6][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][6][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][6][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][6][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][6][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][6][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][6][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif

                                </tr>
                                <tr>
                                    <th style="color: red">日</th>
                                    @if(isset($clinicInfo['businessHour'][0]) && !empty($clinicInfo['businessHour'][0]))
                                        @php($count = count($clinicInfo['businessHour'][0]))
                                        @php($m_start = "--")
                                        @php($m_end = "--")
                                        @php($a_start = "--")
                                        @php($a_end = "--")
                                        @if($clinicInfo['businessHour'][0][0]['start'] <= "13:00")
                                            @php($m_start = $clinicInfo['businessHour'][0][0]['start'])
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][0][$i]['start'] <= "13:00")
                                                    @php($m_end = $clinicInfo['businessHour'][0][$i]['start'])
                                                {{-- @else
                                                    @php($m_end = $clinicInfo['businessHour'][1][0]) --}}
                                                @endif
                                            @endfor
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][0][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][0][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][0][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][0][$i]['start'])
                                                @endif
                                            @endfor
                                        @else
                                            @php($m_start = "--")
                                            @php($m_end = "--")
                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][0][$i]['start'] > "13:00")
                                                    @php($a_start = $clinicInfo['businessHour'][0][$i]['start'])
                                                    @break
                                                @endif
                                            @endfor

                                            @for($i = 0; $i < $count; $i++)
                                                @if($clinicInfo['businessHour'][0][$i]['start'] > "13:00")
                                                    @php($a_end = $clinicInfo['businessHour'][0][$i]['start'])
                                                @endif
                                            @endfor
                                        @endif
                                        <td>{{$m_start . " ~ " . $m_end}}</td>
                                        <td>{{$a_start . " ~ " . $a_end}}</td>
                                    @else
                                        <td>--</td>
                                        <td>--</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th style="color: red">祝</th>
                                    <td>{{isset($clinicInfo['businessHour']['holiday'][0]['start'])?$clinicInfo['businessHour']['holiday'][0]['start']." ~ ".$clinicInfo['businessHour']['holiday'][0]['end']:"--"}}</td>
                                    <td>{{isset($clinicInfo['businessHour']['holiday'][1]['start'])?$clinicInfo['businessHour']['holiday'][1]['start']." ~ ".$clinicInfo['businessHour']['holiday'][1]['end']:"--"}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @else
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/app/owner/clinic/search')}}">動物病院検索</a></li>
                    <li class="breadcrumb-item active" aria-current="page"></li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>該当する病院はありません</h2>
                    <a href="{{URL::to('/app/owner/clinic/search')}}" class="btn btn-primary btn-lg">検索画面へ戻る</a>
                </div>
            </div>
        </div>
    @endif
@endsection
