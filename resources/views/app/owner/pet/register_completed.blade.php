@extends('layouts.app.owner')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
            @if(isset($isDeleted))
                ペット情報削除
            @elseif(isset($isUpdate))
                ペット情報更新
            @else
                ペット情報登録
            @endif
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            @if(isset($isDeleted))
                                <h4>ペットの登録情報の削除が完了しました</h4>
                            @elseif(isset($isUpdate))
                                <h4>ペットの登録情報の更新が完了しました</h4>
                            @else
                                <h4>ペットの登録が完了しました</h4>
                            @endif
                            <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">戻る</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
