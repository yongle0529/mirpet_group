{{-- @extends('layouts.app.owner')

@section('content') --}}
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/clinic.css" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <script src="{{URL::to('/')}}/assets/js/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/assets/css/jquery.datetimepicker.min.css"/>
    <script src="{{URL::to('/')}}/assets/js/jquery.datetimepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    {{-- <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script> --}}
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{URL::to('/')}}/assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>


    {{--<link href="{{URL::to('/')}}/assets/demo/demo.css" rel="stylesheet"/>--}}
    <style>
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }
    </style>

    <style type="text/css">
        #image_preview_front{

        padding: 10px;

        }

        #image_preview_front img{

        width: 200px;

        padding: 5px;

        }

        #image_preview_back{

        padding: 10px;

        }

        #image_preview_back img{

        width: 200px;

        padding: 5px;

        }

        #image_preview_pet{

        padding: 10px;

        }

        #image_preview_pet img{

        width: 200px;

        padding: 5px;

        }
    </style>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white;">
    <div class="container">
        <div class="navbar-translate">
            @guest
                <a class="navbar-brand" href="{{URL::to('/')}}" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @else
                <a class="navbar-brand" href="{{URL::to('/')}}/app/owner" data-placement="bottom">
                    <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
                </a>
            @endguest

            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">

                @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning btn-round" href="{{URL::to('/owner/protocol')}}">新規会員登録</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary btn-round" href="{{ route('login') }}">会員ログイン</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner')}}" style="color: black;">
                            マイページ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/clinic/search')}}" style="color: black;">
                            医療機関
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            お知らせ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: black;">
                            使い方
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/setting/profile')}}" style="color: black;">
                            アカウント設定
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-default" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">

    <div style="min-height: 80px;">

    </div>
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">

        @if(Request::is('app/owner/setting/*'))
            <div class="container">
                <div class="row mb-3">
                    <style>
                        .menu-btn {
                            min-width: 10vw;
                        }
                    </style>
                    <script>
                        $(function () {
                            var current = "{{Request::path()}}";
                            $('.menu-btn').each(function (index, element) {
                                var rel = $(this).attr('rel');
                                if (current.indexOf(rel) > -1) {
                                    $(element).addClass('btn-primary');
                                    $(element).removeClass('btn-default');
                                }
                            })
                        });
                    </script>

                    <a href="{{route('setting.profile')}}" class="menu-btn btn btn-default" rel="profile">基本情報</a>
                    <a href="{{route('setting.password')}}" class="menu-btn btn btn-default" rel="password">パスワード</a>
                    <a href="{{route('setting.payment')}}" class="menu-btn btn btn-default" rel="payment">支払い情報</a>
                    <a href="{{route('setting.payhistory')}}" class="menu-btn btn btn-default" rel="payhistory">支払い履歴</a>
                    <a href="{{route('setting.reminder')}}" class="menu-btn btn btn-default" rel="reminder">リマインダー設定</a>
                </div>
            </div>
        @endif
    <script>
        function insurance_view(id) {
            if (id == 1) {
                $("#insurance_name_area").show();
                $("#insurance_no_area").show();
                $("#insurance_date_area").show();
                $("#pet_reg_insurance_back_area").show();
                $("#pet_reg_insurance_front_area").show();
                document.getElementById("insurance_name").disabled = false;
                document.getElementById("insurance_no").disabled = false;
                document.getElementById("insurance_from_year").disabled = false;
                document.getElementById("insurance_from_month").disabled = false;
                document.getElementById("insurance_from_date").disabled = false;
                document.getElementById("insurance_to_year").disabled = false;
                document.getElementById("insurance_to_month").disabled = false;
                document.getElementById("insurance_to_date").disabled = false;
            } else {
                $("#insurance_name_area").hide();
                $("#insurance_no_area").hide();
                $("#pet_reg_insurance_back_area").hide();
                $("#pet_reg_insurance_front_area").hide();
                $("#insurance_date_area").hide();
                document.getElementById("insurance_name").disabled = true;
                document.getElementById("insurance_no").disabled = true;
                document.getElementById("insurance_from_year").disabled = true;
                document.getElementById("insurance_from_month").disabled = true;
                document.getElementById("insurance_from_date").disabled = true;
                document.getElementById("insurance_to_year").disabled = true;
                document.getElementById("insurance_to_month").disabled = true;
                document.getElementById("insurance_to_date").disabled = true;
            }
        };
        $(function () {
            $('select[name="pet_reg_type"]').change(function () {
                var radioval = $(this).val();
                if (radioval == 1) {
                    $('#dog_reg_genre_area').show();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("0");
                } else if (radioval == 2) {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').show();
                    $('#pet_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("1");
                } else if (radioval == 3) {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').show();
                    $('#pet_reg_genre_area').show();
                    $('#genre_option').val("3");
                } else {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_genre_area').show();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("2");
                }
            });

            $('input[name="pet_reg_agreement"]').change(function () {
                var isAgree = $('#pet_reg_agreement').prop('checked');

                if (isAgree) {
                    $('#submit_btn').prop("disabled", false);
                } else {
                    $('#submit_btn').prop("disabled", true);
                }
            });
        });
    </script>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">ペット情報登録</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ URL::to('/app/owner/pet/register') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="pet_reg_name" class="col-md-4 col-form-label text-md-right">ペットのお名前</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_name" type="text" class="form-control{{ $errors->has('pet_reg_name') ? ' is-invalid' : '' }}" name="pet_reg_name" placeholder="たろう" value="{{ old('pet_reg_name') }}" required autofocus>

                                    @if ($errors->has('pet_reg_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_name_kana" class="col-md-4 col-form-label text-md-right">ペットのお名前(カナ)</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_name_kana" type="text" class="form-control{{ $errors->has('pet_reg_name_kana') ? ' is-invalid' : '' }}" name="pet_reg_name_kana" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="タロウ" value="{{ old('pet_reg_name_kana') }}" required>

                                    @if ($errors->has('pet_reg_name_kana'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_name_kana') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_gender" class="col-md-4 col-form-label text-md-right">性別</label>

                                <div class="col-md-6">
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_gender" value="1" required {{ old('pet_reg_gender') == "1" ? 'checked' : '' }}>男の子
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_gender" value="2" {{ old('pet_reg_gender') == "2" ? 'checked' : '' }}>女の子
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>

                                    @if ($errors->has('pet_reg_gender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('pet_reg_gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_type" class="col-md-4 col-form-label text-md-right">動物の種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="pet_reg_type">
                                        <option value="1">イヌ</option>
                                        <option value="2">ネコ</option>
                                        <option value="4">ハムスター</option>
                                        <option value="5">フェレット</option>
                                        <option value="6">爬虫類</option>
                                        <option value="7">ウサギ</option>
                                        <option value="8">鳥</option>
                                        <option value="3">その他</option>
                                    </select>
                                    {{-- <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="1" required {{ old('pet_reg_type') == "1" ? 'checked' : '' }}>イヌ
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="2" {{ old('pet_reg_type') == "2" ? 'checked' : '' }}>ネコ
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="3" {{ old('pet_reg_type') == "3" ? 'checked' : '' }}>ハムスター
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="4" {{ old('pet_reg_type') == "4" ? 'checked' : '' }}>フェレット
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="5" {{ old('pet_reg_type') == "5" ? 'checked' : '' }}>爬虫類
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="6" {{ old('pet_reg_type') == "6" ? 'checked' : '' }}>ウサギ
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="7" {{ old('pet_reg_type') == "7" ? 'checked' : '' }}>鳥
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_type" value="8" {{ old('pet_reg_type') == "8" ? 'checked' : '' }}>その他
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div> --}}
                                    @if ($errors->has('pet_reg_type'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="genre_option" id="genre_option" value="0"/>
                            <div id="dog_reg_genre_area" class="form-group row">
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="dog_reg_genre" id="pet_reg_genre">
                                        <option value="アメリカン・コッカー・スパニエル">アメリカン・コッカー・スパニエル</option>
                                        <option value="イタリアン・グレーハウンド">イタリアン・グレーハウンド</option>
                                        <option value="イングリッシュ・コッカー・スパニエル">イングリッシュ・コッカー・スパニエル</option>
                                        <option value="ウエスト・ハイランド・ホワイト・テリア">ウエスト・ハイランド・ホワイト・テリア</option>
                                        <option value="ウェルシュ・コーギー・ペンブローク">ウェルシュ・コーギー・ペンブローク</option>
                                        <option value="カニンヘン・ダックスフンド">カニンヘン・ダックスフンド</option>
                                        <option value="キャバリア・キング・チャールズ・スパニエル">キャバリア・キング・チャールズ・スパニエル</option>
                                        <option value="グレート・ピレニーズ">グレート・ピレニーズ</option>
                                        <option value="ゴールデン・レトリーバー">ゴールデン・レトリーバー</option>
                                        <option value="シー・ズー">シー・ズー</option>
                                        <option value="シェットランド・シープドッグ">シェットランド・シープドッグ</option>
                                        <option value="シベリアン・ハスキー">シベリアン・ハスキー</option>
                                        <option value="ジャーマン・シェパード・ドッグ">ジャーマン・シェパード・ドッグ</option>
                                        <option value="ジャック・ラッセル・テリア">ジャック・ラッセル・テリア</option>
                                        <option value="スタンダード・プードル">スタンダード・プードル</option>
                                        <option value="セント・バーナード">セント・バーナード</option>
                                        <option value="ダルメシアン">ダルメシアン</option>
                                        <option value="チワワ">チワワ</option>
                                        <option value="トイ・プードル">トイ・プードル</option>
                                        <option value="ドーベルマン">ドーベルマン</option>
                                        <option value="バーニーズ・マウンテン・ドッグ">バーニーズ・マウンテン・ドッグ</option>
                                        <option value="パグ">パグ</option>
                                        <option value="パピヨン">パピヨン</option>
                                        <option value="ビーグル">ビーグル</option>
                                        <option value="ビション・フリーゼ">ビション・フリーゼ</option>
                                        <option value="フラットコーテッド・レトリーバー">フラットコーテッド・レトリーバー</option>
                                        <option value="ブルドッグ">ブルドッグ</option>
                                        <option value="フレンチ・ブルドッグ">フレンチ・ブルドッグ</option>
                                        <option value="ペキニーズ">ペキニーズ</option>
                                        <option value="ボーダー・コリー">ボーダー・コリー</option>
                                        <option value="ボクサー">ボクサー</option>
                                        <option value="ボストン・テリア">ボストン・テリア</option>
                                        <option value="ポメラニアン">ポメラニアン</option>
                                        <option value="ボルゾイ">ボルゾイ</option>
                                        <option value="マルチーズ">マルチーズ</option>
                                        <option value="ミックス">ミックス</option>
                                        <option value="ミニチュア・シュナウザー">ミニチュア・シュナウザー</option>
                                        <option value="ミニチュア・ダックスフンド">ミニチュア・ダックスフンド</option>
                                        <option value="ミニチュア・ピンシャー">ミニチュア・ピンシャー</option>
                                        <option value="ヨークシャー・テリア">ヨークシャー・テリア</option>
                                        <option value="ラブラドール・レトリーバー">ラブラドール・レトリーバー</option>
                                        <option value="紀州">紀州</option>
                                        <option value="甲斐">甲斐</option>
                                        <option value="四国">四国</option>
                                        <option value="柴">柴</option>
                                        <option value="秋田">秋田</option>
                                        <option value="日本スピッツ">日本スピッツ</option>
                                        <option value="北海道">北海道</option>
                                        <option value="狆">狆</option>
                                        <option value="その他">その他</option>
                                    </select>
                                </div>
                            </div>
                            <div id="cat_reg_genre_area" class="form-group row" style="display: none">
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="cat_reg_genre" id="pet_reg_genre">
                                        <option value="アビシニアン">アビシニアン</option>
                                        <option value="アメリカンカール">アメリカンカール</option>
                                        <option value="アメリカンショートヘア">アメリカンショートヘア</option>
                                        <option value="アメリカンボブテイル">アメリカンボブテイル</option>
                                        <option value="アメリカンワイヤーヘア">アメリカンワイヤーヘア</option>
                                        <option value="エキゾチックショートヘア">エキゾチックショートヘア</option>
                                        <option value="エジプシャンマウ">エジプシャンマウ</option>
                                        <option value="オシキャット">オシキャット</option>
                                        <option value="オリエンタル">オリエンタル</option>
                                        <option value="キムリック">キムリック</option>
                                        <option value="コーニッシュレックス">コーニッシュレックス</option>
                                        <option value="コラット">コラット</option>
                                        <option value="サイベリアン">サイベリアン</option>
                                        <option value="ジャパニーズボブテイル">ジャパニーズボブテイル</option>
                                        <option value="シャム">シャム</option>
                                        <option value="シャルトリュー">シャルトリュー</option>
                                        <option value="シンガプーラ">シンガプーラ</option>
                                        <option value="スコティッシュフォールド">スコティッシュフォールド</option>
                                        <option value="スノーシュー">スノーシュー</option>
                                        <option value="スフィンクス">スフィンクス</option>
                                        <option value="セルカークレックス">セルカークレックス</option>
                                        <option value="ソマリ">ソマリ</option>
                                        <option value="ターキッシュアンゴラ">ターキッシュアンゴラ</option>
                                        <option value="ターキッシュバン">ターキッシュバン</option>
                                        <option value="デボンレックス">デボンレックス</option>
                                        <option value="トンキニーズ">トンキニーズ</option>
                                        <option value="ノルウェージャンフォレストキャット">ノルウェージャンフォレストキャット</option>
                                        <option value="バーマン">バーマン</option>
                                        <option value="バーミーズ">バーミーズ</option>
                                        <option value="ハバナブラウン">ハバナブラウン</option>
                                        <option value="バリニーズ">バリニーズ</option>
                                        <option value="ピクシーボブ">ピクシーボブ</option>
                                        <option value="ヒマラヤン">ヒマラヤン</option>
                                        <option value="ブリティッシュショートヘア">ブリティッシュショートヘア</option>
                                        <option value="ペルシャ">ペルシャ</option>
                                        <option value="ベンガル">ベンガル</option>
                                        <option value="ボンベイ">ボンベイ</option>
                                        <option value="マンクス">マンクス</option>
                                        <option value="マンチカン">マンチカン</option>
                                        <option value="ミックス">ミックス</option>
                                        <option value="メインクーン">メインクーン</option>
                                        <option value="ラガマフィン">ラガマフィン</option>
                                        <option value="ラグドール">ラグドール</option>
                                        <option value="ラパーマ">ラパーマ</option>
                                        <option value="ロシアンブルー">ロシアンブルー</option>
                                        <option value="日本猫">日本猫</option>
                                        <option value="その他">その他</option>
                                    </select>
                                </div>
                            </div>
                            <div id="pet_reg_type_other_area" class="form-group row" style="display: none">
                                <label for="pet_reg_type_other" class="col-md-4 col-form-label text-md-right">動物の種類(その他)</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_type_other" type="text" class="form-control{{ $errors->has('pet_reg_type_other') ? ' is-invalid' : '' }}" name="pet_reg_type_other" placeholder="ハムスター" value="{{ old('pet_reg_type_other') }}">

                                    @if ($errors->has('pet_reg_type_other'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_type_other') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div id="pet_reg_genre_area" class="form-group row" style="display: none">
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_genre" type="text" class="form-control{{ $errors->has('pet_reg_genre') ? ' is-invalid' : '' }}" name="pet_reg_genre" placeholder="ポメラニアン" value="{{ old('pet_reg_genre') }}">

                                    @if ($errors->has('pet_reg_genre'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_genre') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">生年月日</label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_year" class="form-control" name="pet_reg_birth_year" placeholder="年">
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ old('pet_reg_birth_year') == $year ? 'selected' : '' }}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_month" class="form-control" name="pet_reg_birth_month" placeholder="月">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ old('pet_reg_birth_month') == $month ? 'selected' : '' }}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_date" class="form-control" name="pet_reg_birth_date" placeholder="日">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ old('pet_reg_birth_date') == $day ? 'selected' : '' }}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('pet_reg_birth_year'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('pet_reg_birth_year') }}</strong>
                                        </span>
                                    @endif
                                    @if ($errors->has('pet_reg_birth_month'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('pet_reg_birth_month') }}</strong>
                                        </span>
                                    @endif
                                    @if ($errors->has('pet_reg_birth_date'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('pet_reg_birth_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="insurance_option" class="col-md-4 col-form-label text-md-right">保険加入</label>

                                <div class="col-md-6">
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" onclick="insurance_view(1)" name="insurance_option" value="1" required checked>有り
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" onclick="insurance_view(0)" name="insurance_option" value="0">無し
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_reg_insurance_front_area" class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">保険証(表の写真)</label>
                                <div class="col-md-6">
                                    <div id="image_preview_front"></div>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            <img id="img-pet_reg_insurance_front">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_reg_insurance_front" id="pet_reg_insurance_front" accept=".png, .jpg, .jpeg">
                                            </span>
                                            <button type="button" id="front_remove" class="btn btn-danger btn-round">削除</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_reg_insurance_back_area" class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">保険証(裏の写真)</label>
                                <div class="col-md-6">
                                    <div id="image_preview_back"></div>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            <img id="img-pet_reg_insurance_back">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_reg_insurance_back" id="pet_reg_insurance_back" accept=".png, .jpg, .jpeg">
                                            </span>
                                            <button type="button" id="back_remove" class="btn btn-danger btn-round">削除</button>
                                            {{-- <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="now-ui-icons ui-1_simple-remove"></i> Remove</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_name_area">
                                <label for="insurance_name" class="col-md-4 col-form-label text-md-right">保険会社名</label>

                                <div class="col-md-6">
                                    <input id="insurance_name" type="text" class="form-control" name="insurance_name" required>
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_no_area">
                                <label for="insurance_no" class="col-md-4 col-form-label text-md-right">保険証番号</label>

                                <div class="col-md-6">
                                    <input id="insurance_no" type="text" class="form-control" name="insurance_no" required>
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_date_area">
                                <label class="col-md-4 col-form-label text-md-right">保険証の期限</label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_year" class="form-control" name="insurance_from_year" placeholder="年" required>
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year + 4)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ old('insurance_from_year') == $year ? 'selected' : '' }}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_month" class="form-control" name="insurance_from_month" placeholder="月" required>
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ old('insurance_from_month') == $month ? 'selected' : '' }}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_date" class="form-control" name="insurance_from_date" placeholder="日" required>
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ old('insurance_from_date') == $day ? 'selected' : '' }}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/><br/>
                                <label class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_year" class="form-control" name="insurance_to_year" placeholder="年" required>
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year + 4)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ old('insurance_to_year') == $year ? 'selected' : '' }}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_month" class="form-control" name="insurance_to_month" placeholder="月" required>
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ old('insurance_to_month') == $month ? 'selected' : '' }}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_date" class="form-control" name="insurance_to_date" placeholder="日" required>
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ old('insurance_to_date') == $day ? 'selected' : '' }}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_image" class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">ペットの写真</label>
                                <div class="col-md-6">
                                    <div id="image_preview_pet"></div>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            <img id="img-pet_reg_insurance_front">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_profile_image" id="pet_profile_image" accept=".png, .jpg, .jpeg">
                                            </span>
                                            <button type="button" id="profile_remove" class="btn btn-danger btn-round">削除</button>
                                            {{-- <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="now-ui-icons ui-1_simple-remove"></i> Remove</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0 justify-content-md-center text-center">
                                <div class="col-md-12">
                                    <a href="{{URL::to('/app/owner')}}" class="btn btn-dark btn-lg">戻る</a>
                                    <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                        ペットを登録する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer mt-3" data-background-color="black">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="{{URL::to('/app/owner/footer/company')}}" >
                                会社概要
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/rule')}}">
                                個人情報保護方針
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/transaction')}}">
                                特定商取引法に基づく表記
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{URL::to('/app/owner/footer/userprotocal')}}">
                                利用規約（飼い主様向け）
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright" id="copyright">
                    &copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script>
                    , Mirpet, Inc.
                </div>
            </div>
        </footer>
    </div>
</div>
</body>
<script type="text/javascript">
$("#pet_reg_insurance_front").change(function(){

    $('#image_preview_front').html("");

    var total_file=document.getElementById("pet_reg_insurance_front").files.length;

    for(var i=0;i<total_file;i++)

    {

        $('#image_preview_front').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");

    }

});

$("#pet_reg_insurance_back").change(function(){

    $('#image_preview_back').html("");

    var total_file=document.getElementById("pet_reg_insurance_back").files.length;

    for(var i=0;i<total_file;i++)

    {

        $('#image_preview_back').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");

    }

});

$("#pet_profile_image").change(function(){

    $('#image_preview_pet').html("");

    var total_file=document.getElementById("pet_profile_image").files.length;

    for(var i=0;i<total_file;i++)

    {

        $('#image_preview_pet').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");

    }

});

$(document).ready(function(){
    $("#front_remove").click(function(){
        $("#pet_reg_insurance_front").val("");
        $("#image_preview_front").empty();
    });
});

$(document).ready(function(){
    $("#back_remove").click(function(){
        $("#pet_reg_insurance_back").val("");
        $("#image_preview_back").empty();
    });
});

$(document).ready(function(){
    $("#profile_remove").click(function(){
        $("#pet_profile_image").val("");
        $("#image_preview_pet").empty();
    });
});
</script>
</html>
