@extends('layouts.app.owner')

@section('content')
    <script>
        $(document).ready(function(){
            $("#front_remove").click(function(){
                $("#pet_reg_insurance_front").val("");
                $("#img-pet_reg_insurance_front").attr('src', '');
            });
        });

        $(document).ready(function(){
            $("#back_remove").click(function(){
                $("#pet_reg_insurance_back").val("");
                $("#img-pet_reg_insurance_back").attr('src', '');
            });
        });

        $(document).ready(function(){
            $("#profile_remove").click(function(){
                $("#pet_profile_image").val("");
                $("#img-pet_profile_image").attr('src', '');
            });
        });
        function insurance_view(id) {
            if (id == 1) {
                $("#insurance_name_area").show();
                $("#insurance_no_area").show();
                $("#insurance_date_area").show();
                $("#pet_reg_insurance_back_area").show();
                $("#pet_reg_insurance_front_area").show();
            } else {
                $("#insurance_name_area").hide();
                $("#insurance_no_area").hide();
                $("#pet_reg_insurance_back_area").hide();
                $("#pet_reg_insurance_front_area").hide();
                $("#insurance_date_area").hide();
            }
        };
        $(function () {
            $('select[name="pet_reg_type"]').change(function () {
                var radioval = $(this).val();
                if (radioval == 1) {
                    $('#dog_reg_genre_area').show();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("0");
                } else if (radioval == 2) {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').show();
                    $('#pet_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("1");
                } else if (radioval == 3) {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_type_other_area').show();
                    $('#pet_reg_genre_area').show();
                    $('#genre_option').val("3");
                } else {
                    $('#dog_reg_genre_area').hide();
                    $('#cat_reg_genre_area').hide();
                    $('#pet_reg_genre_area').show();
                    $('#pet_reg_type_other_area').hide();
                    $('#genre_option').val("2");
                }
            });
            $("#delete").click(function () {
                location.href = "{{ URL::to("/app/owner/pet/delete/".$pet->id) }}";
            });

            /* upload image preview */
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                }

            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    //画像でない場合は処理終了
                    if(input.files[0].type.indexOf("image") < 0){
                        alert("画像ファイルを指定してください。");
                        return false;
                    }

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img-'+input.id).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    return true;
                }
                return false;
            }

            /* function changeImgSelButLabel() {
                $('.fileinput-new').hide();
                $('.fileinput-exists').show();
            } */

            $("#pet_reg_insurance_front, #pet_reg_insurance_back, #pet_profile_image").change(function(){
                return readURL(this);
            });

        });
    </script>
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">ペット登録情報更新</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ URL::to("/app/owner/pet/update/".$pet->id) }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="pet_reg_name" class="col-md-4 col-form-label text-md-right">ペットのお名前</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_name" type="text" class="form-control{{ $errors->has('pet_reg_name') ? ' is-invalid' : '' }}" name="pet_reg_name" placeholder="たろう" value="{{$pet->name}}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_name_kana" class="col-md-4 col-form-label text-md-right">ペットのお名前(カナ)</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_name_kana" type="text" class="form-control{{ $errors->has('pet_reg_name_kana') ? ' is-invalid' : '' }}" name="pet_reg_name_kana" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="タロウ" value="{{$pet->name_kana}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_gender" class="col-md-4 col-form-label text-md-right">性別</label>

                                <div class="col-md-6">
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_gender" value="1" required {{ $pet->gender == "1" ? 'checked' : '' }}>男の子
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="pet_reg_gender" value="2" {{ $pet->gender == "2" ? 'checked' : '' }}>女の子
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pet_reg_type" class="col-md-4 col-form-label text-md-right">動物の種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="pet_reg_type">
                                        <option value="1" {{$pet->type == 1 ? 'selected' : ''}}>イヌ</option>
                                        <option value="2" {{$pet->type == 2 ? 'selected' : ''}}>ネコ</option>
                                        <option value="4" {{$pet->type == 4 ? 'selected' : ''}}>ハムスター</option>
                                        <option value="5" {{$pet->type == 5 ? 'selected' : ''}}>フェレット</option>
                                        <option value="6" {{$pet->type == 6 ? 'selected' : ''}}>爬虫類</option>
                                        <option value="7" {{$pet->type == 7 ? 'selected' : ''}}>ウサギ</option>
                                        <option value="8" {{$pet->type == 8 ? 'selected' : ''}}>鳥</option>
                                        <option value="3" {{$pet->type == 3 ? 'selected' : ''}}>その他</option>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="genre_option" id="genre_option" value="0"/>
                            <div id="dog_reg_genre_area" class="form-group row" <?php if($pet->type != 1) echo "style='display: none'"; ?>>
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="dog_reg_genre" id="pet_reg_genre">
                                        <option value="アメリカン・コッカー・スパニエル" {{$pet->genre == 'アメリカン・コッカー・スパニエル' ? 'selected' : ''}}>アメリカン・コッカー・スパニエル</option>
                                        <option value="イタリアン・グレーハウンド" {{$pet->genre == 'イタリアン・グレーハウンド' ? 'selected' : ''}}>イタリアン・グレーハウンド</option>
                                        <option value="イングリッシュ・コッカー・スパニエル" {{$pet->genre == 'イングリッシュ・コッカー・スパニエル' ? 'selected' : ''}}>イングリッシュ・コッカー・スパニエル</option>
                                        <option value="ウエスト・ハイランド・ホワイト・テリア" {{$pet->genre == 'ウエスト・ハイランド・ホワイト・テリア' ? 'selected' : ''}}>ウエスト・ハイランド・ホワイト・テリア</option>
                                        <option value="ウェルシュ・コーギー・ペンブローク" {{$pet->genre == 'ウェルシュ・コーギー・ペンブローク' ? 'selected' : ''}}>ウェルシュ・コーギー・ペンブローク</option>
                                        <option value="カニンヘン・ダックスフンド" {{$pet->genre == 'カニンヘン・ダックスフンド' ? 'selected' : ''}}>カニンヘン・ダックスフンド</option>
                                        <option value="キャバリア・キング・チャールズ・スパニエル" {{$pet->genre == 'キャバリア・キング・チャールズ・スパニエル' ? 'selected' : ''}}>キャバリア・キング・チャールズ・スパニエル</option>
                                        <option value="グレート・ピレニーズ" {{$pet->genre == 'グレート・ピレニーズ' ? 'selected' : ''}}>グレート・ピレニーズ</option>
                                        <option value="ゴールデン・レトリーバー" {{$pet->genre == 'ゴールデン・レトリーバー' ? 'selected' : ''}}>ゴールデン・レトリーバー</option>
                                        <option value="シー・ズー" {{$pet->genre == 'シー・ズー' ? 'selected' : ''}}>シー・ズー</option>
                                        <option value="シェットランド・シープドッグ" {{$pet->genre == 'シェットランド・シープドッグ' ? 'selected' : ''}}>シェットランド・シープドッグ</option>
                                        <option value="シベリアン・ハスキー" {{$pet->genre == 'シベリアン・ハスキー' ? 'selected' : ''}}>シベリアン・ハスキー</option>
                                        <option value="ジャーマン・シェパード・ドッグ" {{$pet->genre == 'ジャーマン・シェパード・ドッグ' ? 'selected' : ''}}>ジャーマン・シェパード・ドッグ</option>
                                        <option value="ジャック・ラッセル・テリア" {{$pet->genre == 'ジャック・ラッセル・テリア' ? 'selected' : ''}}>ジャック・ラッセル・テリア</option>
                                        <option value="スタンダード・プードル" {{$pet->genre == 'スタンダード・プードル' ? 'selected' : ''}}>スタンダード・プードル</option>
                                        <option value="セント・バーナード" {{$pet->genre == 'セント・バーナード' ? 'selected' : ''}}>セント・バーナード</option>
                                        <option value="ダルメシアン" {{$pet->genre == 'ダルメシアン' ? 'selected' : ''}}>ダルメシアン</option>
                                        <option value="チワワ" {{$pet->genre == 'チワワ' ? 'selected' : ''}}>チワワ</option>
                                        <option value="トイ・プードル" {{$pet->genre == 'トイ・プードル' ? 'selected' : ''}}>トイ・プードル</option>
                                        <option value="ドーベルマン" {{$pet->genre == 'ドーベルマン' ? 'selected' : ''}}>ドーベルマン</option>
                                        <option value="バーニーズ・マウンテン・ドッグ" {{$pet->genre == 'バーニーズ・マウンテン・ドッグ' ? 'selected' : ''}}>バーニーズ・マウンテン・ドッグ</option>
                                        <option value="パグ" {{$pet->genre == 'パグ' ? 'selected' : ''}}>パグ</option>
                                        <option value="パピヨン" {{$pet->genre == 'パピヨン' ? 'selected' : ''}}>パピヨン</option>
                                        <option value="ビーグル" {{$pet->genre == 'ビーグル' ? 'selected' : ''}}>ビーグル</option>
                                        <option value="ビション・フリーゼ" {{$pet->genre == 'ビション・フリーゼ' ? 'selected' : ''}}>ビション・フリーゼ</option>
                                        <option value="フラットコーテッド・レトリーバー" {{$pet->genre == 'フラットコーテッド・レトリーバー' ? 'selected' : ''}}>フラットコーテッド・レトリーバー</option>
                                        <option value="ブルドッグ" {{$pet->genre == 'ブルドッグ' ? 'selected' : ''}}>ブルドッグ</option>
                                        <option value="フレンチ・ブルドッグ" {{$pet->genre == 'フレンチ・ブルドッグ' ? 'selected' : ''}}>フレンチ・ブルドッグ</option>
                                        <option value="ペキニーズ" {{$pet->genre == 'ペキニーズ' ? 'selected' : ''}}>ペキニーズ</option>
                                        <option value="ボーダー・コリー" {{$pet->genre == 'ボーダー・コリー' ? 'selected' : ''}}>ボーダー・コリー</option>
                                        <option value="ボクサー" {{$pet->genre == 'ボクサー' ? 'selected' : ''}}>ボクサー</option>
                                        <option value="ボストン・テリア" {{$pet->genre == 'ボストン・テリア' ? 'selected' : ''}}>ボストン・テリア</option>
                                        <option value="ポメラニアン" {{$pet->genre == 'ポメラニアン' ? 'selected' : ''}}>ポメラニアン</option>
                                        <option value="ボルゾイ" {{$pet->genre == 'ボルゾイ' ? 'selected' : ''}}>ボルゾイ</option>
                                        <option value="マルチーズ" {{$pet->genre == 'マルチーズ' ? 'selected' : ''}}>マルチーズ</option>
                                        <option value="ミックス" {{$pet->genre == 'ミックス' ? 'selected' : ''}}>ミックス</option>
                                        <option value="ミニチュア・シュナウザー" {{$pet->genre == 'ミニチュア・シュナウザー' ? 'selected' : ''}}>ミニチュア・シュナウザー</option>
                                        <option value="ミニチュア・ダックスフンド" {{$pet->genre == 'ミニチュア・ダックスフンド' ? 'selected' : ''}}>ミニチュア・ダックスフンド</option>
                                        <option value="ミニチュア・ピンシャー" {{$pet->genre == 'ミニチュア・ピンシャー' ? 'selected' : ''}}>ミニチュア・ピンシャー</option>
                                        <option value="ヨークシャー・テリア" {{$pet->genre == 'ヨークシャー・テリア' ? 'selected' : ''}}>ヨークシャー・テリア</option>
                                        <option value="ラブラドール・レトリーバー" {{$pet->genre == 'ラブラドール・レトリーバー' ? 'selected' : ''}}>ラブラドール・レトリーバー</option>
                                        <option value="紀州" {{$pet->genre == '紀州' ? 'selected' : ''}}>紀州</option>
                                        <option value="甲斐" {{$pet->genre == '甲斐' ? 'selected' : ''}}>甲斐</option>
                                        <option value="四国" {{$pet->genre == '四国' ? 'selected' : ''}}>四国</option>
                                        <option value="柴" {{$pet->genre == '柴' ? 'selected' : ''}}>柴</option>
                                        <option value="秋田" {{$pet->genre == '秋田' ? 'selected' : ''}}>秋田</option>
                                        <option value="日本スピッツ" {{$pet->genre == '日本スピッツ' ? 'selected' : ''}}>日本スピッツ</option>
                                        <option value="北海道" {{$pet->genre == '北海道' ? 'selected' : ''}}>北海道</option>
                                        <option value="狆" {{$pet->genre == '狆' ? 'selected' : ''}}>狆</option>
                                        <option value="その他" {{$pet->genre == 'その他' ? 'selected' : ''}}>その他</option>
                                    </select>
                                </div>
                            </div>
                            <div id="cat_reg_genre_area" class="form-group row" <?php if($pet->type != 2) echo "style='display: none'"; ?>>
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="cat_reg_genre" id="pet_reg_genre">
                                        <option value="アビシニアン" {{$pet->genre == 'アビシニアン' ? 'selected' : ''}}>アビシニアン</option>
                                        <option value="アメリカンカール" {{$pet->genre == 'アメリカンカール' ? 'selected' : ''}}>アメリカンカール</option>
                                        <option value="アメリカンショートヘア" {{$pet->genre == 'アメリカンショートヘア' ? 'selected' : ''}}>アメリカンショートヘア</option>
                                        <option value="アメリカンボブテイル" {{$pet->genre == 'アメリカンボブテイル' ? 'selected' : ''}}>アメリカンボブテイル</option>
                                        <option value="アメリカンワイヤーヘア" {{$pet->genre == 'アメリカンワイヤーヘア' ? 'selected' : ''}}>アメリカンワイヤーヘア</option>
                                        <option value="エキゾチックショートヘア" {{$pet->genre == 'エキゾチックショートヘア' ? 'selected' : ''}}>エキゾチックショートヘア</option>
                                        <option value="エジプシャンマウ" {{$pet->genre == 'エジプシャンマウ' ? 'selected' : ''}}>エジプシャンマウ</option>
                                        <option value="オシキャット" {{$pet->genre == 'オシキャット' ? 'selected' : ''}}>オシキャット</option>
                                        <option value="オリエンタル" {{$pet->genre == 'オリエンタル' ? 'selected' : ''}}>オリエンタル</option>
                                        <option value="キムリック" {{$pet->genre == 'キムリック' ? 'selected' : ''}}>キムリック</option>
                                        <option value="コーニッシュレックス" {{$pet->genre == 'コーニッシュレックス' ? 'selected' : ''}}>コーニッシュレックス</option>
                                        <option value="コラット" {{$pet->genre == 'コラット' ? 'selected' : ''}}>コラット</option>
                                        <option value="サイベリアン" {{$pet->genre == 'サイベリアン' ? 'selected' : ''}}>サイベリアン</option>
                                        <option value="ジャパニーズボブテイル" {{$pet->genre == 'ジャパニーズボブテイル' ? 'selected' : ''}}>ジャパニーズボブテイル</option>
                                        <option value="シャム" {{$pet->genre == 'シャム' ? 'selected' : ''}}>シャム</option>
                                        <option value="シャルトリュー" {{$pet->genre == 'シャルトリュー' ? 'selected' : ''}}>シャルトリュー</option>
                                        <option value="シンガプーラ" {{$pet->genre == 'シンガプーラ' ? 'selected' : ''}}>シンガプーラ</option>
                                        <option value="スコティッシュフォールド" {{$pet->genre == 'スコティッシュフォールド' ? 'selected' : ''}}>スコティッシュフォールド</option>
                                        <option value="スノーシュー" {{$pet->genre == 'スノーシュー' ? 'selected' : ''}}>スノーシュー</option>
                                        <option value="スフィンクス" {{$pet->genre == 'スフィンクス' ? 'selected' : ''}}>スフィンクス</option>
                                        <option value="セルカークレックス" {{$pet->genre == 'セルカークレックス' ? 'selected' : ''}}>セルカークレックス</option>
                                        <option value="ソマリ" {{$pet->genre == 'ソマリ' ? 'selected' : ''}}>ソマリ</option>
                                        <option value="ターキッシュアンゴラ" {{$pet->genre == 'ターキッシュアンゴラ' ? 'selected' : ''}}>ターキッシュアンゴラ</option>
                                        <option value="ターキッシュバン" {{$pet->genre == 'ターキッシュバン' ? 'selected' : ''}}>ターキッシュバン</option>
                                        <option value="デボンレックス" {{$pet->genre == 'デボンレックス' ? 'selected' : ''}}>デボンレックス</option>
                                        <option value="トンキニーズ" {{$pet->genre == 'トンキニーズ' ? 'selected' : ''}}>トンキニーズ</option>
                                        <option value="ノルウェージャンフォレストキャット" {{$pet->genre == 'ノルウェージャンフォレストキャット' ? 'selected' : ''}}>ノルウェージャンフォレストキャット</option>
                                        <option value="バーマン" {{$pet->genre == 'バーマン' ? 'selected' : ''}}>バーマン</option>
                                        <option value="バーミーズ" {{$pet->genre == 'バーミーズ' ? 'selected' : ''}}>バーミーズ</option>
                                        <option value="ハバナブラウン" {{$pet->genre == 'ハバナブラウン' ? 'selected' : ''}}>ハバナブラウン</option>
                                        <option value="バリニーズ" {{$pet->genre == 'バリニーズ' ? 'selected' : ''}}>バリニーズ</option>
                                        <option value="ピクシーボブ" {{$pet->genre == 'ピクシーボブ' ? 'selected' : ''}}>ピクシーボブ</option>
                                        <option value="ヒマラヤン" {{$pet->genre == 'ヒマラヤン' ? 'selected' : ''}}>ヒマラヤン</option>
                                        <option value="ブリティッシュショートヘア" {{$pet->genre == 'ブリティッシュショートヘア' ? 'selected' : ''}}>ブリティッシュショートヘア</option>
                                        <option value="ペルシャ" {{$pet->genre == 'ペルシャ' ? 'selected' : ''}}>ペルシャ</option>
                                        <option value="ベンガル" {{$pet->genre == 'ベンガル' ? 'selected' : ''}}>ベンガル</option>
                                        <option value="ボンベイ" {{$pet->genre == 'ボンベイ' ? 'selected' : ''}}>ボンベイ</option>
                                        <option value="マンクス" {{$pet->genre == 'マンクス' ? 'selected' : ''}}>マンクス</option>
                                        <option value="マンチカン" {{$pet->genre == 'マンチカン' ? 'selected' : ''}}>マンチカン</option>
                                        <option value="ミックス" {{$pet->genre == 'ミックス' ? 'selected' : ''}}>ミックス</option>
                                        <option value="メインクーン" {{$pet->genre == 'メインクーン' ? 'selected' : ''}}>メインクーン</option>
                                        <option value="ラガマフィン" {{$pet->genre == 'ラガマフィン' ? 'selected' : ''}}>ラガマフィン</option>
                                        <option value="ラグドール" {{$pet->genre == 'ラグドール' ? 'selected' : ''}}>ラグドール</option>
                                        <option value="ラパーマ" {{$pet->genre == 'ラパーマ' ? 'selected' : ''}}>ラパーマ</option>
                                        <option value="ロシアンブルー" {{$pet->genre == 'ロシアンブルー' ? 'selected' : ''}}>ロシアンブルー</option>
                                        <option value="日本猫" {{$pet->genre == '日本猫' ? 'selected' : ''}}>日本猫</option>
                                        <option value="その他" {{$pet->genre == 'その他' ? 'selected' : ''}}>その他</option>
                                    </select>
                                </div>
                            </div>
                            <div id="pet_reg_type_other_area" class="form-group row" style="display: none">
                                <label for="pet_reg_type_other" class="col-md-4 col-form-label text-md-right">動物の種類(その他)</label>

                                <div class="col-md-6">
                                    <input id="pet_reg_type_other" type="text" class="form-control{{ $errors->has('pet_reg_type_other') ? ' is-invalid' : '' }}" name="pet_reg_type_other" placeholder="ハムスター" value="{{ old('pet_reg_type_other') }}">

                                    @if ($errors->has('pet_reg_type_other'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pet_reg_type_other') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div id="pet_reg_genre_area" class="form-group row" <?php if($pet->type ==1 || $pet->type == 2) echo "style='display: none'"; ?>>
                                <label for="pet_reg_genre" class="col-md-4 col-form-label text-md-right">種類</label>

                                <div class="col-md-6">
                                    <input required id="pet_reg_genre" type="text" class="form-control{{ $errors->has('pet_reg_genre') ? ' is-invalid' : '' }}" name="pet_reg_genre" placeholder="ポメラニアン" value="{{ $pet->genre }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">生年月日</label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_year" class="form-control" name="pet_reg_birth_year" placeholder="年">
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ \Carbon\Carbon::parse($pet->birthday)->year == $year ? 'selected' : '' }}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_month" class="form-control" name="pet_reg_birth_month" placeholder="月">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ \Carbon\Carbon::parse($pet->birthday)->month == $month ? 'selected' : '' }}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select required id="pet_reg_birth_date" class="form-control" name="pet_reg_birth_date" placeholder="日">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ \Carbon\Carbon::parse($pet->birthday)->day == $day ? 'selected' : '' }}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="insurance_option" class="col-md-4 col-form-label text-md-right">保険加入</label>

                                <div class="col-md-6">
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" onclick="insurance_view(1)" name="insurance_option" value="1" required <?php if($pet->insurance_option == 1) echo "checked"; ?>>有り
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" onclick="insurance_view(0)" name="insurance_option" value="0" <?php if($pet->insurance_option == 0) echo "checked"; ?>>無し
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_reg_insurance_front_area" class="form-group row" <?php if($pet->insurance_option == 0) echo "style='display: none'"; ?>>
                                <label class="col-md-4 col-form-label text-md-right">保険証(表の写真)</label>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            @if($pet->insurance_front)
                                                <img id="img-pet_reg_insurance_front" src="{{URL::to('images/insurance/'.$pet->insurance_front)}}">
                                            @else
                                                <img id="img-pet_reg_insurance_front">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_reg_insurance_front" id="pet_reg_insurance_front" accept=".png, .jpg, .jpeg">
                                            </span>
                                            @if($pet->insurance_front != null)
                                                <a href="{{URL::to("/app/owner/pet/insurance_delete/".$pet->id."/1")}}" class="btn btn-danger btn-round" data-dismiss="fileinput">削除</a>
                                            @else
                                                <button type="button" id="front_remove" class="btn btn-danger btn-round">削除</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_reg_insurance_back_area" class="form-group row" <?php if($pet->insurance_option == 0) echo "style='display: none'"; ?>>
                                <label class="col-md-4 col-form-label text-md-right">保険証(裏の写真)</label>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            @if($pet->insurance_back)
                                                <img id="img-pet_reg_insurance_back" src="{{URL::to('images/insurance/'.$pet->insurance_back)}}">
                                            @else
                                                <img id="img-pet_reg_insurance_back">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_reg_insurance_back" id="pet_reg_insurance_back" accept=".png, .jpg, .jpeg">
                                            </span>
                                            @if($pet->insurance_back != null)
                                                <a href="{{URL::to("/app/owner/pet/insurance_delete/".$pet->id."/2")}}" class="btn btn-danger btn-round" data-dismiss="fileinput">削除</a>
                                            @else
                                                <button type="button" id="back_remove" class="btn btn-danger btn-round">削除</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_name_area" <?php if($pet->insurance_option == 0) echo "style='display: none'"; ?>>
                                <label for="insurance_name" class="col-md-4 col-form-label text-md-right">保険会社名</label>

                                <div class="col-md-6">
                                    <input id="insurance_name" type="text" class="form-control{{-- $errors->has('insurance_name') ? ' is-invalid' : '' --}}" name="insurance_name" value="{{$pet->insurance_name}}">

                                    {{-- @if ($errors->has('insurance_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('insurance_name') }}</strong>
                                    </span>
                                    @endif --}}
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_no_area" <?php if($pet->insurance_option == 0) echo "style='display: none'"; ?>>
                                <label for="insurance_no" class="col-md-4 col-form-label text-md-right">保険証番号</label>

                                <div class="col-md-6">
                                    <input id="insurance_no" type="text" class="form-control{{-- $errors->has('insurance_no') ? ' is-invalid' : '' --}}" name="insurance_no" value="{{$pet->insurance_no}}">

                                    {{-- @if ($errors->has('insurance_no'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('insurance_no') }}</strong>
                                    </span>
                                    @endif --}}
                                </div>
                            </div>

                            <div class="form-group row" id="insurance_date_area" <?php if($pet->insurance_option == 0) echo "style='display: none'"; ?>>
                                <label class="col-md-4 col-form-label text-md-right">保険証の期限</label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_year" class="form-control" name="insurance_from_year" placeholder="年">
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year + 4)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ old('insurance_from_year') == $year ? 'selected' : '' }}
                                                                {{substr($pet->insurance_from_date, 0, 4) == $year ? 'selected' : ''}}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_month" class="form-control" name="insurance_from_month" placeholder="月">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ old('insurance_from_month') == $month ? 'selected' : '' }}
                                                                {{substr($pet->insurance_from_date, 6, 2) == $month ? 'selected' : ''}}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_from_date" class="form-control" name="insurance_from_date" placeholder="日">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ old('insurance_from_date') == $day ? 'selected' : '' }}
                                                                {{substr($pet->insurance_from_date, 8, 2) == $day ? 'selected' : ''}}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/><br/>
                                <label class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_year" class="form-control" name="insurance_to_year" placeholder="年">
                                                        <option value="">----</option>
                                                        <?php $years = array_reverse(range(today()->year - 30, today()->year + 4)); ?>
                                                        @foreach($years as $year)
                                                            <option
                                                                value="{{ $year }}"
                                                                {{ old('insurance_to_year') == $year ? 'selected' : '' }}
                                                                {{substr($pet->insurance_to_date, 0, 4) == $year ? 'selected' : ''}}
                                                            >{{ $year }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">年</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_month" class="form-control" name="insurance_to_month" placeholder="月">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 12) as $month)
                                                            <option
                                                                value="{{ $month }}"
                                                                {{ old('insurance_to_month') == $month ? 'selected' : '' }}
                                                                {{substr($pet->insurance_to_date, 6, 2) == $month ? 'selected' : ''}}
                                                            >{{ $month }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">月</div>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="insurance_to_date" class="form-control" name="insurance_to_date" placeholder="日">
                                                        <option value="">--</option>
                                                        @foreach(range(1, 31) as $day)
                                                            <option
                                                                value="{{ $day }}"
                                                                {{ old('insurance_to_date') == $day ? 'selected' : '' }}
                                                                {{substr($pet->insurance_to_date, 8, 2) == $day ? 'selected' : ''}}
                                                            >{{ $day }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-auto my-2">日</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="pet_image" class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">ペットの写真</label>
                                <div class="col-md-6">
                                    <div id="image_preview_pet"></div>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail img-raised">
                                            @if($pet->profile_picture_path)
                                                <img id="img-pet_profile_image" src="{{URL::to('images/insurance/'.$pet->profile_picture_path)}}">
                                            @else
                                                <img id="img-pet_profile_image">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                                        <div>
                                            <span class="btn btn-raised btn-round btn-default btn-file">
                                                <span class="fileinput-new">ファイル選択</span>
                                                {{-- <span class="fileinput-exists">変更</span> --}}
                                                <input type="file" name="pet_profile_image" id="pet_profile_image" accept=".png, .jpg, .jpeg">
                                            </span>
                                            @if($pet->profile_picture_path != null)
                                                <a href="{{URL::to("/app/owner/pet/pet_image_delete/".$pet->id)}}" class="btn btn-danger btn-round" data-dismiss="fileinput">削除</a>
                                            @else
                                                <button type="button" id="profile_remove" class="btn btn-danger btn-round">削除</button>
                                            @endif
                                            {{-- <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="now-ui-icons ui-1_simple-remove"></i> Remove</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0 justify-content-md-center text-center">
                                <div class="col-md-12">
                                    <a href="{{URL::to('/app/owner')}}" class="btn btn-dark btn-lg">戻る</a>
                                    <a href="#" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#deleteModal">削除する</a>
                                    <button id="submit_btn" type="submit" class="btn btn-primary btn-lg">
                                        更新する
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ペットの登録情報を本当に削除しますか？<br>削除すると元に戻すことはできません。
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                    <button id="delete" type="button" class="btn btn-primary">削除</button>
                </div>
            </div>
        </div>
    </div>
@endsection
