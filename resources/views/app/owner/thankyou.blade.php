@extends('layouts.app.owner')

@section('content')

    <div class="container" style="margin:80px auto;">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h2>お問い合わせが送信されました。</h2>
                <h4>回答まで少しお待ちください。</h4>
                <a href="{{URL::to('/app/owner')}}" class="btn btn-primary btn-lg">戻る</a>
            </div>

        </div>
    </div>

@endsection
