<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>明細</title>
</head>
<body>
  <div>
     <div class="col-md-12">
        <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">診察内容の詳細</h5>

        <h5 class="description" style="">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>日時</th>
                        <td>{{$diag->reserve_datetime}}</td>
                    </tr>
                    <tr>
                        <th>ペット</th>
                        <td>{{\App\Pet::find($diag['pet_id'])->name}}</td>
                    </tr>
                    <tr>
                        <th>料金</th>
                        <td>￥{{number_format($diag->price)}}</td>
                    </tr>
                    <tr>
                        <th>問診内容</th>
                        <td>{!! nl2br($diag->content) !!}</td>
                    </tr>
                    <tr>
                        <th>病院データ</th>
                        <td>
                            @if(!empty($clinicInfo))
                            {{-- @isset($clinicInfo['eyecatchImages'])
                                @foreach($clinicInfo['eyecatchImages'] as $image)
                                    <img src="{{URL::to('/assets/img/clinics/'.$clinic['id'].'/'.$image)}}" width="100%">
                                @endforeach
                            @endisset --}}
                            院長 : {!! nl2br(e($clinicInfo['owner'])) !!}
                            &nbsp;&nbsp;&nbsp;
                            保険 :   {!! nl2br(e($clinic['insurance'])) !!}
                            &nbsp;&nbsp;&nbsp;
                            病院記述 : {!! nl2br(e($clinic['clinic_description'])) !!}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </h5>
    </div>
    <div class="col-md-12">
        <h5 class="font-weight-bold mt-4 mb-2 border-bottom border-warning">料金</h5>

        <h5 class="description" style="">
            {!! nl2br($diag->content) !!}

        </h5>
    </div>
  </div>
</body>
</body>
</html>
