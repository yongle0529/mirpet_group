{{-- @extends('layouts.clinic_app')

@section('content')
    <div class="container">
        <h3 class="font-weight-bold mb-4 border-bottom border-dark">
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>解約の申し込みを受け付けました。登録メールアドレスにメールを送信しましたので、ご確認ください。</h4><br/>
                            <a href="{{URL::to('/clinic')}}" class="btn btn-primary btn-lg">閉じる</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection --}}

@extends('layouts.clinic_app')

@section('body')
    <div class="container" style="margin-top:100px">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row justify-content-md-center text-center">
                        <div class="col-md-12 m-5">
                            <h4>解約の申し込みを受け付けました。<br/>登録メールアドレスにメールを送信しましたので、ご確認ください。</h4><br/>
                            <a href="{{URL::to('/clinic')}}" class="btn btn-primary btn-lg">閉じる</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

