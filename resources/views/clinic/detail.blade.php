@extends('layouts.clinic_app')

@section('body')

    <div class="container" style="margin:50px auto;">
        <div class="row">

            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">ご利用ガイド</h3>
            </div>
            <div class="col-md-12">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">1. カレンダーにて予約</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="">
                    オンライン相談が可能な曜日、時間枠を自由に設定できます。患者様にシステムを通じて予約してもらいます。普段の対面での診療など日常業務に支障なくオンライン相談が行えます。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="{{URL::to('/')}}/assets/img/detail_cal.jpg">
            </div>

            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">2. ビデオ通話・相談</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="">
                    予約時間になったら、ビデオ通話を開始し、オンライン相談を行うことができます。事前に予約詳細ページにて、問診情報などを確認していただき、効率的にオンラインでの相談を実施できます。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="{{URL::to('/')}}/assets/img/detail_chat.jpg">
            </div>
            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">3. 決済</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="">
                    飼い主様がシステム上に登録したクレジットカードによる決済が可能です。オンラインでの予約から決済までをワンストップで実施することができます。現在、VISA・Mastercardに対応しております。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="{{URL::to('/')}}/assets/img/detail_credit.jpg">
            </div>
            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">4. 薬などの配送</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="">
                    オンライン相談の結果、薬などの処方があった場合は動物病院の方から飼い主様へ送付していただきます。将来的には、この配送までをシステム上で一括管理できるようにする予定です。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="{{URL::to('/')}}/assets/img/detail_shipping.jpg">
            </div>
            <div class="col-md-12 text-center">
                <a href="{{URL::to('/')}}/clinic" class="btn btn-primary btn-round btn-lg">戻る</a>
            </div>
        </div>
    </div>
@endsection
