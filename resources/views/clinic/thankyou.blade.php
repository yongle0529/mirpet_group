@extends('layouts.clinic')
@section('navi')
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/clinic#whatismirpet" style="color: black;">
                    <p>みるペットとは</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/clinic/detail" style="color: black;">
                    <p>ご利用ガイド</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/clinic#ours" style="color: black;">
                    <p>私たちについて</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/clinic#qa" onclick='scrollToElement($("#qa"))' style="color: black;">
                    <p>よくある質問</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/clinic#inquiry" style="color: black;">
                    <p>お問い合わせ</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-warning btn-round" href="{{URL::to('/')}}/owner">
                    <p>一般の方はこちら</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-primary btn-round" href="#">
                    <p>会員ログイン</p>
                </a>
            </li>
        </ul>
    </div>
@endsection

@section('body')

    <div class="container" style="margin:80px auto;">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h2>お問い合わせいただきありがとうございました</h2>
                <h4>担当者からの連絡をお待ち下さい。</h4>
                <a href="{{URL::to('/')}}/clinic" class="btn btn-primary btn-lg">戻る</a>
            </div>

        </div>
    </div>

@endsection
