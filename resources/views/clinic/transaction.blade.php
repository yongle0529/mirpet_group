@extends('layouts.clinic_app')

@section('body')

    <div class="container" style="margin:50px auto;">
        <div class="row">
                <div id="transaction" class="row justify-content-md-center">
                        <div class="col-md-12 border-bottom border-danger mb-4">
                            <h3 class="title">特定商取引法に基づく表記</h3>
                        </div>
                        <div class="col-md-10 text-left">
                            <h4>販売事業者名</h4>
                            <ol>株式会社みるペット</ol>

                            <h4>代表者または運営統括責任者名前</h4>
                            <ol>代表取締役社長　浅沼　直之</ol>

                            <h4>販売事業者所在地</h4>
                            <ol>〒106-0061　東京都中央区銀座７丁目１８番１３クオリア銀座５０４</ol>

                            <h4>問い合わせ先</h4>
                            <ol><a class="nav-link" href="{{URL::to('/')}}/clinic#inquiry">お問い合わせフォーム</a></ol>

                            <h4>電話番号</h4>
                            <ol>上記にてお問い合わせください</ol>

                            <h4>販売価格</h4>
                            <ol>商品毎に記載</ol>

                            <h4>商品代金以外に必要な費用</h4>
                            <ol>消費税</ol>

                            <h4>代金の支払い時期および方法</h4>
                            <ol>クレジットカード（支払い時期は、各カード会社引き落とし日）</ol>

                            <h4>返品の取り扱い条件、解約条件</h4>
                            <ol>商品の特性上、不可</ol>


                            <h4>不良品の取り扱い条件</h4>
                            <ol>商品の特性上、不可</ol>
                        </div>
                    </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">姓</div>
                        <div id="confirm-lastname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">名</div>
                        <div id="confirm-firstname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">セイ</div>
                        <div id="confirm-lastname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メイ</div>
                        <div id="confirm-firstname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">医院名</div>
                        <div id="confirm-clinic-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">役職</div>
                        <div id="confirm-user-role"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">年代</div>
                        <div id="confirm-user-age"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-clinic-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-clinic-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">ご希望の連絡方法</div>
                        <div id="confirm-contact-method"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望日時</div>
                        <div id="confirm-contact-method-tel-date"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望時間帯</div>
                        <div id="confirm-contact-method-tel-timezone"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">みるペットを知ったきっかけ</div>
                        <div id="confirm-referer"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('input[name="contact-method[]"]').change(function () {
                $('.contact_method_tel_area').hide();
                $('.modal_contact_method_tel_area').hide();

                $('input[name="contact-method[]"]:checked').each(function () {
                    var val = $(this).val();
                    if (val == "tel") {
                        $('.contact_method_tel_area').show();
                        $('.modal_contact_method_tel_area').show();
                    }
                })
            });

            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-lastname-kanji').text($('input[name="lastname-kanji"]').val());
                    $('#confirm-firstname-kanji').text($('input[name="firstname-kanji"]').val());
                    $('#confirm-lastname-kana').text($('input[name="lastname-kana"]').val());
                    $('#confirm-firstname-kana').text($('input[name="firstname-kana"]').val());
                    $('#confirm-clinic-name').text($('input[name="clinic-name"]').val());
                    $('#confirm-user-role').text($('input[name="user-role"]').val());
                    $('#confirm-user-age').text($('[name="user-age"]').val());
                    $('#confirm-clinic-mail').text($('input[name="clinic-mail"]').val());
                    $('#confirm-clinic-tel').text($('input[name="clinic-tel"]').val());

                    var contactMethod = [];
                    $('input[name="contact-method[]"]:checked').each(function () {
                        contactMethod.push($(this).data('label'));
                    });
                    $('#confirm-contact-method').text(contactMethod.length == 0 ? '選択されていません' : contactMethod);
                    $('#confirm-contact-method-tel-date').text($('[name="contact-method-tel-date"]').val());
                    $('#confirm-contact-method-tel-timezone').text($('[name="contact-method-tel-timezone"]').val());
                    $('#confirm-referer').text($('[name="referer"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>

@endsection
