@extends('layouts.clinic_app')

@section('body')

<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" media="all" href="{{URL::to('/')}}/assets/rule/users.ff0a16e6264403200113.css">
<meta name="csrf-param" content="authenticity_token">
<meta name="csrf-token" content="RrCvaWvEpI4/LQ+63EWGGk9nx6o1v4nviily62hO65se2AC7pg0B2SLHPpr9t88nB52X/1GCerf6OYqysD2ChQ==">

<link type="text/css" rel="stylesheet" charset="UTF-8" href="{{URL::to('/')}}/assets/rule/translateelement.css"></head>
<body>
  <!-- Google Tag Manager -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-KRXS8B" height="0" width="0" style="display:none;visibility:hidden" ></iframe>
</noscript>
<script async="" src="{{URL::to('/')}}/assets/rule/lt.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/saved_resource"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/linkid.js.download"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/analytics.js.download"></script><script src="{{URL::to('/')}}/assets/rule/940796602644488" async=""></script><script async="" src="{{URL::to('/')}}/assets/rule/fbevents.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/gtm.js.download"></script><script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KRXS8B');
</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n; n.loaded=!0; n.version='2.0'; n.queue=[]; t=b.createElement(e); t.async=!0;
t.src=v; s=b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s) }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');
fbq('init', '940796602644488');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=940796602644488&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

        <div  class="container" style="margin:50px auto;">
            <div id="rule" class="row">

                <div class="col-md-12">
                    <div class="o-wrapper__body">
                        <main>
                        <div class="o-container o-container@desktop c-background c-background--main">
                          <div class="o-container__column o-container__column@desktop">

                            <div class="o-container__item o-container__item@desktop">
                              <section>
                                <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                  <div class="c-segment__inner c-segment__inner@desktop">
                                    <h1 class="c-heading c-heading--3xl@desktop c-heading--3xl">個人情報保護方針</h1>
                                  </div>
                                </div>
                              </section>
                            </div>
                            <div class="o-container__item o-container__item@desktop">
                              <section>
                                <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                  <div class="c-segment__inner c-segment__inner--3large@desktop">
                                    <div class="c-agreement">
                                      <section>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">１．	目的</h3>
                                        <ol class="c-agreement__list">
                                            株式会社みるペット（以下、「当社」といいます。）は、当社が企画・運営するオンライン相談システム「みるペット」（以下、「本サービス」といいます。）を通じて取得される個人情報の取扱いを定めることを目的として、本個人情報保護方針を定めます。また、個人情報の保護に関する法律（以下「個人情報保護法」といいます。）その他の関係法令とともに、これを遵守します。
                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">２．	定義</h3>
                                        <ol class="c-agreement__list">
                                            (1) 「個人情報」とは、個人に関する情報であり、本サービスを利用する一切の個人（以下、「利用者」といい、個人顧客、取引先、従業員など一切の個人が該当します。）に関する情報であって、住所、氏名、電話番号、電子メールアドレスなどの文字、映像、音声などによって当該個人を識別できる情報をいいます。また、その情報のみでは識別できない場合でも、他の情報と容易に照合することができ、結果的に個人を識別できるものも個人情報に含まれます。

                                        </ol>
                                        <ol class="c-agreement__list">
                                            (2) 「登録動物病院等」とは、本サービスを相談、獣医療行為等の提供者側として導入・利用している動物病院その他の団体をいいます。
                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">３．	個人情報の取り扱いに関するご意見、お問い合わせ、苦情、開示請求等の窓口</h3>
                                        <ol class="c-agreement__list">
                                            当社の個人情報の取り扱いに関するご意見、お問い合わせ、苦情、および開示請求等につきましては、下記相談窓口までご連絡ください。直接ご来社いただいてのお申し出は受けかねますので、その旨ご了承賜りますようお願いいたします。

                                        </ol>
                                        <ol class="c-agreement__list">
                                            株式会社みるペット　個人情報お問い合わせ窓口
                                        </ol>
                                        <ol class="c-agreement__list">
                                            〒104-0061
                                        </ol>
                                        <ol class="c-agreement__list">
                                            東京都中央区銀座７丁目１８番１３クオリア銀座５０４
                                        </ol>
                                        <ol class="c-agreement__list">
                                            E-mail：privacy@mirpet.co.jp
                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">４．	個人情報の取得</h3>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">当社は、以下の場合に個人情報を取得します。 </h3>
                                        <ol class="c-agreement__list">
                                          (1) 端末操作を通じて利用者が氏名、連絡先情報等を入力する場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (2) 利用者が直接または書面等で提供する場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (3) 利用者による本サービスの利用に伴って自動的に送信される場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (4) 登録動物病院等による本サービスの利用に伴って、登録動物病院等が本サービス上で情報送信する場合、および登録動物病院等が直接当社に交付する場合（利用者の相談、診察・処方等に関する情報）
                                        </ol>

                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">５．	個人情報の利用目的</h3>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">当社は、以下の目的のために個人情報を利用します。当社がやむをえず下記の目的以外の理由で利用を行う場合は、利用者にその旨を通知し、その同意を得た上で利用します。  </h3>
                                        <ol class="c-agreement__list">
                                          (1) 当社が利用者の個人認証を行い、本サービスを提供するため

                                        </ol>
                                        <ol class="c-agreement__list">
                                          (2) 本サービスの紹介等を目的とした記事作成用のモニター・取材対象者を募集するため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (3) 利用者の予約・通院状況等の確認をし、必要なリマインダ等の連絡をするため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (4) 料金の支払・返金に関する利用者への連絡をするため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (5) 当社が登録動物病院等による本サービスの適正な利用を担保するため（サービス利用料の算定等を含み、相談、診察・処方内容等の必要な限度での確認に限る）

                                        </ol>
                                        <ol class="c-agreement__list">
                                          (6) 利用者に属性情報に基づくプロモーション情報を配信するため（個人を特定できない範囲での利用者の性別等の把握、当社サービス内および当社が契約するサービス内での行動・アクセス履歴の把握）
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (7) 個人を特定できない範囲で本サービスに関する統計情報の作成および利用を行うため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (8) 個人を特定できない範囲内で登録動物病院等その他の第三者へのマーケティング資料を作成し、提供するため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (9) 各種アンケート、キャンペーン等のご案内、応募受付、当選者への連絡、プレゼント発送等を行うため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (10) 当社が提供するサービスのご案内や資料の送付を行うため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (11) 本サービスに関する利用者からのお問合せ、ご相談および苦情への対応ならびに紛争の解決のために必要な業務を行うため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (12) 本サービスの改善、不具合対応、および開発を行うため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (13) 本サービスの利用規約に違反した利用者による本サービスの利用を停止等するため
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (14) その他上記目的に関連・付随する目的の達成に必要な場合
                                        </ol>
                                      </section>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>

                            <div class="o-container__item o-container__item@desktop">
                              <section>
                                <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                  <div class="c-segment__inner c-segment__inner--3large@desktop">
                                    <div class="c-agreement">
                                      <section>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">６．	個人情報の第三者提供について</h3>
                                        <ol class="c-agreement__list">
                                          1. 当社は、原則として、利用者の同意を得ずに個人情報を第三者に提供いたしません。
                                        </ol>
                                        <ol class="c-agreement__list">
                                          なお、利用者は、自らが本サービスを通じて登録動物病院等に対し相談、診療等の予約をした場合、本サービスの利用にあたり利用者が入力した情報が、当該登録動物病院等に共有されることについて同意するものとします。
                                          また、以下の場合は、同意なく個人情報を提供することがあります。
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (2) 人の生命、身体又は財産の保護のために必要がある場合であって、利用者の同意を得ることが困難である場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (3) 公衆衛生の向上または児童の健全な育成の推進のために特に必要がある場合であって、利用者の同意を得ることが困難である場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (4) 国の機関もしくは地方公共団体またはその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合であって、利用者の同意を得ることによってその事務の遂行に支障を及ぼすおそれがあると当社が判断した場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (5) 裁判所、検察庁、警察、弁護士会、消費者センターまたはこれらに準じる機関から、個人情報についての開示を求められた場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (6) 利用者から明示的に第三者への開示または提供を求められた場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (7) 利用目的の達成に必要な範囲内において個人情報取扱い業務の一部または全部を委託する場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (8) 合併その他の事由による事業の承継に伴って提供される場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (9) 当社の正当な業務の範囲内であって、利用者の利益が侵害されるおそれのない提供を行う場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (10) 利用者が入力したクレジットカード情報等の決済に必要な情報を、利用者の本サービス利用に伴う対価を決済するために必要な限度で外部決済業者に対して提供する場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          2. 当社は、本サービスの提供・品質向上や、新規サービス開発のために必要な場合、または調査・研究・分析のために登録動物病院等やその他の研究機関に提供する必要がある場合には、氏名・住所など直接特定の個人を識別できる情報を排除した上で、必要な範囲で第三者に個人情報を提供します。
                                        </ol>
                                        <ol class="c-agreement__list">
                                          3. 登録動物病院等は本サービスを利用するに当たり必要な限度で当社に利用者の診察・処方等に関する情報を提供する（登録動物病院等による当社への第三者提供）ことがあります。利用者は予めこれに同意し、当社及び登録動物病院に対して異議を述べないものとします。
                                        </ol>

                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">７．	免責</h3>
                                        <ol class="c-agreement__list">
                                          以下の場合、第三者による個人情報の取得に関して当社は何らの責任を負いません。

                                        </ol>
                                        <ol class="c-agreement__list">
                                          (1) 利用者自らが第三者に個人情報を明らかにする場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (2) 利用者または利用者以外の者が本サービスにおいて入力した情報により、期せずして個人の識別が可能な状態となってしまった場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (3) 本サービス外の外部サイトにおいて、利用者が個人情報を提供し、それが第三者により取得・利用された場合
                                        </ol>
                                        <ol class="c-agreement__list">
                                          (4) 利用者以外の者が利用者を識別できる情報(ID・パスワード等)を入手した場合
                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">８．	顧客企業等の第三者における個人情報管理について</h3>
                                        <ol class="c-agreement__list">
                                          利用者の同意に基づき、登録医療機関、その他顧客企業等の第三者に提供された個人情報は、各提供先の第三者の責任により管理されます。

                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">９．	個人情報の委託について</h3>
                                        <ol class="c-agreement__list">
                                          当社は利用目的の達成に必要な範囲内において個人情報の取扱いの全部または一部を委託する場合があります。なお、当社が個人情報の取扱いを委託する場合は、適切な委託先を選定し、個人情報が安全に管理されるよう適切に監督するものとします。
                                        </ol>

                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">１０．個人情報の正確性について</h3>
                                        <ol class="c-agreement__list">
                                          当社は、ご提供いただいた個人情報を正確にデータ処理するように努めます。ただし、ご提供いただいた個人情報の内容が正確かつ最新であることについては、利用者が責任を負うものとします。

                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">１１．個人情報の開示等</h3>
                                        <ol class="c-agreement__list">
                                          1. 利用者またはその適法な代理人に限り、当社に対して利用目的の通知、個人情報の開示、追加、訂正、削除、利用停止、および第三者への提供の停止（以下、「開示等」といいます。）を求めることができるものとし、当社は個人情報保護法の定めに従ってこれに遅滞なく対応します。利用者は、かかる個人情報の削除や利用停止等により、本サービスを受けることができなくなる場合があります。また、当社が開示等に対応することによって、以下のいずれかに該当する場合は、開示等に対応できない場合がございます。

                                        </ol>
                                        <ol class="c-agreement__list">
                                          (1) 利用者または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合

                                        </ol>
                                        <ol class="c-agreement__list">
                                          (2) 当社の業務の適正な実施に著しい支障を及ぼすおそれがある場合

                                        </ol>
                                        <ol class="c-agreement__list">
                                          2. 個人情報の利用目的の通知、および個人情報の開示等の請求については、当社は１回ごとに当社所定の手数料￥1,000を徴収致します。利用者は、開示請求にあたっては、当社指定の様式（開示等申請書）をご利用ください。

                                        </ol>
                                        <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">１２．個人情報保護方針の変更</h3>
                                        <ol class="c-agreement__list">
                                          当社は法令等で定めがある場合を除き、本個人情報保護方針を随時変更することができるものとします。変更を行う場合には、本サービスを提供するウェブサイト上にて変更後の規程を掲載するものとします。

                                        </ol>
                                        <ol class="c-agreement__list text-md-right">
                                          2019年1月16日制定

                                        </ol>
                                        <ol class="c-agreement__list text-md-right">
                                          株式会社みるペット

                                        </ol>
                                        <ol class="c-agreement__list text-md-right">
                                          代表取締役社長　浅沼　直之

                                        </ol>
                                      </section>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>

                          </div>
                        </div>

                        <div class="c-overlay c-overlay--invisible js-overlay"></div>
                       </main>
                      </div>
                </div>
            </div>


    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">姓</div>
                        <div id="confirm-lastname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">名</div>
                        <div id="confirm-firstname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">セイ</div>
                        <div id="confirm-lastname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メイ</div>
                        <div id="confirm-firstname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">医院名</div>
                        <div id="confirm-clinic-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">役職</div>
                        <div id="confirm-user-role"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">年代</div>
                        <div id="confirm-user-age"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-clinic-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-clinic-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">ご希望の連絡方法</div>
                        <div id="confirm-contact-method"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望日時</div>
                        <div id="confirm-contact-method-tel-date"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望時間帯</div>
                        <div id="confirm-contact-method-tel-timezone"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">みるペットを知ったきっかけ</div>
                        <div id="confirm-referer"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('input[name="contact-method[]"]').change(function () {
                $('.contact_method_tel_area').hide();
                $('.modal_contact_method_tel_area').hide();

                $('input[name="contact-method[]"]:checked').each(function () {
                    var val = $(this).val();
                    if (val == "tel") {
                        $('.contact_method_tel_area').show();
                        $('.modal_contact_method_tel_area').show();
                    }
                })
            });

            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-lastname-kanji').text($('input[name="lastname-kanji"]').val());
                    $('#confirm-firstname-kanji').text($('input[name="firstname-kanji"]').val());
                    $('#confirm-lastname-kana').text($('input[name="lastname-kana"]').val());
                    $('#confirm-firstname-kana').text($('input[name="firstname-kana"]').val());
                    $('#confirm-clinic-name').text($('input[name="clinic-name"]').val());
                    $('#confirm-user-role').text($('input[name="user-role"]').val());
                    $('#confirm-user-age').text($('[name="user-age"]').val());
                    $('#confirm-clinic-mail').text($('input[name="clinic-mail"]').val());
                    $('#confirm-clinic-tel').text($('input[name="clinic-tel"]').val());

                    var contactMethod = [];
                    $('input[name="contact-method[]"]:checked').each(function () {
                        contactMethod.push($(this).data('label'));
                    });
                    $('#confirm-contact-method').text(contactMethod.length == 0 ? '選択されていません' : contactMethod);
                    $('#confirm-contact-method-tel-date').text($('[name="contact-method-tel-date"]').val());
                    $('#confirm-contact-method-tel-timezone').text($('[name="contact-method-tel-timezone"]').val());
                    $('#confirm-referer').text($('[name="referer"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>

@endsection
