@extends('layouts.clinic_app')
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" media="all" href="{{URL::to('/')}}/assets/rule/users.ff0a16e6264403200113.css">
<meta name="csrf-param" content="authenticity_token">
<meta name="csrf-token" content="RrCvaWvEpI4/LQ+63EWGGk9nx6o1v4nviily62hO65se2AC7pg0B2SLHPpr9t88nB52X/1GCerf6OYqysD2ChQ==">

<link type="text/css" rel="stylesheet" charset="UTF-8" href="{{URL::to('/')}}/assets/rule/translateelement.css"></head>
<body>
  <!-- Google Tag Manager -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-KRXS8B" height="0" width="0" style="display:none;visibility:hidden" ></iframe>
</noscript>
<script async="" src="{{URL::to('/')}}/assets/rule/lt.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/saved_resource"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/linkid.js.download"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/analytics.js.download"></script><script src="{{URL::to('/')}}/assets/rule/940796602644488" async=""></script><script async="" src="{{URL::to('/')}}/assets/rule/fbevents.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/gtm.js.download"></script><script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KRXS8B');
</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n; n.loaded=!0; n.version='2.0'; n.queue=[]; t=b.createElement(e); t.async=!0;
t.src=v; s=b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s) }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');
fbq('init', '940796602644488');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=940796602644488&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
@section('body')

    <div class="container" style="margin:50px auto;">

        <div id="company" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">会社概要</h3>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_6.jpg" class="rounded" style="height:300px; object-fit: cover;">
            </div>
            <div class="col-md-8">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>社名</td>
                        <td>株式会社みるペット</td>
                    </tr>
                    <tr>
                        <td>事業内容</td>
                        <td>動物病院の業務関連システムの企画、デザイン、開発、制作、管理及び運営業務</td>
                    </tr>
                    <tr>
                        <td>代表者</td>
                        <td>浅沼　直之</td>
                    </tr>
                    <tr>
                        <td>設立</td>
                        <td>2019年1月16日</td>
                    </tr>
                    <tr>
                        <td>本店所在地</td>
                        <td>〒104-0061　東京都中央区銀座７丁目１８ー１３　クオリア銀座５０４</td>
                    </tr>
                    {{--  <tr>
                        <td>電話番号</td>
                        <td>03-6910-3242</td>
                    </tr>  --}}
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
