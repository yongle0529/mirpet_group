@extends('layouts.clinic')

@section('header')
    <style>
        @media screen and (max-width: 320px) {
            .n-logo {
                margin-bottom: 10vh !important;
            }
        }
    </style>
    <div class="page-header" style="height: 100vh;">
        <div class="page-header-image" style="background-image:url('{{URL::to('/')}}/assets/img/header_pets.jpg');">
        </div>
        <div class="container">
            <div class="content-center brand" style="height: 100vh;">
                <img class="n-logo" src="{{URL::to('/')}}/assets/img/mirpet_logo_A_02.png" alt="みるペット" style="max-width: 20vh; margin-top: 30vh; margin-bottom: 20vh;">

                <h5 class="mt-2 font-weight-bold">動物の医療、ヘルスケアを<br class="br-sp">もっと便利に、もっと身近に</h5>
                <!--<h3 class="mt-0 mb-2 font-weight-bold">ペットの未来へ1ステップ！</h3>-->
                <h6 class="mt-0 font-weight-bold">みるペットは動物病院と飼い主様、患者様の<br class="br-sp">架け橋となり、<br class="br-pc">「満足のできる医療環境」の<br class="br-sp">構築を目指します。</h6>
                <a href="javascript:void(0)" onclick='scrollToElement($("#inquiry"))' class="btn btn-primary btn-round btn-lg">お問い合わせはこちら</a>
            </div>

        </div>
    </div>
@endsection
@section('body')
    <div class="container">
        <div id="whatismirpet" class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">みるペットとは</h3>
            </div>
            <div class="col-md-7">
                <h5 class="description" style="">
                    {{--<span style="font-size: 2.4rem; font-weight: bold;">--}}
                    みるペットは、動物、飼い主様を動物病院とインターネットで繋ぐ、オンライン相談システムです。
                    お好きな時間に予約をすることで、どこにいてもいつもと同じ動物病院、同じ先生に相談することができます。
                    予約、相談（診察）、決済までをオンライン上で完結することができます。
                </h5>
            </div>
            <div class="col-md-5">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_1.jpg" width="100%">
            </div>
        </div>
        <div class="row" style="margin-top: 1.75rem;">
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">1.予約</h3>
                    </div>
                    <div class="card-body">
                        カレンダーでお好きな日時、時間を選択して予約。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">2.相談(診察)
                        </h3>
                    </div>
                    <div class="card-body">
                        予約した時間になったら、通話ボタンを押して相談開始。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">3.決済
                        </h3>
                    </div>
                    <div class="card-body">
                        相談が終わったら、事前登録しているクレジットカードにて自動決済。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">4.薬の郵送
                        </h3>
                    </div>
                    <div class="card-body">
                        薬の処方があるときは、後日動物病院から薬を送付します。
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center text-center">
            <div class="col-md-12">
                <h3 style="font-weight: bold;">オンラインシステムは、患者様、動物病院共にメリットが！</h3>
            </div>

            <div class="col-md-6">
                <div class="card" data-background-color="blue" style="min-height: 250px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">
                            <i class="fas fa-user"></i> 患者様
                        </h3>
                    </div>
                    <div class="card-body" style="text-align: left; padding: 0 24px 0 0;">
                        <ul>
                            <li>待ち時間の解消が可能に</li>
                            <li>通院によるストレスの軽減（とくにネコちゃん）</li>
                            <li>治療継続率があがることで
                                <ul>
                                    <li>疾患のコントロールがしやすくなる</li>
                                    <li>重症化を防ぎやすくなる</li>
                                </ul>
                            </li>
                            <li>いままでよりももっと気軽に相談が可能に</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" data-background-color="blue" style="min-height: 250px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">
                            <i class="fas fa-clinic-medical"></i> 動物病院
                        </h3>
                    </div>
                    <div class="card-body" style="text-align: left; padding: 0 24px 0 0;">
                        <ul>
                            <li>病院運営の効率化
                                <ul>
                                    <li>予約診療による外来の分散化</li>
                                    <li>調剤時間の効率化 など</li>
                                </ul>
                            </li>
                            <li>飼い主様の満足度の向上</li>
                            <li>他動物病院との差別化</li>
                            <li>飼い主様とのコミュニケーション機会の創出</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a href="{{URL::to('/')}}/clinic/detail" class="btn btn-primary btn-round btn-lg">もっと詳しく</a>
            </div>
        </div>

        <div id="qa" class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">よくある質問</h3>
            </div>
            <div class="col-md-12">
                <div>
                    <div class="boxQA">
                        <div class="box-title">みるペットは何ができるんですか？</div>
                        <p>「みるペット」はオンライン相談システムです。予約、ビデオ電話での相談、決済までをワンストップで行えるサービスです。</p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">導入費用はいくらかかりますか？</div>
                        <p>「みるペット」は、初期導入費用はかかりません。また、月額の固定費用もかかりません。オンライン相談を行わなかった月は費用は発生いたしません。
                        </p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">導入する際、動物病院で準備するものはありますか？</div>
                        <p>インターネットができる環境と、PC(Windows/Mac)であればGoogle Chrome、iPadであればSafariがご利用できる端末をご用意ください。パソコンにマイク・カメラが付随していない場合は、外付けのマイク・カメラセットをご用意いただく必要がございます。
                        </p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">飼い主様の決済する方法を教えてください。</div>
                        <p>現在クレジットカード決済に対応しており、VISA / Mastercard をご利用いただけます。
                        </p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">実際に稼働するかわかりませんが、デモ画面などを触ることは可能ですか。</div>
                        <p>はい、可能です。弊社までご一報いただけましたら、デモアカウントにて本番同等の環境にてお試しいただくことが可能です。 ご希望の際は、こちらまでお問い合わせください。
                        </p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">オンライン「診療」は可能ですか？</div>
                        <p>現在の法律の解釈では、「診療は対面で直接行うこと」となっております。ただし、獣医師の裁量で音声または動画での情報による判断で、再診における経過のための薬の処方は可能となっております。初診でのオンライン上での薬の処方はできません。
                        </p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">動物医療保険は使用できますか？</div>
                        <p>保険会社によります。詳しくは、各保険会社様へ直接お問い合わせください。
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="ours" class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">私たちについて</h3>
            </div>
            <div class="col-md-12">
                <h5 class="description" style="">
                    みるペットは、動物と飼い主様が健やかな生活を長く送ることができるように、より良い予防・医療サービスを受けることができる環境作りを目的として、動物医療業界の効率化と利便性の向上を目指します。
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">社名について</h3>
            </div>
            <div class="col-md-12">
                <img class="mx-auto d-block mb-5" src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 300px;">
                <h5 class="description" style="">
                    「みる」にはいろいろな意味があります。「見る」、「診る」、「看る」・・・
                    私たちは、動物たちの健康を見守りたい。
                    動物病院と患者様の架け橋となり、診察や看病をサポートする会社でありたい。
                    そんな想いを込めました。
                    ロゴデザインは、動物が獣医療者・飼い主様と手を取り合っている様を表し、３者の協力の重要性を表現しています。
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">代表挨拶</h3>
            </div>
            <div class="col-md-4 text-center mt-2 mb-4">
                <img src="{{URL::to('/')}}/assets/img/ceo_pic.jpg" class="rounded" style="height:350px; object-fit: cover;">
            </div>
            <div class="col-md-8 ">


                <h5 class="description" style="">
                    私は、1頭でも多くの動物を助けたいとの想いから獣医師を志しました。<br>
                    大学卒業後、動物病院で臨床の経験を積む中であることを思うようになりました。それは、動物病院に来る動物たちはその時点で病気やケガがかなり進行していることが多いということです。「なんでもっと早く来れなかったのだろう？」、「なんでリスクがあるのに予防や対策をしていなかったのだろう？」と感じることが頻繁にありました。その理由は、飼い主様の得られる情報の質や量が足りていないことだと考え、啓発の重要性を感じました。<br>
                    動物病院で働いていると、そこに来た動物、飼い主様にしか情報を伝えることができないと思い、より多くの方へ影響を与えられると思い、動物用医薬品会社へ転職しました。そこでの仕事は、病気や薬、予防の最新情報を動物病院へ届け、同時に飼い主様への啓発の方法などのアドバイスをするものでした。動物病院の中と外、双方の立場を経験し、様々な動物病院を見ることで、今の業界には何が必要で、自分には何ができるのかを考えるようになりました。<br>
                    そんな中で、日々進歩する情報技術を用いることで、動物たちの健康維持に必要な医療へのアクセスのしやすさを向上させられると思い、この会社を立ち上げました。常に、より良い動物医療環境をどう構築するのかを考えながら、サービスを生み出していきます。どうぞよろしくお願いいたします。
                    <p class="text-right mt-4">代表取締役社長　浅沼直之</p>
                </h5>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">役員紹介</h3>
            </div>
            <div class="col-md-12 mt-2">
                <h4 class="mt-2 font-weight-bold">代表取締役社長　浅沼　直之
                </h4>
                <p>1983年生まれ。北里大学獣医畜産学部（現獣医学部）卒業。
                    一般開業動物病院に勤務後、企業系動物病院で勤務。
                    院長を経験後、2015年に動物用医薬品会社であるメリアル・ジャパン株式会社（現ベーリンガーインゲルハイムアニマルヘルスジャパン株式会社）に入社。
                    コンパニオン・アニマル部の学術営業として一般開業動物病院を担当。2019年1月、株式会社みるペットを創業。</p>
            </div>
            <div class="col-md-12 mt-4">
                <h4 class="mt-2 font-weight-bold">取締役　石川　直人
                </h4>
                <p>University of Southern California（米国 南カルフォルニア大学）都市計画・開発学部卒業。卒業後、マッキャンヘルスケアワールドワイドジャパン（現マッキャンヘルス）にて、OTC医薬品・ヘルスケア用品のブランドマーケティング・コミュニケーションや製薬企業のコーポレートブランディングなどを担当。
                    現在、株式会社United Flowers代表取締役や複数のベンチャー企業の役員を兼任。2015年8月より株式会社ヘルストラストの代表取締役に就任。
                </p>
            </div>
            <div class="col-md-12 mt-4">
                <h4 class="mt-2 font-weight-bold">取締役　安川　修平
                </h4>
                <p>早稲田大学を卒業後、総合商社-映像制作会社を経て2008年（株）マッキャンエリクソンに入社。
                    広告営業としてコンタクトレンズメーカーや製薬会社など主にヘルスケア領域のクライアントを担当。
                    2013年にサラウンド株式会社を設立、代表取締役に就任。
                    2015年からはノーザンライツ株式会社取締役を兼務。
                    専門領域はマーケティング・営業全般。
                </p>
            </div>
        </div>

        <div id="inquiry" class="row mb-4 justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">お問い合わせ</h3>
            </div>
            <form id="inquiry_form" method="POST" action="{{ URL::to('/clinic/inquiry') }}" class="row col-md-8">
                @csrf
                <div class="col-md-12">
                    <h5 class="font-weight-bold">お名前</h5>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname-kanji">姓</label>
                        <input id="lastname-kanji" name="lastname-kanji" type="text" value="" placeholder="山本" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname-kanji">名</label>
                        <input id="firstname-kanji" name="firstname-kanji" type="text" value="" placeholder="太郎" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <h5 class="font-weight-bold">フリガナ</h5>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastname-kana">セイ</label>
                        <input id="lastname-kana" name="lastname-kana" type="text" value="" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="ヤマモト" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="firstname-kana">メイ</label>
                        <input id="firstname-kana" name="firstname-kana" type="text" value="" pattern="^[ァ-ン]+$" title="※カタカタのみ入力できます" placeholder="タロウ" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">医院名</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-name" name="clinic-name" type="text" value="" placeholder="みるペット動物病院" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">役職</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="user-role" name="user-role" type="text" value="" placeholder="院長" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">年代</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="user-age" name="user-age" required>
                            <option value="">選択して下さい</option>
                            @for ($i = 1; $i < 9; $i++)
                                <option
                                    value="{{$i}}0代"
                                    {{ $i."0代" == old('user-age') ? 'selected' : '' }}>{{$i}}0代
                                </option>
                            @endfor
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">メールアドレス</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-mail" name="clinic-mail" type="email" value="" placeholder="info@mirpet.co.jp" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">電話番号(ハイフンなし)</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="clinic-tel" name="clinic-tel" type="tel" value="" pattern="^[0-9]+$" title="※数字のみ入力できます" placeholder="0369103242" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">ご希望の連絡方法</h5>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="tel" data-label="電話" checked>電話
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="contact-method[]" value="email" data-label="メール" checked>メール
                            <span class="form-check-sign">
                                <span class="check"></span>
                            </span>
                        </label>
                    </div>
                </div>

                <div class="col-md-12 contact_method_tel_area">
                    <h5 class="font-weight-bold">電話連絡希望日時</h5>
                </div>

                <div class="col-md-12 contact_method_tel_area">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="contact-method-tel-date" class="col-form-label">連絡希望日時</label>
                            <select class="form-control" id="contact-method-tel-date" name="contact-method-tel-date">
                                <option value="指定なし" {{ empty(old('contact-method-tel-date')) ? 'selected' : '' }}>指定なし</option>
                                @php(setlocale(LC_ALL, 'ja_JP.UTF-8'))
                                @for ($i = 1; $i <= 7; $i++)
                                    @php($targetDate = \Carbon\Carbon::now()->addDay($i)->formatLocalized('%Y年%m月%d日(%a)'))
                                    <option value="{{$targetDate}}" {{ $targetDate == old('contact-method-tel-date') ? 'selected' : '' }}>{{$targetDate}}
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="contact-method-tel-date" class="col-form-label">連絡希望時間帯</label>
                            <select class="form-control" id="contact-method-tel-timezone" name="contact-method-tel-timezone">
                                @foreach(config('const.INQUIRY_TEL_DESIRED_TIMEZONE') as $time)
                                    <option value="{{$time}}" {{ $time == old('contact-method-tel-timezone') ? 'selected' : '' }}>{{$time}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">みるペットを知ったきっかけ</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="referer" name="referer" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.INQUIRY_REFERER') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('referer') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ内容</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.INQUIRY_TYPE') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('type') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ詳細</h5>
                </div>
                <div class="col-md-12">
                    <div class="textarea-container">
                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                    </div>
                </div>


                <div class="col-md-12 text-center mt-4">
                    <button id="confirm" type="button" class="btn btn-primary btn-lg">入力内容を確認</button>
                </div>


            </form>

        </div>

        <div id="company" class="row justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">会社概要</h3>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_3.jpg" class="rounded" style="height:300px; object-fit: cover;">
            </div>
            <div class="col-md-8">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>社名</td>
                        <td>株式会社みるペット</td>
                    </tr>
                    <tr>
                        <td>事業内容</td>
                        <td>動物病院の業務関連システムの企画、デザイン、開発、制作、管理及び運営業務</td>
                    </tr>
                    <tr>
                        <td>代表者</td>
                        <td>浅沼　直之</td>
                    </tr>
                    <tr>
                        <td>設立</td>
                        <td>2019年1月16日</td>
                    </tr>
                    <tr>
                        <td>本店所在地</td>
                        <td>〒104-0061　東京都中央区銀座７丁目１８ー１３　クオリア銀座５０４</td>
                    </tr>
                    {{--  <tr>
                        <td>電話番号</td>
                        <td>03-6910-3242</td>
                    </tr>  --}}
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">姓</div>
                        <div id="confirm-lastname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">名</div>
                        <div id="confirm-firstname-kanji"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">セイ</div>
                        <div id="confirm-lastname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メイ</div>
                        <div id="confirm-firstname-kana"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">医院名</div>
                        <div id="confirm-clinic-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">役職</div>
                        <div id="confirm-user-role"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">年代</div>
                        <div id="confirm-user-age"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-clinic-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-clinic-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">ご希望の連絡方法</div>
                        <div id="confirm-contact-method"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望日時</div>
                        <div id="confirm-contact-method-tel-date"></div>
                    </div>
                    <div class="col-6 modal_contact_method_tel_area">
                        <div class="font-weight-bold">連絡希望時間帯</div>
                        <div id="confirm-contact-method-tel-timezone"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">みるペットを知ったきっかけ</div>
                        <div id="confirm-referer"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('input[name="contact-method[]"]').change(function () {
                $('.contact_method_tel_area').hide();
                $('.modal_contact_method_tel_area').hide();

                $('input[name="contact-method[]"]:checked').each(function () {
                    var val = $(this).val();
                    if (val == "tel") {
                        $('.contact_method_tel_area').show();
                        $('.modal_contact_method_tel_area').show();
                    }
                })
            });

            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-lastname-kanji').text($('input[name="lastname-kanji"]').val());
                    $('#confirm-firstname-kanji').text($('input[name="firstname-kanji"]').val());
                    $('#confirm-lastname-kana').text($('input[name="lastname-kana"]').val());
                    $('#confirm-firstname-kana').text($('input[name="firstname-kana"]').val());
                    $('#confirm-clinic-name').text($('input[name="clinic-name"]').val());
                    $('#confirm-user-role').text($('input[name="user-role"]').val());
                    $('#confirm-user-age').text($('[name="user-age"]').val());
                    $('#confirm-clinic-mail').text($('input[name="clinic-mail"]').val());
                    $('#confirm-clinic-tel').text($('input[name="clinic-tel"]').val());

                    var contactMethod = [];
                    $('input[name="contact-method[]"]:checked').each(function () {
                        contactMethod.push($(this).data('label'));
                    });
                    $('#confirm-contact-method').text(contactMethod.length == 0 ? '選択されていません' : contactMethod);
                    $('#confirm-contact-method-tel-date').text($('[name="contact-method-tel-date"]').val());
                    $('#confirm-contact-method-tel-timezone').text($('[name="contact-method-tel-timezone"]').val());
                    $('#confirm-referer').text($('[name="referer"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection
