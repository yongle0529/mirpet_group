@if($data['flag'] == '1')
{{$data['username']}}様

お世話になっております。
みるペットサポートです。

下記相談・診療のご請求金額が決定しましたのでご連絡致します。

予約受付日時　　　　　{{$data['created_at']}}
飼い主様氏名　　　　　{{$data['username']}}
動物名　　　　　　　　{{$data['petname']}}
予約動物病院名　　　  {{$data['clinicname']}}
動物病院電話番号　　  {{$data['clinictel']}}
相談・診療時間　　　　{{$data['start_datetime']}}～{{$data['end_datetime']}}
ご請求金額　　　　　　　ログイン後管理画面を参照

下記よりログインし、マイページよりご請求金額をご確認ください。

https://mirpet.co.jp/app/owner/clinic/reserve/check/{{$data['diagId']}}

以上、よろしくお願いいたします。

──────────────────────
株式会社みるペット
https://mirpet.co.jp/
〒106-0061
東京都中央区銀座7丁目18番13クオリア銀座504
──────────────────────
@else
{{$data['clinicname']}}様

お世話になっております。
みるペットサポートです。

お振込み金額が決定しましたのでご連絡致します。

ご利用期間　　　　　{{substr($data['end_datetime'], 0, 4)}}年{{substr($$data['end_datetime'], 5, 2)}}月
お振込予定日　　    {{substr($data['end_datetime'], 0, 4)}}/{{substr($$data['end_datetime'], 5, 2)}}/{{substr($$data['end_datetime'], 8, 2)}}
お振込口座　　　　　ご登録済銀行
お振込金額　　　　　ログイン後管理画面を参照

下記よりログインし、管理画面よりお振込み金額をご確認ください。

https://mirpet.co.jp/app/clinic/

以上、よろしくお願いいたします。

──────────────────────
株式会社みるペット
https://mirpet.co.jp/
〒106-0061
東京都中央区銀座7丁目18番13クオリア銀座504
──────────────────────

@endif
