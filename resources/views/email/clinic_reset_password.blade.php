@component('mail::message')
みるペットサポートです。
みるペットのログインパスワードの再設定を行います。

@component('mail::button', ['url' => $data['url']])
パスワード再設定
@endcomponent

このリンクは60分間有効です。<br/>

このメールに心当たりがない場合は、このまま削除してください。<br/>

──────────────────────<br/>
株式会社みるペット<br/>
https://mirpet.co.jp/<br/>
〒106-0061<br/>
東京都中央区銀座7丁目18番13クオリア銀座504<br/>
──────────────────────<br/>
@endcomponent
