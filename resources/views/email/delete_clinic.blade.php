▼問い合わせ受付日時
{{\Carbon\Carbon::now()->format('Y/m/d H:i:s')}}
▼病院名
{{$data['last_name']}}
▼医院名
{{$data['clinic_name']}}
▼メールアドレス
{{$data['email']}}
▼電話番号
{{$data['tel']}}
▼病院住所
{{$data['url']}}
▼問い合わせ内容
{{$data['type']}}
株式会社みるペット

──────────────────────
https://mirpet.co.jp/
〒106-0061
東京都中央区銀座7丁目18番13クオリア銀座504
──────────────────────
