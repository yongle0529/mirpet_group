<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport"/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/clinic.css" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    {{--<link href="{{URL::to('/')}}/assets/demo/demo.css" rel="stylesheet"/>--}}
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, ".SFNSDisplay-Regular", "Hiragino Sans", ヒラギノ角ゴシック, "Hiragino Kaku Gothic Pro", "ヒラギノ角ゴ Pro W3", Meiryo, メイリオ, Osaka, "MS PGothic", arial, helvetica, sans-serif !important;
        }
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }
    </style>

    <!--   Core JS Files   -->
    <script src="{{URL::to('/')}}/assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>
{{--<!--  Google Maps Plugin    -->--}}
{{--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--}}
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{URL::to('/')}}/assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        // $(document).ready(function () {
        //     // the body of this function is in assets/js/now-ui-kit.js
        //     nowuiKit.initSliders();
        // });

        $(document).ready(function () {
            if (hash != null && hash != 'undefined') {
                var elm = "#" + hash;
                scrollToElement($(elm));
            }
        });

        function scrollToElement(element) {

            if (element.length != 0) {
                $("html, body").animate({
                    scrollTop: element.offset().top - 60
                }, 1000);
                $('#bodyClick').click();
            }
        }
    </script>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white; font-size: 14px">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="{{URL::to('/')}}/clinic" data-placement="bottom">
                <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        {{-- @yield('navi') --}}
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#whatismirpet"))' style="color: black;">
                        <p>みるペットとは</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/')}}/clinic/detail" style="color: black;">
                        <p>ご利用ガイド</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#ours"))' style="color: black;">
                        <p>私たちについて</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#qa"))' style="color: black;">
                        <p>よくある質問</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#inquiry"))' style="color: black;">
                        <p>お問い合わせ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-warning btn-round" href="{{URL::to('/')}}/owner">
                        <p>一般の方はこちら</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link btn btn-primary btn-round" href="{{URL::to('/')}}/clinic/login">
                        <p>獣医師ログイン</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="wrapper">

    @yield('header')
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">
            @yield('body')
            <footer class="footer" data-background-color="black" style=" font-size: 14px">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="javascript:void(0)" onclick='scrollToElement($("#company"))' >
                                    会社概要
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/')}}/clinic/rule">
                                    個人情報保護方針
                                </a>
                            </li>
                            <li>
                                    <a class="nav-link" href="{{URL::to('/')}}/clinic/transaction">
                                        特定商取引法に基づく表記
                                    </a>
                            </li>
                            <li>
                                    <a class="nav-link" target="" href="{{URL::to('/owner/userprotocal')}}">
                                        利用規約（飼い主様向け）
                                    </a>
                            </li>
                            <li>
                                    <a class="nav-link" target="" href="{{URL::to('/')}}/clinic/userprotocal">
                                        利用規約（動物病院向け)
                                    </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Mirpet, Inc.
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

</html>
