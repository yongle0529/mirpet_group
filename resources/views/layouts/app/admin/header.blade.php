<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
     <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <!-- Ionicons -->
    {{-- <link rel="stylesheet" href="{{ URL::to('/') }}/admins/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/admins/plugins/ekko-lightbox/ekko-lightbox.css">
    {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
    <!-- jQuery -->
    <script src="{{ URL::to('/') }}/admins/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ URL::to('/') }}/admins/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ URL::to('/') }}/admins/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="{{ URL::to('/') }}/admins/plugins/datatables/jquery.dataTables.js"></script>
    <script src="{{ URL::to('/') }}/admins/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- ChartJS -->
    <script src="{{ URL::to('/') }}/admins/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="{{ URL::to('/') }}/admins/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="{{ URL::to('/') }}/admins/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="{{ URL::to('/') }}/admins/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ URL::to('/') }}/admins/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="{{ URL::to('/') }}/admins/plugins/moment/moment.min.js"></script>
    <script src="{{ URL::to('/') }}/admins/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Select2 -->
    <script src="{{ URL::to('/') }}/admins/plugins/select2/js/select2.full.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ URL::to('/') }}/admins/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="{{ URL::to('/') }}/admins/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="{{ URL::to('/') }}/admins/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::to('/') }}/admins/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ URL::to('/') }}/admins/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL::to('/') }}/admins/dist/js/demo.js"></script>
    <script src="{{ URL::to('/') }}/admins/plugins/filterizr/jquery.filterizr.min.js"></script>

    <script src="{{ URL::to('/') }}/admins/plugins/filterizr/jquery.filterizr.min.js"></script>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
    <div class="wrapper">

        <!-- Navbar -->
        {{-- <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="{{ URL::to('/') }}/admins/#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ URL::to('/') }}/admins/index3.html" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ URL::to('/') }}/admins/#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
                    </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="{{ URL::to('/') }}/admins/#">
                        <i class="far fa-comments"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{ URL::to('/') }}/admins/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{ URL::to('/') }}/admins/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        John Pierce
                                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">I got your message bro</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{ URL::to('/') }}/admins/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Nora Silvester
                                        <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">The subject goes here</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="{{ URL::to('/') }}/admins/#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ URL::to('/') }}/admins/#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="{{ URL::to('/') }}/admins/#">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav> --}}
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{URL::to('/app/admin')}}" class="brand-link">
                <img src="{{ URL::to('/') }}/admins/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">MirPet Admin</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ URL::to('/') }}/admins/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="{{ URL::to('/') }}/admins/#" class="d-block">Alexander Pierce</a>
                    </div>
                </div> --}}

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <?php if($menuactive==1){ ?>
                            <a href="{{URL::to('/app/admin')}}" class="nav-link active">
                                <?php }else{?>
                                    <a href="{{URL::to('/app/admin')}}" class="nav-link">
                                    <?php }?>
                                <i class="nav-icon fas fa-notes-medical"></i>
                                <p>
                                    診察一覧
                                </p>
                            </a>
                        </li>

                            <?php if($menuactive==21 or $menuactive==22 or $menuactive==23){ ?>
                            <li class="nav-item has-treeview menu-open">
                            <a href="{{URL::to('/app/admin/analysis')}}" class="nav-link active">
                                <?php }else{?>
                            <li class="nav-item has-treeview ">
                            <a href="{{URL::to('/app/admin/analysis')}}" class="nav-link">
                                 <?php }?>
                                <i class="nav-icon fas fa-chart-pie"></i>
                                <p>
                                    分析
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <?php if($menuactive==21){ ?>
                                    <a href="{{URL::to('/app/admin/hospital_analy')}}" class="nav-link active">
                                        <?php }else{?>
                                     <a href="{{URL::to('/app/admin/hospital_analy')}}" class="nav-link">
                                        <?php }?>
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>売上動物病院</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                <?php if($menuactive==22){ ?>
                                    <a href="{{URL::to('/app/admin/owner_analy')}}" class="nav-link active">
                                <?php }else{?>
                                <a href="{{URL::to('/app/admin/owner_analy')}}" class="nav-link">
                                <?php }?>
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>売上飼い主様</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                <?php if($menuactive==23){ ?>
                                    <a href="{{URL::to('/app/admin/other_analy')}}" class="nav-link active">
                                <?php }else{?>
                                <a href="{{URL::to('/app/admin/other_analy')}}" class="nav-link">
                                <?php }?>
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>その他</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            @if($menuactive==3)
                            <a href="{{URL::to('/app/admin/hospital_manage')}}" class="nav-link active">
                            @else
                            <a href="{{URL::to('/app/admin/hospital_manage')}}" class="nav-link">
                            @endif
                                <i class="nav-icon fas fa-hospital-alt"></i>
                                <p>
                                    動物病院管理
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            @if($menuactive==4)
                            <a href="{{URL::to('/app/admin/petadmin_manage')}}" class="nav-link active">
                            @else
                            <a href="{{URL::to('/app/admin/petadmin_manage')}}" class="nav-link">
                            @endif
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    飼い主様管理
                                </p>
                            </a>
                        </li>
                        @if($menuactive==51 or $menuactive==52 or $menuactive==53)
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{URL::to('/app/admin/alram_message_new')}}" class="nav-link active">
                        @else
                        <li class="nav-item has-treeview">
                            <a href="{{URL::to('/app/admin/alram_message_new')}}" class="nav-link ">
                        @endif
                                <i class="nav-icon far fa-envelope"></i>
                                <p>
                                    お知らせ
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                {{-- <li class="nav-item">
                                    @if($menuactive==51)
                                    <a href="{{URL::to('/app/admin/alram_message_new')}}" class="nav-link active">
                                    @else
                                    <a href="{{URL::to('/app/admin/alram_message_new')}}" class="nav-link ">
                                    @endif
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>新作</p>
                                    </a>
                                </li> --}}
                                <li class="nav-item">
                                    @if($menuactive==52)
                                    <a href="{{URL::to('/app/admin/alram_message_receive')}}" class="nav-link active">
                                    @else
                                    <a href="{{URL::to('/app/admin/alram_message_receive')}}" class="nav-link ">
                                    @endif
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>受信</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    @if($menuactive==53)
                                    <a href="{{URL::to('/app/admin/alram_message_send')}}" class="nav-link active">
                                    @else
                                    <a href="{{URL::to('/app/admin/alram_message_send')}}" class="nav-link ">
                                    @endif
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>送信</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            @if($menuactive==6)
                            <a href="{{URL::to('/app/admin/admin_setting')}}" class="nav-link active">
                            @else
                            <a href="{{URL::to('/app/admin/admin_setting')}}" class="nav-link ">
                            @endif
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    設定
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            @if($menuactive==7)
                            <a href="{{URL::to('/app/admin/account_setting')}}" class="nav-link active">
                            @else
                            <a href="{{URL::to('/app/admin/account_setting')}}" class="nav-link ">
                            @endif
                                <i class="nav-icon fas fa-user-edit"></i>
                                <p>
                                    プロフィール
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{URL::to('/admin/logout')}}" class="nav-link ">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    ログアウト
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2019
            <div class="float-right d-none d-sm-inline-block">
                {{-- <b>Version</b> 3.0.0 --}}
                <a href="#">会社概要</a>.</strong> 個人情報保護方針.
            </div>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->


    @stack('extra-script')
</body>

</html>
