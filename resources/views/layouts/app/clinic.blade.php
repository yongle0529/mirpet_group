<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('/')}}/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        みるペット | ペット向けオンライン相談・診療システムのみるペット
    </title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport"/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet"/>
    <link href="{{URL::to('/')}}/assets/css/clinic.css" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}


<!-- Scripts -->
    <!--   Core JS Files   -->
    <script src="{{URL::to('/')}}/assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{URL::to('/')}}/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="{{URL::to('/')}}/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{URL::to('/')}}/assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
    <script src="{{URL::to('/')}}/assets/js/index.js" type="text/javascript"></script>
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    {{--<link href="{{URL::to('/')}}/assets/demo/demo.css" rel="stylesheet"/>--}}
    <style>font-family: -apple-system, BlinkMacSystemFont, ".SFNSDisplay-Regular", "Hiragino Sans", ヒラギノ角ゴシック, "Hiragino Kaku Gothic Pro", "ヒラギノ角ゴ Pro W3", Meiryo, メイリオ, Osaka, "MS PGothic", arial, helvetica, sans-serif !important;
        .title {
            margin-bottom: 8px;
        }

        .border-bottom {
            border-width: 3px !important;
        }

        .description {
            font-size: 1rem;
            line-height: 1.5rem;
            color: black;
        }
    </style>
</head>

<body class="index-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top" style="background-color: white; font-size: 14px">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="{{URL::to('/')}}/app/clinic" data-placement="bottom">
                <img src="{{URL::to('/')}}/assets/img/mirpet_logo_C_01.png" style="max-width: 150px;">
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar middle-bar" style="background: black;"></span>
                <span class="navbar-toggler-bar bottom-bar" style="background: black;"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">

                {{-- @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning btn-round" href="{{ route('register') }}">新規会員登録</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary btn-round" href="{{ route('login') }}">ログイン</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/clinic')}}" style="color: black;">
                            受付状況
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/clinic/reserve')}}" style="color: black;">
                            予約状況
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/clinic/search')}}" style="color: black;">
                            飼い主・動物検索
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/clinic/setting')}}" style="color: black;">
                            各種設定​
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{URL::to('/app/owner/faq')}}" style="color: black;">
                            よくある質問
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-default" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/app/clinic')}}" style="color: black;">
                        受付状況
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/app/clinic/reserve')}}" style="color: black;">
                        予約状況
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/app/clinic/search')}}" style="color: black;">
                        飼い主・動物検索
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/app/clinic/setting/profile')}}" style="color: black;">
                        各種設定​
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/app/clinic/faq')}}" style="color: black;">
                        よくある質問
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-default" href="{{ route('clinic.logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('clinic.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">

    <div style="min-height: 80px;">

    </div>
    <div class="main">
        <div class="section" style="padding: 1rem 0 0 0;">
            @if(Request::is('app/clinic/setting/*'))
            <div class="container">
                <div class="row mb-3">
                    <style>
                        .menu-btn {
                            min-width: 10vw;
                        }
                    </style>
                    <script>
                        $(function () {
                            var current = "{{Request::path()}}";
                            $('.menu-btn').each(function (index, element) {
                                var rel = $(this).attr('rel');
                                if (current.indexOf(rel) > -1) {
                                    $(element).addClass('btn-primary');
                                    $(element).removeClass('btn-default');
                                }
                            })
                        });
                    </script>

                    <a href="{{route('setting.clinic_profile')}}" class="menu-btn btn btn-default" rel="profile">病院情報設定</a>
                    <a href="{{route('setting.clinic_reserve')}}" class="menu-btn btn btn-default" rel="reserve">予約設定​</a>
                    <a href="{{route('setting.clinic_menu')}}" class="menu-btn btn btn-default" rel="menu">相談・診療メニューの追加</a>
                    <a href="{{route('setting.clinic_password')}}" class="menu-btn btn btn-default" rel="password">パスワード設定</a>
                    <a href="{{route('setting.clinic_payment')}}" class="menu-btn btn btn-default" rel="payment">取引口座設定​</a>
                </div>
            </div>
            @endif

            @yield('content')
            <footer class="footer mt-5" data-background-color="black" style="font-size: 14px">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="{{URL::to('/app/clinic/footer/company')}}" >
                                    会社概要
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/clinic/footer/rule')}}">
                                    個人情報保護方針
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{URL::to('/app/clinic/footer/transaction')}}">
                                    特定商取引法に基づく表記
                                </a>
                            </li>
                            <li>
                                    <a class="nav-link" target="_blank" href="{{URL::to('/')}}/clinic/userprotocal_pop">
                                        利用規約（動物病院向け）
                                    </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Mirpet, Inc.
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
