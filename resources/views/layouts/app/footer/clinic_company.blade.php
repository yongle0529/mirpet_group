@extends('layouts.app.clinic')
@section('content')
    <div class="container">

        <div id="company" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">会社概要</h3>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_6.jpg" class="rounded" style="height:300px; object-fit: cover;">
            </div>
            <div class="col-md-8">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>社名</td>
                        <td>株式会社みるペット</td>
                    </tr>
                    <tr>
                        <td>事業内容</td>
                        <td>動物病院の業務関連システムの企画、デザイン、開発、制作、管理及び運営業務</td>
                    </tr>
                    <tr>
                        <td>代表者</td>
                        <td>浅沼　直之</td>
                    </tr>
                    <tr>
                        <td>設立</td>
                        <td>2019年1月16日</td>
                    </tr>
                    <tr>
                        <td>本店所在地</td>
                        <td>〒104-0061　東京都中央区銀座７丁目１８ー１３　クオリア銀座５０４</td>
                    </tr>
                    {{--  <tr>
                        <td>電話番号</td>
                        <td>03-6910-3242</td>
                    </tr>  --}}
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">氏名</div>
                        <div id="confirm-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-name').text($('input[name="name"]').val());
                    $('#confirm-mail').text($('input[name="mail"]').val());
                    $('#confirm-tel').text($('input[name="tel"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection
