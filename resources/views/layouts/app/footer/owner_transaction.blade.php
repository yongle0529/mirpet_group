@extends('layouts.app.owner')
@section('content')
    <div class="container">

            <div id="transaction" class="row justify-content-md-center">
                    <div class="col-md-12 border-bottom border-danger mb-4">
                        <h3 class="title">特定商取引法に基づく表記</h3>
                    </div>
                    <div class="col-md-10 text-left">
                        <h4>販売事業者名</h4>
                        <ol>株式会社みるペット</ol>

                        <h4>代表者または運営統括責任者名前</h4>
                        <ol>代表取締役社長　浅沼　直之</ol>

                        <h4>販売事業者所在地</h4>
                        <ol>〒106-0061　東京都中央区銀座７丁目１８番１３クオリア銀座５０４</ol>

                        <h4>問い合わせ先</h4>
                        <ol><a class="nav-link" href="{{URL::to('/app/owner/contactus')}}">お問い合わせフォーム</a></ol>

                        <h4>電話番号</h4>
                        <ol>上記にてお問い合わせください</ol>

                        <h4>販売価格</h4>
                        <ol>商品毎に記載</ol>

                        <h4>商品代金以外に必要な費用</h4>
                        <ol>消費税</ol>

                        <h4>代金の支払い時期および方法</h4>
                        <ol>クレジットカード（支払い時期は、各カード会社引き落とし日）</ol>

                        <h4>返品の取り扱い条件、解約条件</h4>
                        <ol>商品の特性上、不可</ol>


                        <h4>不良品の取り扱い条件</h4>
                        <ol>商品の特性上、不可</ol>
                    </div>
                </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">氏名</div>
                        <div id="confirm-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-name').text($('input[name="name"]').val());
                    $('#confirm-mail').text($('input[name="mail"]').val());
                    $('#confirm-tel').text($('input[name="tel"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection
