@extends('layouts.app.clinic')
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" media="all" href="{{URL::to('/')}}/assets/rule/users.ff0a16e6264403200113.css">
<meta name="csrf-param" content="authenticity_token">
<meta name="csrf-token" content="RrCvaWvEpI4/LQ+63EWGGk9nx6o1v4nviily62hO65se2AC7pg0B2SLHPpr9t88nB52X/1GCerf6OYqysD2ChQ==">

<link type="text/css" rel="stylesheet" charset="UTF-8" href="{{URL::to('/')}}/assets/rule/translateelement.css"></head>
<body>
  <!-- Google Tag Manager -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-KRXS8B" height="0" width="0" style="display:none;visibility:hidden" ></iframe>
</noscript>
<script async="" src="{{URL::to('/')}}/assets/rule/lt.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/saved_resource"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/linkid.js.download"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/f.txt"></script><script type="text/javascript" async="" src="{{URL::to('/')}}/assets/rule/analytics.js.download"></script><script src="{{URL::to('/')}}/assets/rule/940796602644488" async=""></script><script async="" src="{{URL::to('/')}}/assets/rule/fbevents.js.download"></script><script async="" src="{{URL::to('/')}}/assets/rule/gtm.js.download"></script><script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KRXS8B');
</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n; n.loaded=!0; n.version='2.0'; n.queue=[]; t=b.createElement(e); t.async=!0;
t.src=v; s=b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s) }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');
fbq('init', '940796602644488');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=940796602644488&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
@section('content')
    <div class="container">

            <div id="userprotocal" class="row">

                    <div class="col-md-12">
                        <div class="o-wrapper__body">
                            <main>
                            <div class="o-container o-container@desktop c-background c-background--main">
                              <div class="o-container__column o-container__column@desktop">
        
                                <div class="o-container__item o-container__item@desktop">
                                  <section>
                                    <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                      <div class="c-segment__inner c-segment__inner@desktop">
                                        <h1 class="c-heading c-heading--3xl@desktop c-heading--3xl">みるペット利用規約</h1>
                                      </div>
                                    </div>
                                  </section>
                                </div>
                                <div class="o-container__item o-container__item@desktop">
                                  <section>
                                    <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                      <div class="c-segment__inner c-segment__inner--3large@desktop">
                                        <div class="c-agreement">
                                          <section>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１条（適用）</h3>
                                            <ol class="c-agreement__list">
                                                    1. 本規約は、本サービス提供条件および本サービスの利用に関する株式会社みるペット（以下、「当社」といいます。）とユーザーとの間の権利義務関係を定めることを目的とし、ユーザーと当社との間の本サービスの利用に関わる一切の関係に適用されます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. 本規約の内容と、本規約外における本サービスの説明・ヘルプ・投稿・ガイドライン等（以下、「個別規定」といいます。）とが異なる場合は、個別規定が優先して適用されるものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. ユーザーが未成年である場合には、親権者等の法定代理人の同意（本規約への同意を含みます。）を得て本サービスを利用してください。また、本規約に同意した時点で未成年であったユーザーが、成年に達した後に本サービスを利用した場合、未成年であった間の利用行為を追認したものとみなします。
                                            </ol>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第２条（定義）</h3>
                                            <ol class="c-agreement__list">
                                                    1. 「本サービス」とは、当社が企画・運営するオンライン相談、診療支援システム「みるペット」をいいます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. 「ユーザー」とは、本サービスを利用して登録動物病院等による相談、診療等を受ける動物の飼い主様個人をいいます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. 「本サイト等」とは、当社が、本サービスを提供するためのウェブサイトをいいます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. 「本契約」とは、当社がユーザーに対し、本サービスを提供し、ユーザーが当社、登録動物病院等に対し、本規約に基づき発生する本サービス利用料を支払うことを約することをいいます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    5. 「登録動物病院等」とは、本サイト等に登録している動物病院やその他法人であり、動物に関しての相談や医療等に従事する者をいいます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    6. 「登録獣医師等」とは、本サイト等に登録している獣医師その他の獣医療従事者であり、動物に関しての相談、診療等に従事する者をいいます。
                                            </ol>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第３条（ユーザー）</h3>
                                            <ol class="c-agreement__list">
                                                    1. ユーザーは、本サービスの利用に際し、本規約の定めに従うことを承諾したものとみなします。
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. ユーザーは、自らの意思によって本サービスを利用するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. ユーザーは、本サービスの利用にあたり、本サイト等の定めるところに従いユーザー登録を行う必要があり、ユーザー登録の完了をもって本契約が成立するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. 当社は、ユーザーが以下に定める事由のいずれかに該当すると判断した場合、登録を拒否または抹消することができるものとします。なお、当社は、当該拒否または抹消について一切の責任を負わず、拒否または抹消の理由を説明する義務を負わないものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (1) 本規約に違反する行為を行うおそれがある場合または過去に違反した事実が判明した場合
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (2) 登録時に当社に提供された情報に虚偽の記載や記載漏れがあった場合
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (3) その他当社が不適切と判断した場合
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    5. ユーザーは、本サービスにおいて登録した情報の内容について一切の責任を負います。また、ユーザーは本サービス上で自ら作成・保存したデータおよび登録動物病院等が本サービスを経由してユーザーに提供したデータ（以下「ユーザーデータ」といいます。）の保管およびバックアップについては自ら責任を負うものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    6. ユーザーは、本サービスにおいて登録した情報を、本サービスを利用するために必要な範囲内で、ユーザー自らが変更、追加、削除できるものとし、常にユーザーが責任をもって正確な状態に保つものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    7. ユーザーは、自らの意思により本サービスへの登録を削除し、退会することができます。本サービスから退会した場合には、ユーザーのアカウント情報を復活させることができなくなるとともに、ユーザーデータにアクセスすることができなくなります。当社は退会済みユーザーのユーザーデータについては削除するものとします。ただし、退会済みユーザーのユーザーデータのうち、登録動物病院等が当該ユーザーデータを保管する義務または必要性のある限りにおいて継続保管・利用することができるものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    8. ユーザーは、自らの責任と費用において、本サービスの利用に必要な環境（ハードウェア、ソフトウェア、インターネット接続回線、セキュリティーの確保等）を整備するものとします。当社は、ユーザーのデータの情報セキュリティを確保するために、本サービスに関するインフラストラクチャ、ネットワーク、アプリケーション及びデータ等を対象とした情報セキュリティ対策を行うこととします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    9. ユーザーは、本サービスで利用可能なID、パスワード等が第三者に知られないように管理する責任を負い、ID、パスワード等がユーザーの故意または過失により第三者に知られることによって自身に損害が生じた場合、その損害を自らの責任で負担するものとし、当社はこれに一切の責任を負わないものとします。
                                            </ol>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第４条（予約・相談、診療）</h3>
                                            <ol class="c-agreement__list">
                                                    1. ユーザーは、本サービスへの登録を完了することにより、本サイト等を通じて、登録動物病院等に所属する登録獣医師等に対し、動物に関しての相談、診療等の予約をすることができます。原則として予約した登録獣医師等が相談、診療等を実施するが、登録動物病院等及び登録獣医師等側の都合により、予約時と違う登録獣医師等が診療を行う場合があることに同意します。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. ユーザーは、相談、診療等の予約にあたり、本サイト等に定めるところに従い、必要な情報を入力するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. ユーザーは、相談、診療メニューに応じ、登録動物病院等において対面での診察を受けた後でなければ本サイト等を通じたオンラインによる診察を受けられない場合があることに同意するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. ユーザーは、ユーザーが本サイト等を通じて登録動物病院等に所属する登録獣医師等に対し動物に関しての相談、診療等の予約をした場合、本サービスの利用にあたりユーザーが入力した情報が、当該動物病院等に共有されることについて同意するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    5. ユーザーが第1項に従って動物に関しての相談、診療等の予約をし、前項に基づき登録動物病院等に対してユーザーの入力情報が通知された時点で、ユーザーと当該登録動物病院等との間で動物に関しての相談、診療契約が締結されるものとします。ユーザーの動物に対して相談、診察・検査・処置等を行い、または医薬品の配送を行うのは登録獣医師等またはその所属する登録動物病院等のスタッフであり、当社は、これらの行為についていかなる責任も負わないものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    6. 動物に関しての相談、診療契約に関連して生じた問い合わせ、苦情、請求、紛争等については、ユーザー及び登録動物病院等及び登録動物病院等に所属する登録獣医師等との間で解決するものとします。当社は、これらの紛争等については、一切責任を負うことなく、また、その解決に関与する義務を負わないものとします。
                                            </ol>
        
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第５条（料金）</h3>
                                            <ol class="c-agreement__list">
                                                    1. ユーザーは、本サービスの利用にあたり、本サイト等上に表示される料金（以下、「本サービス利用料」といいます。）を当社に対して支払うものとします。
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. 本サービス利用料には、本サービスを通じて登録動物病院等より提供される動物に関しての相談、診療等及びそれを予約する対価としての予約料などが含まれます。当社が受領する本サービス利用料には、登録動物病院等を代理して受領するものが含まれますが、これらの料金の受領をもって、当社が相談、診療等や医薬品の販売を行っているものとみなされるものではありません。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. 保険診療においては、患者様の状況や契約保険会社等によって相談、診療内容や費用が異なるため、通常の対面診療の際と同様に、相談、診療前に金額を確定することができません。事前に確定できる予約料や、システム利用料は、本サイト等上に原則明示します。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. 本サービス利用料の支払いは、ユーザーが本サイト等の利用にあたり登録したクレジットカードによってなされるものとします。ユーザーは、クレジットカードの登録情報に変更または誤りがあった場合、直ちに修正するものとします。ユーザーのクレジットカードによる決済が1回でもできなかった場合、当社は、ユーザーによる本サービスの利用を停止させることができるものとします。
                                            </ol>
                                          </section>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                </div>
        
                                <div class="o-container__item o-container__item@desktop">
                                  <section>
                                    <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                      <div class="c-segment__inner c-segment__inner--3large@desktop">
                                        <div class="c-agreement">
                                          <section>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第６条（予約料の特約）</h3>
                                            <ol class="c-agreement__list">
                                                    1. ユーザーは、本サイト等を通じて、相談、診療等の予約をすることにより、当社に対し予約料を支払う義務を負います。予約料の金額はそれぞれの登録医療機関が設定したものとし、本サイト等上に表示されるところに従うものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. ユーザーは、本サイト等を通じて予約日時を変更またはキャンセルすることができます。ユーザーが予約をキャンセルする場合、本サイト等上に表示される所定の手続に従い申請することにより、予約料の支払いは免除されるものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. 前項にかかわらず、ユーザーが予約した診療日当日に、ユーザーの都合により予約を当日キャンセルする場合、キャンセル料としてそのまま予約料はお支払いいただきます。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. ユーザーが予約した相談、診療開始時刻から本サイト等上で表示する一定時間が経過するまでにオンラインでの診療等が開始されない場合、ユーザーの都合により予約をキャンセルしたものとみなし、キャンセル料としてそのまま予約料はお支払いいただきます。ただし、ユーザーから第2項に従い返金の申請があった場合において、当社が登録獣医師等またはその所属する登録動物病院等の都合により相談、診療が開始されなかったことを確認した場合には、予約料の支払いは免除されるものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    5. ユーザーは、登録動物病院等の都合により、ユーザーに対して予約日時のキャンセルの通知がなされる場合があることに同意します。予約キャンセルについては第2項に従い、予約料の支払いは免除されるものとします。
                                            </ol>
        
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第７条（相談、診療費用）</h3>
                                            <ol class="c-agreement__list">
                                                    1. 当社は、本サイト等上で、相談、診療メニューに応じ、登録動物病院等が設定する相談、診療費用の見込み金額を示すものとします。
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. ユーザーは、登録獣医師等の相談、診察内容により、実際の相談、診療費用が、前項に従い本サイト等上に表示された金額を上回ることや下回ることがあることに同意するものとします。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    3. ユーザーは、相談、診療等の実施後、当社に対し、確定した相談、診療費用を支払う義務を負います。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    4. 保険診療につき、ユーザーが本サイト等を通じて予約を行い診療等を受けたにもかかわらず、ユーザーが提示する健康保険証が失効等していた場合には、ユーザーは診療費用全額の支払義務を負います。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    5. ユーザーは、ユーザーが予約した相談、診療日時に限り、本サイト等を通じた登録獣医師等によるオンラインでの動物に関した相談、診療等を受診することができます。ユーザーは、予約した診療時間内に登録獣医師等によるオンラインでの診療等が完了しなかった場合でも、その時間内に受けた診療内容に応じて診療費用の支払義務を負うことに同意するものとします。ただし、登録獣医師等またはその所属する登録動物病院等の責めに基づく事由により診療等が完了しなかった場合は、この限りではありません。
                                            </ol>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第８条（禁止事項・損害補償）</h3>
                                            <ol class="c-agreement__list">
                                                    1. ユーザーは、以下の各行為を行ってはならないものとします。
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (1) 当社、登録動物病院等及び登録獣医師等に対し虚偽の情報を提供する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (2) 他人の動物に関した相談、診療を受ける目的で本サービスを利用する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (3) 本サービスの利用に関し、自らまたは第三者のために不正な利益を得ようとする行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (4) 他人の知的財産権、プライバシーに関する権利、その他の権利または利益を侵害する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (5) コンピューター・ウイルスその他の有害なコンピューター・プログラムを含む情報を送信する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (6) 個人や団体を誹謗中傷する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (7) 本サービスで得た情報を本サービスの利用目的の範囲を超えて第三者に譲渡する行為または営利目的で譲渡する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (8) 公序良俗に反する行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (9) 法令に反する一切の行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (10) 本サービスの運営を妨げる行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (10) 本サービスの運営を妨げる行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (11) その他本サービスの提供を継続することが困難であると当社が判断する一切の行為
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    2. ユーザーが本規約の各条項に違反し、当社または第三者に対して損害を与えた場合は、ユーザーは当社または第三者に対し損害賠償義務を負うものとします。
        
                                            </ol>
        
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第９条（情報の変更・削除、本契約の解除等）</h3>
                                            <ol class="c-agreement__list">
                                                    当社は、ユーザーが本規約に違反する行為をし、もしくはその行為をする恐れがある場合、ユーザーによる本サービスの利用が不適切であると当社が判断する場合、ユーザーによる本サービスの利用により本サービスの運営に支障が生じると当社が判断する場合、当該ユーザーに何ら事前の通知をすることなく以下の措置を講じることができるものとします。なお、当社はユーザーに対し、下記の措置を講じる理由について説明する義務を負いません。
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (1) ユーザーが本サービスにおいて登録した情報の全部または一部についての変更または削除
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (2) 本契約の解除およびそれに伴う本サービス利用の停止、または、本サービスのユーザーとしての登録の抹消
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (3) その他当社が必要と認める措置
                                            </ol>
        
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１０条（サービス内容の変更）</h3>
                                            <ol class="c-agreement__list">
                                                    当社は、ユーザーの事前の承諾なしに本サービスの内容を変更することができるものとします。
        
                                            </ol>
                                            <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１１条（サービスの停止・終了等）</h3>
                                            <ol class="c-agreement__list">
                                                    当社は、以下のいずれかに該当する事由によりユーザーへの事前の通知および承諾を要することなく、本サービスを停止または終了することができます。
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (1) 本サービス運営のためのシステムの保守、更新等を定期的または臨時に行う場合
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (2) ウィルス被害、火災、停電、天災地変などの不可抗力により、本サービスの提供が困難な場合
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (3) 第三者の故意または過失による行為によって発生した本システムの不具合について対策を講じる必要がある場合
        
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (4) 法令等の改正、成立により本サービスの運営が困難となった場合
                                            </ol>
                                            <ol class="c-agreement__list">
                                                    (5) その他、当社が本サービスの提供の停止・終了が必要と判断した場合
        
                                            </ol>
                                          </section>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                </div>
        
                                <div class="o-container__item o-container__item@desktop">
                                        <section>
                                          <div class="c-segment c-segment--fit c-segment--fit@desktop">
                                            <div class="c-segment__inner c-segment__inner--3large@desktop">
                                              <div class="c-agreement">
                                                <section>
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１２条（免責）</h3>
                                                  <ol class="c-agreement__list">
                                                        1. 当社は、登録獣医師等またはその所属する登録動物病院等の第三者の情報、ユーザー等が本サービスに登録し掲載する情報等に関し、内容の正確性、有用性、完全性等について何らの保証をしないものとします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        2. 当社は、本サービスの利用に関し、以下のことを保証しないものとします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (1) 本サービスが中断しないこと
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (2) 本サービスにエラーが生じないこと
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (3) 本サービスの利用に関し通信回線等の障害がないこと
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (4) 本サイト等上のコンテンツに関する盗用、毀損または改ざんがないこと
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (5) 本サイト等に対する不正アクセス・ハッキング等のサイバー攻撃がないこと
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        3. 当社は、ユーザーの本サービスへの登録および本サービスの利用から生じる一切の損害に関して、責任を負わないものとします。
                                                  </ol>
        
                                                  <ol class="c-agreement__list">
                                                        4. 当社は、当社によるユーザー情報の変更、削除または消失、本サービスの内容の変更、本サービスの提供の停止または終了、本サービスの利用不能、本サービスの利用によるデータの消失または機器の故障若しくは損傷、その他本サービスに関連してユーザーが被った損害につき、一切責任を負わないものとします。
                                                  </ol>
        
                                                  <ol class="c-agreement__list">
                                                        5. 前3条の規定により、ユーザーの情報が変更・削除された場合、本契約が解除された場合、本サービス内容が変更された場合、または本サービスが停止・終了した場合においても、ユーザーと登録獣医師等またはその所属する登録動物病院等との間で成立する相談、診療契約の内容に影響を及ぼすものではありません。
                                                  </ol>
        
                                                  <ol class="c-agreement__list">
                                                        6. 当社は、ユーザーによる本サービスの利用により、ユーザーの疾病・疾患が治癒し、相談、診療等が終了することを保証するものではなく、また、登録獣医師等またはその所属する登録動物病院等がユーザーの相談、診療等を開始・継続することを保証するものではありません。
                                                  </ol>
        
                                                  <ol class="c-agreement__list">
                                                        7. 当社が本サービスに関してユーザーに対して損害賠償責任を負うべき場合でも、ユーザーは当社に損害発生についての故意または重過失がある場合に限り損害賠償を請求することができるものとします。この場合において、ユーザーは、当社の責任は直接損害に限られ、また、当社がユーザーに対して賠償する損害の累計額は、当社が本サービスに関連して当該ユーザーから受領した本サービス利用料の合計額を上限とすることに同意するものとします。
                                                  </ol>
        
        
        
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１３条（暴力団等排除条項）</h3>
                                                  <ol class="c-agreement__list">
                                                        1. ユーザーは、現在、暴力団、暴力団員、暴力団員でなくなった時から5年を経過しない者、暴力団準構成員、暴力団関係企業、総会屋等、社会運動等標ぼうゴロまたは特殊知能暴力団等、その他これらに準ずる者(以下、これらを「暴力団員等」といいます。)に該当しないことを表明し、かつ将来にわたっても該当しないことを確約するものとします。
        
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        2. ユーザーは、自らまたは第三者を利用して次の各号の一にでも該当する行為を行わないことを確約するものとします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (1) 暴力的な要求行為
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (2) 法的な責任を超えた不当な要求行為
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (3) 取引に関して、脅迫的な言動をし、または暴力を用いる行為
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (4) 風説を流布し、偽計を用いまたは威力を用いて、当社、他の利用者、その他第三者の信用を毀損し、または、当社、他の利用者、その他第三者の業務を妨害する行為
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        (5) その他前各号に準ずる行為
                                                  </ol>
        
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１４条（提供情報の利用）</h3>
                                                  <ol class="c-agreement__list">
                                                        1. 本サービスにおける個人情報の取り扱いについては、みるペットの「個人情報保護方針」に従うものとします。ユーザーは、本サービスを利用する場合には、みるペットの「個人情報保護方針」の各規定に従うことを承諾したものとみなします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        2. 当社は、本サービスを提供する上でユーザーにとって必要な情報を、ユーザーに対し、Eメール、郵便、電話、対面での伝達等によって連絡をすることができるものとします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        3. 当社は、当社の定める、みるペットの「個人情報保護方針」で定義される個人情報を含まない限りにおいて、登録情報または本サービスの利用状況についての情報を、あらゆるものに二次利用することができるものとします。これらの情報に関わる知的財産権は当社が保有するものとします。
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        4. ユーザーは、本サービスの利用に伴う発生する各種ログ情報を含むユーザーデータを、本サービスの提供する機能を通じてのみ閲覧・利用できるものとし、当社はログその他のユーザーデータの提供依頼等に対応する義務を負わないものとします。
                                                  </ol>
        
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１５条（規約の変更）</h3>
                                                  <ol class="c-agreement__list">
                                                        当社は、ユーザーの承諾を得ることなく、本規約を随時変更することができ、本サイト等上に表示された時点より効力を生じるものとします。
        
                                                  </ol>
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１６条（地位譲渡）</h3>
                                                  <ol class="c-agreement__list">
                                                        1. ユーザーは、当社の書面による事前の承諾なく、本規約に基づく権利または義務につき、第三者に対し、譲渡、移転、担保設定、その他の処分をすることや、第三者に相続させることはできません。
        
                                                  </ol>
                                                  <ol class="c-agreement__list">
                                                        2. 当社が本サービスにかかる事業を第三者に譲渡する場合には、ユーザーの承諾を得ることなく、当該事業譲渡に伴い、本規約に基づく権利および義務並びにユーザーの登録情報その他の顧客情報等を含む本契約上の地位を当該事業譲渡の譲受人に譲渡することができるものとします。なお、このことは、事業譲渡のみならず、会社分割その他事業が移転するあらゆる場合においても同様とします。
        
                                                  </ol>
                                                  <h3 class="c-agreement__title c-agreement__item c-agreement__item--l">第１７条（準拠法および管轄裁判所）</h3>
                                                  <ol class="c-agreement__list">
                                                        本サービスおよび本規約を含む本契約の準拠法は日本法とします。本サービスおよび本規約を含む本契約に関して生じる一切の紛争については、東京地方裁判所または東京簡易裁判所を第 1 審の専属的合意管轄裁判所とします。
        
                                                  </ol>
                                                </section>
                                              </div>
                                            </div>
                                          </div>
                                        </section>
                                      </div>
                              </div>
                            </div>
        
                            <div class="c-overlay c-overlay--invisible js-overlay"></div>
                           </main>
                          </div>
                    </div>
                </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">氏名</div>
                        <div id="confirm-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-name').text($('input[name="name"]').val());
                    $('#confirm-mail').text($('input[name="mail"]').val());
                    $('#confirm-tel').text($('input[name="tel"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection