@extends('layouts.app.owner')

@section('content')
    <form action="{{URL::to('/app/owner/payment')}}" method="post">
        <script src="https://checkout.pay.jp/" class="payjp-button"
                data-key="{{env('PAY_JP_PUBLIC_KEY_TEST')}}"
                data-text="カードを登録する"
                data-submit-text="カードを登録する"></script>
        <script>
            $(function () {
                $('#payjp_cardSubmit').val('カードを登録する');
            });

        </script>
        @csrf
    </form>

@endsection
