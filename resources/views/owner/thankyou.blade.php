@extends('layouts.owner')
@section('navi')
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#whatmirpet" style="color: black;">
                    <p>みるペットとは</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner/detail" style="color: black;">
                    <p>ご利用ガイド</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#clinic" style="color: black;">
                    <p>動物病院を探す</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#qa" style="color: black;">
                    <p>よくある質問</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-info btn-round" href="{{URL::to('/')}}/clinic">
                    <p>獣医師の方はこちら</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-primary btn-round" href="{{URL::to('/')}}/login">
                    <p>会員ログイン</p>
                </a>
            </li>
        </ul>
    </div>
@endsection

@section('body')

    <div class="container" style="margin:80px auto;">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h2>お問い合わせが送信されました。</h2>
                <h4>回答まで少しお待ちください。</h4>
                <a href="{{URL::to('/')}}" class="btn btn-primary btn-lg">戻る</a>
            </div>

        </div>
    </div>

@endsection
