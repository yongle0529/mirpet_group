@extends('layouts.owner_app')

@section('navi')
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#whatmirpet" style="color: black;">
                    <p>みるペットとは</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner/detail" style="color: black;">
                    <p>ご利用ガイド</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#clinic" style="color: black;">
                    <p>動物病院を探す</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#qa" style="color: black;">
                    <p>よくある質問</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#inquiry" style="color: black;">
                    <p>お問い合わせ</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-info btn-round" href="{{URL::to('/')}}/clinic">
                    <p>獣医師の方はこちら</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-primary btn-round" href="{{URL::to('/')}}/login">
                    <p>会員ログイン</p>
                </a>
            </li>
        </ul>
    </div>
@endsection

@section('body')

    <div class="container" style="margin:50px auto;">
        <div id="transaction" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">特定商取引法に基づく表記</h3>
            </div>
            <div class="col-md-10 text-left">
                <h4>販売事業者名</h4>
                <ol>株式会社みるペット</ol>

                <h4>代表者または運営統括責任者名前</h4>
                <ol>代表取締役社長　浅沼　直之</ol>

                <h4>販売事業者所在地</h4>
                <ol>〒106-0061　東京都中央区銀座７丁目１８番１３クオリア銀座５０４</ol>

                <h4>問い合わせ先</h4>
                <ol><a class="nav-link" href="{{URL::to('/')}}/owner#inquiry">お問い合わせフォーム</a></ol>

                <h4>電話番号</h4>
                <ol>上記にてお問い合わせください</ol>

                <h4>販売価格</h4>
                <ol>商品毎に記載</ol>

                <h4>商品代金以外に必要な費用</h4>
                <ol>消費税</ol>

                <h4>代金の支払い時期および方法</h4>
                <ol>クレジットカード（支払い時期は、各カード会社引き落とし日）</ol>

                <h4>返品の取り扱い条件、解約条件</h4>
                <ol>商品の特性上、不可</ol>


                <h4>不良品の取り扱い条件</h4>
                <ol>商品の特性上、不可</ol>
            </div>
        </div>

    </div>
@endsection
