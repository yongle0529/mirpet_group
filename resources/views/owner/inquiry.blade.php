@extends('layouts.owner')

@section('navi')
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#whatmirpet" style="color: black;">
                    <p>みるペットとは</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner/detail" style="color: black;">
                    <p>ご利用ガイド</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#clinic" style="color: black;">
                    <p>動物病院を探す</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner#qa" style="color: black;">
                    <p>よくある質問</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-info btn-round" href="{{URL::to('/')}}/clinic">
                    <p>獣医師の方はこちら</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-primary btn-round" href="#">
                    <p>会員ログイン</p>
                </a>
            </li>
        </ul>
    </div>
@endsection

@section('body')

    <div class="container" style="margin:50px auto;">

        <div id="begin" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">ご利用ガイド</h3>
            </div>

            <div class="col-md-10 text-center">
                <h5>
                    オンライン「相談」であれば、どんな症状や病気、しつけなどのこともインターネットを通じて動物病院に相談することができます。
                    オンライン「診療」に関しては、現在の法律の解釈では認められておりません。
                    ただし、初診時に動物病院にて対面で獣医師の診断を受けており、その後の経過の治療に関しては、獣医師の判断において、オンライン上での判断、薬の処方が認められております。
                    どんな、病気、疾患がオンライン上で対応可能なのかは、動物病院へご確認下さい。
                </h5>
            </div>
            <div class="col-md-12 text-center">
                <a href="{{URL::to('/')}}/owner#clinic" class="btn btn-primary btn-round btn-lg">動物病院を探す</a>
            </div>
        </div>

        <div class="row mt-4">

            <div class="col-md-12">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">Step1　利用登録・予約</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="color: black; line-height: 2rem;">
                    サイトで利用登録を行い、飼い主様情報、動物情報を入力していただきます。
                    登録後、オンライン相談を受けたい動物病院を検索していただき、好きな日時、時間で予約を取ります。
                    予約時に簡単な問診を記入することもできます。

                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="https://blog.hootsuite.com/wp-content/uploads/2017/06/social-media-content-calendar-940x470.jpg" style="height:150px; width: 300px; object-fit: cover;">
            </div>

            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">Step2　動物病院とビデオ通話・相談</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="color: black; line-height: 2rem;">
                    予約時間数分前になりましたら、システムにログインをしてください。
                    時間になると動物病院からコールがかかってきますので、オンライン相談を開始してください。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="https://ishicome-cdn.medpeer.jp/news/sites/default/files/2016-11/215.jpg" style="height:150px;  width: 300px;object-fit: cover;">
            </div>
            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">Step3　お会計（クレジットカード決済）</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="color: black; line-height: 2rem;">
                    診察を受け終えたら、診察内容を確認してください。
                    事前に登録して頂いたクレジットカード情報を元に決済を行います。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="https://www.greedyrates.ca/wp-content/uploads/2018/09/Visa-or-Mastercard-394x222-c-default.jpg" style="height: 150px;">
            </div>
            <div class="col-md-12 mt-3">
                <blockquote class="blockquote blockquote-primary">
                    <p class="mb-0">Step4　薬、会計明細等のお受け取り</p>
                </blockquote>
            </div>
            <div class="col-md-8">
                <h5 class="description" style="color: black; line-height: 2rem;">
                    動物病院で発送の準備が完了次第郵送され、薬や会計明細などが届きます。
                </h5>
            </div>
            <div class="col-md-4 d-flex flex-wrap align-items-center">
                <img class="mx-auto" src="https://img.sirabee.com/wp/wp-content/uploads/2016/12/18222118/sirabee20161219sagawakyuubin-600x400.jpg" style="height:150px;  width: 300px;object-fit: cover;">
            </div>
            <div class="col-md-12 text-center">
                <a href="{{URL::to('/')}}/owner" class="btn btn-primary btn-round btn-lg">戻る</a>
            </div>
        </div>
    </div>
@endsection
