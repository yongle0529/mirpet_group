@extends('layouts.owner')

@section('navi')
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="{{URL::to('/')}}/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#whatmirpet"))' style="color: black;">
                    <p>みるペットとは</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/')}}/owner/detail" style="color: black;">
                    <p>ご利用ガイド</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#clinic"))' style="color: black;">
                    <p>動物病院を探す</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#qa"))' style="color: black;">
                    <p>よくある質問</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" onclick='scrollToElement($("#inquiry"))' style="color: black;">
                    <p>お問い合わせ</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-info btn-round" href="{{URL::to('/')}}/clinic">
                    <p>獣医師の方はこちら</p>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn btn-primary btn-round" href="{{URL::to('/')}}/login?ref=owner">
                    <p>会員ログイン</p>
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link btn btn-blue btn-round" href="{{URL::to('/admin/login')}}">
                    <p>管理者フェジ</p>
                </a>
            </li> --}}
        </ul>
    </div>
@endsection

@section('header')
    <style>
        @media screen and (max-width: 320px) {
            .n-logo {
                margin-bottom: 20vh !important;
            }
        }
    </style>
    <div class="page-header" style="height: 100vh;">
        <div class="page-header-image" style="background-image:url('{{URL::to('/')}}/assets/img/header_owner.jpg');">
        </div>
        <div class="container">
            <div class="content-center brand" style="height: 100vh;">
                <img class="n-logo" src="{{URL::to('/')}}/assets/img/mirpet_logo_A_02.png" alt="みるペット" style="max-width: 20vh; margin-top: 30vh; margin-bottom: 30vh;">
                <h5 class="mt-2 font-weight-bold">いつもの先生ともっと身近に、<br class="br-sp">もっと便利に</h5>
                <h3 class="mt-0 mb-2 font-weight-bold">オンライン相談サービス<br>「みるペット」</h3>
            </div>

        </div>
    </div>
@endsection

@section('body')
    <div class="container">
        <div class="row mt-2">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">オンライン相談サービスとは</h3>
            </div>

            <div class="col-md-7">
                <h5>
                    「オンライン相談」とは、パソコンやスマートフォンを利用してインターネット上で獣医師などに相談をすることです。
                    インターネット環境があれば、飼い主様の自由な時間に予約をして相談をすることが可能なため、動物病院に行く時間がない、
                    動物を連れて行く手段がない方など、多くの方にとってより動物医療を受けやすくなります。
                    <br>
                    <span style="font-size:0.8rem;">※オンライン「診療」に関しては、初診での薬の処方はできませんが、再診の症例に関してはオンライン相談の範疇で薬の処方を行うことも可能です。</span>
                </h5>
            </div>
            <div class="col-md-5">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_4.jpg" width="100%">
            </div>
        </div>
        <div id="whatmirpet" class="row mt-4">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">みるペットとは</h3>
            </div>
            <div class="col-md-7">

                <h5 class="description" style="color: black; line-height: 2rem;">
                    {{--<span style="font-size: 2.4rem; font-weight: bold;">--}}
                    みるペットは、動物、飼い主様を動物病院とインターネットで繋ぐ、オンライン相談システムです。
                    お好きな時間に予約をすることで、どこにいてもいつもと同じ動物病院、同じ先生に相談することができます。
                    予約、相談（診察）、決済までをオンライン上で完結することができます。
                </h5>
            </div>
            <div class="col-md-5">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_5.jpg" width="100%">
            </div>
        </div>
        <div class="row" style="margin-top: 1.75rem;">
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">1.予約</h3>
                    </div>
                    <div class="card-body">
                        カレンダーでお好きな日時、時間を選択して予約。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">2.相談(診察)
                        </h3>
                    </div>
                    <div class="card-body">
                        予約した時間になったら、通話ボタンを押して相談開始。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">3.決済
                        </h3>
                    </div>
                    <div class="card-body">
                        相談が終わったら、事前登録しているクレジットカードにて自動決済。
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" data-background-color="orange" style="height: 180px;">

                    <div class="card-header text-center">
                        <h3 class="card-title title-up" style="margin-top: 0.75em;">4.薬の郵送
                        </h3>
                    </div>
                    <div class="card-body">
                        薬の処方があるときは、後日動物病院から薬を送付します。
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center text-center">
            <div class="col-md-12">
                <a href="{{URL::to('/')}}/owner/detail" class="btn btn-primary btn-round btn-lg">もっと詳しく</a>
            </div>
        </div>

        <div class="row justify-content-md-center text-center">
            <div class="col-md-12">
                <h3 class="title">オンライン診療サービス<br class="br-sp">「みるペット」のメリット</h3>
            </div>

            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_1.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>相談の待ち時間解消</h5>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_2.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>通院時間解消<br>交通費の節約
                </h5>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_3.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>会計の待ち時間<br>調剤の待ち時間解消
                </h5>
            </div>

            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_4.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>好きな時間・場所で相談を受けることが可能
                </h5>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_5.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>動物の来院によるストレスの解消（とくにネコちゃん）
                </h5>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/merit_icon_6.png" class="" style="width: 200px; height:200px; object-fit: cover;">
                <h5>細かなフォローが可能となり、重症化を防ぐことができる</h5>
            </div>
        </div>

        <div id="clinic" class="row justify-content-md-center text-center">
            <div class="col-md-12">
                <h3 class="title">「みるペット」導入動物病院様</h3>
            </div>

            <div class="col-md-4">
                <img src="https://www.biz-up.biz/sys/wp-content/uploads/2012/01/logo_0211.gif" class="" style="max-width: 200px;">
            </div>
            <div class="col-md-4">
                <img src="http://toyoda-pc.com/img/toyoda-pet-clinic-logo.png" class="" style="max-width: 200px;">
            </div>
            <div class="col-md-4">
                <img src="https://img03.ti-da.net/usr/t/u/e/tuezu/nagisa_logo_blog.jpg" class="" style="max-width: 200px;">
            </div>
        </div>
        <div class="row justify-content-md-center text-center">
            <div class="col-md-12">
                <a href="{{URL::to('/app/owner/clinic/search')}}" class="btn btn-primary btn-round btn-lg">動物病院を探す</a>
            </div>
        </div>

        <div id="qa" class="row">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">よくある質問</h3>
            </div>
            <div class="col-md-12">
                <div>
                    <div class="boxQA">
                        <div class="box-title">どんな症状、病気でもオンライン相談が可能ですか？</div>
                        <p>オンラインでの相談のみであれば、基本的にはどんな病気、症状についても可能です。ただし、治療継続中の疾患に対してのお薬の処方、送付を希望の場合は、獣医師が判断した場合のみ可能になります。詳しくは、動物病院へご確認ください。</p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">利用料はかかりますか？</div>
                        <p>会員登録はすべて無料になります。オンライン相談を利用時のみ、通常の来院時と同じ相談料等がかかります。またシステム利用料として￥２００（税抜）がかかります。薬などの送付が必要な場合はさらに送料がかかります。</p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">支払いはどのように行いますか？</div>
                        <p>クレジットカード払いにてお支払いいただいております。カードのブランドは、VISA/Mastarcardが対象となっております。</p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">動物用医療保険は適用されますか？</div>
                        <p>保険会社、動物病院によります。詳しくは、保険会各社、または動物病院へ直接お問い合わせください。</p>
                    </div>
                    <div class="boxQA">
                        <div class="box-title">どのような機器で利用することが可能ですか？</div>
                        <p>スマートフォンもしくはPCにて利用することが可能です。</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="inquiry" class="row mb-4 justify-content-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">お問い合わせ</h3>
            </div>
            <form id="inquiry_form" method="POST" action="{{ URL::to('/owner/inquiry') }}" class="row col-md-8">
                @csrf
                <div class="col-md-12">
                    <h5 class="font-weight-bold">お名前</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="name" name="name" type="text" value="" placeholder="山本" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">メールアドレス</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="mail" name="mail" type="email" value="" placeholder="info@mirpet.co.jp" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">電話番号(ハイフンなし)</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="tel" name="tel" type="tel" value="" pattern="^[0-9]+$" title="※数字のみ入力できます" placeholder="0369103242" class="form-control" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ内容</h5>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" required>
                            <option value="">選択して下さい</option>
                            @foreach(config('const.OWNER_INQUIRY_TYPE') as $val)
                                <option value="{{$val}}"
                                    {{ $val == old('type') ? 'selected' : '' }}>{{$val}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="font-weight-bold">お問い合わせ詳細</h5>
                </div>
                <div class="col-md-12">
                    <div class="textarea-container">
                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="80" placeholder="お問い合わせ内容をご自由に記載下さい。"></textarea>
                    </div>
                </div>

                <div class="col-md-12 text-center mt-4">
                    <button id="confirm" type="button" class="btn btn-primary btn-lg">入力内容を確認</button>
                </div>
            </form>
        </div>

        <div id="price" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">料金について</h3>
            </div>

            <div class="col-md-10 text-center">
                <h5>
                    「みるペット」は登録料はかかりません。実際に相談が完了するまでは、一切費用はかかりません。<br><br>
                    診察後は、来院時と同じように相談代金に加えて、薬等を配送するための配送費用が別途かかります。また、サービス利用料として１回あたり￥２００（税抜）がかかります。
                    お支払いはクレジットカード（VISA、MasteCard、対応）で承っております。<br><br>
                    動物用医療保利用の場合は、ご利用保険会社、動物病院によって適用の可否が異なりますので、直接お問い合わせください。
                </h5>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">サポート環境について</h3>
            </div>
            <div class="col-md-12 text-center">
                <blockquote class="blockquote blockquote-danger">
                    <p class="mb-0">「みるペット」では以下の環境についてのみサポートしております。<br>
                        非サポート環境では正常に相談が受けられない可能性がありますのでご注意ください。</p>
                </blockquote>
            </div>
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>PC</th>
                        <th>iOS</th>
                        <th>Android</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>OS</td>
                        <td>Windows 7以降<br>mac OS 10.10以上</td>
                        <td>iOS10.x以上</td>
                        <td>Android 5.x以上</td>
                    </tr>
                    <tr>
                        <td>ブラウザ</td>
                        <td>Google Chrome</td>
                        <td>Safari / Google Chrome</td>
                        <td>Google Chrome</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 text-center">
                <blockquote class="blockquote" style="border-color: orange;color: orange;">
                    <p class="mb-0" style="font-size: 1.5rem;font-weight: bold;">ネットワーク推奨環境</p>
                    <p class="mb-0">実効速度1.5Mbps以上を推奨</p>
                </blockquote>
            </div>
        </div>


        <div id="company" class="row justify-content-md-center">
            <div class="col-md-12 border-bottom border-danger mb-4">
                <h3 class="title">会社概要</h3>
            </div>
            <div class="col-md-4">
                <img src="{{URL::to('/')}}/assets/img/eyecatch_6.jpg" class="rounded" style="height:300px; object-fit: cover;">
            </div>
            <div class="col-md-8">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>社名</td>
                        <td>株式会社みるペット</td>
                    </tr>
                    <tr>
                        <td>事業内容</td>
                        <td>動物病院の業務関連システムの企画、デザイン、開発、制作、管理及び運営業務</td>
                    </tr>
                    <tr>
                        <td>代表者</td>
                        <td>浅沼　直之</td>
                    </tr>
                    <tr>
                        <td>設立</td>
                        <td>2019年1月16日</td>
                    </tr>
                    <tr>
                        <td>本店所在地</td>
                        <td>〒104-0061　東京都中央区銀座７丁目１８ー１３　クオリア銀座５０４</td>
                    </tr>
                    {{--  <tr>
                        <td>電話番号</td>
                        <td>03-6910-3242</td>
                    </tr>  --}}
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">お問い合わせ内容確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="font-weight-bold">氏名</div>
                        <div id="confirm-name"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">メールアドレス</div>
                        <div id="confirm-mail"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">電話番号</div>
                        <div id="confirm-tel"></div>
                    </div>
                    <div class="col-6">
                        <div class="font-weight-bold">お問い合わせ内容</div>
                        <div id="confirm-type"></div>
                    </div>
                    <div class="col-12">
                        <div class="font-weight-bold">お問い合わせ詳細</div>
                        <div id="confirm-comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">閉じる</button>
                    <button id="confirm_btn" type="button" class="btn btn-primary btn-lg">送信する</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#confirm").on("click", function () {
                if ($("#inquiry_form")[0].checkValidity()) {
                    $('#confirm-name').text($('input[name="name"]').val());
                    $('#confirm-mail').text($('input[name="mail"]').val());
                    $('#confirm-tel').text($('input[name="tel"]').val());
                    $('#confirm-type').text($('[name="type"]').val());
                    $('#confirm-comment').html($('[name="comment"]').val() == '' ? '入力されていません' : $('[name="comment"]').val().replace(/\r?\n/g, '<br />'));
                    $('#confirmModal').modal();
                    return;
                } else {
                    $("#inquiry_form")[0].reportValidity();
                    return;
                }
            });

            $("#confirm_btn").on("click", function () {
                $("#inquiry_form").submit();
            });
        });
    </script>
@endsection
