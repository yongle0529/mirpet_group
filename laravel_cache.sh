#!/bin/bash
rm -f bootstrap/cache/config.php
php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan view:clear
composer dump-autoload
php artisan clear-compiled
