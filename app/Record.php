<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $table = 'records';

    protected $fillable = ['user_id', 'clinic_id', 'diag_id', 'pet_id', 'user_payment_token', 'clinic_payment_token', 'price'];
}
