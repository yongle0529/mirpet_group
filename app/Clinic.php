<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Clinic
 *
 * @property int $id
 * @property string $name
 * @property string $name_kana
 * @property string $manager_name
 * @property string $manager_name_kana
 * @property mixed|null $option_pet
 * @property mixed|null $option_medical_course
 * @property mixed|null $option_other
 * @property mixed|null $clinic_info
 * @property string $email
 * @property string $tel
 * @property string $zip_code
 * @property string $prefecture
 * @property string $city
 * @property string $address
 * @property string|null $logo_image_path
 * @property string|null $url
 * @property string $ref_code
 * @property mixed|null $bank_info
 * @property int $status 0: 初期登録状態, 1:公開中, 2:掲載停止
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereBankInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereClinicInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereLogoImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionMedicalCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionPet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic wherePrefecture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereRefCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereZipCode($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClinicMenu[] $clinicMenus
 */
class Clinic extends Authenticatable
{
    use Notifiable;

    protected $guarded = [
        'id', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'admin_clinic_id',
        'name',
        'name_kana',
        'manager_name',
        'manager_name_kana',
        'option_pet',
        'email',
        'email_verified_at',
        'password',
        'tel',
        'zip_code',
        'prefecture',
        'city',
        'address',
        'url',
        'bank_info',
        'reset_token',
        'status',
        'eyecatchimage',
        'clinic_info_photos'
    ];

    public function clinicMenus()
    {
        return $this->hasMany(ClinicMenu::class)->orderBy('id');
    }

    public function diagnosis()
    {
        return $this->hasMany(Diagnosis::class)->orderBy('id');
    }

    public function isFavorite($userId)
    {
        return $this->hasMany(FavoriteClinic::class)
            ->where('user_id', $userId)
            ->where('del_flg', 0)
            ->orderBy('id')
            ->count();
    }
    public function Clinic_Menu_Insert($menu_description, $menu_title, array $priceData, $menu_insurance, $id, $menu_time_option)
    {
        DB::table('clinic_menus')->insert(
            [
                'clinic_id' => $id,
                'base_id' => 0,
                'description' => $menu_description,
                'is_insurance_applicable' => $menu_insurance,
                'price_list' => $priceData['price_list'],
                'name' => $menu_title,
                'description' => $menu_description,
                'option' => $menu_time_option,
            ]
        );
        return;
    }
    public function Clinic_Menu_Udate($priceData, $menu_title, $menu_description, $menu_insurance, $id)
    {
        DB::table('clinic_menus')
            ->where('id', $id)
            ->update([
                'price_list' => $priceData['price_list'],
                'is_insurance_applicable' => $menu_insurance,
                'name' => $menu_title,
                'description' => $menu_description,
            ]);
        return;
    }
    public function Clinic_Menu_Udate1($priceData, $menu_title, $menu_description, $menu_insurance, $id)
    {
        DB::table('clinic_menus')
            ->where('id', $id)
            ->update([
                'price_list' => $priceData['price_list'],
                'is_insurance_applicable' => $menu_insurance,
                'name' => $menu_title,
                'description' => $menu_description,
            ]);
        return;
    }
    public function reserve_update(array $clinic_data, array $clinicMenu_data, $id, $clinic_menu_description, $clinic_menu_title)
    {
        DB::table('clinics')
            ->where('id', $id)
            ->update([
                'clinic_info' => $clinic_data['clinic_info']
            ]);
        DB::table('clinic_menus')
            ->where('clinic_id', $id)
            ->update([
                'price_list' => $clinicMenu_data['price_list'],
                'name' => $clinic_menu_title,
                'description' => $clinic_menu_description,
                'option' => $clinicMenu_data['option']
            ]);
        return;
    }
    public function reserve_insert($clinic_info, array $clinicMenu_data, $id) //,$clinic_menu_description,$clinic_menu_title)
    {
        DB::table('clinics')
            ->where('id', $id)
            ->update([
                'clinic_info' => $clinic_info,
                'menu_time_option' => $clinicMenu_data['option']
            ]);
        $query = DB::table('clinic_menus')
            ->where('clinic_id', $id)
            ->get();
        foreach ($query as $row) {
            DB::table('clinic_menus')
                ->where('id', $row->id)
                ->update([
                    'option' => $clinicMenu_data['option']
                ]);
        }
        $query1 = DB::table('clinic_menus')
            ->where('base_id', $id)
            ->get();
        foreach ($query1 as $row) {
            DB::table('clinic_menus')
                ->where('id', $row->id)
                ->update([
                    'option' => $clinicMenu_data['option']
                ]);
        }
        // DB::table('clinic_menus')->insert(
        //     ['clinic_id' => $id,
        //     //'name' => $clinic_menu_title,
        //     //'description' => $clinic_menu_description,
        //     //'price_list' => $clinicMenu_data['price_list'],
        //     'option' => $clinicMenu_data['option']
        //     ]
        // );
        return;
    }
}
