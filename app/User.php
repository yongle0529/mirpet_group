<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property int $role
 * @property string|null $tel
 * @property string|null $zip_code
 * @property string|null $prefecture
 * @property string|null $city
 * @property string|null $address
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePrefecture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereZipCode($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FavoriteClinic[] $favoriteClinics
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pet[] $pets
 * @property string|null $payment_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePaymentToken($value)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'admin_user_id',
        'first_name',
        'last_name',
        'first_name_kana',
        'last_name_kana',
        'email',
        'password',
        'tel',
        'zip_code',
        'prefecture',
        'city',
        'address',
        'role',
        'payment_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function favoriteClinics()
    {
        return $this->hasMany(FavoriteClinic::class)->orderBy('id');
    }

    public function pets()
    {
        return $this->hasMany(Pet::class)->orderBy('id');
    }
    public function UserUpdate(array $data, array $data1, $url)
    {
        DB::table('clinics')
            ->where('id', $data1['clinic_id'])
            ->update([
                'name' => $data['name'],
                'name_kana' => $data['name_kana'],
                'manager_name' => $data['manager_name'],
                'url' => $url, //$data['url'],
                'email' => $data['email'],
                'tel' => $data['tel'],
                'prefecture' => $data['prefecture'],
                'city' => $data['city'],
                'address' => $data['address'],
                'clinic_description' => $data1['clinic_description'],
                'insurance' => $data1['insurance'],
                'clinic_info_photos' => $data1['clinic_info_photos'],
                'eyecatchimage' => $data1['eyecatchimage'],
                'option_medical_course' => $data1['option_medical_course'],
                'option_pet' => $data1['option_pet']
            ]);

        // DB::table('clinic_menus')
        // ->where('clinic_id', $data1['clinic_id'])
        // ->update(['description' => $data1['comment'],
        //         'is_insurance_applicable' => $data1['state'],
        //         'insurance_company' => $data1['insurance_company'],
        // ]);
        return;
    }
    public function Clinic_menus($id)
    {
        return DB::table('clinic_menus')
            ->where('clinic_id', $id)
            ->where('base_id', 0)
            ->get();
    }

    public function descript_company_insert(array $data, array $data1)
    {
        DB::table('clinics')
            ->where('id', $data1['clinic_id'])
            ->update([
                'name' => $data['name'],
                'name_kana' => $data['name_kana'],
                'manager_name' => $data['manager_name'],
                'url' => $data['url'],
                'email' => $data['email'],
                'tel' => $data['tel'],
                'prefecture' => $data['prefecture'],
                'city' => $data['city'],
                'address' => $data['address'],
                'tel' => $data['tel'],
                'url' => $data['url'],
                'option_medical_course' => $data1['option_medical_course']
            ]);


        DB::table('clinic_menus')->insert(
            [
                'clinic_id' => $data1['clinic_id'],
                'description' => $data1['comment'],
                'is_insurance_applicable' => $data1['state'],
                'insurance_company' => $data1['insurance_company']
            ]
        );
        return;
    }
    public function getUser($id)
    {
        return  DB::table('users')
            ->where('id', $id)
            ->get();
    }
}
