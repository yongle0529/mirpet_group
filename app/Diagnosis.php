<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    protected $table = 'diagnosis';

    protected $fillable = ['cata_no', 'user_id', 'clinic_id', 'clinic_menu_id', 'pet_id', 'reserve_datetime', 'start_datetime', 'end_datetime', 'res_time', 'price', 'content', 'status', 'hospital_code', 'reserve_method', 'clinic_name'];

    public function clinic()
    {
        return $this->belongsTo(Clinic::class)->orderBy('id');
    }

    public function clinicMenu()
    {
        return $this->belongsTo(ClinicMenu::class)->orderBy('id');
    }
}
