<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReserveTimetable
 *
 * @property int $id
 * @property int $clinic_id
 * @property int $time_slot_type
 * @property int $time_slot
 * @property int $acceptable_count
 * @property int $reserved_count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereAcceptableCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereClinicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereReservedCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereTimeSlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereTimeSlotType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReserveTimetable whereDate($value)
 */
class ReserveTimetable extends Model
{
    protected $fillable = ['clinic_id', 'date', 'time_slot', 'time_slot_type', 'diag_id'];
}
