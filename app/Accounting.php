<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    protected $table = 'accountings';

    protected $fillable = ['clinic_id', 'diagnosis_id', 'origi_filename', 'save_filename', 'extension', 'medical_expenses',
    'fee','clinic_fee','delivery_cost','sum','comment','status','next_remind_date'];
}
