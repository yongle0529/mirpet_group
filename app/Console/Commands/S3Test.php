<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class S3Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:s3_test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'S3 Upload Test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = '/Users/hirohisa.tanigawa/Pictures/20190425_receipt_2.jpg';
        $fileName = str_random(64) . ".jpg";
        $tmpImagePath = '/tmp/' . $fileName;
        $image = Image::make($file)->encode('jpg');
        $image->save($tmpImagePath);

        $path = Storage::disk('s3')->putFileAs('pet-insurance/', new File($tmpImagePath), $fileName, 'public');
        echo Storage::disk('s3')->url('pet-insurance/' . $fileName);

        // Delete temp image
        unlink($tmpImagePath);
    }
}
