<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class PetCatano extends Model
{
    //
    protected $table = 'pet_catano';

    protected $fillable = ['pet_id', 'user_id', 'cata_no'];

    public function petCatanoInsert($pet_id, $user_id, $cata_no,$clinic_id)
    {
        DB::table('pet_catano')->insert(
            [
                'user_id' => $user_id,
                'cata_no' => $cata_no,
                'pet_id' => $pet_id,
                'clinic_id' => $clinic_id,
            ]
        );
        return;
    }
    public function petCatanoUpdate($pet_id, $user_id, $cata_no,$clinic_id)
    {
        DB::table('pet_catano')
        ->where('pet_id', $pet_id)
        ->update([
                'user_id' => $user_id,
                'cata_no' => $cata_no,
                'pet_id' => $pet_id,
                'clinic_id' => $clinic_id,

        ]);
    }
}
