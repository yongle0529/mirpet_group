<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyMail extends Model
{
    protected $table = 'reply_mails';

    protected $fillable = ['email', 'subject', 'comment', 'status', 'del_flg'];
}
