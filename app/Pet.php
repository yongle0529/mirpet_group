<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Pet
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $name_kana
 * @property int $gender
 * @property int $type
 * @property string|null $type_other
 * @property string $genre
 * @property string $birthday
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereGenre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereTypeOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereUserId($value)
 * @property int $del_flg
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereDelFlg($value)
 */
class Pet extends Model
{
    protected $guarded = array('id');

    public function user()
    {
        return $this->belongsTo(User::class)->orderBy('id');
    }
    public function getSearch($pet_diagId, $petId, $pet_name, $pet_type, $pet_other_type, $pet_age, $user_diagId, $user_name, $user_name_kana, $tel, $prefecture, $city, $owner_cata_flag, $pet_cata_flag, $clinicid)
    {
        if($pet_diagId!="" || $user_diagId!=""){
            if($petId!=""){
                return DB::table('users')
                ->join('pets', 'users.id', '=', 'pets.user_id')
                ->join('pet_catano', 'pet_catano.pet_id', '=', 'pets.id')

                ->join('owner_catano', 'owner_catano.clinic_id', '=', 'pet_catano.clinic_id')
                ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
                ->where('owner_catano.clinic_id', $clinicid)
                ->where('owner_catano.cata_no', 'like', '%' . $pet_diagId . '%')
                ->where('owner_catano.cata_no', 'like', '%' . $user_diagId . '%')
                ->where('pet_catano.clinic_id', $clinicid)
                ->where('pet_catano.cata_no', 'like', '%' . $petId . '%')
                // ->where('pets.id', 'like', '%' . $petId . '%')
                ->where('diagnosis.status', '!=', 0)
                ->where('pets.name', 'like', '%' . $pet_name . '%')
                ->where('pets.type', 'like', '%' . $pet_type . '%')
                ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
                ->where('pets.birthday', 'like', '%' . $pet_age . '%')
                // ->where('diagnosis.id','like', $user_diagId.'%')
                ->where('users.last_name', 'like', '%' . $user_name . '%')
                ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
                ->where('users.tel', 'like', '%' . $tel . '%')
                ->where('users.prefecture', 'like', '%' . $prefecture . '%')
                ->where('users.city', 'like', '%' . $city . '%')
                ->where('diagnosis.clinic_id', $clinicid)
                ->get();
            }else{
                return DB::table('users')
                ->join('pets', 'users.id', '=', 'pets.user_id')
                ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
                ->join('owner_catano', 'owner_catano.clinic_id', '=', 'diagnosis.clinic_id')
                // ->join('pet_catano', 'pet_catano.pet_id', '=', 'pets.id')
                ->where('owner_catano.clinic_id', $clinicid)
                ->where('owner_catano.cata_no', 'like', '%' . $pet_diagId . '%')
                ->where('owner_catano.cata_no', 'like', '%' . $user_diagId . '%')
                // ->where('pet_catano.clinic_id', $clinicid)
                // ->where('pet_catano.cata_no', 'like', '%' . $petId . '%')
                // ->where('pets.id', 'like', '%' . $petId . '%')
                ->where('diagnosis.status', '!=', 0)
                ->where('pets.name', 'like', '%' . $pet_name . '%')
                ->where('pets.type', 'like', '%' . $pet_type . '%')
                ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
                ->where('pets.birthday', 'like', '%' . $pet_age . '%')
                // ->where('diagnosis.id','like', $user_diagId.'%')
                ->where('users.last_name', 'like', '%' . $user_name . '%')
                ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
                ->where('users.tel', 'like', '%' . $tel . '%')
                ->where('users.prefecture', 'like', '%' . $prefecture . '%')
                ->where('users.city', 'like', '%' . $city . '%')
                ->where('diagnosis.clinic_id', $clinicid)
                ->get();
            }

        // }elseif($pet_cata_flag == 1){

        // }elseif($pet_cata_flag == 1 && $pet_cata_flag == 1){

        }else{
            if($petId!=""){
                return DB::table('users')
                ->join('pets', 'users.id', '=', 'pets.user_id')

                // ->join('owner_catano', 'owner_catano.clinic_id', '=', 'diagnosis.clinic_id')
                ->join('pet_catano', 'pet_catano.pet_id', '=', 'pets.id')
                ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
                // ->where('owner_catano.clinic_id', $clinicid)
                // ->where('owner_catano.cata_no', 'like', '%' . $pet_diagId . '%')
                // ->where('owner_catano.cata_no', 'like', '%' . $user_diagId . '%')
                ->where('pet_catano.clinic_id', $clinicid)
                ->where('pet_catano.cata_no', 'like', '%' . $petId . '%')
                // ->where('pets.id', 'like', '%' . $petId . '%')
                ->where('diagnosis.status', '!=', 0)
                ->where('pets.name', 'like', '%' . $pet_name . '%')
                ->where('pets.type', 'like', '%' . $pet_type . '%')
                ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
                ->where('pets.birthday', 'like', '%' . $pet_age . '%')
                // ->where('diagnosis.id','like', $user_diagId.'%')
                ->where('users.last_name', 'like', '%' . $user_name . '%')
                ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
                ->where('users.tel', 'like', '%' . $tel . '%')
                ->where('users.prefecture', 'like', '%' . $prefecture . '%')
                ->where('users.city', 'like', '%' . $city . '%')
                ->where('diagnosis.clinic_id', $clinicid)
                ->get();
            }else{
                return DB::table('users')
                ->join('pets', 'users.id', '=', 'pets.user_id')
                ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
                // ->join('owner_catano', 'owner_catano.diag_id', '=', 'diagnosis.id')
                // ->join('pet_catano', 'pet_catano.pet_id', '=', 'pets.id')
                // ->where('owner_catano.clinic_id', $clinicid)
                // ->where('owner_catano.cata_no', 'like', '%' . $pet_diagId . '%')
                // ->where('pet_catano.clinic_id', $clinicid)
                // ->where('pet_catano.cata_no', 'like', '%' . $petId . '%')
                // ->where('pets.id', 'like', '%' . $petId . '%')
                ->where('diagnosis.status', '!=', 0)
                ->where('pets.name', 'like', '%' . $pet_name . '%')
                ->where('pets.type', 'like', '%' . $pet_type . '%')
                ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
                ->where('pets.birthday', 'like', '%' . $pet_age . '%')
                // ->where('diagnosis.id','like', $user_diagId.'%')
                ->where('users.last_name', 'like', '%' . $user_name . '%')
                ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
                ->where('users.tel', 'like', '%' . $tel . '%')
                ->where('users.prefecture', 'like', '%' . $prefecture . '%')
                ->where('users.city', 'like', '%' . $city . '%')
                ->where('diagnosis.clinic_id', $clinicid)
                ->get();
            }

        }

        // if ($owner_cata_flag == 1) {
        //     return DB::table('users')
        //         ->join('pets', 'users.id', '=', 'pets.user_id')
        //         ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
        //         ->join('owner_catano', 'owner_catano.diag_id', '=', 'diagnosis.id')
        //         ->where('owner_catano.clinic_id', $clinicid)
        //         ->where('owner_catano.cata_no', 'like', '%' . $pet_diagId . '%')
        //         // ->where('pets.id', 'like', '%' . $petId . '%')
        //         ->where('diagnosis.status', '!=', 0)
        //         ->where('pets.name', 'like', '%' . $pet_name . '%')
        //         ->where('pets.type', 'like', '%' . $pet_type . '%')
        //         ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
        //         ->where('pets.birthday', 'like', '%' . $pet_age . '%')
        //         // ->where('diagnosis.id','like', $user_diagId.'%')
        //         ->where('users.last_name', 'like', '%' . $user_name . '%')
        //         ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
        //         ->where('users.tel', 'like', '%' . $tel . '%')
        //         ->where('users.prefecture', 'like', '%' . $prefecture . '%')
        //         ->where('users.city', 'like', '%' . $city . '%')
        //         ->where('diagnosis.clinic_id', $clinicid)
        //         ->get();
        // } elseif ($pet_cata_flag == 1) {
        //     return DB::table('users')
        //         ->join('pets', 'users.id', '=', 'pets.user_id')
        //         ->join('pet_catano', 'pet_catano.pet_id', '=', 'pets.id')
        //         ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')

        //         // ->where('diagnosis.id', 'like', '%' . $pet_diagId . '%')
        //         ->where('pet_catano.clinic_id', $clinicid)
        //         ->where('pet_catano.cata_no', 'like', '%' . $petId . '%')
        //         ->where('diagnosis.status', '!=', 0)
        //         ->where('pets.name', 'like', '%' . $pet_name . '%')
        //         ->where('pets.type', 'like', '%' . $pet_type . '%')
        //         ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
        //         ->where('pets.birthday', 'like', '%' . $pet_age . '%')
        //         // ->where('diagnosis.id','like', $user_diagId.'%')
        //         ->where('users.last_name', 'like', '%' . $user_name . '%')
        //         ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
        //         ->where('users.tel', 'like', '%' . $tel . '%')
        //         ->where('users.prefecture', 'like', '%' . $prefecture . '%')
        //         ->where('users.city', 'like', '%' . $city . '%')
        //         ->where('diagnosis.clinic_id', $clinicid)
        //         ->get();
        // } else {
        //     return DB::table('users')
        //         ->join('pets', 'users.id', '=', 'pets.user_id')
        //         ->join('diagnosis', 'diagnosis.pet_id', '=', 'pets.id')
        //         ->where('diagnosis.id', 'like', '%' . $pet_diagId . '%')
        //         ->where('pets.id', 'like', '%' . $petId . '%')
        //         ->where('diagnosis.status', '!=', 0)
        //         ->where('pets.name', 'like', '%' . $pet_name . '%')
        //         ->where('pets.type', 'like', '%' . $pet_type . '%')
        //         ->where('pets.genre', 'like', '%' . $pet_other_type . '%')
        //         ->where('pets.birthday', 'like', '%' . $pet_age . '%')
        //         // ->where('diagnosis.id','like', $user_diagId.'%')
        //         ->where('users.last_name', 'like', '%' . $user_name . '%')
        //         ->where('users.first_name', 'like', '%' . $user_name_kana . '%')
        //         ->where('users.tel', 'like', '%' . $tel . '%')
        //         ->where('users.prefecture', 'like', '%' . $prefecture . '%')
        //         ->where('users.city', 'like', '%' . $city . '%')
        //         ->where('diagnosis.clinic_id', $clinicid)
        //         ->get();
        // }


        // ->join('pets', 'users.id', '=', 'pets.user_id')
    }
    public function getReserve($r_date, $clinic_id)
    {
        return DB::table('diagnosis')
            ->join('pets', 'diagnosis.user_id', '=', 'pets.user_id')
            ->join('users', 'users.id', '=', 'pets.user_id')
            ->where('diagnosis.clinic_id', $clinic_id)
            ->where('diagnosis.status', 0)
            ->where('diagnosis.reserve_datetime', 'like', '%' . $r_date . '%')
            ->get();
    }
    public function getReserve_sel($datetime, $clinic_id)
    {
        return DB::table('diagnosis')
            ->join('pets', 'diagnosis.pet_id', '=', 'pets.id')
            ->join('users', 'users.id', '=', 'pets.user_id')
            ->join('clinics', 'clinics.id', '=', 'diagnosis.clinic_id')
            ->where('diagnosis.clinic_id', $clinic_id)
            ->where('diagnosis.status', 0)
            ->where('diagnosis.reserve_datetime', $datetime)
            ->get();
    }
}
