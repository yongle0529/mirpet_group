<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OwnerCatano extends Model
{
    //
    protected $table = 'owner_catano';

    protected $fillable = ['user_id', 'cata_no', 'diag_id', 'pet_id', 'clinic_menu_id'];

    public function ownerCatanoInsert($user_id, $cata_no, $diag_id, $pet_id, $clinic_menu_id,$clinic_id)
    {
        DB::table('owner_catano')->insert(
            [
                'user_id' => $user_id,
                'cata_no' => $cata_no,
                'diag_id' => $diag_id,
                'pet_id' => $pet_id,
                'clinic_menu_id' => $clinic_menu_id,
                'clinic_id' => $clinic_id
            ]
        );
        return;
    }
    public function ownerCatanoUdate($user_id, $cata_no, $diag_id, $pet_id, $clinic_menu_id,$clinic_id)
    {
        DB::table('owner_catano')
        ->where('user_id', $user_id)
        ->where('clinic_id', $clinic_id)
        ->update([
                'user_id' => $user_id,
                'cata_no' => $cata_no,
                'pet_id' => $pet_id,
                'clinic_menu_id' => $clinic_menu_id,
                'clinic_id' => $clinic_id
        ]);
    }
}
