<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClinicMenu
 *
 * @property-read \App\Clinic $clinic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $clinic_id
 * @property string $name
 * @property string $description
 * @property mixed $price_list
 * @property int $is_insurance_applicable 0: 保険適用不可, 1:保険適用可
 * @property mixed|null $option
 * @property int $status 0: 初期登録状態, 1:公開中, 2:掲載停止
 * @property int $del_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereClinicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereDelFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereIsInsuranceApplicable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu wherePriceList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereUpdatedAt($value)
 */
class ClinicMenu extends Model
{
    protected $table = 'clinic_menus';

    protected $fillable = ['clinic_id','base_id', 'name', 'description', 'price_list', 'is_insurance_applicable', 'option',
    'insurance_company','status','del_flg'];
    public function diagnosis()
    {
        return $this->hasMany(Diagnosis::class)->orderBy('id');
    }

    public function clinic()
    {
        return $this->belongsTo(Clinic::class)->orderBy('id');
    }
}
