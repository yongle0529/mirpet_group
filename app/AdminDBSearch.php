<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class AdminDBSearch extends Model
{
    //
    public function clinic_get()
    {
        return DB::table('clinics')
                ->get();
    }
    public function pets_get()
    {
        return DB::table('pets')
            ->get();
    }
    public function hospitalManageSearch(array $data)
    {
        if($data['hos_num']==null and $data['hos_name'] == null and $data['hos_tel'] == null and $data['city'] == null and  $data['prefecture'] == '0' ){
            return DB::table('clinics')
             ->get();
        } elseif($data['hos_num'] != null){
            return DB::table('clinics')
                ->where('id', $data['hos_num'])
                ->get();
        } elseif ($data['hos_name'] != null) {
            return DB::table('clinics')
                ->where('name', $data['hos_name'])
                ->get();
        } elseif ($data['hos_tel'] != null) {
            return DB::table('clinics')
                ->where('tel', $data['hos_tel'])
                ->get();
        } elseif ($data['prefecture'] != '0') {
            return DB::table('clinics')
                ->where('prefecture', $data['prefecture'])
                ->get();
        } elseif ($data['city'] != null) {
            return DB::table('clinics')
                ->where('city', $data['city'])
                ->get();
        }

    }
    public function getAdmin(array $data)
    {
        return DB::table('admins')
        ->where('email', $data['old_email'])
        ->where('password', $data['old_password'])
        ->first();
    }

    public function petManageSearch(array $data)
    {
        if ($data['hos_num'] == null and $data['hos_name'] == null and $data['hos_tel'] == null and $data['city'] == null and $data['prefecture'] == '0') {
            return DB::table('users')
                ->get();
        } elseif ($data['hos_num'] != null) {
            return DB::table('users')
                ->where('id', $data['hos_num'])
                ->get();
        } elseif ($data['hos_name'] != null) {
            return DB::table('users')
                ->where('last_name', $data['hos_name'])
                ->get();
        } elseif ($data['hos_tel'] != null) {
            return DB::table('users')
                ->where('tel', $data['hos_tel'])
                ->get();
        } elseif ($data['prefecture'] != '0') {
            return DB::table('users')
                ->where('prefecture', $data['prefecture'])
                ->get();
        } elseif ($data['city'] != null) {
            return DB::table('users')
                ->where('city', $data['city'])
                ->get();
        }

    }
    public function hospitalAnalysisSearch(array $data)
    {
        if($data['presendDate']==0 and $data['termDate1']!=0 and $data['termDate2']!=0){
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                //->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->where('diagnosis.reserve_datetime', '>=', $data['termDate1'])
                ->where('diagnosis.reserve_datetime', '<=', $data['termDate2'])
                ->get();
        }elseif($data['presendDate'] != 0 and $data['termDate1'] != 0 and $data['termDate2'] != 0){
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->where('diagnosis.reserve_datetime', '>=', $data['termDate1'])
                ->where('diagnosis.reserve_datetime', '<=', $data['termDate2'])
                ->get();

        }else{
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->get();
        }

    }
    public function ownerAnalysisSearch(array $data)
    {
        if ($data['presendDate'] == 0 and $data['termDate1'] != 0 and $data['termDate2'] != 0) {
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                //->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->where('diagnosis.reserve_datetime', '>=', $data['termDate1'])
                ->where('diagnosis.reserve_datetime', '<=', $data['termDate2'])
                ->where('diagnosis.pet_id', 'like', '%' . $data['pet_id'] . '%')
                ->get();
        } elseif ($data['presendDate'] != 0 and $data['termDate1'] != 0 and $data['termDate2'] != 0) {
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->where('diagnosis.reserve_datetime', '>=', $data['termDate1'])
                ->where('diagnosis.reserve_datetime', '<=', $data['termDate2'])
                ->where('diagnosis.pet_id', 'like', '%' . $data['pet_id'] . '%')
                ->get();

        } else {
            return DB::table('clinics')
                ->join('diagnosis', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['presendDate'] . '%')
                ->where('diagnosis.pet_id', 'like', '%' . $data['pet_id'] . '%')
                ->get();
        }

    }
    public function examScheduleSearch(array $data)
    {
        // return DB::table('clinics')
        //     ->join('clinic_menus', 'clinics.id', '=', 'clinic_menus.clinic_id')
        //     ->join('diagnosis', 'clinics.id', '=', 'diagnosis.clinic_id')
        //     ->join('users', 'users.id', '=', 'diagnosis.user_id')
        //     ->join('pets', 'pets.id', '=', 'diagnosis.pet_id')
        //     ->join('accountings', 'accountings.diagnosis_id', '=', 'diagnosis.id')
        //     ->get();
        if($data['year']==0 and $data['month']==0 and $data['day'] == 0){
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->get();
            }
            // return DB::table('diagnosis')
            // ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
            // ->where('clinics.id', 'like', '%' . $data['hos_no'] . '%')
            // ->where('clinics.name', 'like', '%' . $data['hos_name'] . '%')
            // ->where('clinics.tel', 'like', '%' . $data['tel'] . '%')
            // ->where('clinics.prefecture', 'like', '%' . $data['city'] . '%')
            // ->where('clinics.city', 'like', '%' . $data['district'] . '%')
            // ->get();
        }elseif($data['year']!=0 and $data['month']==0 and $data['day'] == 0){
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '%')
                ->get();
            }
        } elseif ($data['year'] == 0 and $data['month'] != 0 and $data['day'] == 0) {
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['month'] . '%')
                ->get();
            }
        } elseif ($data['year'] == 0 and $data['month'] == 0 and $data['day'] != 0) {
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['day'] . '%')
                ->get();
            }
        } elseif ($data['year'] != 0 and $data['month'] != 0 and $data['day'] == 0) {
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'].'-'.$data['month'] . '%')
                ->get();
            }
        } elseif ($data['year'] != 0 and $data['month'] != 0 and $data['day'] != 0) {
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                   ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
               ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
               ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
               ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
               ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
               ->where('diagnosis.reserve_datetime', 'like', '%' . $data['year'] . '-' . $data['month'].'-'.$data['day'] . '%')
                ->get();
            }
        } elseif ($data['year'] == 0 and $data['month'] != 0 and $data['day'] != 0) {
            if($data['hos_no'] != "")
            {
                return DB::table('diagnosis')
                    ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                    ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                    ->where('clinics.id', $data['hos_no'])
                    ->get();
            }elseif($data['hos_name'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                ->where('clinics.name', $data['hos_name'] )
                ->get();
            }elseif($data['tel'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                ->where('clinics.tel', $data['tel'] )
                ->get();
            }elseif($data['city'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                ->where('clinics.prefecture',  $data['city'])
                ->get();
            }elseif($data['district'] != ""){
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                ->where('clinics.city', $data['district'])
                ->get();
            }else{
                return DB::table('diagnosis')
                ->join('clinics', 'diagnosis.clinic_id', '=', 'clinics.id')
                ->where('diagnosis.reserve_datetime', 'like', '%' .  $data['month'] . '-' . $data['day'] . '%')
                ->get();
            }
        }

    }
}

