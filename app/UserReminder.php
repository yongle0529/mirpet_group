<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReminder extends Model
{
    protected $fillable = ['user_id', 'flag', 'time','status'];
    public function user()
    {
        return $this->belongsTo(User::class)->orderBy('id');
    }
}
