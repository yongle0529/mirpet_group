<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProfileDeleteMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['flag'] == '1') {
            return $this
                ->subject('【みるペット】解約が完了致しました。')
                ->text('email.profile_delete');
        } else {
            return $this
                ->subject('【みるペット】解約申し込みの受け付け致しました')
                ->text('email.profile_delete');
        }
    }
}
