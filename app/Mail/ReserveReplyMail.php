<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReserveReplyMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['flag'] == '1') {
            return $this
                ->subject('【みるペット】ご請求金額が決まりました')
                ->text('email.reserve_reply');
        } else {
            return $this
                ->subject('【みるペット】振込金額決定のお知らせ')
                ->text('email.reserve_reply');
        }
    }
}
