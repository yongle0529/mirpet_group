<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OwnerReserveMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['flag'] == '1') {
            return $this
                ->subject('【みるペット】予約完了のお知らせ')
                ->text('email.owner_reserve');
        } else if ($this->data['flag'] == '2') {
            return $this
                ->subject('【みるペット】予約変更致しました')
                ->text('email.owner_reserve');
        } else {
            return $this
                ->subject('【みるペット】予約キャンセル致しました')
                ->text('email.owner_reserve');
        }
    }
}
