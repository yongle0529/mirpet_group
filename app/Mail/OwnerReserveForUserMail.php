<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OwnerReserveForUserMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['flag'] == '1') {
            return $this
                ->subject('【みるペット】予約を受け付けました')
                ->text('email.owner_reserve_for_user');
        } else if ($this->data['flag'] == '2') {
            return $this
                ->subject('【みるペット】予約の変更を受け付けました')
                ->text('email.owner_reserve_for_user');
        } else {
            return $this
                ->subject('【みるペット】予約のキャンセルを受け付けました')
                ->text('email.owner_reserve_for_user');
        }
    }
}
