<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FavoriteClinic
 *
 * @property int $id
 * @property int $user_id
 * @property int $clinic_id
 * @property int $del_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereClinicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereDelFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Clinic $clinic
 * @property-read \App\User $user
 */
class FavoriteClinic extends Model
{

    protected $guarded = array('id');

    public function user()
    {
        return $this->belongsTo(User::class)->orderBy('id');
    }

    public function clinic()
    {
        return $this->belongsTo(Clinic::class)->orderBy('id');
    }
}
