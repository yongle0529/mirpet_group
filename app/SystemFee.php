<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemFee extends Model
{
    protected $fillable = ['clinic_fee', 'owner_fee'];
}
