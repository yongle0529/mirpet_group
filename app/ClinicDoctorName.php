<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicDoctorName extends Model
{
    //
    protected $table = 'charge_doctor_names';

    protected $fillable = ['clinic_id', 'clinic_name'];
}
