<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (env('APP_ENV') != 'local') {
            $routeUri = $request->path();
            $user = $request->getUser();
            $pass = $request->getPassword();
            if ($routeUri != "healthcheck.html" && $user == 'mirpet' && $pass == 'mirpet_pass') {
                return $next($request);
            }

            $headers = ['WWW-Authenticate' => 'Basic'];
            return response('Unauthorized', 401, $headers);
        }

        return $next($request);

    }
}
