<?php
namespace App\Http\Middleware;

use Closure;
use Artisan;

class CacheKiller {

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (env('APP_ENV') === 'local') {
            Artisan::call('cache:clear');
            Artisan::call('view:clear');
            
            /* $cachedViewsDirectory = app('path.storage').'/framework/views/';

            if ($handle = opendir($cachedViewsDirectory)) {

                while (false !== ($entry = readdir($handle))) {
                    if(strstr($entry, '.')) continue;    
                    @unlink($cachedViewsDirectory . $entry);    
                }

                closedir($handle);
            } */
        }

        return $next($request);

    }

}