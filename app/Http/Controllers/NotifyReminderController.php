<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounting;
use App\Clinic;
use App\ClinicMenu;
use App\FavoriteClinic;
use App\Pet;
use App\User;
use App\Diagnosis;
use App\ReserveTimetable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Payjp\Charge;
use Payjp\Customer;
use Payjp\Payjp;
use Payjp\Tenant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\OwnerReserveForUserMail;
use App\Mail\OwnerReserveMail;
use App\Record;
use App\DiagFile;
use App\UserReminder;
use Illuminate\Support\Facades\URL;

class NotifyReminderController extends Controller
{


    public function sendEmail()
    {


        $reminders = UserReminder::where('status', null)->get();
        // $diags = Diagnosis::whereUserId(Auth::id())->where('status', 0)->get();
        // $user = Auth::user();

        // $arr = Auth::id();
        if (count($reminders) > 0) {

            foreach ($reminders as $reminder) {
                if ($reminder->flag == 1) {
                    $d = strtotime("+1 days");
                    $real_time = date("Y-m-d H:i:s", $d);
                    $diags = Diagnosis::where('user_id', $reminder->user_id)->where('status', 0)->get();
                    $cont_query = new User();
                    $users = $cont_query->getUser($reminder->user_id);
                    foreach ($users as $user);
                    //$user->email;

                    foreach ($diags as $diag) {
                        if (substr($real_time, 0, 10) == substr($diag->reserve_datetime, 0, 10)) {
                            // $attr =$diag->reserve_datetime;
                            $pet = Pet::find($diag->pet_id);
                            $clinic = Clinic::find($diag->clinic_id);
                            $userinfo = User::find($diag->user_id);
                            $data = [
                                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                                'useremail' => $userinfo->email,
                                'usertel' => $userinfo->tel,
                                'clinicname' => $clinic->name,
                                'clinicemail' => $clinic->email,
                                'clinictel' => $clinic->tel,
                                'petname' => $pet->name,
                                'reserve_datetime' => $diag->reserve_datetime,
                                'diagId' => $diag->id,
                                'created_at' => $diag->created_at
                            ];

                            Mail::to($userinfo->email)->send(new \App\Mail\ReminderDayMail($data));
                            // Mail::to($clinic->email)->send(new \App\Mail\ReminderDayMail($data));
                            $record = UserReminder::where('id', $reminder->id);

                            $record->update([
                                'status' => 1,
                            ]);
                        }
                    }
                } else if ($reminder->flag == 2) {
                    $txt = "+" . $reminder->time . " days";
                    $d = strtotime($txt);
                    $real_time = date("Y-m-d", $d);
                    $diags = Diagnosis::where('user_id', $reminder->user_id)->where('status', 0)->get();
                    $cont_query = new User();
                    $users = $cont_query->getUser($reminder->user_id);
                    foreach ($users as $user);
                    //$user->email;

                    foreach ($diags as $diag) {
                        if ($real_time == substr($diag->reserve_datetime, 0, 10)) {
                            // $attr =$diag->reserve_datetime;
                            $pet = Pet::find($diag->pet_id);
                            $clinic = Clinic::find($diag->clinic_id);
                            $userinfo = User::find($diag->user_id);
                            $data = [
                                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                                'useremail' => $userinfo->email,
                                'usertel' => $userinfo->tel,
                                'clinicname' => $clinic->name,
                                'clinicemail' => $clinic->email,
                                'clinictel' => $clinic->tel,
                                'petname' => $pet->name,
                                'reserve_datetime' => $diag->reserve_datetime,
                                'diagId' => $diag->id,
                                'created_at' => $diag->created_at
                            ];

                            Mail::to($userinfo->email)->send(new \App\Mail\ReminderDayMail($data));
                            // Mail::to($clinic->email)->send(new \App\Mail\ReminderDayMail($data));
                            $record = UserReminder::where('id', $reminder->id);

                            $record->update([
                                'status' => 1,
                            ]);
                        }
                    }
                } else if ($reminder->flag == 3) {
                    $txt = "+" . $reminder->time . " hours";
                    $d = strtotime($txt);
                    $real_time = date("Y-m-d H:i:s", $d);
                    $diags = Diagnosis::where('user_id', $reminder->user_id)->where('status', 0)->get();
                    $cont_query = new User();
                    $users = $cont_query->getUser($reminder->user_id);
                    foreach ($users as $user);
                    //$user->email;

                    foreach ($diags as $diag) {
                        if (substr($real_time, 0, 13) == substr($diag->reserve_datetime, 0, 13)) {
                            // $attr =$diag->reserve_datetime;
                            $pet = Pet::find($diag->pet_id);
                            $clinic = Clinic::find($diag->clinic_id);
                            $userinfo = User::find($diag->user_id);
                            $data = [
                                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                                'useremail' => $userinfo->email,
                                'usertel' => $userinfo->tel,
                                'clinicname' => $clinic->name,
                                'clinicemail' => $clinic->email,
                                'clinictel' => $clinic->tel,
                                'petname' => $pet->name,
                                'reserve_datetime' => $diag->reserve_datetime,
                                'diagId' => $diag->id,
                                'created_at' => $diag->created_at
                            ];

                            Mail::to($userinfo->email)->send(new \App\Mail\ReminderDayMail($data));
                            // Mail::to($clinic->email)->send(new \App\Mail\ReminderDayMail($data));
                            $record = UserReminder::where('id', $reminder->id);

                            $record->update([
                                'status' => 1,
                            ]);
                        }
                    }
                } else if ($reminder->flag == 4) {
                    $txt = "+" . $reminder->time . " minutes";
                    $d = strtotime($txt);
                    $real_time = date("Y-m-d H:i:s", $d);

                    $diags = Diagnosis::where('user_id', $reminder->user_id)->where('status', 0)->get();
                    $cont_query = new User();
                    $users = $cont_query->getUser($reminder->user_id);
                    foreach ($users as $user);
                    //$user->email;

                    foreach ($diags as $diag) {
                        // $attr =$real_time;
                        if (substr($real_time, 0, 16) == substr($diag->reserve_datetime, 0, 16)) {
                            // $attr =$user->email;
                            $pet = Pet::find($diag->pet_id);
                            $clinic = Clinic::find($diag->clinic_id);
                            $userinfo = User::find($diag->user_id);
                            $data = [
                                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                                'useremail' => $userinfo->email,
                                'usertel' => $userinfo->tel,
                                'clinicname' => $clinic->name,
                                'clinicemail' => $clinic->email,
                                'clinictel' => $clinic->tel,
                                'petname' => $pet->name,
                                'reserve_datetime' => $diag->reserve_datetime,
                                'diagId' => $diag->id,
                                'created_at' => $diag->created_at
                            ];

                            Mail::to($userinfo->email)->send(new \App\Mail\ReminderDayMail($data));
                            //Mail::to($clinic->email)->send(new \App\Mail\ReminderDayMail($data));
                            $record = UserReminder::where('id', $reminder->id);

                            $record->update([
                                'status' => 1,
                            ]);
                        }
                    }
                }
            }
        }

        $attr = "ok";
        return $attr; //Response()->json($attr);
    }
    public function checkEmail()
    {
        return 0;
    }
}
