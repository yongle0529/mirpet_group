<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\ClinicMenu;
use App\AdminDBSearch;
use App\FavoriteClinic;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pet;
use App\Accounting;
use App\Diagnosis;
use App\ReserveTimetable;
use Payjp\Charge;
use Payjp\Customer;
use Payjp\Payjp;
use Payjp\Tenant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\OwnerReserveForUserMail;
use App\Mail\OwnerReserveMail;
use App\Record;
use App\DiagFile;
use App\UserReminder;
use App\Inquiry;
use App\OwnerInquiry;
use App\ReplyMail;
use App\SystemFee;
use Illuminate\Support\Facades\URL;
use Illuminate\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use PDF;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin', 'verified']);
    }
    public function exam_schedule()
    {
        $menuactive = 1;
        return view('app.admin.examSchedule.exam_schedule', compact('menuactive'));
    }
    public function AdminAccountsetting()
    {
        $menuactive = 7;
        $adminuser = Auth::user();
        return view('app.admin.AdminAccount.accountSetting', compact('menuactive', 'adminuser'));
    }
    public function AdminAccountNewsetting(Request $request)
    {

        if ($request->get('new_password') == '' or $request->get('new_email') == '') {
            // The passwords matches

            // echo "<script>alert('以前の暗号が正確ではありません');history.go(-1)</script>";
            return redirect()->back()->with("error", "現在のパスワードが違います。");
        }
        // echo Auth::user()->email;
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->email = $request->get('new_email');
        $user->save();
        return redirect()->back()->with("success", "パスワードが変更されました。");
    }
    public function getHospitalAnalysis()
    {
        $db = new AdminDBSearch();

        $clinic_query = $db->clinic_get();

        $menuactive = 21;
        return view('app.admin.analysis.hospital_analysis', compact('menuactive', 'clinic_query'));
    }
    public function getOwnerAnalysis()
    {
        $menuactive = 22;
        return view('app.admin.analysis.owner_analysis', compact('menuactive'));
    }

    public function getOtherAnalysis()
    {
        $db = new AdminDBSearch();

        $clinic_query = $db->clinic_get();

        $menuactive = 23;
        return view('app.admin.analysis.other_analysis', compact('menuactive', 'clinic_query'));
    }
    public function getHospitalManage()
    {
        $menuactive = 3;
        return view('app.admin.HospitalManage.hospital_manage', compact('menuactive'));
    }
    public function getPetAdminManage()
    {
        $menuactive = 4;
        return view('app.admin.petAdminManage.admin_manage', compact('menuactive'));
    }
    public function getAlramMessageNew($id, $flag)
    {
        $menuactive = 51;
        if ($flag == 0) {
            $user = OwnerInquiry::find($id);
            $username = $user->name;
            $clinicname = "";
        } else {
            $user = Inquiry::find($id);
            $username = $user->last_name . $user->first_name;
            $clinicname = $user->clinic_name;
        }
        return view('app.admin.AlramMessage.alram_message_new', compact('menuactive', 'user', 'username', 'id', 'flag', 'clinicname'));
    }
    public function getAlramMessageReceive()
    {
        $menuactive = 52;
        $inquiries = Inquiry::all();
        $ownerInquiries = OwnerInquiry::all();
        return view('app.admin.AlramMessage.alram_message_receive', compact('menuactive', 'inquiries', 'ownerInquiries'));
    }
    public function deleteClinic($id)
    {
        $inquiry = Inquiry::find($id);
        $clinic = \App\Clinic::where('email', $inquiry->email)->first();
        $clinicId = $clinic->id;
        $clinic_email = $clinic->email;
        $clinic->delete();
        $clinic_inquirys = \App\Inquiry::where('email', $clinic_email)->get();
        foreach ($clinic_inquirys as $clinic_inquiry) {
            $clinic_inquiry->delete();
        }
        $accountings = \App\Accounting::where('clinic_id', $clinicId)->get();
        foreach ($accountings as $accounting) {
            $accounting->delete();
        }
        $doc_names = \App\ClinicDoctorName::where('clinic_id', $clinicId)->get();
        foreach ($doc_names as $doc_name) {
            $doc_name->delete();
        }
        $clinic_menus = \App\ClinicMenu::where('clinic_id', $clinicId)->get();
        foreach ($clinic_menus as $clinic_menu) {
            $clinic_menu->delete();
        }
        $diags = \App\Diagnosis::where('clinic_id', $clinicId)->get();
        foreach ($diags as $diag) {
            $diag_files = \App\DiagFile::where('diag_id', $diag->id)->get();
            foreach ($diag_files as $diag_file) {
                $diag_file->delete();
            }
            $diag->delete();
        }
        $clinic_favs = \App\FavoriteClinic::where('clinic_id', $clinicId)->get();
        foreach ($clinic_favs as $clinic_fav) {
            $clinic_fav->delete();
        }
        $timetables = \App\ReserveTimetable::where('clinic_id', $clinicId)->get();
        foreach ($timetables as $timetable) {
            $timetable->delete();
        }
        return redirect('/app/admin/alram_message_receive');
    }
    public function replyInquiry($id, $flag, Request $request)
    {
        $menuactive = 51;
        if ($flag == 0) {
            $user = OwnerInquiry::find($id);
            $username = $user->name;
            $user->update([
                'status' => 1
            ]);
        } else {
            $user = Inquiry::find($id);
            $username = $user->last_name . $user->first_name;
            $user->update([
                'status' => 1
            ]);
        }
        if ($user->type == "病院登録申請") {
            $clinic = Clinic::where('email', $user->email)->first();
            $clinic->update([
                'email_verified_at' => Carbon::now()
            ]);
            $data = [
                'clinicname' => $user->clinic_name,
                'username' => $user->last_name . $user->first_name
            ];
            ReplyMail::create([
                'email' => $user->email,
                'subject' => $request->input('subject'),
                'comment' => $request->input('comment')
            ]);
            // For User
            Mail::to($user->email)->send(new \App\Mail\ClinicRegiserAdminMail($data));
        } else {
            $data = [
                'subject' => $request->input('subject'),
                'comment' => $request->input('comment')
            ];
            ReplyMail::create([
                'email' => $user->email,
                'subject' => $request->input('subject'),
                'comment' => $request->input('comment')
            ]);
            // For User
            Mail::to($user->email)->send(new \App\Mail\AdminReplyInquiryMail($data));
        }
        // return redirect('/app/admin/alram_message_receive');
        return view('app.admin.AlramMessage.reply_completed', compact('menuactive', 'username'));
    }
    public function getAlramMessageSend()
    {
        $menuactive = 53;
        $replyMails = ReplyMail::all();
        return view('app.admin.AlramMessage.alram_message_send', compact('menuactive', 'replyMails'));
    }
    public function getAdminSetting()
    {
        $menuactive = 6;
        $fee = SystemFee::find(1);
        return view('app.admin.AdminSetting.admin_setting', compact('menuactive', 'fee'));
    }
    public function postAdminSetting(Request $request)
    {
        $menuactive = 6;
        $fee = SystemFee::find(1);
        $fee->update([
            'clinic_fee' => $request->input('clinic_fee'),
            'owner_fee' => $request->input('owner_fee')
        ]);
        return view('app.admin.AdminSetting.admin_setting', compact('menuactive', 'fee'));
    }
    public function postExamScheduleSearch(Request $request)
    {
        if ($request->input("month") != 0 and strlen($request->input("month")) == 1) {
            $month = '0' . $request->input("month");
        } else {
            $month = $request->input("month");
        }
        if ($request->input("day") != 0 and strlen($request->input("day")) == 1) {
            $day = '0' . $request->input("day");
        } else {
            $day = $request->input("day");
        }
        $data = [
            'year' => $request->input("year"),
            'month' => $month,
            'day' => $day,
            'hos_no' => $request->input("hos_no"),
            'hos_name' => $request->input("hos_name"),
            'tel' => $request->input("tel"),
            'city' => $request->input("city"),
            'district' => $request->input("district")
        ];
        $db = new AdminDBSearch();
        $query = $db->examScheduleSearch($data);
        $menuactive = 1;
        return view('app.admin.examSchedule.view_exam_schedule', compact('menuactive', 'query', 'data'));
    }
    public function postHospitalAnalysisSearch(Request $request)
    {
        if ($request->input('year') != 0 and $request->input('month') != 0) {
            $presendDate = $request->input('year') . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y-m');
        } elseif ($request->input('year') != 0 and $request->input('month') == 0) {
            $presendDate = $request->input('year') . "-" . "8" . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y');
        } elseif ($request->input('year') == 0 and $request->input('month') != 0) {
            $presendDate = "0" . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('m');
        } else {
            $presendDate = "0";
        }
        //
        if ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') != 0) {
            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . $request->input('day1');
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') == 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . "1" . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } else {
            $termDate1 = "0";
        }

        ///
        if ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') != 0) {
            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . $request->input('day2');
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') == 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . "12" . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } else {
            $termDate2 = "0";
        }

        //
        $prefecture = $request->input('prefecture');
        $clinics = $request->input('clinics');

        $data = [
            'presendDate' => $presendDate,
            'termDate1' => $termDate1,
            'termDate2'   => $termDate2,
        ];
        $pre_data = [
            'year'  => $request->input('year'),
            'month' => $request->input('month'),
            'year1' => $request->input('year1'),
            'month1' => $request->input('month1'),
            'day1' => $request->input('day1'),
            'year2' => $request->input('year2'),
            'month2' => $request->input('month2'),
            'day2' => $request->input('day2'),
        ];
        $menuactive = 21;

        $db = new AdminDBSearch();
        $clinic_query = $db->clinic_get();

        $query = $db->hospitalAnalysisSearch($data);

        return view('app.admin.analysis.view_hospital_get_search', compact('menuactive', 'clinic_query', 'query', 'pre_data', 'prefecture', 'clinics'));
    }
    public function postPetManageSearch(Request $request)
    {
        $data = [
            'hos_num' => $request->input('hos_num'),
            'hos_name' => $request->input('hos_name'),
            'hos_tel' => $request->input('hos_tel'),
            'prefecture' => $request->input('prefecture'),
            'city' => $request->input('city'),
        ];
        $menuactive = 4;
        $db = new AdminDBSearch();
        $query = $db->petManageSearch($data);

        return view('app.admin.petAdminManage.view_pet_manage_search', compact('menuactive', 'query', 'data'));
    }
    public function getHospitalDetailSearch($clinic_id)
    {
        $menuactive = 3;

        $clinic_query = Clinic::find($clinic_id);;
        return view('app.admin.HospitalManage.view_hospital_detail_search', compact('menuactive', 'clinic_query'));
    }
    public function getAnimalRegDetailSearch($pet_id)
    {
        $menuactive = 4;

        $pet_query = Pet::find($pet_id);;
        return view('app.admin.petAdminManage.view_animal_reg_detail_search', compact('menuactive', 'pet_query'));
    }
    public function getPetDetailSearch($user_id)
    {
        $menuactive = 4;

        $user_query = User::find($user_id);;
        return view('app.admin.petAdminManage.view_pet_detail_search', compact('menuactive', 'user_query'));
    }
    public function postHospitalManageSearch(Request $request)
    {
        $data = [
            'hos_num' => $request->input('hos_num'),
            'hos_name' => $request->input('hos_name'),
            'hos_tel' => $request->input('hos_tel'),
            'prefecture' => $request->input('prefecture'),
            'city' => $request->input('city'),
        ];
        $menuactive = 3;
        $db = new AdminDBSearch();
        $query = $db->hospitalManageSearch($data);
        return view('app.admin.HospitalManage.view_hospital_manage_search', compact('menuactive', 'query', 'data'));
    }
    public function postOwnerAnalysisSearch(Request $request)
    {
        if ($request->input('year') != 0 and $request->input('month') != 0) {
            $presendDate = $request->input('year') . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y-m');
        } elseif ($request->input('year') != 0 and $request->input('month') == 0) {
            $presendDate = $request->input('year') . "-" . "8" . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y');
        } elseif ($request->input('year') == 0 and $request->input('month') != 0) {
            $presendDate = "0" . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('m');
        } else {
            $presendDate = "0";
        }
        //
        if ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') != 0) {
            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . $request->input('day1');
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') == 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . "1" . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } else {
            $termDate1 = "0";
        }

        ///
        if ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') != 0) {
            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . $request->input('day2');
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') == 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . "12" . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } else {
            $termDate2 = "0";
        }

        //
        $prefecture = $request->input('prefecture');

        $data = [
            'presendDate' => $presendDate,
            'termDate1' => $termDate1,
            'termDate2' => $termDate2,
            'pet_id' => $request->input('pet_id')
        ];
        $pre_data = [
            'year' => $request->input('year'),
            'month' => $request->input('month'),
            'year1' => $request->input('year1'),
            'month1' => $request->input('month1'),
            'day1' => $request->input('day1'),
            'year2' => $request->input('year2'),
            'month2' => $request->input('month2'),
            'day2' => $request->input('day2'),
            'pet_id' => $request->input('pet_id')
        ];
        $menuactive = 22;

        $db = new AdminDBSearch();

        $query = $db->ownerAnalysisSearch($data);

        return view('app.admin.analysis.view_owner_get_search', compact('menuactive', 'query', 'pre_data', 'prefecture'));
    }
    public function CsvHospitalAnalysis()
    {
        $headers = array(
            'Content-type: text/csv; charset=UTF-8',
            'Content-Disposition: attachment; filename=solutionstuff_example.csv',
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        // $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();
        $reviews = [
            'ReviewID'  => 1
        ];
        $columns = array('動物病院No', '動物病院名', '予約料', '診察料', 'システム利用料', '配送資材料', 'Anonymous', '振込手数料', '振込金額');

        $callback = function () use ($reviews, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($reviews as $review) {
                fputcsv($file, array(1, 2, 3, 4, 5, 6, 7, 8, 9));
            }
            fclose($file);
        };
        return response()->streamDownload($callback, 'analysis.csv', $headers);
    }
    public function CsvOwnerAnalysis()
    {
        $headers = array(
            'Content-type: text/csv; charset=UTF-8',
            'Content-Disposition: attachment; filename=solutionstuff_example.csv',
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        // $reviews = Reviews::getReviewExport($this->hw->healthwatchID)->get();
        $reviews = [
            'ReviewID'  => 1
        ];
        $columns = array('飼い主様番号', '飼い主様名', '患者名', '動物病院名', '予約料', '配送資材料', 'Anonymous', 'システム利用料', '配送手数料', 'ご請求金額');

        $callback = function () use ($reviews, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($reviews as $review) {
                fputcsv($file, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
            }
            fclose($file);
        };
        return response()->streamDownload($callback, 'analysis.csv', $headers);
    }
    public function PdfHospitalAnalysis(Request $request)
    {
        $clinicId = $request->input('clinicId');
        $clinicName = $request->input('clinicName');
        $clinic_a = $request->input('clinic_a');
        $clinic_b = $request->input('clinic_b');
        $clinic_c = $request->input('clinic_c');
        $clinic_d = $request->input('clinic_d');
        $clinic_e = $request->input('clinic_e');
        $clinic_sum = $request->input('clinic_sum');

        $pdf = PDF::loadView('app.admin.analysis.view_pdf_hospital', compact('clinicId', 'clinicName', 'clinic_a', 'clinic_b', 'clinic_c', 'clinic_d', 'clinic_e', 'clinic_sum'));

        return $pdf->download('売上動物病院.pdf');
    }
    public function PdfOwnerAnalysis(Request $request)
    {
        $petID = $request->input('petID');
        $petName = $request->input('petName');
        $ownerName = $request->input('ownerName');
        $clinicName = $request->input('clinicName');
        $pet_a = $request->input('pet_a');
        $pet_b = $request->input('pet_b');
        $pet_c = $request->input('pet_c');
        $pet_d = $request->input('pet_d');
        $pet_sum = $request->input('pet_sum');
        $customPaper = array(0, 0, 1567.00, 783.80);
        $pdf = PDF::loadView('app.admin.analysis.view_pdf_owner', compact('petID', 'petName', 'ownerName', 'clinicName', 'pet_a', 'pet_b', 'pet_c', 'pet_d', 'pet_sum'))->setPaper($customPaper, 'landscape');

        return $pdf->download('売上飼い主様.pdf');
    }
    public function PdfOtherAnalysis(Request $request)
    {

        $hours = $request->input('hours');
        $minutes = $request->input('minutes');
        $seconds = $request->input('seconds');
        $hospital_average_num = $request->input('hospital_average_num');
        $pet_average_num = $request->input('pet_average_num');
        $hos_percent = $request->input('hos_percent');
        $pets_percent = $request->input('pets_percent');
        $mo = $request->input('mo');
        $tu = $request->input('tu');
        $we = $request->input('we');
        $th = $request->input('th');
        $fr = $request->input('fr');
        $sa = $request->input('sa');
        $su = $request->input('su');
        $h09 = $request->input('h09');
        $h912 = $request->input('h912');
        $h1216 = $request->input('h1216');
        $h1619 = $request->input('h1619');
        $h1924 = $request->input('h1924');

        $hos_animal0 = $request->input('hos_animal0');
        $hos_animal1 = $request->input('hos_animal1');
        $hos_animal2 = $request->input('hos_animal2');
        $hos_animal3 = $request->input('hos_animal3');
        $hos_animal4 = $request->input('hos_animal4');
        $hos_animal5 = $request->input('hos_animal5');
        $hos_animal6 = $request->input('hos_animal6');
        $hos_animal7 = $request->input('hos_animal7');


        $pdf = PDF::loadView('app.admin.analysis.view_pdf_other', compact(
            'hours',
            'minutes',
            'seconds',
            'hospital_average_num',
            'pet_average_num',
            'hos_percent',
            'pets_percent',
            'mo',
            'tu',
            'we',
            'th',
            'fr',
            'sa',
            'su',
            'h09',
            'h912',
            'h1216',
            'h1619',
            'h1924',
            'hos_animal0',
            'hos_animal1',
            'hos_animal2',
            'hos_animal3',
            'hos_animal4',
            'hos_animal5',
            'hos_animal6',
            'hos_animal7'
        ));

        return $pdf->download('その他.pdf');
    }
    public function postOtherAnalysisSearch(Request $request)
    {
        if ($request->input('year') != 0 and $request->input('month') != 0) {
            $presendDate = $request->input('year') . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y-m');
        } elseif ($request->input('year') != 0 and $request->input('month') == 0) {
            $presendDate = $request->input('year') . "-" . "8" . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('Y');
        } elseif ($request->input('year') == 0 and $request->input('month') != 0) {
            $presendDate = "0" . "-" . $request->input('month') . "-" . "8";
            $presendDate = Carbon::parse($presendDate)->format('m');
        } else {
            $presendDate = "0";
        }
        //
        if ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') != 0) {
            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . $request->input('day1');
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') != 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . $request->input('month1') . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } elseif ($request->input('year1') != 0 and $request->input('month1') == 0 and $request->input('day1') == 0) {

            $termDate1 = $request->input('year1') . "-" . "1" . "-" . "1";
            $termDate1 = Carbon::parse($termDate1)->format('Y-m-d');
        } else {
            $termDate1 = "0";
        }

        ///
        if ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') != 0) {
            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . $request->input('day2');
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') != 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . $request->input('month2') . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } elseif ($request->input('year2') != 0 and $request->input('month2') == 0 and $request->input('day2') == 0) {

            $termDate2 = $request->input('year2') . "-" . "12" . "-" . "31";
            $termDate2 = Carbon::parse($termDate2)->format('Y-m-d');
        } else {
            $termDate2 = "0";
        }

        //
        $prefecture = $request->input('prefecture');
        $clinics = $request->input('clinics');

        $data = [
            'presendDate' => $presendDate,
            'termDate1' => $termDate1,
            'termDate2' => $termDate2,
        ];
        $pre_data = [
            'year' => $request->input('year'),
            'month' => $request->input('month'),
            'year1' => $request->input('year1'),
            'month1' => $request->input('month1'),
            'day1' => $request->input('day1'),
            'year2' => $request->input('year2'),
            'month2' => $request->input('month2'),
            'day2' => $request->input('day2'),
        ];
        $menuactive = 23;

        $db = new AdminDBSearch();
        $clinic_query = $db->clinic_get();
        $pets_query = $db->pets_get();

        $query = $db->hospitalAnalysisSearch($data);
        return view('app.admin.analysis.view_other_get_search', compact('menuactive', 'clinic_query', 'query', 'pre_data', 'prefecture', 'clinics', 'pets_query'));
    }
}
