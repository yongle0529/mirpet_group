<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Clinic;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;

class ClinicResetPasswordController extends Controller
{
    public function showLinkRequestForm($flag)
    {
        return view('auth.passwords.clinic_email', compact('flag'));
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $clinic = Clinic::where('email', $request->input('email'))->first();

        if (!empty($clinic)) {
            if ($clinic->status != 2) {
                $token = $this->getName(20);
                $res = $clinic->update([
                    'reset_token' => $token
                ]);
                $url = URL::to('/clinic/password/resetpassword/' . $token);
                $data = [
                    'url' => $url
                ];
                if ($res) {
                    Mail::to($clinic->email)->send(new \App\Mail\ClinicResetPassword($data));
                    return redirect('/clinic/password/reset/1');
                }
            } else {
                return redirect('/clinic/password/reset/2');
            }
        } else {
            return redirect('/clinic/password/reset/2');
        }
    }

    public function showResetForm($token)
    {
        $clinic = Clinic::where('reset_token', $token)->first();

        if (!empty($clinic)) {
            return view('auth.passwords.clinic_reset', compact('token'));
        } else {
            return view('auth.passwords.error');
        }
    }

    public function showResetComplete()
    {
        return view('auth.passwords.reset_complete');
    }

    public function resetPassword(Request $request)
    {
        $this->validation($request);

        $clinic = Clinic::where('email', $request->input('email'))->first();

        if (!empty($clinic)) {
            $res = $clinic->update([
                'password' => Hash::make($request->input('password')),
                'reset_token' => null
            ]);
            if ($res) {
                return redirect('/clinic/reset_complete');
            }
        } else {
            return back()->withInput();
        }
    }

    public function getName($n)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    protected function validateEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
    }

    protected function validation(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => [
                'required',
                'min:8',
                'max:20',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/',
                'confirmed',
            ]
        ]);
    }

    public function __construct()
    {
        $this->middleware('guest');
    }
}
