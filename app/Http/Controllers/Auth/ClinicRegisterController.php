<?php

namespace App\Http\Controllers\Auth;

use App\Clinic;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Diagnosis;
use App\Admin;
use Illuminate\Support\Facades\Mail;

class ClinicRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/clinic/bank/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:clinic');
    }

    public function showRegisterForm()
    {
        return view('auth.clinic_register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'clinic_name' => ['required', 'string', 'max:255'],
            'clinic_name_kana' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'first_name_kana' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'last_name_kana' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:clinics'],
            'password' => [
                'required',
                'min:8',
                'max:20',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/',
                'confirmed'
            ],
            'tel' => ['required', 'numeric', 'digits_between:8,11'],
            'zip_code' => ['required', 'numeric', 'digits:7'],
            'prefecture' => ['required', 'string', 'max:10'],
            'city' => ['required', 'string', 'max:20'],
            'address' => ['required', 'string', 'max:255'],
            'url' => ['required', 'string', 'max:255']
        ]);
    }
    function medical_option($option)
    {
        if ($option == 'agree') {
            return true;
        } else {
            return false;
        }
    }
    protected function create_clinics(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            // 'clinic_name' => ['required', 'string', 'max:255'],
            // 'clinic_name_kana' => ['required', 'string', 'max:255'],
            // 'first_name' => ['required', 'string', 'max:255'],
            // 'first_name_kana' => ['required', 'string', 'max:255'],
            // 'last_name' => ['required', 'string', 'max:255'],
            // 'last_name_kana' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:clinics'],
            'password' => [
                'required',
                'min:8',
                'max:20',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/',
                'confirmed'
            ],
            // 'tel' => ['required', 'numeric', 'digits_between:8,11'],
            // 'zip_code' => ['required', 'numeric', 'digits:7'],
            // 'prefecture' => ['required', 'string', 'max:10'],
            // 'city' => ['required', 'string', 'max:20'],
            // 'address' => ['required', 'string', 'max:255'],
            // 'url' => ['required', 'string', 'max:255']
        ]);
        $data = [
            'name' => $request->input('clinic_name'),
            'name_kana' => trim(mb_convert_kana($request->input('clinic_name_kana'), 'C')),
            'manager_name' => $request->input('last_name') . $request->input('first_name'),
            'manager_name_kana' => trim(mb_convert_kana($request->input('last_name_kana'), 'C')) . trim(mb_convert_kana($request->input('first_name_kana'), 'C')),
            "option_pet" => json_encode(
                [
                    'dog' => $this->medical_option($request->input('dog')), //内科
                    'cat' => $this->medical_option($request->input('cat')), //整形外科
                    'hamster' => $this->medical_option($request->input('hamster')), //神経外科
                    'ferret' => $this->medical_option($request->input('ferret')), //皮膚科
                    'rabbit' => $this->medical_option($request->input('rabbit')), //循環器科
                    'bird' => $this->medical_option($request->input('bird')), //泌尿器科
                    'reptile' => $this->medical_option($request->input('reptile')), //眼科
                    'others' => $this->medical_option($request->input('others')),
                ]
            ),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'tel' => str_replace('-', '', mb_convert_kana($request->input('tel'), 'a')),
            'zip_code' => str_replace('-', '', mb_convert_kana($request->input('zip_code'), 'a')),
            'prefecture' => mb_convert_kana($request->input('prefecture'), 'as'),
            'city' => mb_convert_kana($request->input('city'), 'as'),
            'address' => mb_convert_kana($request->input('address'), 'as'),
            'url' => $request->input('clinic_url')
        ];
        $db = new Clinic();
        $query = $db->create($data);

        $id = $query->id;
        // $clinic_query = Clinic::find($id);
        // @if(\App\Clinic::find($diag_query->clinic_id)->id<10)
        // V{{ date('ym').'00'.\App\Clinic::find($diag_query->clinic_id)->id }}
        // @elseif(\App\Clinic::find($diag_query->clinic_id)->id<100)
        // V{{ date('ym').'0'.\App\Clinic::find($diag_query->clinic_id)->id }}
        // @else
        // V{{ date('ym').''.\App\Clinic::find($diag_query->clinic_id)->id }}
        // @endif
        if($id<10){
            $admin_clinic_id = date('ym').'00'.$id;
        }elseif($id<100){
            $admin_clinic_id = date('ym').'0'.$id;
        }else{
            $admin_clinic_id = date('ym').''.$id;
        }
        // $query->update([
        //         'admin_clinic_id' => $admin_clinic_id
        // ]);

        $last_name = $request->input('last_name');
        $first_name = $request->input('first_name');

        if ($query) {
            return redirect('clinic/bank/register/' . $id . '/' . $last_name . '/' . $first_name);
        }
    }

    public function viewBankRegister($id, $last_name, $first_name)
    {
        $clinicId = $id;
        $clinic = Clinic::find($id);
        return view('auth.clinic_bank_register', compact('clinicId', 'clinic', 'last_name', 'first_name'));
    }

    public function BankRegister($id, $last_name, $first_name, \Illuminate\Http\Request $request)
    {
        $clinic = Clinic::find($id);
        $admin = Admin::find(1);

        $data = json_encode([
            'bank_name' => $request->input('bank_name'),
            'bank_branch_name' => $request->input('bank_branch_name'),
            'bank_type' => $request->input('bank_type'),
            'branch_number' => $request->input('branch_number'),
            'bank_account_number' => $request->input('bank_account_number'),
            'bank_account_name' => $request->input('bank_account_name')
        ]);

        $response = $clinic->update([
            'bank_info' => json_encode([
                'bank_name' => $request->input('bank_name'),
                'bank_branch_name' => $request->input('bank_branch_name'),
                'bank_type' => $request->input('bank_type'),
                'branch_number' => $request->input('branch_number'),
                'bank_account_number' => $request->input('bank_account_number'),
                'bank_account_name' => $request->input('bank_account_name')
            ]),
        ]);

        if ($response) {
            $data = [
                'clinicname' => $clinic->name
            ];

            Mail::to($clinic->email)->send(new \App\Mail\ClinicRegiserMail($data));
            $data1 = [
                'last_name' => $last_name,
                'first_name' => $first_name,
                'last_name_kana' => null,
                'clinic_name' => $clinic->name,
                'email' => $clinic->email,
                'tel' => $clinic->tel,
                'type' => "病院登録申請"
            ];
            // Save Database
            \App\Inquiry::create($data1);

            // For Admin
            Mail::to($admin->email)->send(new \App\Mail\InquiryMail($data1));
            return redirect('clinic/verify');
        } else {
            return redirect('clinic/bank/register/' . $id);
        }
    }

    public function updateStart($diagId)
    {
        $diag = Diagnosis::find($diagId);
        $diag->update([
            'start_datetime' => \Carbon\Carbon::now(),
        ]);
    }

    public function viewVerify()
    {
        return view('auth.clinic_verify');
    }

    protected function guard()
    {
        return \Auth::guard('clinic');
    }
}
