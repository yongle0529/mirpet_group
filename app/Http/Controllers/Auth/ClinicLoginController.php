<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Validation\ValidationException;
use App\Clinic;

class ClinicLoginController extends Controller
{
    //use AuthenticatesUsers;

    protected $guard = 'clinic';

    protected $redirectTo = '/app/clinic';

    public function __construct()
    {
        $this->middleware('guest:clinic')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.clinic_login');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $user = Clinic::where('email', $request->email)->first();
        if (!empty($user)) {
            if ($user->status == 2 || $user->email_verified_at == null) {
                return $this->sendFailedLoginResponse($request);
            }
            // Attempt to log the user in
            if (Auth::guard('clinic')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                // if successful, then redirect to their intended locationz
                return redirect()->intended(route('app.clinic.index'));
            }
        }

        // if unsuccessful, then redirect back to the login with the form data
        //return redirect()->back()->withInput($request->only('email', 'remember'));
        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function username()
    {
        return 'email';
    }

    public function logout(Request $request)
    {
        Auth::guard('clinic')->logout(); //$this->guard
        //$request->session()->flush();
        //$request->session()->regenerate();

        return redirect('/clinic');
    }

    protected function guard()
    {
        return Auth::guard($this->guard);
    }
}
