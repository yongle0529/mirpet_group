<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Validation\ValidationException;
use App\Admin;

class AdminLoginController extends Controller
{
    //use AuthenticatesUsers;

    protected $guard = 'admin';

    protected $redirectTo = '/app/admin';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    public function adminshowLoginForm()
    {
        return view('auth.admin_login');
    }
    public function showLoginForm()
    {
        return view('auth.admin_login');
    }
    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {

            return redirect()->intended(route('app.admin.index'));
        }
        // return back()->withInput($request->only('email', 'remember'));
        return $this->sendFailedLoginResponse($request);
    }


    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function username()
    {
        return 'email';
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout(); //$this->guard
        //$request->session()->flush();
        //$request->session()->regenerate();

        return redirect('/admin');
    }

    protected function guard()
    {
        return Auth::guard($this->guard);
    }
}
