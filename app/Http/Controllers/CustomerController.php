<?php
// Our Controller
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Diagnosis;

class CustomerController extends Controller
{
    public function printPDF($id)
    {
        // This  $data array will be passed to our PDF blade
        $diag = Diagnosis::find($id);

        $pdf = PDF::loadView('pdf_view', compact('diag'));

        $pdf->download('Receipt.pdf');

        return $pdf->stream('Receipt.pdf');
    }
}
