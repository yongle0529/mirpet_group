<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\ClinicMenu;
use App\FavoriteClinic;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pet;
use App\OwnerCatano;
use App\PetCatano;
use App\ClinicDoctorName;
use App\Accounting;
use App\Diagnosis;
use App\ReserveTimetable;
use Payjp\Charge;
use Payjp\Customer;
use Payjp\Payjp;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\OwnerReserveForUserMail;
use App\Mail\OwnerReserveMail;
use App\Record;
use App\DiagFile;
use App\SystemFee;
use App\UserReminder;
use Illuminate\Support\Facades\URL;

class ClinicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:clinic', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return redirect('application');
    }

    public function viewClinicSearch()
    {
        return view('app.owner.search');
    }

    public function viewChat($diagId)
    {
        // $diag = Diagnosis::find($diagId);
        // $diag->update([
        //     'start_datetime' => \Carbon\Carbon::now(),
        // ]);
        $flag = 1;
        return view('chat.index', compact('diagId', 'flag'))->with('returnUrl', URL::to('/app/clinic/reserve_end/' . $diagId));
    }

    public function viewApplication(Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $mode = $request->input('mode', 'exam');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $delayedDiags = Diagnosis::where('clinic_id', Auth::id())->where('status', 1)->get();
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $clinicId = Auth::id();
        $delay_count = 0;
        foreach ($delayedDiags as $item) {
            $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
            if (!empty($accounting) && $accounting->status == 2) {
                if ($accounting->delivery_cost != 0) {
                    $delay_count++;
                }
            }
        }

        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting)) {
                    if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                        $remind_count++;
                    }
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' => $bill_count, //count($finishedDiags),
            'shipping' => $delay_count,
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }
        return view('app.clinic.application', compact('diags', 'finishedDiags', 'delayedDiags', 'nextDiags', 'mode', 'statusCount', 'clinicId'));
    }

    public function viewApplicationToday(Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $mode = $request->input('mode', 'exam');
        $year = date("Y");
        $month = date("n");
        $day = date("d");
        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->where('reserve_datetime', 'like', '%' . $year . "-" . $month . "-" . $day . '%')->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $delayedDiags = Diagnosis::where('clinic_id', Auth::id())->where('status', 1)->get();
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $clinicId = Auth::id();
        $delay_count = 0;
        foreach ($delayedDiags as $item) {
            $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
            if (!empty($accounting) && $accounting->status == 2) {
                if ($accounting->delivery_cost != 0) {
                    $delay_count++;
                }
            }
        }

        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting)) {
                    if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                        $remind_count++;
                    }
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' => $bill_count, //count($finishedDiags),
            'shipping' => $delay_count,
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }
        return view('app.clinic.application_today', compact('diags', 'finishedDiags', 'delayedDiags', 'nextDiags', 'mode', 'statusCount', 'clinicId'));
    }
    public function ViewBill(Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $mode = $request->input('mode', 'bill');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $delayedDiags = Diagnosis::where('clinic_id', Auth::id())->where('status', 1)->get();
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $clinicId = Auth::id();
        $delay_count = 0;
        foreach ($delayedDiags as $item) {
            $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();

            if (!empty($accounting)  && $accounting->status == 2) {
                if ($accounting->delivery_cost != 0) {
                    $delay_count++;
                }
            }
        }

        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                    $remind_count++;
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' => $bill_count,
            'shipping' => $delay_count,
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }

        return view('app.clinic.application', compact('diags', 'finishedDiags', 'delayedDiags', 'nextDiags', 'mode', 'statusCount', 'clinicId'));
    }

    public function ViewShipping(Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $mode = $request->input('mode', 'shipping');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $delayedDiags = Diagnosis::where('clinic_id', Auth::id())->where('status', 1)->get();
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $clinicId = Auth::id();
        $delay_count = 0;
        foreach ($delayedDiags as $item) {
            $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
            if (!empty($accounting)  && $accounting->status == 2) {
                if ($accounting->delivery_cost != 0) {
                    $delay_count++;
                }
            }
        }

        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                    $remind_count++;
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' =>  $bill_count, //count($finishedDiags),
            'shipping' => $delay_count,
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }

        return view('app.clinic.application', compact('diags', 'finishedDiags', 'delayedDiags', 'nextDiags', 'mode', 'statusCount', 'clinicId'));
    }

    public function ViewRemind(Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $mode = $request->input('mode', 'remind');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $delayedDiags = Diagnosis::where('clinic_id', Auth::id())->where('status', 1)->get();
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $clinicId = Auth::id();
        $delay_count = 0;
        foreach ($delayedDiags as $item) {
            $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
            if (!empty($accounting)  && $accounting->status == 2) {
                if ($accounting->delivery_cost != 0) {
                    $delay_count++;
                }
            }
        }

        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', $clinicId)->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                    $remind_count++;
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' => $bill_count, //count($finishedDiags),
            'shipping' => $delay_count,
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }

        return view('app.clinic.application', compact('diags', 'finishedDiags', 'delayedDiags', 'nextDiags', 'mode', 'statusCount', 'clinicId'));
    }

    public function viewReserve()
    {
        $data = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        return view('app.clinic.reserve', compact('data'));
    }
    /////////////////////////////
    /////////////////////////////hhhhhh

    public function viewFaq()
    {
        return view('app.clinic.faq');
    }

    public function viewProfile()
    {
        $user = Auth::user();
        $content = new User();
        $clinic_menus = $content->Clinic_menus($user->id);
        $chargeDoctor = ClinicDoctorName::where('clinic_id', $user->id)->get();
        $id = substr($user->created_at, 2, 6);
        $user_id = str_replace("-", "", $id);
        if ($user->id < 10) {
            $user_id = $user_id . "00" . $user->id;
        } elseif ($user->id < 100) {
            $user_id = $user_id . "0" . $user->id;
        } else {
            $user_id = $user_id . "" . $user->id;
        }
        return view('app.clinic.setting.profile_update', compact('user', 'clinic_menus', 'chargeDoctor', 'user_id'));
    }

    public function profileDelete($id)
    {
        $user = Clinic::find($id);
        // if (empty($user)) {
        //     return view('app.owner.index')->with('isError', true);
        // }
        $email = $user->email;
        $username = $user->name;
        $user->update([
            'status' => 2
        ]);
        $clinic_favs = \App\FavoriteClinic::where('clinic_id', $user->id)->get();
        foreach ($clinic_favs as $clinic_fav) {
            $clinic_fav->delete();
        }
        $data = [
            'username' => $username,
            'flag' => '2'
        ];
        $admin = \App\Admin::find(1);
        $data1 = [
            'last_name' => $user->manager_name,
            'first_name' => null,
            'last_name_kana' => null,
            'clinic_name' => $user->name,
            'email' => $user->email,
            'tel' => $user->tel,
            'type' => "病院解約申請"
        ];
        $data2 = [
            'last_name' => $user->manager_name,
            'first_name' => null,
            'last_name_kana' => null,
            'clinic_name' => $user->name,
            'email' => $user->email,
            'tel' => $user->tel,
            'address' => $user->prefecture . " " . $user->city . " " . $user->address,
            'url' => $user->url,
            'type' => "病院解約申請"
        ];
        // Save Database
        \App\Inquiry::create($data1);

        // For User
        Mail::to($email)->send(new \App\Mail\ProfileDeleteMail($data));

        // For Admin
        Mail::to($admin->email)->send(new \App\Mail\DeleteMail($data2));

        Auth::guard('clinic')->logout();

        return redirect('/clinic/delete_complete');
    }

    public function viewReserveSetting()
    {
        $user = Auth::user();
        $content = new User();
        $query = Clinic::where('id', Auth::id())->get();
        // echo count($clinic);
        $old_clinic_id = 0;
        $old_clinic_id = 0;
        $clinic_base_menus = ClinicMenu::where('base_id', $user->id)->get();
        if (count($clinic_base_menus) == 0) {
            $old_clinic_menus = $content->Clinic_menus($old_clinic_id);
            foreach ($old_clinic_menus as $old_clinic_menu) {
                ClinicMenu::create([
                    'clinic_id' => $user->id,
                    'name' => $old_clinic_menu->name,
                    'base_id' => $user->id,
                    'description' => $old_clinic_menu->description,
                    'price_list' => $old_clinic_menu->price_list,
                    'is_insurance_applicable' => $old_clinic_menu->is_insurance_applicable,
                    'option' => $old_clinic_menu->option,
                    'status' => $old_clinic_menu->status,
                    'del_flg' => $old_clinic_menu->del_flg,
                    'insurance_company' => $old_clinic_menu->insurance_company,
                    'created_at' => $old_clinic_menu->created_at,
                    'updated_at' => $old_clinic_menu->updated_at,
                ]);
            }
        }
        $old_clinic_menus = $content->Clinic_menus($old_clinic_id);
        $clinic_menus = $content->Clinic_menus($user->id);
        $clinicMenu = ClinicMenu::where('clinic_id', $user->id)->first();
        $clinicMenus = ClinicMenu::where('clinic_id', $user->id)->get();
        // echo $user->id;
        return view('app.clinic.setting.reserve_state', compact('user', 'query', 'old_clinic_menus', 'clinic_menus', 'clinicMenu', 'clinicMenus'));

        // return view('app.clinic.setting.reserve', compact('user', 'query', 'old_clinic_menus', 'clinic_menus'));
    }
    public function viewReserveSetting_edit()
    {
        $user = Auth::user();
        $content = new User();
        $query = Clinic::where('id', Auth::id())->get();
        // echo count($clinic);
        $old_clinic_id = 0;

        $old_clinic_menus = $content->Clinic_menus($old_clinic_id);
        $clinic_menus = $content->Clinic_menus($user->id);
        $clinicMenu = ClinicMenu::where('clinic_id', $user->id)->first();
        $clinicMenus = ClinicMenu::where('clinic_id', $user->id)->get();
        // echo $user->id;
        // return view('app.clinic.setting.reserve_state', compact('user', 'query', 'old_clinic_menus', 'clinic_menus','clinicMenu','clinicMenus'));

        return view('app.clinic.setting.reserve', compact('user', 'query', 'old_clinic_menus', 'clinic_menus'));
    }
    public function viewMenuSetting()
    {
        $user = Auth::user();
        $content = new User();
        $clinic = Clinic::where('id', Auth::id())->get();
        // echo count($clinic);
        $old_clinic_id = 0;
        $clinic_base_menus = ClinicMenu::where('base_id', $user->id)->get();
        if (count($clinic_base_menus) == 0) {
            $old_clinic_menus = $content->Clinic_menus($old_clinic_id);
            foreach ($old_clinic_menus as $old_clinic_menu) {
                ClinicMenu::create([
                    'clinic_id' => $user->id,
                    'name' => $old_clinic_menu->name,
                    'base_id' => $user->id,
                    'description' => $old_clinic_menu->description,
                    'price_list' => $old_clinic_menu->price_list,
                    'is_insurance_applicable' => $old_clinic_menu->is_insurance_applicable,
                    'option' => $old_clinic_menu->option,
                    'status' => $old_clinic_menu->status,
                    'del_flg' => $old_clinic_menu->del_flg,
                    'insurance_company' => $old_clinic_menu->insurance_company,
                    'created_at' => $old_clinic_menu->created_at,
                    'updated_at' => $old_clinic_menu->updated_at,
                ]);
            }
        }
        $old_clinic_menus = ClinicMenu::where('base_id', $user->id)->get();
        $clinic_menus = $content->Clinic_menus($user->id);
        // echo $user->id;
        return view('app.clinic.setting.menu', compact('user', 'clinic', 'old_clinic_menus', 'clinic_menus'));
    }

    public function showChangePasswordForm()
    {
        $user = Auth::user();
        return view('app.clinic.setting.password', compact('user'));
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "現在のパスワードが違います。");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => [
                'required',
                'min:8',
                'max:20',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/',
                'confirmed'
            ],
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success", "パスワードが変更されました。");
    }

    public function viewPayment()
    {
        $user = Auth::user();

        return view('app.clinic.setting.payment', compact('user'));
    }
    public function updatePayment(Request $request)
    {
        $user = Auth::user();
        $clinic = Clinic::find($user->id);
        $data = json_encode([
            'bank_name' => $request->input('bank_name'),
            'bank_branch_name' => $request->input('bank_branch_name'),
            'bank_type' => $request->input('bank_type'),
            'branch_number' => $request->input('branch_number'),
            'bank_account_number' => $request->input('bank_account_number'),
            'bank_account_name' => $request->input('bank_account_name'),

        ]);

        $response = $clinic->update([
            'bank_info' => json_encode([
                'bank_name' => $request->input('bank_name'),
                'bank_branch_name' => $request->input('bank_branch_name'),
                'bank_type' => $request->input('bank_type'),
                'branch_number' => $request->input('branch_number'),
                'bank_account_number' => $request->input('bank_account_number'),
                'bank_account_name' => $request->input('bank_account_name'),
                'comment' => $request->input('comment')
            ]),
        ]);
        return view('app.clinic.setting.payment_save');
    }
    public function viewPaymentCompleted()
    {
        return view('app.clinic.payment_completed');
    }

    public function viewReserveCompleted()
    {
        return view('app.clinic.reserve_completed');
    }
    //////////////////  reserve state view /////////////////////////////
    public function reservestate()
    {

        $year = date("Y");
        $month = date("n");
        $day = date("j");
        $data['year'] = $year;
        $data['month'] = $month;
        $data['day'] = $day;
        $data['cata_view'] = 0;
        $data['week_day_num'] = 0;
        $data['cata_view'] = 0;
        $r_date = $year . '-' . $month;

        // $clinics = $clinics->where('city', 'like', "%{$inputCity}%");

        // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
        $clinic_id = Auth::id();
        $content = new Pet();
        $query = $content->getReserve($r_date, $clinic_id);

        return view('app.clinic.reserve', compact('data', 'query'))->with('pageName', 'reserve');
    }
    public function reservestate_vew($year, $month, $next_day_num, $datetime)
    {
        $year = substr($datetime, 0, 4);
        $month = substr($datetime, 5, 2);
        $day = substr($datetime, 8, 2);
        if (strlen($month) == 1) {
            $month = '0' . $month;
        }
        if (strlen($day) == 1) {
            $day = '0' . $day;
        }
        $data['year'] = $year;
        $data['month'] = $month;
        $data['day'] = $day;
        $data['cata_view'] = 1;
        $data['week_day_num'] = $next_day_num;

        // $clinics = $clinics->where('city', 'like', "%{$inputCity}%");
        $r_date = $year . '-' . $month;

        $clinic_id = Auth::id();
        $content = new Pet();
        $query = $content->getReserve($r_date, $clinic_id);

        // $query_view = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime',$datetime)->get();
        $query_view = $content->getReserve_sel($datetime, $clinic_id);

        return view('app.clinic.reserve', compact('data', 'query', 'query_view', 'next_day_num'))->with('pageName', 'reserve');
    }
    public function reserveupdate($year, $month, $day, $se_id, $next_day_num)
    {
        if ($se_id == 1) {
            $year = $year - 1;
        } else if ($se_id == 2) {
            $month = $month - 1;
        } else if ($se_id == 3) {
            $month = $month + 1;
        } else if ($se_id == 4) {
            $year = $year + 1;
        }
        if ($month == 13) {
            $year = $year + 1;
            $month = 1;
        }
        if ($month == 0) {
            $year = $year - 1;
            $month = 12;
        }
        $data['year'] = $year;
        $data['month'] = $month;
        $data['day'] = $day;
        $data['cata_view'] = 0;
        $data['week_day_num'] = $next_day_num;
        $r_date = $year . '-' . $month;

        // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
        $clinic_id = Auth::id();
        $content = new Pet();
        $query = $content->getReserve($r_date, $clinic_id);

        return view('app.clinic.reserve', compact('data', 'query'))->with('pageName', 'reserve');
    }
    public function reservedayupdate($year, $month, $day, $next_day_num)
    {
        $data['year'] = $year;
        $data['month'] = $month;
        $data['day'] = $day;
        $data['cata_view'] = 0;
        $data['week_day_num'] = $next_day_num;
        $r_date = $year . '-' . $month;

        // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
        $clinic_id = Auth::id();
        $content = new Pet();
        $query = $content->getReserve($r_date, $clinic_id);

        return view('app.clinic.reserve', compact('data', 'query'))->with('pageName', 'reserve');
    }
    public function ajaxreservenextweek($year, $month, $day, $last_day, $week_id, $next_day_num)
    {
        if ($week_id == 1) {
            $next_day_num -= 1;
            $d = strtotime("$next_day_num  week -1 day");
            $start_week = strtotime("last sunday midnight", $d);
            $day = date("d", $start_week);
            $month = date("m", $start_week);
            $year = date("Y", $start_week);
            // $prev_week = date("Y.m.d", mktime(0,0,0,$month, $day-7, $year));
            $firstdate_timestamp = mktime(0, 0, 0, $month, 1, $year);

            $d = -idate("w", $firstdate_timestamp);
            $last_day = idate("t", $firstdate_timestamp);
            $wday = $year . '-' . $month . '-' . $day;
            $currentWeekNumber = date('w', strtotime($wday));
            // echo $currentWeekNumber;
            $WeekNumber = array(7);
            $n = 0;
            for ($i = $currentWeekNumber; $i > 0; $i--) {
                if ($day - $i < 1) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day - $i;
                }
                $n++;
            }

            for ($i = 0; $i < 7 - $currentWeekNumber; $i++) {
                if ($day + $i > $last_day) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day + $i;
                }
                $n++;
            }
            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = $next_day_num;
            $r_date = $year . '-' . $month;

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();

            $query = $content->getReserve($r_date, $clinic_id, $next_day_num);
            $ownercatano = OwnerCatano::where('clinic_id', $clinic_id)->where('clinic_id', $clinic_id)->get();
            $arr = array('data' => $data, 'WeekNumber' => $WeekNumber, 'query' => $query, 'owner_query' =>  $ownercatano);
        } elseif ($week_id == 2) {
            $year = date("Y");
            $month = date("n");
            $day = date("j");
            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = 0;
            $r_date = $year . '-' . $month;
            $firstdate_timestamp = mktime(0, 0, 0, $month, 1, $year);

            $d = -idate("w", $firstdate_timestamp);
            $last_day = idate("t", $firstdate_timestamp);
            $wday = $year . '-' . $month . '-' . $day;
            $currentWeekNumber = date('w', strtotime($wday));
            // echo $currentWeekNumber;
            $WeekNumber = array(7);
            $n = 0;
            for ($i = $currentWeekNumber; $i > 0; $i--) {
                if ($day - $i < 1) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day - $i;
                }
                $n++;
            }

            for ($i = 0; $i < 7 - $currentWeekNumber; $i++) {
                if ($day + $i > $last_day) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day + $i;
                }
                $n++;
            }

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();
            $query = $content->getReserve($r_date, $clinic_id);
            $ownercatano = OwnerCatano::where('clinic_id', $clinic_id)->get();
            $arr = array('data' => $data, 'WeekNumber' => $WeekNumber, 'query' => $query, 'owner_query' =>  $ownercatano);
            // $arr = array('data' => $data, 'WeekNumber' => $WeekNumber, 'query' => $query);
        } else {
            $next_day_num += 1;
            $d = strtotime("$next_day_num  week -1 day");
            $start_week = strtotime("last sunday midnight", $d);
            $day = date("d", $start_week);
            $month = date("m", $start_week);
            $year = date("Y", $start_week);
            //  echo $day;

            $firstdate_timestamp = mktime(0, 0, 0, $month, 1, $year);

            $d = -idate("w", $firstdate_timestamp);
            $last_day = idate("t", $firstdate_timestamp);
            $wday = $year . '-' . $month . '-' . $day;
            $currentWeekNumber = date('w', strtotime($wday));
            // echo $currentWeekNumber;
            $WeekNumber = array(7);
            $n = 0;
            for ($i = $currentWeekNumber; $i > 0; $i--) {
                if ($day - $i < 1) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day - $i;
                }
                $n++;
            }

            for ($i = 0; $i < 7 - $currentWeekNumber; $i++) {
                if ($day + $i > $last_day) {
                    $WeekNumber[$n] = "";
                } else {
                    $WeekNumber[$n] = $day + $i;
                }
                $n++;
            }

            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = $next_day_num;
            $r_date = $year . '-' . $month;

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();
            $query = $content->getReserve($r_date, $clinic_id);
            $ownercatano = OwnerCatano::where('clinic_id', $clinic_id)->get();
            $arr = array('data' => $data, 'WeekNumber' => $WeekNumber, 'query' => $query, 'owner_query' =>  $ownercatano);
            // $arr = array('data' => $data, 'WeekNumber' => $WeekNumber, 'query' => $query);
        }

        // $arr=$week_id;
        return Response()->json($arr);

        // return view('app.clinic.reserve', compact('data', 'query'))->with('pageName', 'reserve');
    }
    public function reservenextweek($year, $month, $day, $lastday, $week_id, $next_day_num)
    {
        if ($week_id == 1) {
            $next_day_num -= 1;
            $d = strtotime("$next_day_num  week -1 day");
            $start_week = strtotime("last sunday midnight", $d);
            $day = date("d", $start_week);
            $month = date("m", $start_week);
            $year = date("Y", $start_week);

            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = $next_day_num;
            $r_date = $year . '-' . $month;

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();
            $query = $content->getReserve($r_date, $clinic_id, $next_day_num);
        } elseif ($week_id == 2) {
            $year = date("Y");
            $month = date("n");
            $day = date("j");
            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = 0;
            $r_date = $year . '-' . $month;

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();
            $query = $content->getReserve($r_date, $clinic_id);
        } else {
            $next_day_num += 1;
            $d = strtotime("$next_day_num  week -1 day");
            $start_week = strtotime("last sunday midnight", $d);
            $day = date("d", $start_week);
            $month = date("m", $start_week);
            $year = date("Y", $start_week);
            //  echo $day;

            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['cata_view'] = 0;
            $data['week_day_num'] = $next_day_num;
            $r_date = $year . '-' . $month;

            // $query = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->Where('reserve_datetime','like', '%'.$r_date.'%')->get();
            $clinic_id = Auth::id();
            $content = new Pet();
            $query = $content->getReserve($r_date, $clinic_id);
        }


        return view('app.clinic.reserve', compact('data', 'query'))->with('pageName', 'reserve');
    }
    public function viewAccountingCompleted($diagId, $pet_id)
    {
        $data['diagId'] = $diagId;
        $query = Diagnosis::find($diagId);
        $data['pet_id'] = $pet_id;
        $systemfee = SystemFee::find(1);
        return view('app.clinic.accounting_completed', compact('data', 'query', 'systemfee'));
    }
    public function viewAccountingUpdate($diagId, $pet_id, $accountingId)
    {
        $data['diagId'] = $diagId;
        $query = Diagnosis::find($diagId);
        $data['pet_id'] = $pet_id;
        $systemfee = SystemFee::find(1);
        $accounting = Accounting::find($accountingId);
        return view('app.clinic.accounting_update', compact('data', 'query', 'accounting', 'systemfee'));
    }
    public function clinicNameInsert(Request $request)
    {
        $clinic_id = $request->get('clinic_id');
        $clinic_name = $request->get('clinic_name');
        ClinicDoctorName::create([
            'clinic_id' => $clinic_id,
            'clinic_name' => $clinic_name
        ]);
        $user = Auth::user();
        $content = new User();
        $chargeDoctor = ClinicDoctorName::where('clinic_id', $user->id)->get();
        $clinic_menus = $content->Clinic_menus($user->id);
        return view('app.clinic.setting.clinicname_completed');
        //return $this->viewProfile();// view('app.clinic.setting.profile_update', compact('user', 'clinic_menus', 'chargeDoctor'));
    }
    function postAccountingUpdate($accountingId, Request $request)
    {
        $systemfee = SystemFee::first();
        $exampleRadios = $request->input('exampleRadios');
        $accounting = Accounting::find($accountingId);
        $pet_id = $request->input('pet_id');
        $query = Pet::find($pet_id);

        $diagId = $request->input('diagID');
        $origi_filename = "";
        $save_filename = "";
        $extension = "";
        if ($request->hasFile('file_upload')) {
            $origi_filename = $request->file_upload->getClientOriginalName();
            $save_filename = rand(12, 12);
            $save_filename = $save_filename . date("YmdHis");
            $extension = $request->file_upload->getClientOriginalExtension();
            $request->file_upload->move(public_path('file/pdf'), $save_filename . '.' . $extension);
        }
        $medical_expenses = $request->input('medical-expenses');
        if ($exampleRadios == 1) {
            $fee = $request->input('fee');
            $delivery_cost = $request->input('delivery_cost');
            $sum = $request->input('sum');
        } else {
            $fee = $request->input('fee');
            $delivery_cost = 0;
            $sum = $sum = $request->input('sum');
        }

        $pet_year = $request->input('pet_year');
        $pet_month = $request->input('pet_month');
        $pet_day = $request->input('pet_day');
        $comment = $request->input('comment');
        if ($pet_year == '0' or $pet_month == '0' or $pet_day == '0') {
            $accounting->update([
                'clinic_id' => Auth::id(),
                'diagnosis_id' => $diagId,
                'origi_filename' => $origi_filename,
                'save_filename' => $save_filename,
                'extension' => $extension,
                'medical_expenses' => $medical_expenses,
                'fee' => $fee,
                'clinic_fee' => $systemfee->clinic_fee,
                'delivery_cost' => $delivery_cost,
                'sum' => $sum,
                'comment' => $comment,
                'status' => 1,
                'next_remind_date' => null
            ]);
        } else {
            $accounting->update([
                'clinic_id' => Auth::id(),
                'diagnosis_id' => $diagId,
                'origi_filename' => $origi_filename,
                'save_filename' => $save_filename,
                'extension' => $extension,
                'medical_expenses' => $medical_expenses,
                'fee' => $fee,
                'clinic_fee' => $systemfee->clinic_fee,
                'delivery_cost' => $delivery_cost,
                'sum' => $sum,
                'comment' => $comment,
                'status' => 1,
                'next_remind_date' => $pet_year . '-' . $pet_month . '-' . $pet_day
            ]);
        }

        return redirect('/app/clinic/reserve_insert');
        //return view('app.clinic.accounting_send', compact('query'));
    }
    function reserve_reply(Request $request)
    {
        $systemfee = SystemFee::first();
        $exampleRadios = $request->input('exampleRadios');

        $pet_id = $request->input('pet_id');
        $query = Pet::find($pet_id);

        $diagId = $request->input('diagID');
        $origi_filename = "";
        $save_filename = "";
        $extension = "";
        if ($request->hasFile('file_upload')) {
            $origi_filename = $request->file_upload->getClientOriginalName();
            $save_filename = rand(12, 12);
            $save_filename = $save_filename . date("YmdHis");
            $extension = $request->file_upload->getClientOriginalExtension();
            $request->file_upload->move(public_path('file/pdf'), $save_filename . '.' . $extension);
        }
        $medical_expenses = $request->input('medical-expenses');
        if ($exampleRadios == 1) {
            $fee = $request->input('fee');
            $delivery_cost = $request->input('delivery_cost');
            $sum = $request->input('sum');
        } else {
            $fee = $request->input('fee');
            $delivery_cost = 0;
            $sum = $request->input('sum');
        }

        $pet_year = $request->input('pet_year');
        $pet_month = $request->input('pet_month');
        $pet_day = $request->input('pet_day');
        $comment = $request->input('comment');
        if ($pet_year == '0' or $pet_month == '0' or $pet_day == '0') {
            Accounting::create([
                'clinic_id' => Auth::id(),
                'diagnosis_id' => $diagId,
                'origi_filename' => $origi_filename,
                'save_filename' => $save_filename,
                'extension' => $extension,
                'medical_expenses' => $medical_expenses,
                'fee' => $fee,
                'clinic_fee' => $systemfee['clinic_fee'],
                'delivery_cost' => $delivery_cost,
                'sum' => $sum,
                'comment' => $comment
            ]);
        } else {
            Accounting::create([
                'clinic_id' => Auth::id(),
                'diagnosis_id' => $diagId,
                'origi_filename' => $origi_filename,
                'save_filename' => $save_filename,
                'extension' => $extension,
                'medical_expenses' => $medical_expenses,
                'fee' => $fee,
                'clinic_fee' => $systemfee['clinic_fee'],
                'delivery_cost' => $delivery_cost,
                'sum' => $sum,
                'comment' => $comment,
                'next_remind_date' => $pet_year . '-' . $pet_month . '-' . $pet_day
            ]);
        }


        //return view('app.clinic.accounting_send', compact('query'));
        return redirect('/app/clinic/reserve_insert');
    }
    ///////////////////////////////////////////////////////////////////////

    public function sendAccountingMail($diagId, $accountingId)
    {
        $clinic = Auth::user();
        $accounting = Accounting::find($accountingId);
        $diag = Diagnosis::find($diagId);
        $user = User::find($diag->user_id);
        $query = Pet::find($diag->pet_id);
        $userinfo = User::find($diag->user_id);

        if ($user->payment_token != null) {
            Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));

            $response = Charge::create([
                'customer' => $user->payment_token,
                'amount' => $accounting->sum,
                'currency' => 'jpy',
                'description' => $accounting->comment
            ]);

            // should create a record in records table
            $record_save = Record::create(
                [
                    'user_id' => $diag->user_id,
                    'clinic_id' => $diag->clinic_id,
                    'diag_id' => $diagId,
                    'pet_id' => $diag->pet_id,
                    'price' => $accounting->sum
                ]
            );

            if ($accounting->delivery_cost == 0 && $accounting->next_remind_date == null) {
                if ($diag->status == 5) {
                    $diag->update([
                        'status' => 10
                    ]);
                } else {
                    $diag->update([
                        'status' => 4
                    ]);
                }
            }

            $accounting->update([
                'status' => 2
            ]);
            $data = [
                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                'useremail' => $userinfo->email,
                'usertel' => $userinfo->tel,
                'clinicname' => $clinic->name,
                'clinicemail' => $clinic->email,
                'clinictel' => $clinic->tel,
                'petname' => $query->name,
                'reserve_datetime' => $diag->reserve_datetime,
                'created_at' => $diag->created_at,
                'diagId' => $diag->id,
                'start_datetime' => $diag->start_datetime,
                'end_datetime' => $diag->end_datetime,
                'flag' => '1'
            ];

            Mail::to($userinfo->email)->send(new \App\Mail\ReserveReplyMail($data));
            $data1 = [
                'username' => $userinfo->last_name . " " . $userinfo->first_name,
                'useremail' => $userinfo->email,
                'usertel' => $userinfo->tel,
                'clinicname' => $clinic->name,
                'clinicemail' => $clinic->email,
                'clinictel' => $clinic->tel,
                'petname' => $query->name,
                'reserve_datetime' => $diag->reserve_datetime,
                'diagId' => $diag->id,
                'start_datetime' => $diag->start_datetime,
                'end_datetime' => $diag->end_datetime,
                'flag' => '2',
                'created_at' => $diag->created_at
            ];
            //Mail::to($clinic->email)->send(new \App\Mail\ReserveReplyMail($data1));

            return view('app.clinic.accounting_send', compact('query', 'user'));
        } else {
            return redirect('/app/clinic/error');
        }
    }

    public function viewError()
    {
        return view('app.clinic.error');
    }

    public function Clinicname_Del($id)
    {
        $query = ClinicDoctorName::find($id);
        $query->delete();
        return redirect()->back();
    }
    public function viewParcelCompleted($diagId)
    {
        $diag = Diagnosis::find($diagId);
        $diag->update([
            'status' => 2
        ]);
        $clinic = Auth::user();

        $user = User::find($diag->user_id);

        $pet = Pet::find($diag->pet_id);

        $data = [
            'username' => $user->last_name . " " . $user->first_name,
            'useremail' => $user->email,
            'usertel' => $user->tel,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'petname' => $pet->name,
            'reserve_datetime' => $diag->reserve_datetime,
            'created_at' => $diag->created_at,
            'diagId' => $diag->id,
            'start_datetime' => $diag->start_datetime,
            'end_datetime' => $diag->end_datetime,
        ];

        Mail::to($user->email)->send(new \App\Mail\ParcelMail($data));
        return view('app.clinic.parcel_completed', compact('pet'));
    }

    public function viewRemindCompleted($diagId)
    {
        $diag = Diagnosis::find($diagId);
        $diag->update([
            'status' => 3
        ]);
        $clinic = Auth::user();

        $user = User::find($diag->user_id);
        $pet = Pet::find($diag->pet_id);
        $accounting = Accounting::where('diagnosis_id', $diagId)->where('clinic_id', $clinic->id)->first();
        $data = [
            'daigId' => $diagId,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'reserve_datetime' => $diag->reserve_datetime,
            'created_at' => $diag->created_at,
            'start_datetime' => $diag->start_datetime,
            'end_datetime' => $diag->end_datetime,
            'petname' => $pet->name,
            'username' => $user->last_name . " " . $user->first_name,
            'remind_date' => $accounting->next_remind_date
        ];



        Mail::to($user->email)->send(new \App\Mail\RemindMail($data));
        return view('app.clinic.remind_completed', compact('pet', 'user'));
    }

    public function BillSend($id)
    {
        $diag = Diagnosis::find($id);
        $user = User::find($diag->user_id);
        $clinic = Auth::user();
        $pet = Pet::find($diag->pet_id);
        $fee = SystemFee::find(1);
        $price = ($diag->price * $fee->owner_fee) / 100;
        $reservetimetable = ReserveTimetable::where('diag_id', $id)->first();
        if (!empty($reservetimetable)) {
            $reservetimetable->delete();
        }
        Accounting::create([
            'clinic_id' => Auth::id(),
            'diagnosis_id' => $id,
            'medical_expenses' => $diag->price,
            'fee' => $fee->owner_fee,
            'clinic_fee' => $fee->clinic_fee,
            'sum' => $diag->price + $fee->owner_fee,
        ]);
        $data = [
            'daigId' => $id,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'reserve_datetime' => $diag->reserve_datetime,
            'start_datetime' => $diag->start_datetime,
            'end_datetime' => $diag->end_datetime,
            'created_at' => $diag->created_at,
            'petname' => $pet->name,
            'created_at' => $diag->created_at,
            'username' => $user->last_name . " " . $user->first_name
        ];
        Mail::to($user->email)->send(new \App\Mail\BillMail($data));
        $diag->update([
            'status' => 5
        ]);
        return redirect('/app/clinic/reserve_insert');
    }

    public function DeleteDiag($id, Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $diag = Diagnosis::find($id);
        $clinic = Clinic::find($diag->clinic_id);
        $pet = Pet::find($diag->pet_id);
        $user = User::find($diag->user_id);
        $reservetimetable = ReserveTimetable::where('diag_id', $id)->first();
        if (!empty($reservetimetable)) {
            $reservetimetable->delete();
        }

        $data = [
            'username' => $user->last_name . " " . $user->first_name,
            'useremail' => $user->email,
            'usertel' => $user->tel,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'petname' => $pet->name,
            'reserve_datetime' => $diag['reserve_datetime'],
            'created_at' => $diag->created_at,
            'diagId' => $diag->id,
            'flag' => '3'
        ];
        // For User
        Mail::to($user->email)->send(new \App\Mail\OwnerReserveMail($data));
        $diag->update([
            'status' => 6
        ]);

        $mode = $request->input('mode', 'exam');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', Auth::id())->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                    $remind_count++;
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' =>  $bill_count, //count($finishedDiags),
            'shipping' => count($diags),
            'remind' => $remind_count,
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }

        return view('app.clinic.application', compact('diags', 'finishedDiags', 'nextDiags', 'mode', 'statusCount'));
    }

    public function viewRemindUpdated($accountingId)
    {
        $accounting = Accounting::find($accountingId);
        $accounting->update([
            'next_remind_date' => null
        ]);
        return redirect('/app/clinic/reserve_remind');
    }

    public function viewOwnerInfo($userId, $diagId)
    {
        $user = User::find($userId);
        $diag = Diagnosis::find($diagId);
        $pet = Pet::where('user_id', $user->id)->get();
        // $cats = Pet::where('user_id', $user->id)->where('type', 2)->get();
        // $others = Pet::where('user_id', $user->id)->where('type', 3)->get();
        return view('app.clinic.owner_info', compact('user', 'diag', 'pet'));
    }
    public function ownerPetNoUpdate(Request $request)
    {
        $diagid = $request->input('diagid');
        $diag = Diagnosis::find($diagid);
        $owner_catano = $request->input('owner_catano');
        $pet_no = $request->input('pet_no');
        $petid = $request->input('petid');

        $owner = OwnerCatano::where('user_id', $diag->user_id)->where('clinic_id', $diag->clinic_id)->first();
        if (!empty($owner)) {
            $db = new OwnerCatano();
            $db->ownerCatanoUdate($diag->user_id, $owner_catano, $diagid, $diag->pet_id, $diag->clinic_menu_id, $diag->clinic_id);
            // echo "update";
        } else {
            $db = new OwnerCatano();
            $db->ownerCatanoInsert($diag->user_id, $owner_catano, $diagid, $diag->pet_id, $diag->clinic_menu_id, $diag->clinic_id);
            // echo "insert";
        }
        for ($i = 0; $i < count($petid); $i++) {
            $pets = Pet::find($petid[$i]);
            $petno = PetCatano::where('pet_id', $pets->id)->where('clinic_id', $diag->clinic_id)->first();
            if (!empty($petno)) {
                $db = new PetCatano();
                $db->petCatanoUpdate($pets->id, $pets->user_id, $pet_no[$i], $diag->clinic_id);
                // echo "update";
            } else {
                $db = new PetCatano();
                $db->petCatanoInsert($pets->id, $pets->user_id, $pet_no[$i], $diag->clinic_id);
                // echo "insert";
            }
        }
        $user = User::find($diag->user_id);
        $pet = Pet::where('user_id', $diag->user_id)->get();
        // $cats = Pet::where('user_id', $user->id)->where('type', 2)->get();
        // $others = Pet::where('user_id', $user->id)->where('type', 3)->get();
        return view('app.clinic.owner_info', compact('user', 'diag', 'pet'));
    }
    public function viewPetInfo($petId, $diagId)
    {
        $pet = Pet::find($petId);
        $diag = Diagnosis::where('id', $diagId)->first(); //where('status', '!=', 6)->where('status', '!=', 5)->first();
        $cur_dt = date("Y");
        $year = explode("-", $pet->birthday);
        $age = $cur_dt - $year[0];
        if (!empty($diag)) {
            $datetime = explode(" ", $diag->reserve_datetime);
            $date = $datetime[0];
            $time = $datetime[1];
        } else {
            $datetime = "";
            $date = "";
            $time = "";
        }
        return view('app.clinic.pet_info', compact('pet', 'diag', 'age', 'date', 'time', 'diagId'));
    }

    public function viewPurpose($diagId)
    {
        $diag = Diagnosis::find($diagId);
        $diag_files = DiagFile::where('diag_id', $diagId)->get();
        return view('app.clinic.purpose', compact('diag', 'diag_files'));
    }

    public function viewReserveDetail($petId, $diagId)
    {
        $diag = Diagnosis::find($diagId);
        $diagfile = DiagFile::where("diag_id", $diagId)->get();
        $accounting = Accounting::where("diagnosis_id", $diagId)->where("clinic_id", Auth::id())->first();
        $pet = Pet::find($petId);
        $cur_dt = date("Y");
        $year = explode("-", $pet->birthday);
        $age = $cur_dt - $year[0];
        // $datetime = explode(" ", $diag->end_datetime);
        // $date = $datetime[0];
        // $time = $datetime[1];
        return view('app.clinic.reserve_detail', compact('pet', 'diag', 'age', 'date', 'time', 'accounting', 'diagfile'));
    }

    public function viewSearch()
    {
        return view('app.clinic.search');
    }

    // public function viewSearchResult()
    // {
    //     $diags = Diagnosis::all();

    //     return view('app.clinic.search_result', compact('diags'));
    // }

    ////////////////// Superman: Owner Pet Search  ///////////////////////
    public function viewSearchResult(Request $request)
    {
        $pet_diagId = $request->input('pet_diagId');
        $petId = $request->input('petId');
        $pet_name = $request->input('pet_name');
        $pet_type = $request->input('pet_type');
        $pet_other_type = $request->input('pet_other_type');
        $pet_age = $request->input('pet_age');
        $user_diagId = $request->input('user_diagId');
        $user_name = $request->input('user_name');
        $user_name_kana = $request->input('user_name_kana');
        $tel = $request->input('tel');
        $prefecture = $request->input('prefecture');
        $city = $request->input('city');
        $clinicid = Auth::id();
        $owner_catas = OwnerCatano::where('cata_no', $pet_diagId)->where('clinic_id', $clinicid)->first();
        $pet_catas = PetCatano::where('cata_no', $petId)->where('clinic_id', $clinicid)->first();
        if (empty($owner_catas)) $owner_cata_flag = 0;
        else $owner_cata_flag = 1;
        if (empty($pet_catas)) $pet_cata_flag = 0;
        else $pet_cata_flag = 1;
        $content = new Pet();

        $query = $content->getSearch($pet_diagId, $petId, $pet_name, $pet_type, $pet_other_type, $pet_age, $user_diagId, $user_name, $user_name_kana, $tel, $prefecture, $city, $owner_cata_flag, $pet_cata_flag, $clinicid);

        return view('app.clinic.search_result', compact('query'));
    }

    ////////////////////////////////////////////////////////////////////////
    public function postSearch(Request $request)
    {

        $selectedPref = $request->input('prefecture');
        $inputCity = $request->input('city');
        $pet_diagId = $request->input('pet_diagId');
        $pet_type = $request->input('pet_type');
        $petId = $request->input('petId');
        $pet_name = $request->input('pet_name');
        $pet_other_type = $request->input('pet_other_type');
        $pet_age = $request->input('pet_age');
        $user_diagId = $request->input('user_diagId');
        $user_name = $request->input('user_name');
        $user_name_kana = $request->input('user_name_kana');
        $tel = $request->input('tel');

        $diags = Diagnosis::all();

        if (!empty($pet_diagId)) {
            $diags = $diags->where('id', 'like', "%{$pet_diagId}%");
        }
        if (!empty($user_diagId)) {
            $diags = $diags->where('id', 'like', "%{$user_diagId}%");
        }

        return view('app.clinic.search_result', compact('diags'));
    }
    public function viewContact()
    {
        return view('app.clinic.contactus');
    }
    public function reserveDelete($id, Request $request)
    {
        $d = strtotime("+1 day");
        $curr_datetime = date("Y-m-d", $d);
        $diag = Diagnosis::where('id', $id)->first();
        if (empty($diag)) {
            return view('app.clinic.application', compact('diags', 'finishedDiags', 'nextDiags', 'mode', 'statusCount'));
        }
        $diag->delete();

        $mode = $request->input('mode', 'exam');

        $diags = Diagnosis::where('clinic_id', Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 1)->orwhere('status', 5)->get();
        $bill_count = 0;
        foreach ($finishedDiags as $item) {
            if ($item->status == 1 or $item->status == 5) {
                $accounting = Accounting::where('diagnosis_id', $item->id)->first();
                if (((!empty($accounting) and $accounting->status == 0) or (!empty($accounting)  and $accounting->status == 1)) || empty($accounting)) {
                    $bill_count++;
                }
            }
        }
        $nextDiags = Diagnosis::where('clinic_id', Auth::id())->get(); //where('status', 3)->orwhere('status', 0)->orwhere('status', 1)->orwhere('status', 5)->get();
        $remind_count = 0;
        foreach ($nextDiags as $item) {
            if ($item->status == 0 or $item->status == 1 or $item->status == 3 or $item->status == 5) {
                $accounting = Accounting::where('clinic_id', Auth::id())->where('diagnosis_id', $item->id)->first();
                if (!empty($accounting) && $accounting->next_remind_date != null && $accounting->next_remind_date <= $curr_datetime) {
                    $remind_count++;
                }
            }
        }
        $statusCount = [
            'exam' => count($diags),
            'bill' =>  $bill_count, //count($finishedDiags),
            'shipping' => count($diags),
            'remind' => $remind_count //count($nextDiags),
        ];

        switch ($mode) {
            case 'exam':
                break;
            case 'bill':
                break;
            case 'shipping':
                break;
            case 'remind':
                break;
        }

        return view('app.clinic.application', compact('diags', 'finishedDiags', 'nextDiags', 'mode', 'statusCount'));
    }
    ////////////////// superman reserve update //////////////////
    function oldclinicMenuUpdate(Request $request, $id)
    {
        $menu_description = $request->input('description');
        $menu_title = $request->input('first_title');
        $menu_toke_time = $request->input('toke_time');
        $menu_toke_price = $request->input('toke_price');
        $menu_insurance = $request->input('Insurance');
        if (count($menu_toke_price) == 1) {
            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => $menu_toke_time[0], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[0], //$request->input('toke_price')
                        ],
                    ]
                ),
            ]);
        } elseif (count($menu_toke_price) == 2) {
            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => $menu_toke_time[0], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[0], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[1], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[1], //$request->input('toke_price')
                        ],
                    ]
                ),
            ]);
        } elseif (count($menu_toke_price) == 3) {
            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => $menu_toke_time[0], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[0], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[1], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[1], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[2], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[2], //$request->input('toke_price')
                        ],
                    ]
                ),
            ]);
        } elseif (count($menu_toke_price) == 4) {
            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => $menu_toke_time[0], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[0], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[1], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[1], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[2], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[2], //$request->input('toke_price')
                        ],
                        [
                            'title' => $menu_toke_time[3], //$request->input('toke_time').'分相談',
                            'price' => $menu_toke_price[3], //$request->input('toke_price')
                        ],
                    ]
                ),
            ]);
        }

        $content = new Clinic();
        $content->Clinic_Menu_Udate($priceData, $menu_title, $menu_description, $menu_insurance, $id); //,$clinic_menu_description,$clinic_menu_title);
        return view('app.clinic.setting.clinic_menu_insert');
    }
    function newclinicMenuUpdate(Request $request, $id)
    {
        $menu_description = $request->input('description');
        $menu_title = $request->input('first_title');
        $menu_toke_time = $request->input('toke_time');
        $menu_toke_price = $request->input('toke_price');
        $menu_insurance = $request->input('Insurance');
        $priceData = ([
            'price_list' => json_encode(
                [
                    [
                        'title' => $menu_toke_time, //$request->input('toke_time').'分相談',
                        'price' => $menu_toke_price, //$request->input('toke_price')
                    ],
                    // [
                    //     'title' => '相談（セカンドオピニオン）',
                    //     'price' => $request->input('s_second')
                    // ],
                    // [
                    //     'title' => '相談（再診）',
                    //     'price' => $request->input('s_remind')
                    // ],
                    // [
                    //     'title' => 'その他 ',
                    //     'price' => $request->input('s_other')
                    // ]
                ]
            ),
        ]);
        $content = new Clinic();
        $content->Clinic_Menu_Udate1($priceData, $menu_title, $menu_description, $menu_insurance, $id); //,$clinic_menu_description,$clinic_menu_title);
        return view('app.clinic.setting.clinic_menu_insert');
    }
    public function clinicMenuEdit(Request $request)
    {
        $menu_description = $request->input('description');
        $menu_title = $request->input('first_title');
        $menu_toke_time = $request->input('toke_time');
        $menu_toke_price = $request->input('toke_price');
        $menu_insurance = $request->input('Insurance');
        $priceData = ([
            'price_list' => json_encode(
                [
                    [
                        'title' => $menu_toke_time, //$request->input('toke_time').'分相談',
                        'price' => $menu_toke_price, //$request->input('toke_price')
                    ],
                    // [
                    //     'title' => '相談（セカンドオピニオン）',
                    //     'price' => $request->input('s_second')
                    // ],
                    // [
                    //     'title' => '相談（再診）',
                    //     'price' => $request->input('s_remind')
                    // ],
                    // [
                    //     'title' => 'その他 ',
                    //     'price' => $request->input('s_other')
                    // ]
                ]
            ),
        ]);
        $user = Auth::user();
        $Clinic_query = Clinic::find($user->id);
        $content = new Clinic();
        $content->Clinic_Menu_Insert($menu_description, $menu_title, $priceData, $menu_insurance, $user->id, $Clinic_query->menu_time_option); //,$clinic_menu_description,$clinic_menu_title);
        return view('app.clinic.setting.clinic_menu_insert');
    }
    /*public function reserveEdit(Request $request)
    {
        // $request->validate([
        //     's_first' => ['required', 'numeric', 'digits_between:2,11'],
        //     's_second' => ['required', 'numeric', 'digits_between:2,11'],
        //     's_remind' => ['required', 'numeric', 'digits_between:2,11'],
        //     's_other' => ['required', 'numeric', 'digits_between:2,11'],
        // ]);
        $week = $request->input('contact_method');
        $week_len = count($week);
        if ($request->input('time_option') != 100) {
            $options = $request->input('time_option');
            $start1[0] = $request->input('week_time_1_1_1');
            $end1[0] = $request->input('week_time_1_1_2');

            $start1[1] = $request->input('week_time_2_1_1');
            $end1[1] = $request->input('week_time_2_1_2');

            $start1[2] = $request->input('week_time_3_1_1');
            $end1[2] = $request->input('week_time_3_1_2');

            $start1[3] = $request->input('week_time_4_1_1');
            $end1[3] = $request->input('week_time_4_1_2');

            $start1[4] = $request->input('week_time_5_1_1');
            $end1[4] = $request->input('week_time_5_1_2');

            $start1[5] = $request->input('week_time_6_1_1');
            $end1[5] = $request->input('week_time_6_1_2');

            $start1[6] = $request->input('week_time_7_1_1');
            $end1[6] = $request->input('week_time_7_1_2');

            $start2[0] = $request->input('p_week_time_1_1_1');
            $end2[0] = $request->input('p_week_time_1_1_2');

            $start2[1] = $request->input('p_week_time_2_1_1');
            $end2[1] = $request->input('p_week_time_2_1_2');

            $start2[2] = $request->input('p_week_time_3_1_1');
            $end2[2] = $request->input('p_week_time_3_1_2');

            $start2[3] = $request->input('p_week_time_4_1_1');
            $end2[3] = $request->input('p_week_time_4_1_2');

            $start2[4] = $request->input('p_week_time_5_1_1');
            $end2[4] = $request->input('p_week_time_5_1_2');

            $start2[5] = $request->input('p_week_time_6_1_1');
            $end2[5] = $request->input('p_week_time_6_1_2');

            $start2[6] = $request->input('p_week_time_7_1_1');
            $end2[6] = $request->input('p_week_time_7_1_2');
        } else {
            $options = 1;
            $start1[0] = $request->input('week_time_1_2_1');
            $end1[0] = $request->input('week_time_1_2_2');

            $start1[1] = $request->input('week_time_2_2_1');
            $end1[1] = $request->input('week_time_2_2_2');

            $start1[2] = $request->input('week_time_3_2_1');
            $end1[2] = $request->input('week_time_3_2_2');

            $start1[3] = $request->input('week_time_4_2_1');
            $end1[3] = $request->input('week_time_4_2_2');

            $start1[4] = $request->input('week_time_5_2_1');
            $end1[4] = $request->input('week_time_5_2_2');

            $start1[5] = $request->input('week_time_6_2_1');
            $end1[5] = $request->input('week_time_6_2_2');

            $start1[6] = $request->input('week_time_7_2_1');
            $end1[6] = $request->input('week_time_7_2_2');

            $start2[0] = $request->input('p_week_time_1_2_1');
            $end2[0] = $request->input('p_week_time_1_2_2');

            $start2[1] = $request->input('p_week_time_2_2_1');
            $end2[1] = $request->input('p_week_time_2_2_2');

            $start2[2] = $request->input('p_week_time_3_2_1');
            $end2[2] = $request->input('p_week_time_3_2_2');

            $start2[3] = $request->input('p_week_time_4_2_1');
            $end2[3] = $request->input('p_week_time_4_2_2');

            $start2[4] = $request->input('p_week_time_5_2_1');
            $end2[4] = $request->input('p_week_time_5_2_2');

            $start2[5] = $request->input('p_week_time_6_2_1');
            $end2[5] = $request->input('p_week_time_6_2_2');

            $start2[6] = $request->input('p_week_time_7_2_1');
            $end2[6] = $request->input('p_week_time_7_2_2');
        }
        if ($week_len == 1) {
            // $reserve_time1_1 = ([
            //     ['start' => '9:00', 'end' => '12:00'],
            //     ['start' => '13:00', 'end' => '18:00']
            // ]);
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 2) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 3) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            $week[2] => [
                                ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 4) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            $week[2] => [
                                ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                            ],
                            $week[3] => [
                                ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 5) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            $week[2] => [
                                ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                            ],
                            $week[3] => [
                                ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                            ],
                            $week[4] => [
                                ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 6) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            $week[2] => [
                                ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                            ],
                            $week[3] => [
                                ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                            ],
                            $week[4] => [
                                ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                            ],
                            $week[5] => [
                                ['start' => $start1[$week[5] - 1], 'end' => $end1[$week[5] - 1]],
                                ['start' => $start2[$week[5] - 1], 'end' => $end2[$week[5] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        } elseif ($week_len == 7) {
            $data = ([
                "clinic_info" => json_encode(
                    [
                        'owner' => '浅沼',
                        'businessHour' => [
                            $week[0] => [
                                ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                            ],
                            $week[1] => [
                                ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                            ],
                            $week[2] => [
                                ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                            ],
                            $week[3] => [
                                ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                            ],
                            $week[4] => [
                                ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                            ],
                            $week[5] => [
                                ['start' => $start1[$week[5] - 1], 'end' => $end1[$week[5] - 1]],
                                ['start' => $start2[$week[5] - 1], 'end' => $end2[$week[5] - 1]]
                            ],
                            $week[6] => [
                                ['start' => $start1[$week[6] - 1], 'end' => $end1[$week[6] - 1]],
                                ['start' => $start2[$week[6] - 1], 'end' => $end2[$week[6] - 1]]
                            ],
                            'holiday' => null,
                        ],
                        'description' => ['sample'], // [$request->input('description')],
                        'name' => ['sample'], //[$request->input('first_title')],
                        'insurance' => ['sample'], //[$request->input('Insurance')],
                        'eyecatchImages' => ['sample'] //['header_3.jpg']
                    ]
                ),
            ]);
        }
        if ($week_len == 0) {
            echo "<script>alert('曜日をお選びください。');history.go(-1)</script>";
        } else {
            if ($options == 0) $a = 0;
            elseif ($options == 1) $a = 1;
            elseif ($options == 2) $a = 2;
            elseif ($options == 3) $a = 3;
            elseif ($options == 4) $a = 4;
            elseif ($options == 5) $a = 5;

            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => 'sample', //$request->input('toke_time').'分相談',
                            'price' => 'sample', //$request->input('toke_price')
                        ],
                        // [
                        //     'title' => '相談（セカンドオピニオン）',
                        //     'price' => $request->input('s_second')
                        // ],
                        // [
                        //     'title' => '相談（再診）',
                        //     'price' => $request->input('s_remind')
                        // ],
                        // [
                        //     'title' => 'その他 ',
                        //     'price' => $request->input('s_other')
                        // ]
                    ]
                ),
                'option' => json_encode([
                    'timeSlotType' => $a
                ]),
            ]);
            // $clinic_menu_description=$request->input('description');
            // $clinic_menu_title=$request->input('first_title');
            $user = Auth::user();
            $content = new Clinic();
            // $content_menu = new User();
            // $clinic_menus = $content_menu->Clinic_menus($user->id);
            // if(count($clinic_menus)>0){
            // $content->reserve_update($data,$priceData,$user->id);//,$clinic_menu_description,$clinic_menu_title);
            // }else{
            $content->reserve_insert($data, $priceData, $user->id); //,$clinic_menu_description,$clinic_menu_title);
            // }
            return view('app.clinic.setting.reserve_update_completed');
        }
    }*/
    public function reserveEdit(Request $request)
    {
        $week = $request->input('contact_method');
        if (!empty($week)) {
            $week_len = count($week);
            $time_option = $request->input('time_option');
            $monday_chk = $request->input('monday_chk');
            $tuesday_chk = $request->input('tuesday_chk');
            $wednesday_chk = $request->input('wednesday_chk');
            $thursday_chk = $request->input('thursday_chk');
            $friday_chk = $request->input('friday_chk');
            $saturday_chk = $request->input('saturday_chk');
            $sunday_chk = $request->input('sunday_chk');
            $businessHour[0] = "";
            $businessHour[1] = "";
            $businessHour[2] = "";
            $businessHour[3] = "";
            $businessHour[4] = "";
            $businessHour[5] = "";
            $businessHour[6] = "";
            if ($time_option == 4) {
                if (!empty($monday_chk)) {
                    for ($i = 0; $i < count($monday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($monday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($monday_chk) - 1)
                            $businessHour[1] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[1] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($tuesday_chk)) {
                    for ($i = 0; $i < count($tuesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($tuesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($tuesday_chk) - 1)
                            $businessHour[2] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[2] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($wednesday_chk)) {
                    for ($i = 0; $i < count($wednesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($wednesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($wednesday_chk) - 1)
                            $businessHour[3] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[3] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($thursday_chk)) {
                    for ($i = 0; $i < count($thursday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($thursday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($thursday_chk) - 1)
                            $businessHour[4] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[4] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($friday_chk)) {
                    for ($i = 0; $i < count($friday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($friday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($friday_chk) - 1)

                            $businessHour[5] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[5] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($saturday_chk)) {
                    for ($i = 0; $i < count($saturday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($saturday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($saturday_chk) - 1)

                            $businessHour[6] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[6] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($sunday_chk)) {
                    for ($i = 0; $i < count($sunday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($sunday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($sunday_chk) - 1)

                            $businessHour[0] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[0] .= $hour_array[$i]['time_info'];
                    }
                }

                $clinic_info = "{\"businessHour\":{";
                for ($i = 0; $i < count($week); $i++) {
                    $clinic_info .= "\"$week[$i]\":[" . $businessHour[$week[$i]] . "],";
                }
                $clinic_info .= "\"holiday\":null}}";
            }
            if ($time_option == 0) {
                if (!empty($monday_chk)) {
                    for ($i = 0; $i < count($monday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($monday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($monday_chk) - 1)
                            $businessHour[1] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[1] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($tuesday_chk)) {
                    for ($i = 0; $i < count($tuesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($tuesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($tuesday_chk) - 1)
                            $businessHour[2] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[2] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($wednesday_chk)) {
                    for ($i = 0; $i < count($wednesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($wednesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($wednesday_chk) - 1)
                            $businessHour[3] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[3] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($thursday_chk)) {
                    for ($i = 0; $i < count($thursday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($thursday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($thursday_chk) - 1)
                            $businessHour[4] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[4] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($friday_chk)) {
                    for ($i = 0; $i < count($friday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($friday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($friday_chk) - 1)

                            $businessHour[5] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[5] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($saturday_chk)) {
                    for ($i = 0; $i < count($saturday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($saturday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($saturday_chk) - 1)

                            $businessHour[6] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[6] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($sunday_chk)) {
                    for ($i = 0; $i < count($sunday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($sunday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($sunday_chk) - 1)

                            $businessHour[0] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[0] .= $hour_array[$i]['time_info'];
                    }
                }

                $clinic_info = "{\"businessHour\":{";
                for ($i = 0; $i < count($week); $i++) {
                    $clinic_info .= "\"$week[$i]\":[" . $businessHour[$week[$i]] . "],";
                }
                $clinic_info .= "\"holiday\":null}}";
            }
            if ($time_option == 1) {
                if (!empty($monday_chk)) {
                    for ($i = 0; $i < count($monday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($monday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($monday_chk) - 1)
                            $businessHour[1] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[1] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($tuesday_chk)) {
                    for ($i = 0; $i < count($tuesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($tuesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($tuesday_chk) - 1)
                            $businessHour[2] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[2] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($wednesday_chk)) {
                    for ($i = 0; $i < count($wednesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($wednesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($wednesday_chk) - 1)
                            $businessHour[3] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[3] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($thursday_chk)) {
                    for ($i = 0; $i < count($thursday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($thursday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($thursday_chk) - 1)
                            $businessHour[4] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[4] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($friday_chk)) {
                    for ($i = 0; $i < count($friday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($friday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($friday_chk) - 1)

                            $businessHour[5] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[5] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($saturday_chk)) {
                    for ($i = 0; $i < count($saturday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($saturday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($saturday_chk) - 1)

                            $businessHour[6] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[6] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($sunday_chk)) {
                    for ($i = 0; $i < count($sunday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($sunday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($sunday_chk) - 1)

                            $businessHour[0] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[0] .= $hour_array[$i]['time_info'];
                    }
                }

                $clinic_info = "{\"businessHour\":{";
                for ($i = 0; $i < count($week); $i++) {
                    $clinic_info .= "\"$week[$i]\":[" . $businessHour[$week[$i]] . "],";
                }
                $clinic_info .= "\"holiday\":null}}";
            }
            if ($time_option == 2) {
                if (!empty($monday_chk)) {
                    for ($i = 0; $i < count($monday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($monday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($monday_chk) - 1)
                            $businessHour[1] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[1] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($tuesday_chk)) {
                    for ($i = 0; $i < count($tuesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($tuesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($tuesday_chk) - 1)
                            $businessHour[2] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[2] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($wednesday_chk)) {
                    for ($i = 0; $i < count($wednesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($wednesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($wednesday_chk) - 1)
                            $businessHour[3] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[3] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($thursday_chk)) {
                    for ($i = 0; $i < count($thursday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($thursday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($thursday_chk) - 1)
                            $businessHour[4] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[4] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($friday_chk)) {
                    for ($i = 0; $i < count($friday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($friday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($friday_chk) - 1)

                            $businessHour[5] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[5] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($saturday_chk)) {
                    for ($i = 0; $i < count($saturday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($saturday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($saturday_chk) - 1)

                            $businessHour[6] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[6] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($sunday_chk)) {
                    for ($i = 0; $i < count($sunday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($sunday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($sunday_chk) - 1)

                            $businessHour[0] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[0] .= $hour_array[$i]['time_info'];
                    }
                }

                $clinic_info = "{\"businessHour\":{";
                for ($i = 0; $i < count($week); $i++) {
                    $clinic_info .= "\"$week[$i]\":[" . $businessHour[$week[$i]] . "],";
                }
                $clinic_info .= "\"holiday\":null}}";
            }
            if ($time_option == 3) {
                if (!empty($monday_chk)) {
                    for ($i = 0; $i < count($monday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($monday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($monday_chk) - 1)
                            $businessHour[1] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[1] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($tuesday_chk)) {
                    for ($i = 0; $i < count($tuesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($tuesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($tuesday_chk) - 1)
                            $businessHour[2] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[2] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($wednesday_chk)) {
                    for ($i = 0; $i < count($wednesday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($wednesday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($wednesday_chk) - 1)
                            $businessHour[3] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[3] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($thursday_chk)) {
                    for ($i = 0; $i < count($thursday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($thursday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($thursday_chk) - 1)
                            $businessHour[4] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[4] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($friday_chk)) {
                    for ($i = 0; $i < count($friday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($friday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($friday_chk) - 1)

                            $businessHour[5] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[5] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($saturday_chk)) {
                    for ($i = 0; $i < count($saturday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($saturday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($saturday_chk) - 1)

                            $businessHour[6] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[6] .= $hour_array[$i]['time_info'];
                    }
                }
                if (!empty($sunday_chk)) {
                    for ($i = 0; $i < count($sunday_chk); $i++) {
                        $start_mo[$i] = Carbon::parse($sunday_chk[$i])->format('H:i');
                        $t = substr($start_mo[$i], 3, 2);
                        $t = $t + 1;
                        $end_mo[$i] = Carbon::parse(substr($start_mo[$i], 0, 3) . $t)->format('H:i');
                        $hour_array[$i] = ([
                            "time_info" => json_encode(
                                ['start' => $start_mo[$i], 'end' => $end_mo[$i]]
                            )
                        ]);
                        if ($i < count($sunday_chk) - 1)

                            $businessHour[0] .= $hour_array[$i]['time_info'] . ',';
                        else
                            $businessHour[0] .= $hour_array[$i]['time_info'];
                    }
                }

                $clinic_info = "{\"businessHour\":{";
                for ($i = 0; $i < count($week); $i++) {
                    $clinic_info .= "\"$week[$i]\":[" . $businessHour[$week[$i]] . "],";
                }
                $clinic_info .= "\"holiday\":null}}";
            }
            if ($time_option == 1) $a = 1;
            elseif ($time_option == 2) $a = 2;
            elseif ($time_option == 3) $a = 3;
            elseif ($time_option == 4) $a = 4;
            elseif ($time_option == 0) $a = 0;

            $priceData = ([
                'price_list' => json_encode(
                    [
                        [
                            'title' => 'sample', //$request->input('toke_time').'分相談',
                            'price' => 'sample', //$request->input('toke_price')
                        ],
                    ]
                ),
                'option' => json_encode([
                    'timeSlotType' => $a
                ]),
            ]);
            $user = Auth::user();
            $content = new Clinic();
            $content->reserve_insert($clinic_info, $priceData, $user->id);
            return view('app.clinic.setting.reserve_update_completed');
            /*if ($request->input('time_option') != 100) {
                $options = $request->input('time_option');
                $start1[0] = $request->input('week_time_1_1_1');
                $end1[0] = $request->input('week_time_1_1_2');

                $start1[1] = $request->input('week_time_2_1_1');
                $end1[1] = $request->input('week_time_2_1_2');

                $start1[2] = $request->input('week_time_3_1_1');
                $end1[2] = $request->input('week_time_3_1_2');

                $start1[3] = $request->input('week_time_4_1_1');
                $end1[3] = $request->input('week_time_4_1_2');

                $start1[4] = $request->input('week_time_5_1_1');
                $end1[4] = $request->input('week_time_5_1_2');

                $start1[5] = $request->input('week_time_6_1_1');
                $end1[5] = $request->input('week_time_6_1_2');

                $start1[6] = $request->input('week_time_7_1_1');
                $end1[6] = $request->input('week_time_7_1_2');

                $start2[0] = $request->input('p_week_time_1_1_1');
                $end2[0] = $request->input('p_week_time_1_1_2');

                $start2[1] = $request->input('p_week_time_2_1_1');
                $end2[1] = $request->input('p_week_time_2_1_2');

                $start2[2] = $request->input('p_week_time_3_1_1');
                $end2[2] = $request->input('p_week_time_3_1_2');

                $start2[3] = $request->input('p_week_time_4_1_1');
                $end2[3] = $request->input('p_week_time_4_1_2');

                $start2[4] = $request->input('p_week_time_5_1_1');
                $end2[4] = $request->input('p_week_time_5_1_2');

                $start2[5] = $request->input('p_week_time_6_1_1');
                $end2[5] = $request->input('p_week_time_6_1_2');

                $start2[6] = $request->input('p_week_time_7_1_1');
                $end2[6] = $request->input('p_week_time_7_1_2');
            } else {
                $options = 1;
                $start1[0] = $request->input('week_time_1_2_1');
                $end1[0] = $request->input('week_time_1_2_2');

                $start1[1] = $request->input('week_time_2_2_1');
                $end1[1] = $request->input('week_time_2_2_2');

                $start1[2] = $request->input('week_time_3_2_1');
                $end1[2] = $request->input('week_time_3_2_2');

                $start1[3] = $request->input('week_time_4_2_1');
                $end1[3] = $request->input('week_time_4_2_2');

                $start1[4] = $request->input('week_time_5_2_1');
                $end1[4] = $request->input('week_time_5_2_2');

                $start1[5] = $request->input('week_time_6_2_1');
                $end1[5] = $request->input('week_time_6_2_2');

                $start1[6] = $request->input('week_time_7_2_1');
                $end1[6] = $request->input('week_time_7_2_2');

                $start2[0] = $request->input('p_week_time_1_2_1');
                $end2[0] = $request->input('p_week_time_1_2_2');

                $start2[1] = $request->input('p_week_time_2_2_1');
                $end2[1] = $request->input('p_week_time_2_2_2');

                $start2[2] = $request->input('p_week_time_3_2_1');
                $end2[2] = $request->input('p_week_time_3_2_2');

                $start2[3] = $request->input('p_week_time_4_2_1');
                $end2[3] = $request->input('p_week_time_4_2_2');

                $start2[4] = $request->input('p_week_time_5_2_1');
                $end2[4] = $request->input('p_week_time_5_2_2');

                $start2[5] = $request->input('p_week_time_6_2_1');
                $end2[5] = $request->input('p_week_time_6_2_2');

                $start2[6] = $request->input('p_week_time_7_2_1');
                $end2[6] = $request->input('p_week_time_7_2_2');
            }
            if ($week_len == 1) {
                // $reserve_time1_1 = ([
                //     ['start' => '9:00', 'end' => '12:00'],
                //     ['start' => '13:00', 'end' => '18:00']
                // ]);
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 2) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 3) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                $week[2] => [
                                    ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                    ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 4) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                $week[2] => [
                                    ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                    ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                                ],
                                $week[3] => [
                                    ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                    ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 5) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                $week[2] => [
                                    ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                    ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                                ],
                                $week[3] => [
                                    ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                    ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                                ],
                                $week[4] => [
                                    ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                    ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 6) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                $week[2] => [
                                    ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                    ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                                ],
                                $week[3] => [
                                    ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                    ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                                ],
                                $week[4] => [
                                    ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                    ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                                ],
                                $week[5] => [
                                    ['start' => $start1[$week[5] - 1], 'end' => $end1[$week[5] - 1]],
                                    ['start' => $start2[$week[5] - 1], 'end' => $end2[$week[5] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            } elseif ($week_len == 7) {
                $data = ([
                    "clinic_info" => json_encode(
                        [
                            'owner' => '浅沼',
                            'businessHour' => [
                                $week[0] => [
                                    ['start' => $start1[$week[0] - 1], 'end' => $end1[$week[0] - 1]],
                                    ['start' => $start2[$week[0] - 1], 'end' => $end2[$week[0] - 1]]
                                ],
                                $week[1] => [
                                    ['start' => $start1[$week[1] - 1], 'end' => $end1[$week[1] - 1]],
                                    ['start' => $start2[$week[1] - 1], 'end' => $end2[$week[1] - 1]]
                                ],
                                $week[2] => [
                                    ['start' => $start1[$week[2] - 1], 'end' => $end1[$week[2] - 1]],
                                    ['start' => $start2[$week[2] - 1], 'end' => $end2[$week[2] - 1]]
                                ],
                                $week[3] => [
                                    ['start' => $start1[$week[3] - 1], 'end' => $end1[$week[3] - 1]],
                                    ['start' => $start2[$week[3] - 1], 'end' => $end2[$week[3] - 1]]
                                ],
                                $week[4] => [
                                    ['start' => $start1[$week[4] - 1], 'end' => $end1[$week[4] - 1]],
                                    ['start' => $start2[$week[4] - 1], 'end' => $end2[$week[4] - 1]]
                                ],
                                $week[5] => [
                                    ['start' => $start1[$week[5] - 1], 'end' => $end1[$week[5] - 1]],
                                    ['start' => $start2[$week[5] - 1], 'end' => $end2[$week[5] - 1]]
                                ],
                                $week[6] => [
                                    ['start' => $start1[$week[6] - 1], 'end' => $end1[$week[6] - 1]],
                                    ['start' => $start2[$week[6] - 1], 'end' => $end2[$week[6] - 1]]
                                ],
                                'holiday' => null,
                            ],
                            'description' => ['sample'], // [$request->input('description')],
                            'name' => ['sample'], //[$request->input('first_title')],
                            'insurance' => ['sample'], //[$request->input('Insurance')],
                            'eyecatchImages' => ['sample'] //['header_3.jpg']
                        ]
                    ),
                ]);
            }
            if ($week_len == 0) {
                echo "<script>alert('曜日をお選びください。');history.go(-1)</script>";
            } else {
                if ($options == 0) $a = 0;
                elseif ($options == 1) $a = 1;
                elseif ($options == 2) $a = 2;
                elseif ($options == 3) $a = 3;
                elseif ($options == 4) $a = 4;
                elseif ($options == 5) $a = 5;

                $priceData = ([
                    'price_list' => json_encode(
                        [
                            [
                                'title' => 'sample', //$request->input('toke_time').'分相談',
                                'price' => 'sample', //$request->input('toke_price')
                            ],
                            // [
                            //     'title' => '相談（セカンドオピニオン）',
                            //     'price' => $request->input('s_second')
                            // ],
                            // [
                            //     'title' => '相談（再診）',
                            //     'price' => $request->input('s_remind')
                            // ],
                            // [
                            //     'title' => 'その他 ',
                            //     'price' => $request->input('s_other')
                            // ]
                        ]
                    ),
                    'option' => json_encode([
                        'timeSlotType' => $a
                    ]),
                ]);
                // $clinic_menu_description=$request->input('description');
                // $clinic_menu_title=$request->input('first_title');
                $user = Auth::user();
                $content = new Clinic();
                // $content_menu = new User();
                // $clinic_menus = $content_menu->Clinic_menus($user->id);
                // if(count($clinic_menus)>0){
                // $content->reserve_update($data,$priceData,$user->id);//,$clinic_menu_description,$clinic_menu_title);
                // }else{
                $content->reserve_insert($data, $priceData, $user->id); //,$clinic_menu_description,$clinic_menu_title);
                // }
                return view('app.clinic.setting.reserve_update_completed');
            }*/
        } else {
            echo "<script>alert('曜日お選びください.');history.go(-1)</script>";
        }
    }
    ////////  superman //////////////////////

    function viewReserveDelete($id)
    {
        $query = ClinicMenu::find($id);
        $query->delete();
        return view('app.clinic.setting.reserve_delete_complete');
    }
    function ViewClinicMenuUdate($id)
    {
        $query = ClinicMenu::where('id', $id)->first();
        return view('app.clinic.setting.menu_update_view', compact('query'));
    }

    ///////////////////////////////////////
    ///////////////////// superman hospital update /////////////////
    public function Clinicinfo_remove()
    {
        $user = Auth::user();
        $clinics = Clinic::where('id', $user->id)->first();
        $clinics->update([
            'clinic_info_photos' => ''
        ]);
        return redirect()->back();
    }
    public function Cliniceye_remove()
    {
        $user = Auth::user();
        $clinics = Clinic::where('id', $user->id)->first();
        $clinics->update([
            'eyecatchimage' => ''
        ]);
        return redirect()->back();
    }
    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'name_kana' => ['required', 'string', 'max:255'],
            'manager_name' => ['required', 'string', 'max:255'],
            // 'url' => ['', 'string', 'max:0'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'tel' => ['required', 'numeric', 'digits_between:8,11'],
            'prefecture' => ['required', 'string', 'max:10'],
            'city' => ['required', 'string', 'max:20'],
            'address' => ['required', 'string', 'max:255'],

        ]);
        $url = $request->input('url');
        $clinic_img_dir = "assets/img/clinics/" . $user->id;
        if (!is_dir($clinic_img_dir)) {
            umask(0);
            mkdir($clinic_img_dir, 0777);
            @chmod($clinic_img_dir, 0777);
        }
        if ($request->hasFile('clinic_info_photos')) {
            $clinic_info_photos_filename = $request->clinic_info_photos->getClientOriginalName();
            $request->clinic_info_photos->move(public_path($clinic_img_dir), $clinic_info_photos_filename);
        } else {
            $clinic_info_photos_filename = $user->clinic_info_photos;
        }
        if ($request->hasFile('eyecatchimage')) {
            $eyecatchimage_filename = $request->eyecatchimage->getClientOriginalName();
            $request->eyecatchimage->move(public_path($clinic_img_dir), $eyecatchimage_filename);
        } else {
            $eyecatchimage_filename = $user->eyecatchimage;
        }

        //   if(strlen($request->input('insurance_company'))>1){
        //       $state = 1;
        //   }else{
        //       $state = 0;
        //   }
        $dog = false;
        $cat = false;
        $hamster = false;
        $ferret = false;
        $rabbit = false;
        $bird = false;
        $reptile = false;
        $others = false;
        if ($request->input('dog') == 'agree') {
            $dog = true;
        }
        if ($request->input('cat') == 'agree') {
            $cat = true;
        }
        if ($request->input('hamster') == 'agree') {
            $hamster = true;
        }
        if ($request->input('ferret') == 'agree') {
            $ferret = true;
        }
        if ($request->input('rabbit') == 'agree') {
            $rabbit = true;
        }
        if ($request->input('bird') == 'agree') {
            $bird = true;
        }
        if ($request->input('reptile') == 'agree') {
            $reptile = true;
        }
        if ($request->input('others') == 'agree') {
            $others = true;
        }
        $data1 = ([
            "option_medical_course" => json_encode(
                [
                    'internal' => true, //$this->medical_option($request->input('internal')), //内科
                    'orthopedic' => true, //$this->medical_option($request->input('orthopedic')), //整形外科
                    'neurosurgery' => true, //$this->medical_option($request->input('neurosurgery')), //神経外科
                    'dermatology' => true, //$this->medical_option($request->input('dermatology')), //皮膚科
                    'cardiology' => true, //$this->medical_option($request->input('cardiology')), //循環器科
                    'urology' => true, //$this->medical_option($request->input('urology')), //泌尿器科
                    'ophthalmology' => true, //$this->medical_option($request->input('ophthalmology')), //眼科
                    'dentistry' => true, //$this->medical_option($request->input('dentistry')), //歯科
                    'rehabilitation' => true, //$this->medical_option($request->input('rehabilitation')), //リハビリテーション
                    'others' => true, //$this->medical_option($request->input('others')),
                ]
            ),
            "option_pet" => json_encode(
                [
                    'dog' => $dog, //内科
                    'cat' => $cat, //整形外科
                    'hamster' => $hamster, //神経外科
                    'ferret' => $ferret, //皮膚科
                    'rabbit' => $rabbit, //循環器科
                    'bird' => $bird, //泌尿器科
                    'reptile' => $reptile, //眼科
                    'others' => $others,
                ]
            ),
            'clinic_description' => $request->input('comment'),
            'insurance' => $request->input('insurance_company'),
            'clinic_id' => $user->id,
            'clinic_info_photos' => $clinic_info_photos_filename,
            'eyecatchimage' => $eyecatchimage_filename,
            // 'state'=>$state,
        ]);
        $content = new User();


        // $clinic_menus = $content->Clinic_menus($user->id);
        // if(count($clinic_menus)>0){
        $content->UserUpdate($data, $data1, $url);
        // }else{
        //     $content->descript_company_insert($data,$data1);
        // }
        // echo ("<script>alert('正確に変更されました。')</script>");
        // return $this->viewProfile();
        return view('app.clinic.setting.update_completed');
    }
    function medical_option($option)
    {
        if ($option == 'agree') {
            return 'true';
        } else {
            return 'false';
        }
    }
    ///////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////
    public function viewReserveEnd($id)
    {
        $diag = Diagnosis::find($id);
        $accounting = Accounting::where("diagnosis_id", $id)->where("clinic_id", Auth::id())->first();
        if (!empty($accounting)) {
            $accounting->update([
                'next_remind_date' => null,
                // 'status' => '1',
            ]);
        }
        if ($diag->start_datetime != null) {
            $start_time = explode(" ", $diag->start_datetime);
            $start_time1 = explode(":", $start_time[1]);
            $end_time = explode(" ", \Carbon\Carbon::now());
            $end_time1 = explode(":", $end_time[1]);
            $start_hour = $start_time1[0];
            $start_min = $start_time1[1];
            $start_sec = $start_time1[2];
            $end_hour = $end_time1[0];
            $end_min = $end_time1[1];
            $end_sec = $end_time1[2];
            $real_min = 0;
            $real_sec = 0;
            $real_hour = 0;
            if ($start_hour <= $end_hour) {
                $real_hour = $end_hour - $start_hour;
                if ($start_min <= $end_min) {
                    $real_min = $end_min - $start_min;
                    if ($start_sec < $end_sec) {
                        $real_sec = $end_sec - $start_sec;
                    } else if ($start_sec == $end_sec) {
                        $real_sec = '00';
                    } else {
                        $real_sec = ($end_sec + 60) - $start_sec;
                        $real_min = $real_min - 1;
                    }
                } else {
                    $real_min = ($end_min + 60) - $start_min;
                    if ($start_sec < $end_sec) {
                        $real_sec = $end_sec - $start_sec;
                    } else if ($start_sec == $end_sec) {
                        $real_sec = '00';
                    } else {
                        $real_sec = ($end_sec + 60) - $start_sec;
                        $real_min = $real_min - 1;
                    }
                }
            } else {
                $real_hour = ($end_hour + 60) - $start_hour;
                if ($start_min <= $end_min) {
                    $real_min = $end_min - $start_min;
                    if ($start_sec < $end_sec) {
                        $real_sec = $end_sec - $start_sec;
                    } else if ($start_sec == $end_sec) {
                        $real_sec = '00';
                    } else {
                        $real_sec = ($end_sec + 60) - $start_sec;
                        $real_min = $real_min - 1;
                    }
                } else {
                    $real_min = ($end_min + 60) - $start_min;
                    if ($start_sec < $end_sec) {
                        $real_sec = $end_sec - $start_sec;
                    } else if ($start_sec == $end_sec) {
                        $real_sec = '00';
                    } else {
                        $real_sec = ($end_sec + 60) - $start_sec;
                        $real_min = $real_min - 1;
                    }
                }
            }
            if ($real_hour < 10) {
                $real_hour = "0" . $real_hour;
            }
            if ($real_min < 10) {
                $real_min = "0" . $real_min;
            }
            if ($real_sec < 10) {
                $real_sec = "0" . $real_sec;
            }
            $res_time = $real_hour . ":" . $real_min . ":" . $real_sec;
            $diag_end = $diag->update([
                'end_datetime' => \Carbon\Carbon::now(),
                'res_time' => $res_time,
                'status' => '1',
            ]);
        }

        return redirect('/app/clinic');

        // if ($diag_end) {
        //     return redirect('/app/owner/clinic/reserve/check/'.$id);
        // } else {
        //     return redirect('/app/clinic');
        // }
    }
    ///////////////////////////////////////////////////////////////
}
