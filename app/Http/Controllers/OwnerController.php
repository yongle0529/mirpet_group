<?php

namespace App\Http\Controllers;

use App\Accounting;
use App\Clinic;
use App\ClinicMenu;
use App\FavoriteClinic;
use App\Pet;
use App\User;
use App\Diagnosis;
use App\ReserveTimetable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Payjp\Charge;
use Payjp\Customer;
use Payjp\Payjp;
use Payjp\Tenant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\OwnerReserveForUserMail;
use App\Mail\OwnerReserveMail;
use App\Record;
use App\DiagFile;
use App\UserReminder;
use App\SystemFee;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class OwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $reminders = UserReminder::where('user_id', Auth::id())->get();
        $diags = Diagnosis::whereUserId(Auth::id())->where('status', 0)->get();
        $user = Auth::user();
        // if (count($reminders) > 0) {

        //     foreach ($reminders as $reminder) {
        //         if ($reminder->flag == 1) {
        //             $d = strtotime("+1 days");
        //             $real_time = date("Y-m-d h:i:s", $d);
        //             foreach ($diags as $diag) {
        //                 if ($real_time == $diag->reserve_datetime) {
        //                     $data = [
        //                         'username' => $user->name,
        //                         'useremail' => $user->email,
        //                         'usertel' => $user->tel,
        //                         'reserve_datetime' => $diag->reserve_datetime,
        //                     ];
        //                     Mail::to($user->email)->send(new \App\Mail\ReminderDayMail($data));
        //                 }
        //             }
        //         } else if ($reminder->flag == 2) {
        //             //$d = strtotime("+1 days");
        //             $real_time = date("Y-m-d h");
        //             foreach ($diags as $diag) {
        //                 if (strpos($diag->reserve_datetime, $real_time) !== false) {
        //                     $data = [
        //                         'username' => $user->name,
        //                         'useremail' => $user->email,
        //                         'usertel' => $user->tel,
        //                         'reserve_datetime' => $diag->reserve_datetime,
        //                     ];
        //                     Mail::to($user->email)->send(new \App\Mail\ReminderDateMail($data));
        //                 }
        //             }
        //         } else if ($reminder->flag == 3) {
        //             $txt = "+" . $reminder->time . " hours";
        //             $d = strtotime($txt);
        //             $real_time = date("Y-m-d h:i:s", $d);
        //             foreach ($diags as $diag) {
        //                 if ($real_time == $diag->reserve_datetime) {
        //                     $data = [
        //                         'username' => $user->name,
        //                         'useremail' => $user->email,
        //                         'usertel' => $user->tel,
        //                         'reserve_datetime' => $diag->reserve_datetime,
        //                     ];
        //                     Mail::to($user->email)->send(new \App\Mail\ReminderHourMail($data));
        //                 }
        //             }
        //         } else if ($reminder->flag == 4) {

        //             $txt = "+" . $reminder->time . " minutes";
        //             $d = strtotime($txt);
        //             $real_time = date("Y-m-d h:i:s", $d);

        //             foreach ($diags as $diag) {
        //                 if ($real_time == $diag->reserve_datetime) {
        //                     $data = [
        //                         'username' => $user->name,
        //                         'useremail' => $user->email,
        //                         'usertel' => $user->tel,
        //                         'reserve_datetime' => $diag->reserve_datetime,
        //                     ];
        //                     Mail::to($user->email)->send(new \App\Mail\ReminderMinuteMail($data));
        //                 }
        //             }
        //         }
        //     }
        // }
        $this->middleware(['auth', 'verified'])->except(['viewClinicSearch', 'postClinicSearch']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->get();
        $favoriteClinics = FavoriteClinic::whereUserId(Auth::id())->where('del_flg', 0)->get();
        $diags = Diagnosis::whereUserId(Auth::id())->where('status', 0)->get();
        $finishedDiags = Diagnosis::whereUserId(Auth::id())->where('status', '!=', 0)->where('status', '!=', 6)->get();
        $nextDiags = Diagnosis::whereUserId(Auth::id())->where('status', 3)->get();
        $finishedClinics = Clinic::find(Diagnosis::whereUserId(Auth::id())->where('status', 4)->orwhere('status', 2)->get(['clinic_id']));
        // echo count($finishedClinics);
        $d = strtotime("+1 day");
        $curr_date = date("Y-m-d", $d);
        $curr_datetime = date("Y-m-d");
        return view('app.owner.index', compact('pets', 'favoriteClinics', 'diags', 'finishedDiags', 'nextDiags', 'finishedClinics', 'curr_datetime', 'curr_date'));
    }

    public function viewChat($diagId)
    {
        $flag = 2;
        return view('chat.index', compact('diagId', 'flag'))->with('returnUrl', URL::to('/app/owner'));
    }

    public function viewContact()
    {
        return view('app.owner.contactus');
    }

    public function viewClinicSearch()
    {
        $selectedPet = "";
        return view('app.owner.search', compact('selectedPet'));
    }

    public function viewClinicDetail($id, Request $request)
    {
        $isRefFromMypage = $request->input('ref', null) == 'mypage' ? true : false;
        $clinic = Clinic::find($id);
        $isFavoriteClinic = FavoriteClinic::whereUserId(Auth::id())->where('clinic_id', $id)->where('del_flg', 0)->count() == 0 ? false : true;
        $clinic_Menus = ClinicMenu::where('clinic_id', 0)->get();
        $clinic_Menus1 = ClinicMenu::where('clinic_id', $id)->get();
        $optionPet = json_decode($clinic['option_pet'], true);

        return view('app.owner.clinic.detail', compact('clinic', 'clinic_Menus', 'clinic_Menus1', 'isRefFromMypage', 'isFavoriteClinic'));
    }
    public function viewRemindClinicDetail($id, $diagid, Request $request)
    {
        $isRefFromMypage = $request->input('ref', null) == 'mypage' ? true : false;
        $clinic = Clinic::find($id);
        $isFavoriteClinic = FavoriteClinic::whereUserId(Auth::id())->where('clinic_id', $id)->where('del_flg', 0)->count() == 0 ? false : true;
        $clinic_Menus = ClinicMenu::where('clinic_id', 0)->get();
        $remind_diagid = $diagid;
        // echo $diagid;

        return view('app.owner.clinic.remind_detail', compact('clinic', 'clinic_Menus', 'isRefFromMypage', 'isFavoriteClinic', 'remind_diagid'));
    }
    public function viewUpdateReveserClinicDetail($id, $diagid, Request $request)
    {
        $isRefFromMypage = $request->input('ref', null) == 'mypage' ? true : false;
        $clinic = Clinic::find($id);
        $isFavoriteClinic = FavoriteClinic::whereUserId(Auth::id())->where('clinic_id', $id)->where('del_flg', 0)->count() == 0 ? false : true;
        $clinic_Menus = ClinicMenu::where('base_id', $clinic->id)->get();
        $remind_diagid = $diagid;
        // echo $diagid;

        return view('app.owner.clinic.reserveupdate_detail', compact('clinic', 'clinic_Menus', 'isRefFromMypage', 'isFavoriteClinic', 'remind_diagid'));
    }

    public function viewReserveDetail($id, Request $request)
    {
        $isUpdate = $request->input('ref', null) == 'update' ? true : false;

        $diag = Diagnosis::find($id);
        $clinic = Clinic::find($diag->clinic_id);
        $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);

        $diag_files = DiagFile::where('diag_id', $id)->get();
        $txt = "+10 minutes";
        $d = strtotime($txt);
        $real_time = date("Y-m-d H:i:s", $d);
        if ($diag['status'] == 10 || $diag['status'] == 6 || $diag['status'] == 5) {
            return redirect('app/owner');
        } else if ($diag['user_id'] != Auth::id()) {
            return redirect('app/owner');
        } else {
            return view('app.owner.clinic.reserve.detail', compact('diag', 'diag_files', 'clinic', 'clinicMenu', 'isUpdate', 'real_time'));
        }
    }

    public function viewReserveEnd($id)
    {
        $diag = Diagnosis::where('id', $id);

        $diag_end = $diag->update([
            'status' => '1',
        ]);

        if ($diag_end) {
            return redirect('/app/owner/clinic/reserve/check/' . $id);
        } else {
            return redirect('/app/owner');
        }
    }

    public function viewReserveCheck($id, Request $request)
    {
        $isUpdate = $request->input('ref', null) == 'update' ? true : false;

        $diag = Diagnosis::find($id);
        $accounting = Accounting::where('diagnosis_id', $id)->first();
        $record = Record::where('diag_id', $id)->get();
        $clinic = Clinic::find($diag->clinic_id);
        $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);
        $systemfee = SystemFee::find(1);
        if ($diag['status'] == 6) {
            return redirect('app/owner');
        } else if ($diag['user_id'] != Auth::id()) {
            return redirect('app/owner');
        } else {
            return view('app.owner.clinic.reserve.check', compact('diag', 'clinic', 'clinicMenu', 'isUpdate', 'accounting', 'record', 'systemfee'));
        }
    }

    public function viewReserveUpdate($id, Request $request)
    {
        $diag = Diagnosis::find($id);
        $diag_files = DiagFile::where('diag_id', $id)->get();
        $flag = 0;
        if (!empty($diag_files)) {
            $flag = count($diag_files);
        }
        $clinic = Clinic::find($diag->clinic_id);
        $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);
        $pets = Pet::where('user_id', $diag->user_id)->get();
        $clinic_Menus = ClinicMenu::where('clinic_id', 0)->get();

        return view('app.owner.clinic.reserve.update', compact('diag', 'diag_files', 'clinic', 'clinicMenu', 'clinic_Menus', 'pets', 'flag'));
    }

    public function viewReserve($clinicId, $menuId, Request $request)
    {
        $clinicMenu = ClinicMenu::whereId($menuId)->first();
        $clinics = Clinic::all();
        $clinic = Clinic::whereId($clinicId)->first();
        return view('app.owner.clinic.reserve.index', compact('clinicMenu', 'clinics', 'clinic'));
    }
    public function viewRemindReserve($diagclinicId, $menuId, $diagid, Request $request)
    {
        // if ($duplicate == 1) {
        $diag = Diagnosis::find($diagid);
        $clinicId = $diagclinicId;
        // } else {
        //     $clinicId = $id;
        //     $diag = 0;
        // }

        $diag_files = DiagFile::where('diag_id', $diagid)->get();
        $clinicMenu = ClinicMenu::where('id', $menuId)->first();

        $isUpdate = $request->input('ref', null) != 'update' ? true : false;
        $clinic = Clinic::find($clinicId)->first();
        // $clinicMenus = ClinicMenu::whereClinic_id($clinicId)->get();
        $clinicMenus = ClinicMenu::where('id', $menuId)->get();
        $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');
        $accounting = Accounting::where('diagnosis_id', $diagid)->first();

        //echo $clinicMenus;
        return view('app.owner.clinic.reserve.renew', compact('diag', 'clinic', 'clinicMenus', 'clinicMenu', 'diag_files', 'pets', 'isUpdate', 'accounting'));
        // $clinicMenu = ClinicMenu::whereId($menuId)->first();
        // $clincs = Clinic::all();
        // $clinic = Clinic::whereId($clinicId)->first();
        // return view('app.owner.clinic.reserve.index', compact('clinicMenu', 'clinics', 'clinic'));
    }
    public function viewuserUdateReserve($diagclinicId, $menuId, $diagid, Request $request)
    {
        // if ($duplicate == 1) {
        //     $diag = Diagnosis::find($diagid);
        //     $clinicId = $diagclinicId;
        // // } else {
        // //     $clinicId = $id;
        // //     $diag = 0;
        // // }

        // $diag_files = DiagFile::where('diag_id', $diagid)->get();
        // $clinicMenu = ClinicMenu::where('id',$menuId)->first();

        // $isUpdate = $request->input('ref', null) != 'update' ? true : false;
        // $clinic = Clinic::find($clinicId)->first();
        // // $clinicMenus = ClinicMenu::whereClinic_id($clinicId)->get();
        // $clinicMenus = ClinicMenu::where('id',$menuId)->get();
        // $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');
        // $accounting = Accounting::where('diagnosis_id', $diagid)->first();

        // //echo $clinicMenus;
        // return view('app.owner.clinic.reserve.renew', compact('diag', 'clinic', 'clinicMenus', 'clinicMenu', 'diag_files', 'pets', 'isUpdate','accounting'));
        // $clinicMenu = ClinicMenu::whereId($menuId)->first();
        // $clincs = Clinic::all();
        // $clinic = Clinic::whereId($clinicId)->first();
        // return view('app.owner.clinic.reserve.index', compact('clinicMenu', 'clinics', 'clinic'));



        $diag = Diagnosis::find($diagid);
        $diag_files = DiagFile::where('diag_id', $diagid)->get();
        $diag_files_json = json_encode($diag_files);
        $flag = 0;
        if (!empty($diag_files)) {
            $flag = count($diag_files);
        }
        $clinic = Clinic::find($diag->clinic_id);
        // $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);
        $clinicMenu = ClinicMenu::where('id', $menuId)->first();
        $pets = Pet::whereUserId(Auth::id())->pluck('name', 'id');
        // $clinic_Menus = ClinicMenu::where('clinic_id', 0)->get();
        $clinicMenus = ClinicMenu::where('id', $menuId)->get();

        return view('app.owner.clinic.reserve.update', compact('diag', 'diag_files', 'clinic', 'clinicMenu', 'pets', 'flag', 'diag_files_json'));
    }

    public function viewReserveLast($clinicId, $menuId, Request $request)
    {
        $clinicMenu = ClinicMenu::whereId($menuId)->first();
        $clinic = Clinic::whereId($clinicId)->first();
        $datetime = $request->input('date') . ' ' . $request->input('time');
        $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');

        return view('app.owner.clinic.reserve.reserve', compact('clinicMenu', 'datetime', 'pets', 'clinic'));
    }
    public function viewRemindReserveLast($clinicId, $menuId, $diagid, Request $request)
    {
        $clinicMenu = ClinicMenu::whereId($menuId)->first();
        $clinic = Clinic::whereId($clinicId)->first();
        $datetime = $request->input('date') . ' ' . $request->input('time');
        $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');

        $diag = Diagnosis::find($diagid);
        // $clinicId = $diagclinicId;
        // } else {
        //     $clinicId = $id;
        //     $diag = 0;
        // }

        $diag_files = DiagFile::where('diag_id', $diagid)->get();
        // $clinicMenu = ClinicMenu::where('id',$menuId)->first();

        // $isUpdate = $request->input('ref', null) != 'update' ? true : false;
        $clinic = Clinic::find($clinicId);
        // $clinicMenus = ClinicMenu::whereClinic_id($clinicId)->get();
        // $clinicMenus = ClinicMenu::where('id',$menuId)->get();
        // $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');
        $accounting = Accounting::where('diagnosis_id', $diagid)->first();

        //echo $clinicMenus;
        // return view('app.owner.clinic.reserve.renew', compact('diag', 'clinic', 'clinicMenus', 'clinicMenu', 'diag_files', 'pets', 'isUpdate','accounting'));

        return view('app.owner.clinic.reserve.remindreserve', compact('clinicMenu', 'datetime', 'diag_files', 'pets', 'clinic', 'diagid', 'diag'));
    }

    public function postReserveRegister(Request $request)
    {
        $user = Auth::user();
        if ($user->payment_token != null) {
            $validatedData = $request->validate([
                'clinic' => ['required', 'numeric'],
                'clinicmenu' => ['required', 'numeric'],
                'datetime' => ['required', 'date_format:"Y-m-d H:i"'],
                'pet' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'content' => ['required', 'string'],
                'clinic_name' => ['required', 'string'],
            ]);
            if ($request->input('code_flag') == true) {
                $diag_save = Diagnosis::create(
                    [
                        'user_id' => Auth::id(),
                        'clinic_id' => $validatedData['clinic'],
                        'clinic_menu_id' => $validatedData['clinicmenu'],
                        'pet_id' => $validatedData['pet'],
                        'reserve_datetime' => $validatedData['datetime'],
                        'price' => $validatedData['price'],
                        'content' => $validatedData['content'],
                        'reserve_method' => $request->input('online'),
                        'clinic_name' => $validatedData['clinic_name']
                    ]
                );
            } else {
                $diag_save = Diagnosis::create(
                    [
                        'user_id' => Auth::id(),
                        'clinic_id' => $validatedData['clinic'],
                        'clinic_menu_id' => $validatedData['clinicmenu'],
                        'pet_id' => $validatedData['pet'],
                        'reserve_datetime' => $validatedData['datetime'],
                        'price' => $validatedData['price'],
                        'content' => $validatedData['content'],
                        'hospital_code' => $request->input('hospital_code'),
                        'reserve_method' => $request->input('online'),
                        'clinic_name' => $validatedData['clinic_name']
                    ]
                );
            }
            $pet = Pet::find($validatedData['pet']);
            $diag = Diagnosis::find($diag_save->id);
            $diag->update([
                'cata_no' => $diag->id
            ]);

            if ($request->input('fileuploader-list-photos') != "" && $request->input('fileuploader-list-photos') != null) {
                $checks = $request->input('fileuploader-list-photos');
                $flag = 0;
                if (strpos($checks, '0:/')) {
                    $flag++;
                }
                if ($flag > 0) {
                    foreach ($request->photos as $photo) {
                        $filename = date('YmdHis') . "-" . $photo->getClientOriginalName();
                        $extension = $photo->getClientOriginalExtension();
                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                            $photo->move(public_path('images/insurance'), $filename);
                            DiagFile::create([
                                'diag_id' => $diag->id,
                                'filename' => $filename,
                                'type' => '1',
                            ]);
                        } else if ($extension == 'mp4' || $extension == 'avi' || $extension == 'wmv' || $extension == 'mov') {
                            $photo->move(public_path('images/insurance'), $filename);
                            DiagFile::create([
                                'diag_id' => $diag->id,
                                'filename' => $filename,
                                'type' => '2',
                            ]);
                        }
                    }
                }
            }
            if ($diag_save) {
                ReserveTimetable::create([
                    'clinic_id' => $validatedData['clinic'],
                    'diag_id' => $diag->id,
                    'date' => $request->input('date'),
                    'time_slot' => $request->input('slot'),
                    'time_slot_type' => $request->input('time_slot_type'),
                ]);
            }

            $clinic = Clinic::whereId($validatedData['clinic'])->first();
            $user = Auth::user();
            $curr_date = date("Y-m-d H:i:s");
            $data = [
                'username' => $user->last_name . " " . $user->first_name,
                'useremail' => $user->email,
                'usertel' => $user->tel,
                'clinicname' => $clinic->name,
                'clinicemail' => $clinic->email,
                'clinictel' => $clinic->tel,
                'petname' => $pet->name,
                'reserve_datetime' => $validatedData['datetime'] . ":00",
                'diagId' => $diag->id,
                'flag' => '1',
                'created_at' => $curr_date
            ];
            // For User
            Mail::to($user->email)->send(new \App\Mail\OwnerReserveMail($data));

            // For Clinic
            Mail::to($clinic->email)->send(new \App\Mail\OwnerReserveForUserMail($data));

            return view('app.owner.clinic.reserve.register_completed');
        } else {
            return redirect('/app/owner/clinic/reserve/error');
        }
    }

    public function postReserveUpdate($id, $menuId, Request $request)
    {
        $validatedData = $request->validate([
            'pet' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
            'content' => ['required', 'string'],
            'reserve_datetime' => ['required', 'string'],
        ]);

        $diag = Diagnosis::find($id);

        if (!empty($diag)) {
            $reservetimetable = ReserveTimetable::where('diag_id', $id)->first();
            $reservetimetable->update([
                'date' => $validatedData['reserve_datetime']
            ]);
            if ($request->input('code_flag') == true) {
                $diag_save = $diag->update([
                    'pet_id' => $validatedData['pet'],
                    'clinic_menu_id' => $menuId,
                    'price' => $validatedData['price'],
                    'content' => $validatedData['content'],
                    'reserve_datetime' => $validatedData['reserve_datetime'],
                    'hospital_code' => null,
                    'reserve_method' => $request->input('option'),
                    'clinic_name' => $request->input('clinic_name')
                ]);
            } else {
                $diag_save = $diag->update([
                    'pet_id' => $validatedData['pet'],
                    'clinic_menu_id' => $menuId,
                    'price' => $validatedData['price'],
                    'content' => $validatedData['content'],
                    'reserve_datetime' => $validatedData['reserve_datetime'],
                    'hospital_code' => $request->input('hospital_code'),
                    'reserve_method' => $request->input('option'),
                    'clinic_name' => $request->input('clinic_name')
                ]);
            }
        }

        if ($request->input('fileuploader-list-photos') != "" && $request->input('fileuploader-list-photos') != null) {
            $checks = $request->input('fileuploader-list-photos');
            $flag = 0;
            if (strpos($checks, '0:/')) {
                $flag++;
            }
            // for ($i = 0; $i < count($checks); $i++) {
            //     if (substr($checks[$i], 0, 3) == "0:/") {
            //         $flag++;
            //     }
            // }
            // if (is_array($checks) || is_object($checks)) {
            //     foreach ($checks as $check) {
            //         if (substr($check, 0, 3) == "0:/") {
            //             $flag++;
            //         }
            //     }
            // }
            if ($checks == "[]") {
                $diag_files = DiagFile::where('diag_id', $id)->get();
                if (count($diag_files) > 0) {
                    foreach ($diag_files as $diag_file) {
                        $diag_file->delete();
                    }
                }
            }
            if ($flag > 0) {
                $diag_files = DiagFile::where('diag_id', $id)->get();
                if (count($diag_files) > 0) {
                    foreach ($diag_files as $diag_file) {
                        $diag_file->delete();
                    }
                }
                foreach ($request->photos as $photo) {
                    $filename = date('YmdHis') . "-" . $photo->getClientOriginalName();
                    $extension = $photo->getClientOriginalExtension();
                    if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                        $photo->move(public_path('images/insurance'), $filename);
                        DiagFile::create([
                            'diag_id' => $id,
                            'filename' => $filename,
                            'type' => '1',
                        ]);
                    } else {
                        $photo->move(public_path('images/insurance'), $filename);
                        DiagFile::create([
                            'diag_id' => $id,
                            'filename' => $filename,
                            'type' => '2',
                        ]);
                    }
                }
            }
        }

        // if ($request->hasFile('photos_add')) {
        //     foreach ($request->photos_add as $photo) {
        //         $filename = date('YmdHis') . "-" . $photo->getClientOriginalName();
        //         $extension = $photo->getClientOriginalExtension();
        //         if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
        //             $photo->move(public_path('images/insurance'), $filename);
        //             DiagFile::create([
        //                 'diag_id' => $id,
        //                 'filename' => $filename,
        //                 'type' => '1',
        //             ]);
        //         } else if ($extension == 'mp4' || $extension == 'avi' || $extension == 'wmv') {
        //             $photo->move(public_path('images/insurance'), $filename);
        //             DiagFile::create([
        //                 'diag_id' => $id,
        //                 'filename' => $filename,
        //                 'type' => '2',
        //             ]);
        //         }
        //     }
        // }

        $clinic = Clinic::whereId($diag->clinic_id)->first();
        $pet = Pet::find($validatedData['pet']);
        $user = Auth::user();
        $curr_date = date("Y-m-d H:i:s");
        $data = [
            'username' => $user->last_name . " " . $user->first_name,
            'useremail' => $user->email,
            'usertel' => $user->tel,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'petname' => $pet->name,
            'reserve_datetime' => $validatedData['reserve_datetime'],
            'diagId' => $diag->id,
            'flag' => '2',
            'created_at' => $curr_date
        ];
        // For User
        Mail::to($user->email)->send(new \App\Mail\OwnerReserveMail($data));

        // For Clinic
        Mail::to($clinic->email)->send(new \App\Mail\OwnerReserveForUserMail($data));

        return view('app.owner.clinic.reserve.register_completed')->with('isUpdate', true);
    }

    public function reserveDelete($id)
    {
        $diag = Diagnosis::where('id', $id)->where('user_id', Auth::id())->first();

        $reservetimetable = ReserveTimetable::where('diag_id', $id)->first();
        $reservetimetable->delete();
        if (empty($diag)) {
            return view('app.owner.index')->with('isError', true);
        }
        $diag->delete();

        $clinic = Clinic::whereId($diag->clinic_id)->first();
        $pet = Pet::find($diag->pet_id);
        $user = Auth::user();
        $data = [
            'username' => $user->last_name . " " . $user->first_name,
            'useremail' => $user->email,
            'usertel' => $user->tel,
            'clinicname' => $clinic->name,
            'clinicemail' => $clinic->email,
            'clinictel' => $clinic->tel,
            'petname' => $pet->name,
            'reserve_datetime' => $diag->reserve_datetime,
            'diagId' => $diag->id,
            'flag' => '3',
            'created_at' => $diag->created_at
        ];
        // For User
        Mail::to($user->email)->send(new \App\Mail\OwnerReserveMail($data));

        // For Clinic
        Mail::to($clinic->email)->send(new \App\Mail\OwnerReserveForUserMail($data));

        return view('app.owner.clinic.reserve.register_completed')->with('isDeleted', true);
    }

    public function profileDelete($id)
    {
        $user = User::find($id);
        // if (empty($user)) {
        //     return view('app.owner.index')->with('isError', true);
        // }
        $email = $user->email;
        $username = $user->last_name . " " . $user->first_name;
        $user->delete();
        $data = [
            'username' => $username,
            'flag' => '1'
        ];

        // For User
        Mail::to($email)->send(new \App\Mail\ProfileDeleteMail($data));

        return redirect('app/owner');
    }

    public function addFavorite($id)
    {
        $favoriteClinic = FavoriteClinic::where('user_id', Auth::id())->where('clinic_id', $id)->first();

        if (empty($favoriteClinic)) {
            $clinic = Clinic::whereId($id)->first();
            if (!empty($clinic)) {
                FavoriteClinic::create(
                    [
                        'user_id' => Auth::id(),
                        'clinic_id' => $id
                    ]
                );
                $isFav = true;
            } else {
                $isFav = false;
            }
        } else {
            if ($favoriteClinic->del_flg == 1) {
                $favoriteClinic->del_flg = 0;
                $favoriteClinic->save();
                $isFav = true;
            } else {
                $favoriteClinic->del_flg = 1;
                $favoriteClinic->save();
                $isFav = false;
            }
        }

        return response()->json(['status' => $isFav]);
    }

    public function postClinicSearch(Request $request)
    {

        $selectedPref = $request->input('prefecture');
        $inputCity = $request->input('city');
        $hospital_name = $request->input('hospital_name');
        $selectedPet = $request->input('pet_type');
        $clinics = Clinic::wherePrefecture($selectedPref)->where('status', '!=', 2);

        if (!empty($inputCity)) {
            $clinics = $clinics->where('city', 'like', "%{$inputCity}%");
        }

        if (!empty($hospital_name)) {
            $clinics = $clinics->where('name', 'like', "%{$hospital_name}%");
        }

        $clinics = $clinics->get();

        return view('app.owner.search', compact('clinics', 'selectedPref', 'inputCity', 'hospital_name', 'selectedPet'));
    }

    public function viewPetRegister()
    {
        return view('app.owner.pet.register');
    }

    public function postPetRegister(Request $request)
    {
        $validatedData = $request->validate([
            'pet_reg_name' => ['required', 'string', 'max:50'],
            'pet_reg_name_kana' => ['required', 'string', 'max:50'],
            'pet_reg_gender' => ['required', 'numeric', 'gte:1', 'lte:2'],
            'pet_reg_type' => ['required', 'numeric', 'gte:1', 'lte:7'],
            'pet_reg_type_other' => ['nullable', 'string', 'max:100'],
            'pet_reg_birth_year' => ['required', 'numeric', 'digits:4'],
            'pet_reg_birth_month' => ['required', 'numeric', 'digits_between:1,2'],
            'pet_reg_birth_date' => ['required', 'numeric', 'digits_between:1,2'],
            'pet_reg_insurance_front' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'pet_reg_insurance_back' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'pet_profile_image' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'insurance_option' => ['required', 'numeric', 'gte:0', 'lte:1'],
        ]);

        $birthday = $validatedData['pet_reg_birth_year'] . "-" . $validatedData['pet_reg_birth_month'] . "-" . $validatedData['pet_reg_birth_date'];
        $birthday = Carbon::parse($birthday)->format('Y-m-d');

        if ($request->input('genre_option') == "0") {
            $genre = $request->input('dog_reg_genre');
        } else if ($request->input('genre_option') == "1") {
            $genre = $request->input('cat_reg_genre');
        } else {
            $genre = $request->input('pet_reg_genre');
        }
        if ($validatedData['insurance_option'] == 1) {
            // $request->validate([
            //     'insurance_name' => ['required', 'string'],
            //     'insurance_no' => ['required', 'string'],
            //     'insurance_from_year' => ['required', 'numeric', 'digits:4'],
            //     'insurance_from_month' => ['required', 'numeric', 'digits_between:1,2'],
            //     'insurance_from_day' => ['required', 'numeric', 'digits_between:1,2'],
            //     'insurance_to_year' => ['required', 'numeric', 'digits:4'],
            //     'insurance_to_month' => ['required', 'numeric', 'digits_between:1,2'],
            //     'insurance_to_day' => ['required', 'numeric', 'digits_between:1,2']
            // ]);
            $from_date = $request->input('insurance_from_year') . "-" . $request->input('insurance_from_month') . "-" . $request->input('insurance_from_date');
            $from_date = Carbon::parse($from_date)->format('Y-m-d');

            $to_date = $request->input('insurance_to_year') . "-" . $request->input('insurance_to_month') . "-" . $request->input('insurance_to_date');
            $to_date = Carbon::parse($to_date)->format('Y-m-d');
            $pet = [
                'user_id' => Auth::id(),
                'name' => $validatedData['pet_reg_name'],
                'name_kana' => $validatedData['pet_reg_name_kana'],
                'gender' => $validatedData['pet_reg_gender'],
                'type' => $validatedData['pet_reg_type'],
                'type_other' => empty($validatedData['pet_reg_type_other']) ? null : $validatedData['pet_reg_type_other'],
                'genre' => $genre,
                'birthday' => $birthday,
                'insurance_name' => $request->input('insurance_name'),
                'insurance_no' => $request->input('insurance_no'),
                'insurance_from_date' => $from_date,
                'insurance_to_date' => $to_date,
                'insurance_option' => $validatedData['insurance_option']
            ];
        } else {
            $pet = [
                'user_id' => Auth::id(),
                'name' => $validatedData['pet_reg_name'],
                'name_kana' => $validatedData['pet_reg_name_kana'],
                'gender' => $validatedData['pet_reg_gender'],
                'type' => $validatedData['pet_reg_type'],
                'type_other' => empty($validatedData['pet_reg_type_other']) ? null : $validatedData['pet_reg_type_other'],
                'genre' => $genre,
                'birthday' => $birthday,
            ];
        }

        if (isset($validatedData['pet_reg_insurance_front']) && $validatedData['pet_reg_insurance_front']) {
            $filename = date('YmdHis') . "-front." . $validatedData['pet_reg_insurance_front']->getClientOriginalExtension();
            $validatedData['pet_reg_insurance_front']->move(public_path('images/insurance'), $filename);
            // if ($pet->insurance_front)
            //     unlink(public_path('images/insurance/') . $pet->insurance_front);
            $pet['insurance_front'] = $filename;
        }

        if (isset($validatedData['pet_reg_insurance_back']) && $validatedData['pet_reg_insurance_back']) {
            $filename = date('YmdHis') . "-back." . $validatedData['pet_reg_insurance_back']->getClientOriginalExtension();
            $validatedData['pet_reg_insurance_back']->move(public_path('images/insurance'), $filename);
            // if ($pet->insurance_back)
            //     unlink(public_path('images/insurance/') . $pet->insurance_back);
            $pet['insurance_back'] = $filename;
        }

        if (isset($validatedData['pet_profile_image']) && $validatedData['pet_profile_image']) {
            $filename = date('YmdHis') . "-pet." . $validatedData['pet_profile_image']->getClientOriginalExtension();
            $validatedData['pet_profile_image']->move(public_path('images/insurance'), $filename);
            // if ($pet->insurance_back)
            //     unlink(public_path('images/insurance/') . $pet->insurance_back);
            $pet['profile_picture_path'] = $filename;
        }

        Pet::create($pet);

        return view('app.owner.pet.register_completed');
    }

    public function viewPetUpdate($id)
    {
        $pet = Pet::where('id', $id)->where('user_id', Auth::id())->where('del_flg', 0)->first();

        return view('app.owner.pet.update', compact('pet'));
    }

    public function postPetUpdate($id, Request $request)
    {
        $pet = Pet::where('id', $id)->where('user_id', Auth::id())->where('del_flg', 0)->first();
        if (empty($pet)) {
            return view('app.owner.pet.update')->with('isError', true);
        }

        $validatedData = $request->validate([
            'pet_reg_name' => ['required', 'string', 'max:50'],
            'pet_reg_name_kana' => ['required', 'string', 'max:50'],
            'pet_reg_gender' => ['required', 'numeric', 'gte:1', 'lte:2'],
            'pet_reg_type' => ['required', 'numeric', 'gte:1', 'lte:3'],
            'pet_reg_type_other' => ['nullable', 'string', 'max:100'],
            'pet_reg_birth_year' => ['required', 'numeric', 'digits:4'],
            'pet_reg_birth_month' => ['required', 'numeric', 'digits_between:1,2'],
            'pet_reg_birth_date' => ['required', 'numeric', 'digits_between:1,2'],
            'pet_reg_insurance_front' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'pet_reg_insurance_back' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'pet_profile_image' => 'image|mimes:jpeg,png,jpg', //|max:2048
            'insurance_option' => ['required', 'numeric', 'gte:0', 'lte:1'],
        ]);

        $birthday = $validatedData['pet_reg_birth_year'] . "-" . $validatedData['pet_reg_birth_month'] . "-" . $validatedData['pet_reg_birth_date'];
        $birthday = Carbon::parse($birthday)->format('Y-m-d');
        if ($request->input('genre_option') == "0") {
            $genre = $request->input('dog_reg_genre');
        } else if ($request->input('genre_option') == "1") {
            $genre = $request->input('cat_reg_genre');
        } else {
            $genre = $request->input('pet_reg_genre');
        }
        if ($validatedData['insurance_option'] == 1) {
            $from_date = $request->input('insurance_from_year') . "-" . $request->input('insurance_from_month') . "-" . $request->input('insurance_from_date');
            $from_date = Carbon::parse($from_date)->format('Y-m-d');

            $to_date = $request->input('insurance_to_year') . "-" . $request->input('insurance_to_month') . "-" . $request->input('insurance_to_date');
            $to_date = Carbon::parse($to_date)->format('Y-m-d');
        } else {
            $from_date = null;
            $to_date = null;
        }
        $pet->name = $validatedData['pet_reg_name'];
        $pet->name_kana = $validatedData['pet_reg_name_kana'];
        $pet->gender = $validatedData['pet_reg_gender'];
        $pet->type = $validatedData['pet_reg_type'];
        $pet->type_other = empty($validatedData['pet_reg_type_other']) ? null : $validatedData['pet_reg_type_other'];
        $pet->genre = $genre;
        $pet->birthday = $birthday;
        $pet->insurance_name = $request->input('insurance_name');
        $pet->insurance_no = $request->input('insurance_no');
        $pet->insurance_from_date = $from_date;
        $pet->insurance_to_date = $to_date;
        $pet->insurance_option = $validatedData['insurance_option'];

        if (isset($validatedData['pet_reg_insurance_front']) && $validatedData['pet_reg_insurance_front']) {
            $filename = date('YmdHis') . "-front." . $validatedData['pet_reg_insurance_front']->getClientOriginalExtension();
            $validatedData['pet_reg_insurance_front']->move(public_path('images/insurance'), $filename);
            if ($pet->insurance_front)
                unlink(public_path('images/insurance/') . $pet->insurance_front);
            $pet->insurance_front = $filename;
        }

        if (isset($validatedData['pet_reg_insurance_back']) && $validatedData['pet_reg_insurance_back']) {
            $filename = date('YmdHis') . "-back." . $validatedData['pet_reg_insurance_back']->getClientOriginalExtension();
            $validatedData['pet_reg_insurance_back']->move(public_path('images/insurance'), $filename);
            if ($pet->insurance_back)
                unlink(public_path('images/insurance/') . $pet->insurance_back);
            $pet->insurance_back = $filename;
        }

        if (isset($validatedData['pet_profile_image']) && $validatedData['pet_profile_image']) {
            $filename = date('YmdHis') . "-pet." . $validatedData['pet_profile_image']->getClientOriginalExtension();
            $validatedData['pet_profile_image']->move(public_path('images/insurance'), $filename);
            if ($pet->profile_picture_path)
                unlink(public_path('images/insurance/') . $pet->profile_picture_path);
            $pet->profile_picture_path = $filename;
        }

        $pet->save();

        return view('app.owner.pet.register_completed')->with('isUpdate', true);
    }

    public function petDelete($id)
    {
        $pet = Pet::where('id', $id)->where('user_id', Auth::id())->where('del_flg', 0)->first();
        if (empty($pet)) {
            return view('app.owner.pet.update')->with('isError', true);
        }

        $pet->del_flg = 1;
        $pet->save();

        return view('app.owner.pet.register_completed')->with('isDeleted', true);
    }

    public function insuranceDelete($id, $flag)
    {
        $pet = Pet::find($id);
        if ($flag == 1) {
            Storage::disk('public')->delete($pet->insurance_front);
            $pet->insurance_front = null;
        } else {
            Storage::disk('public')->delete($pet->insurance_back);
            $pet->insurance_back = null;
        }
        $pet->save();

        return redirect('/app/owner/pet/update/' . $pet['id']);
    }

    public function petImageDelete($id)
    {
        $pet = Pet::find($id);
        Storage::disk('public')->delete($pet->profile_picture_path);
        $pet->profile_picture_path = null;
        $pet->save();

        return redirect('/app/owner/pet/update/' . $pet['id']);
    }

    public function reserveImageDelete($filename, $diagId, $menuId, $clinicId)
    {
        $diag_file = DiagFile::where('filename', $filename)->first();
        Storage::disk('public')->delete($diag_file->filename);
        $diag_file->delete();

        return redirect('/app/owner/clinic/userupdate_reserve/' . $clinicId . '/' . $menuId . '/' . $diagId);
    }

    public function viewProfile(Request $request)
    {
        $user = Auth::user();
        return view('app.owner.setting.profile_update', compact('user'));
    }

    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $data = $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'first_name_kana' => ['required', 'string', 'max:255'],
            'last_name_kana' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'tel' => ['required', 'numeric', 'digits_between:8,11'],
            'zip_code' => ['required', 'numeric', 'digits:7'],
            'prefecture' => ['required', 'string', 'max:10'],
            'city' => ['required', 'string', 'max:20'],
            'address' => ['required', 'string', 'max:255'],
        ]);

        $user->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'first_name_kana' => trim(mb_convert_kana($data['first_name_kana'], 'C')),
            'last_name_kana' => trim(mb_convert_kana($data['last_name_kana'], 'C')),
            'email' => $data['email'],
            'tel' => str_replace('-', '', mb_convert_kana($data['tel'], 'a')),
            'zip_code' => str_replace('-', '', mb_convert_kana($data['zip_code'], 'a')),
            'prefecture' => mb_convert_kana($data['prefecture'], 'as'),
            'city' => mb_convert_kana($data['city'], 'as'),
            'address' => mb_convert_kana($data['address'], 'as'),
        ]);

        return view('app.owner.setting.update_completed');
    }

    public function viewPayment(Request $request)
    {
        $user = Auth::user();
        Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));
        $customer = !empty($user->payment_token) ? Customer::retrieve($user->payment_token) : null;
        $cardInfo = !empty($customer) ? isset($customer->cards->data[0]) ? $customer->cards->data[0] : null : null;

        return view('app.owner.setting.payment', compact('user', 'cardInfo'));
    }

    public function deleteCreditCard(Request $request)
    {
        $user = Auth::user();
        Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));
        $customer = Customer::retrieve($user->payment_token);
        $cardId = $customer->cards->data[0]->id;
        $card = $customer->cards->retrieve($cardId);
        $card->delete();
        $customer->delete();
        $user->update([
            'payment_token' => null
        ]);

        return view('app.owner.setting.update_completed')->with('deleteCreditCard', true);
    }

    public function showChangePasswordForm(Request $request)
    {
        return view('auth.passwords.change_password');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "現在のパスワードが違います。");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }
        $request->validate([
            'current-password' => 'required',
            'new-password' => [
                'required',
                'min:8',
                'max:20',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/',
                'confirmed'
            ],
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success", "パスワードが変更されました。");
    }

    public function viewPaymentHistory()
    {
        $payments = Record::where('user_id', Auth::id())->get();
        return view('app.owner.setting.payment_history', compact('payments'));
    }

    public function viewReminder()
    {
        $user = Auth::user();
        $reminder = UserReminder::where('user_id', $user->id)->first();
        if (empty($reminder)) {
            UserReminder::create([
                'user_id' => $user->id,
                'flag' => 1,
                'time' => 1,
            ]);
            UserReminder::create([
                'user_id' => Auth::id(),
                'flag' => 4,
                'time' => 10,
            ]);
        }
        $reminds = UserReminder::where('user_id', Auth::id())->get();
        $oneday = 0;
        $datetime = 0;
        $hour = 0;
        $minute = 0;
        $oneday_remind = '';
        $datetime_remind = '';
        $hour_remind = '';
        $minute_remind = '';
        foreach ($reminds as $remind) {
            if ($remind->flag == 1) {
                $oneday = 1;
                $oneday_remind = $remind;
            }
            if ($remind->flag == 2) {
                $datetime = 1;
                $datetime_remind = $remind;
            }
            if ($remind->flag == 3) {
                $hour = 1;
                $hour_remind = $remind;
            }
            if ($remind->flag == 4) {
                $minute = 1;
                $minute_remind = $remind;
            }
        }
        return view('app.owner.setting.reminder', compact('user', 'reminds', 'oneday', 'datetime', 'hour', 'minute', 'oneday_remind', 'datetime_remind', 'hour_remind', 'minute_remind'));
    }

    public function updateReminder(Request $request)
    {
        $checks = UserReminder::where('user_id', Auth::id())->get();
        if (count($checks) > 0) {
            foreach ($checks as $check) {
                $check->delete();
            }
        }
        $oneday = $request->input('oneday');
        if ($oneday == true) {
            UserReminder::create([
                'user_id' => Auth::id(),
                'flag' => 1,
                'time' => 1,
            ]);
        }
        $datetime = $request->input('datetime');
        $datetime1 = $request->input('datetime1');
        if ($datetime == true) {
            UserReminder::create([
                'user_id' => Auth::id(),
                'flag' => 2,
                'time' => $datetime1,
            ]);
        }
        $hour = $request->input('hour');
        $hour1 = $request->input('hour1');
        if ($hour == true) {
            UserReminder::create([
                'user_id' => Auth::id(),
                'flag' => 3,
                'time' => $hour1,
            ]);
        }
        $minute = $request->input('minute');
        $minute1 = $request->input('minute1');
        if ($minute == true) {
            UserReminder::create([
                'user_id' => Auth::id(),
                'flag' => 4,
                'time' => $minute1,
            ]);
        }

        return view('app.owner.setting.reminder_completed');
    }

    public function paymentTest()
    {

        return view('payment.test');
    }

    public function payjpRegisterUser(Request $request)
    {

        $user = Auth::user();
        $userId = env('APP_ENV') == 'production' ? Auth::id() : env('APP_ENV') . "_" . Auth::id();


        Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));

        if (empty($user->payment_token) || $user->payment_token == null) {
            $response = Customer::create([
                'email' => $user->email,
                'description' => $user->last_name . " " . $user->first_name,
                'id' => '0' . $userId,
                'card' => $request->input('payjp-token')
            ]);

            $user->payment_token = '0' . $userId;
            $user->save();
        } else {
            $customer = Customer::retrieve($user->payment_token);
            $customer->cards->create([
                'card' => $request->input('payjp-token'),
                'default' => true
            ]);
        }

        return view('app.owner.setting.update_completed')->with('registerCreditCard', true);
    }

    public function paymentChargeTest()
    {
        $user = Auth::user();

        Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));
        $response = Charge::create([
            'customer' => $user->payment_token,
            'amount' => 500,
            'currency' => 'jpy',
            'description' => '2019/04/04 TEST医療費'
        ]);

        dd($response);
    }

    public function paymentCardListTest()
    {

        $user = Auth::user();

        Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));
        $response = Customer::retrieve($user->payment_token);

        dd($response->cards->all(['limit' => 1])->data);
    }

    public function viewReserveRenew($duplicate, $id, Request $request)
    {
        if ($duplicate == 1) {
            $diag = Diagnosis::find($id);
            $clinicId = $diag->clinic_id;
        } else {
            $clinicId = $id;
            $diag = 0;
        }

        $diag_files = DiagFile::where('diag_id', $id)->get();
        $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);

        $isUpdate = $request->input('ref', null) == 'update' ? true : false;
        $clinic = Clinic::find($clinicId);
        $clinicMenus = ClinicMenu::whereClinic_id($clinicId)->get();
        $pets = Pet::whereUserId(Auth::id())->where('del_flg', 0)->pluck('name', 'id');

        //echo $clinicMenus;
        return view('app.owner.clinic.reserve.renew', compact('diag', 'clinic', 'clinicMenus', 'clinicMenu', 'diag_files', 'pets', 'isUpdate'));
    }

    public function viewReserveConfirm($id, Request $request)
    {
        $isUpdate = $request->input('ref', null) == 'update' ? true : false;

        $diag = Diagnosis::find($id);
        $user = Auth::user();
        $accounting = Accounting::where('diagnosis_id', $id)->first();
        if ($user->payment_token != null) {

            $clinic = Clinic::find($diag->clinic_id);
            $clinicMenu = ClinicMenu::find($diag->clinic_menu_id);

            if (empty($clinic) || empty($clinicMenu)) {
                return redirect('/app/owner');
            }

            // Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY') : env('PAY_JP_SECRET_KEY'));
            // $response = Tenant::create(
            //     array(
            //         'id' => 'test',
            //         "name" => "test",
            //         "platform_fee_rate" => "10.15",
            //         "minimum_transfer_amount" => 1000,
            //         "bank_account_holder_name" => "ヤマダ タロウ",
            //         "bank_code" => "0001",
            //         "bank_branch_code" => "001",
            //         "bank_account_type" => "普通",
            //         "bank_account_number" => "0001000",
            //     )
            // );

            $user = Auth::user();

            Payjp::setApiKey(env('APP_ENV') == 'production' ? env('PAY_JP_SECRET_KEY_TEST') : env('PAY_JP_SECRET_KEY_TEST'));

            $response = Charge::create([
                'customer' => $user->payment_token,
                'amount' => $accounting->sum,
                'currency' => 'jpy',
                //'platform_fee' => 100,
                //'tenant' => $response->id,
                'description' => $diag->description
            ]);

            // should create a record in records table
            $record_save = Record::create(
                [
                    'user_id' => Auth::id(),
                    'clinic_id' => $diag->clinic_id,
                    'diag_id' => $id,
                    'pet_id' => $diag->pet_id,
                    'price' => $diag->price
                ]
            );

            return view('app.owner.clinic.reserve.confirm', compact('diag', 'clinic', 'clinicMenu', 'isUpdate', 'accounting'));
        } else {
            return redirect('/app/owner/clinic/reserve/error');
        }
    }

    public function viewReserveError()
    {
        return view('app.owner.clinic.reserve.error');
    }

    public function postReserveRenew($id, $menuId, Request $request)
    {
        $validatedData = $request->validate([
            'pet' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
            'content' => ['required', 'string'],
            'reserve_datetime' => ['required', 'string'],
        ]);

        $diag = Diagnosis::find($id);

        if (!empty($diag)) {
            // $diag_save = $diag->update([
            //     'pet_id' => $validatedData['pet'],
            //     'price' => $validatedData['price'],
            //     'content' => $validatedData['content'],
            //     'reserve_datetime' => $validatedData['reserve_datetime'],
            //     'status' => 0
            // ]);
            if ($request->input('code_flag') == true) {
                $diag_save = $diag->update([
                    'pet_id' => $validatedData['pet'],
                    'clinic_menu_id' => $menuId,
                    'price' => $validatedData['price'],
                    'content' => $validatedData['content'],
                    'reserve_datetime' => $validatedData['reserve_datetime'],
                    'hospital_code' => null,
                    'reserve_method' => $request->input('option'),
                    'clinic_name' => $request->input('clinic_name'),
                    'status' => 0
                ]);
            } else {
                $diag_save = $diag->update([
                    'pet_id' => $validatedData['pet'],
                    'clinic_menu_id' => $menuId,
                    'price' => $validatedData['price'],
                    'content' => $validatedData['content'],
                    'reserve_datetime' => $validatedData['reserve_datetime'],
                    'hospital_code' => $request->input('hospital_code'),
                    'reserve_method' => $request->input('option'),
                    'clinic_name' => $request->input('clinic_name'),
                    'status' => 0
                ]);
            }
        }
        $pet = Pet::find($validatedData['pet']);
        if ($request->input('fileuploader-list-photos') != "" && $request->input('fileuploader-list-photos') != null && $request->input('fileuploader-list-photos') != "[]") {
            $diag_files = DiagFile::where('diag_id', $id)->get();
            if (count($diag_files) > 0) {
                foreach ($diag_files as $diag_file) {
                    $diag_file->delete();
                }
            }
            foreach ($request->photos as $photo) {
                $filename = date('YmdHis') . "-" . $photo->getClientOriginalName();
                $extension = $photo->getClientOriginalExtension();
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                    $photo->move(public_path('images/insurance'), $filename);
                    DiagFile::create([
                        'diag_id' => $id,
                        'filename' => $filename,
                        'type' => '1',
                    ]);
                } else if ($extension == 'mp4' || $extension == 'avi' || $extension == 'wmv' || $extension == 'mov') {
                    $photo->move(public_path('images/insurance'), $filename);
                    DiagFile::create([
                        'diag_id' => $id,
                        'filename' => $filename,
                        'type' => '2',
                    ]);
                }
            }
        }
        // $clinic = Clinic::whereId($validatedData['clinic'])->first();
        // $user = Auth::user();
        // $data = [
        //     'username' => $user->last_name . " " . $user->first_name,
        //     'useremail' => $user->email,
        //     'usertel' => $user->tel,
        //     'clinicname' => $clinic->name,
        //     'clinicemail' => $clinic->email,
        //     'clinictel' => $clinic->tel,
        //     'petname' => $pet->name,
        //     'reserve_datetime' => $validatedData['datetime'] . ":00",
        //     'diagId' => $diag->id,
        //     'flag' => '4'
        // ];
        // // For User
        // Mail::to($user->email)->send(new \App\Mail\OwnerReserveMail($data));

        // // For Clinic
        // Mail::to($clinic->email)->send(new \App\Mail\OwnerReserveForUserMail($data));
        return view('app.owner.clinic.reserve.register_completed')->with('isUpdate', true);
    }
}
///////////////////////////////////////////////////////////
