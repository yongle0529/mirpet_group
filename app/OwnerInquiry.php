<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerInquiry extends Model
{
    protected $guarded = array('id');
}
