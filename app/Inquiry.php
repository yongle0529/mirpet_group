<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Inquiry
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $last_name_kana
 * @property string $first_name_kana
 * @property string $clinic_name
 * @property string $role
 * @property string $email
 * @property string $tel
 * @property string|null $contact_method
 * @property string|null $contact_desired_date
 * @property string|null $contact_desired_timezone
 * @property string $referer
 * @property string $type
 * @property string|null $comment
 * @property int $status
 * @property int $del_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereClinicName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereContactDesiredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereContactDesiredTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereContactMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereDelFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereFirstNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereLastNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $age
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inquiry whereAge($value)
 */
class Inquiry extends Model
{
    protected $guarded = array('id');
}
