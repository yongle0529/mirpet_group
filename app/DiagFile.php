<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiagFile extends Model
{
    protected $fillable = ['diag_id', 'filename', 'type'];

    public function product()
    {
        return $this->belongsTo('App\Diagnosis');
    }
}
