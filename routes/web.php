<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (config('app.env') != 'local') {
    URL::forceScheme('https');
}
//=============Admin===========
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Auth\AdminLoginController@adminshowLoginForm');
    Route::post('/login', 'Auth\AdminLoginController@adminLogin');
    Route::get('/logout', 'Auth\AdminLoginController@logout');
});
// Route::view('mirpetadmin/', 'auth.admin_login');
// Route::get('mirpetadmin', 'Auth\AdminLoginController@adminshowLoginForm')->name('admin.login');

Route::middleware('auth:admin')->prefix('app/admin')->group(function () {
    Route::get('/', 'AdminController@exam_schedule')->name('app.admin.index');
    Route::get('/delete_clinic/{id}', 'AdminController@deleteClinic')->name('app.delete.clinic');
    Route::get('/account_setting', 'AdminController@AdminAccountsetting')->name('app.admin.account_setting');
    Route::post('/account_new_setting', 'AdminController@AdminAccountNewsetting')->name('app.admin.account_new_setting');
    Route::get('/hospital_analy', 'AdminController@getHospitalAnalysis')->name('app.admin.hospital_analy');
    Route::get('/owner_analy', 'AdminController@getOwnerAnalysis')->name('app.admin.owner_analy');
    Route::get('/other_analy', 'AdminController@getOtherAnalysis')->name('app.admin.other_analy');
    Route::get('/hospital_manage', 'AdminController@getHospitalManage')->name('app.admin.hospital_manage');
    Route::get('/petadmin_manage', 'AdminController@getPetAdminManage')->name('app.admin.inpetadmin_managedex');
    Route::get('/alram_message_new/{id}/{flag}', 'AdminController@getAlramMessageNew')->name('app.admin.alram_message_new');
    Route::get('/alram_message_receive', 'AdminController@getAlramMessageReceive')->name('app.admin.alram_message_receive');
    Route::get('/alram_message_send', 'AdminController@getAlramMessageSend')->name('app.admin.alram_message_send');
    Route::post('/reply_inquiry/{id}/{flag}', 'AdminController@replyInquiry')->name('app.admin.reply_inquiry');
    Route::get('/admin_setting', 'AdminController@getAdminSetting')->name('app.admin.admin_setting');
    Route::post('/updateFee', 'AdminController@postAdminSetting');
    Route::get('/hospital_detail_search/{clinic_id}', 'AdminController@getHospitalDetailSearch')->name('app.admin.hospital.dtalil_search');
    Route::get('/pet_detail_search/{user_id}', 'AdminController@getPetDetailSearch')->name('app.admin.pet.dtalil_search');
    Route::get('/animal_reg_detail_search/{user_id}', 'AdminController@getAnimalRegDetailSearch')->name('app.admin.animal_reg.dtalil_search');
    Route::get('/hospital_analysis_csvexport', 'AdminController@CsvHospitalAnalysis');
    Route::post('/hospital_pdf_download', 'AdminController@PdfHospitalAnalysis');
    Route::get('/owner_analysis_csvexport', 'AdminController@CsvOwnerAnalysis');
    Route::post('/exam_schedule_search', 'AdminController@postExamScheduleSearch')->name('app.admin.exam_schedule_search');
    Route::post('/hospital_analysis_search', 'AdminController@postHospitalAnalysisSearch')->name('app.admin.hospital_analysis_search');
    Route::get('/hospital_manage_search', 'AdminController@postHospitalManageSearch')->name('app.admin.hospital_manage_search');
    Route::get('/pet_manage_search', 'AdminController@postPetManageSearch')->name('app.admin.pet_manage_search');
    Route::post('/owner_analysis_search', 'AdminController@postOwnerAnalysisSearch')->name('app.admin.owner_analysis_search');
    Route::post('/owner_pdf_download', 'AdminController@PdfOwnerAnalysis')->name('app.admin.owner_analysis_search');
    Route::post('/other_pdf_download', 'AdminController@PdfOtherAnalysis')->name('app.admin.other_analysis_search');
    Route::post('/other_analysis_search', 'AdminController@postOtherAnalysisSearch')->name('app.admin.other_analysis_search');
});
//=============================
// ============ LP ============

Route::view('/', 'owner.index');

// ------------ 獣医師向け ------------
Route::group(['prefix' => 'clinic'], function () {
    Route::view('/', 'clinic.index');
    Route::view('detail', 'clinic.detail');
    Route::view('delete_complete', 'clinic.delete_complete');
    Route::view('rule', 'clinic.rule');
    Route::view('userprotocal', 'clinic.userprotocal');
    Route::view('userprotocal_pop', 'clinic.userprotocalpop');
    Route::view('transaction', 'clinic.transaction');
    Route::view('thankyou', 'clinic.thankyou');
    Route::get('reset_complete', 'Auth\ClinicResetPasswordController@showResetComplete');
    Route::get('login', 'Auth\ClinicLoginController@showLoginForm')->name('clinic.login');
    Route::post('login', 'Auth\ClinicLoginController@login')->name('clinic.login.submit');
    Route::post('logout', 'Auth\ClinicLoginController@logout')->name('clinic.logout');
    // Route::get('login', 'Auth\ClinicLoginController@showLoginForm')->name('clinic.login');
    // Route::post('login', 'Auth\ClinicLoginController@login')->name('clinic.login');
    // Route::post('logout', 'Auth\ClinicLoginController@logout')->name('clinic.logout');
    // Route::get('register', 'Auth\ClinicRegisterController@showRegisterForm')->name('clinic.register');
    // Route::post('register', 'Auth\ClinicRegisterController@create')->name('clinic.register');
    // Route::get('register', 'Auth\ClinicRegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\ClinicRegisterController@create_clinics');
    Route::get('verify', 'Auth\ClinicRegisterController@viewVerify');
    Route::get('bank/register/{id}/{last_name}/{first_name}', 'Auth\ClinicRegisterController@viewBankRegister')->name('clinic.view.bank.register');
    Route::post('bank/register/{id}/{last_name}/{first_name}', 'Auth\ClinicRegisterController@BankRegister')->name('clinic.bank.register');
    Route::get('password/reset/{flag}', 'Auth\ClinicResetPasswordController@showLinkRequestForm')->name('clinic.password.request');
    Route::post('password/email', 'Auth\ClinicResetPasswordController@sendResetLinkEmail')->name('clinic.password.email');
    Route::get('password/resetpassword/{token}', 'Auth\ClinicResetPasswordController@showResetForm')->name('clinic.password.reset');
    Route::post('password/reset', 'Auth\ClinicResetPasswordController@resetPassword')->name('clinic.password.update');
});

// Route::view('/clinic/login', 'auth.clinic_login');
Route::view('/clinic/register', 'auth.clinic_register');
Route::view('/clinic/protocol', 'auth.clinic_protocol');
Route::view('/clinic/company', 'clinic.company');
//Route::view('/clinic/bank/register', 'auth.clinic_bank_register')->name('clinic.bank.register.show');

Route::post('/clinic/inquiry', function (\Illuminate\Http\Request $request) {

    $data = [
        'last_name' => $request->input('lastname-kanji'),
        'first_name' => $request->input('firstname-kanji'),
        'last_name_kana' => $request->input('lastname-kana'),
        'first_name_kana' => $request->input('firstname-kana'),
        'clinic_name' => $request->input('clinic-name'),
        'role' => $request->input('user-role'),
        'age' => $request->input('user-age'),
        'email' => $request->input('clinic-mail'),
        'tel' => $request->input('clinic-tel'),
        'contact_method' => str_replace(array("tel", "email"), array("電話", "メール"), empty($request->input('contact-method')) ? '未選択' : implode(',', $request->input('contact-method'))),
        'contact_desired_date' => $request->input('contact-method-tel-date'),
        'contact_desired_timezone' => $request->input('contact-method-tel-timezone'),
        'referer' => $request->input('referer'),
        'type' => $request->input('type'),
        'comment' => $request->input('comment'),
    ];
    // Save Database
    \App\Inquiry::create($data);

    // For Admin
    Mail::to('info@mirpet.co.jp')->send(new \App\Mail\InquiryMail($data));

    // For User
    Mail::to($data['email'])->send(new \App\Mail\InquiryForUserMail($data));

    return view('clinic.thankyou');
});


// ------------ オーナー向け ------------
Route::view('/owner', 'owner.index');
Route::view('/owner/protocol', 'auth.protocol');
Route::view('/owner/detail', 'owner.detail');
Route::view('/owner/rule', 'owner.rule');
Route::view('/owner/company', 'owner.company');
Route::view('/owner/userprotocal', 'owner.userprotocal');
Route::view('/owner/userprotocal_pop', 'owner.userprotocalpop');
Route::view('/owner/transaction', 'owner.transaction');
Route::post('/owner/inquiry', function (\Illuminate\Http\Request $request) {

    $data = [
        'name' => $request->input('name'),
        'email' => $request->input('mail'),
        'tel' => $request->input('tel'),
        'type' => $request->input('type'),
        'comment' => $request->input('comment'),
    ];
    // Save Database
    \App\OwnerInquiry::create($data);

    // For Admin
    Mail::to('info@mirpet.co.jp')->send(new \App\Mail\OwnerInquiryMail($data));

    // For User
    Mail::to($data['email'])->send(new \App\Mail\OwnerInquiryForUserMail($data));

    return view('owner.thankyou');
});

Auth::routes(['verify' => true]);


// ============ APP ============

// ------------ 獣医師向け ------------
Route::middleware('auth:clinic')->prefix('app/clinic')->group(function () {
    Route::get('error', 'ClinicController@viewError')->name('clinic.error');
    Route::get('/', 'ClinicController@viewApplication')->name('app.clinic.index');
    Route::get('/application', 'ClinicController@viewApplication');
    Route::get('/application_today', 'ClinicController@viewApplicationToday');
    Route::get('/reserve_insert', 'ClinicController@viewBill');
    Route::get('/reserve_shipping', 'ClinicController@ViewShipping');
    Route::get('/reserve_remind', 'ClinicController@ViewRemind');
    //Route::get('/reserve', 'ClinicController@viewReserve')->name('app.clinic.reserve');
    Route::get('/faq', 'ClinicController@viewFaq')->name('app.clinic.faq');
    Route::get('/ownerinfo/{userId}/{diagId}', 'ClinicController@viewOwnerInfo')->name('app.clinic.ownerinfo');
    Route::get('/petinfo/{petId}/{diagId}', 'ClinicController@viewPetInfo')->name('app.clinic.petinfo');
    Route::get('/purpose/{diagId}', 'ClinicController@viewPurpose')->name('app.clinic.purpose');
    Route::get('/reservedetail/{petId}/{diagId}', 'ClinicController@viewReserveDetail')->name('app.clinic.reservedetail');
    Route::get('/accounting_completed/{diagId}/{pet_id}', 'ClinicController@viewAccountingCompleted')->name('app.clinic.accounting_completed');
    Route::get('/parcel_completed/{diagId}', 'ClinicController@viewParcelCompleted')->name('app.clinic.parcel_completed');
    Route::get('/remind_completed/{diagId}', 'ClinicController@viewRemindCompleted')->name('app.clinic.remind_completed');
    Route::get('/remind_updated/{accountingId}', 'ClinicController@viewRemindUpdated')->name('app.clinic.remind_completed');
    Route::delete('/delete/{id}', 'ClinicController@reserveDelete')->name('application.delete');
    Route::get('/bill_send/{id}', 'ClinicController@BillSend')->name('application.bill_send');
    Route::get('/delete_diag/{id}', 'ClinicController@DeleteDiag')->name('application.delete_diag');
    // Route::get('/logout', function (\Illuminate\Http\Request $request) {
    //     return redirect('');
    // });

    Route::get('/send_accounting_mail/{diagId}/{accountingId}', 'ClinicController@sendAccountingMail');

    Route::get('/accounting_update/{diagId}/{pet_id}/{accountingId}', 'ClinicController@viewAccountingUpdate');
    Route::post('/accounting_update/{accountingId}', 'ClinicController@postAccountingUpdate');

    ////////////////////////////////////////////////////////////////////////
    Route::get('/reserve_end/{diagId}', 'ClinicController@viewReserveEnd')->name('app.clinic.reserve.end');
    ////////////////////////////////////////////////////////////////////////
    ///////////////  reserve state view  ///////////////////////

    Route::get('/reserve', 'ClinicController@reservestate')->name('application.reservestate');

    /////////////////////////////////////////////////////////////

    ///////////////  reserve update view  ///////////////////////

    Route::get('/reserve_update/{year}/{month}/{day}/{id}/{next_day_num}', 'ClinicController@reserveupdate')->name('application.reservestate');

    /////////////////////////////////////////////////////////////
    ///////////////  reserve day_update view  ///////////////////////

    Route::get('/reserve_day_update/{year}/{month}/{day}/{next_day_num}', 'ClinicController@reservedayupdate')->name('application.reservestate');

    /////////////////////////////////////////////////////////////
    ///////////////  reserve day_update view  ///////////////////////

    Route::get('/reserve_next_week/{year}/{month}/{day}/{lastday}/{week_id}/{week_day_num}', 'ClinicController@reservenextweek')->name('application.reservestate');
    Route::get('/reserve_next_week1/{year}/{month}/{day}/{lastday}/{week_id}/{week_day_num}', 'ClinicController@ajaxreservenextweek')->name('application.reservestate');

    /////////////////////////////////////////////////////////////
    ///////////////  reserve reserve_state_view view  ///////////////////////

    Route::get('/reserve_state_view/{year}/{month}/{day}/{datetime}', 'ClinicController@reservestate_vew')->name('application.reservestate');

    /////////////////////////////////////////////////////////////
    ///////////////  reserve reserve_reply view  ///////////////////////

    Route::post('/reserve_reply', 'ClinicController@reserve_reply')->name('application.reserve_reply');

    /////////////////////////////////////////////////////////////
    ///////////////  Owner Pet Search  ///////////////////////

    Route::get('/search_result', 'ClinicController@viewSearchResult')->name('application.reserve_reply');

    /////////////////////////////////////////////////////////////
    //////////// OwnerPetNoChange ///////////////////////////
    Route::post('/ownerPetNo_update', 'ClinicController@ownerPetNoUpdate')->name('owner.ownerPetNo_update');
    Route::post('/clinic_name_insert', 'ClinicController@clinicNameInsert')->name('owner.clinic_name_insert');
    Route::get('/contactus', 'ClinicController@viewContact');


    // Route::get('chat/{key}', function ($key) {
    //     return view('chat.index')->with('returnUrl', URL::to('/app/clinic/reserve_end/' . $key));
    // });

    // Route::get('chat/{diagId}', 'ClinicController@viewChat');

    Route::prefix('setting')->group(function () {

        Route::get('profile', 'ClinicController@viewProfile')->name('setting.clinic_profile');
        Route::get('clinic_info_remove', 'ClinicController@Clinicinfo_remove')->name('setting.clinic_info_remove');
        Route::get('clinic_eye_remove', 'ClinicController@Cliniceye_remove')->name('setting.clinic_eye_remove');
        Route::get('clinic_name_del/{id}', 'ClinicController@Clinicname_Del')->name('setting.clinic_clinic_name_del');
        Route::delete('delete/{id}', 'ClinicController@profileDelete')->name('clinic.delete');
        //////////////// superman profile update //////////////////////
        Route::post('profile', 'ClinicController@updateProfile');
        ///////////////////////////////////////////////////////////////
        //////////////// superman reserve update //////////////////////
        Route::post('reseve_edit', 'ClinicController@reserveEdit');
        ///////////////////////////////////////////////////////////////
        //////////////// superman menu update //////////////////////
        Route::post('menu_edit', 'ClinicController@clinicMenuEdit');
        ///////////////////////////////////////////////////////////////
        //////////////// superman menu update //////////////////////
        Route::post('oldmenu_update/{id}', 'ClinicController@oldclinicMenuUpdate');
        Route::post('newmenu_update/{id}', 'ClinicController@newclinicMenuUpdate');
        ///////////////////////////////////////////////////////////////
        Route::get('payment', 'ClinicController@viewPayment')->name('setting.clinic_payment');
        Route::get('payment_completed', 'ClinicController@viewPaymentCompleted')->name('setting.clinic_payment_completed');
        Route::post('payment_save', 'ClinicController@updatePayment')->name('setting.update_payment');
        Route::get('delcc', 'ClinicController@deleteCreditCard')->name('setting.deleteCreditCard');

        //////////////  superman  ////////////////////////
        Route::get('reserve', 'ClinicController@viewReserveSetting')->name('setting.clinic_reserve');
        Route::get('reserve_edit', 'ClinicController@viewReserveSetting_edit')->name('setting.clinic_reserve_edit');
        /////////////////////////////////////////////////////

        //////////////  superman  ////////////////////////
        Route::get('menu', 'ClinicController@viewMenuSetting')->name('setting.clinic_menu');
        /////////////////////////////////////////////////////

        //////////////// reserve delete ///////////////////////
        Route::get('reseve_delete/{key}', 'ClinicController@viewReserveDelete');
        //////////////// reserve delete ///////////////////////
        Route::get('menu_update/{key}', 'ClinicController@ViewClinicMenuUdate');


        //////////////////////////////////////////////////////
        Route::get('reserve_completed', 'ClinicController@viewReserveCompleted')->name('setting.clinic_reserve_completed');
        Route::post('reserve', 'ClinicController@updateReserveSetting');

        Route::get('password', 'ClinicController@showChangePasswordForm')->name('setting.clinic_password');
        Route::post('password', 'ClinicController@changePassword')->name('setting.clinic_changePassword');
    });
    Route::prefix('footer')->group(function () {
        Route::view('company', 'layouts.app.footer.clinic_company');
        Route::view('rule', 'layouts.app.footer.clinic_rule');
        Route::view('transaction', 'layouts.app.footer.clnic_transaction');
        Route::view('userprotocal', 'layouts.app.footer.clinic_userprotocal');
    });
});

Route::prefix('app/clinic')->group(function () {
    Route::get('/search', 'ClinicController@viewSearch')->name('app.clinic.search');
    // Route::get('/search_result', 'ClinicController@viewSearchResult')->name('app.clinic.search_result');
    Route::post('/search', 'ClinicController@postSearch');
});

// ------------ オーナー向け ------------
Route::get('app/owner/send_email', 'NotifyReminderController@sendEmail');
Route::get('app/owner/check_email', 'NotifyReminderController@checkEmail');
// Route::get('app/owner/email_check', 'OwnerController@Email_check');
Route::prefix('app/owner')->middleware(['auth', 'verified'])->group(function () {

    Route::get('/', 'OwnerController@index');
    Route::get('/contactus', 'OwnerController@viewContact');
    Route::get('/reserve_image_delete/{filename}/{diagId}/{menuId}/{clinicId}', 'OwnerController@reserveImageDelete');
    Route::post('/inquiry', function (\Illuminate\Http\Request $request) {

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('mail'),
            'tel' => $request->input('tel'),
            'type' => $request->input('type'),
            'comment' => $request->input('comment'),
        ];
        // Save Database
        \App\OwnerInquiry::create($data);

        // For Admin
        Mail::to('info@mirpet.co.jp')->send(new \App\Mail\OwnerInquiryMail($data));

        // For User
        Mail::to($data['email'])->send(new \App\Mail\OwnerInquiryForUserMail($data));

        return view('app.owner.thankyou');
    });

    Route::prefix('payment')->group(function () {
        Route::get('/', 'OwnerController@paymentTest');
        Route::post('/', 'OwnerController@payjpRegisterUser');

        Route::get('charge', 'OwnerController@paymentChargeTest');
        Route::get('cardlist', 'OwnerController@paymentCardListTest');
    });

    Route::prefix('setting')->group(function () {
        Route::get('profile', 'OwnerController@viewProfile')->name('setting.profile');
        Route::post('profile', 'OwnerController@updateProfile');

        Route::get('payment', 'OwnerController@viewPayment')->name('setting.payment');
        Route::get('delcc', 'OwnerController@deleteCreditCard')->name('setting.deleteCreditCard');

        Route::get('payhistory', 'OwnerController@viewPaymentHistory')->name('setting.payhistory');
        //Route::get('insurance', 'OwnerController@viewInsurance')->name('setting.insurance');
        //Route::post('insurance', 'OwnerController@updateInsurance');
        Route::get('reminder', 'OwnerController@viewReminder')->name('setting.reminder');
        Route::post('reminder', 'OwnerController@updateReminder')->name('setting.updateReminder');

        Route::get('password', 'OwnerController@showChangePasswordForm')->name('setting.password');
        Route::post('password', 'OwnerController@changePassword')->name('setting.changePassword');
        Route::delete('delete/{id}', 'OwnerController@profileDelete')->name('profile.delete');
    });
    Route::prefix('footer')->group(function () {
        Route::view('company', 'layouts.app.footer.owner_company');
        Route::view('rule', 'layouts.app.footer.owner_rule');
        Route::view('transaction', 'layouts.app.footer.owner_transaction');
        Route::view('userprotocal', 'layouts.app.footer.owner_userprotocal');
    });

    // Route::get('chat/{key}', function ($key) {
    //     return view('chat.index')->with('returnUrl', URL::to('/app/owner'));
    // });

    Route::prefix('clinic')->group(function () {
        // Route::get('search', 'OwnerController@viewClinicSearch');
        // Route::post('search', 'OwnerController@postClinicSearch');

        Route::get('detail/{id}', 'OwnerController@viewClinicDetail');
        Route::get('remind_detail/{id}/{diagid}', 'OwnerController@viewRemindClinicDetail');
        Route::get('update_detail/{id}/{diagid}', 'OwnerController@viewUpdateReveserClinicDetail');
        Route::get('fav/{id}', 'OwnerController@addFavorite');

        Route::get('reserve/{clinicId}/{menuId}', 'OwnerController@viewReserve')->where('clinicId', '[0-9]+');
        Route::get('remind_reserve/{clinicId}/{menuId}/{diagid}', 'OwnerController@viewRemindReserve')->where('clinicId', '[0-9]+');
        Route::get('userupdate_reserve/{clinicId}/{menuId}/{diagid}', 'OwnerController@viewuserUdateReserve')->where('clinicId', '[0-9]+');
        Route::get('reserve/register/{clinicId}/{menuId}', 'OwnerController@viewReserveLast');
        Route::get('reserve/remind_register/{clinicId}/{menuId}/{diagid}', 'OwnerController@viewRemindReserveLast');
        Route::post('reserve/register', 'OwnerController@postReserveRegister');
        Route::get('reserve/detail/{diagId}', 'OwnerController@viewReserveDetail')->name('reserve.detail');
        // Route::get('reserve/update/{diagId}', 'OwnerController@viewReserveUpdate')->name('reserve.update');
        Route::get('reserve/end/{diagId}', 'OwnerController@viewReserveEnd')->name('reserve.end');
        // Route::post('reserve/update/{diagId}', 'OwnerController@postReserveUpdate')->name('reserve.update');
        Route::post('reserve/update1/{diagId}/{menuId}', 'OwnerController@postReserveUpdate')->name('reserve.update1');
        Route::delete('reserve/delete/{id}', 'OwnerController@reserveDelete')->name('reserve.delete');
        Route::get('reserve/check/{diagId}', 'OwnerController@viewReserveCheck')->name('reserve.check');
        Route::get('reserve/confirm/{diagId}', 'OwnerController@viewReserveConfirm')->name('reserve.confirm');
        Route::get('reserve/error', 'OwnerController@viewReserveError')->name('reserve.error');
        //Route::post('reserve/check/{diagId}', 'OwnerController@viewReserveCheck')->name('reserve.check');

        Route::get('reserve/renew/{duplicate}/{diagId}', 'OwnerController@viewReserveRenew')->name('reserve.renew.edit');
        Route::post('reserve/renew/{diagId}/{menuId}', 'OwnerController@postReserveRenew')->name('reserve.renew.update');
    });

    Route::prefix('pet')->group(function () {
        Route::get('register', 'OwnerController@viewPetRegister');
        Route::post('register', 'OwnerController@postPetRegister');
        Route::get('update/{id}', 'OwnerController@viewPetUpdate');
        Route::post('update/{id}', 'OwnerController@postPetUpdate');
        Route::get('delete/{id}', 'OwnerController@petDelete');
        Route::get('insurance_delete/{id}/{flag}', 'OwnerController@insuranceDelete');
        Route::get('pet_image_delete/{id}', 'OwnerController@petImageDelete');
    });
});

Route::prefix('app/owner/clinic')->group(function () {
    Route::get('search', 'OwnerController@viewClinicSearch');
    Route::get('search_result', 'OwnerController@postClinicSearch');
});

Route::get('/images/{filename}', function ($filename, Illuminate\Http\Request $request) {
    $path = public_path() . "/images/$filename";

    if (!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/customer/print-pdf/{diagId}', 'customercontroller@printpdf')->name('customer.printpdf');

Route::get('/app/chat/{key}', 'ClinicController@viewChat')->name('app.clinic.chat');

Route::post('/app/chat/{key}', 'OwnerController@viewChat')->name('app.owner.chat');

Route::get('/app/updatestart/{diagId}', 'Auth\ClinicRegisterController@updateStart');
