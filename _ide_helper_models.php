<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Clinic
 *
 * @property int $id
 * @property string $name
 * @property string $name_kana
 * @property mixed|null $option_pet
 * @property mixed|null $option_medical_course
 * @property mixed|null $option_other
 * @property mixed|null $clinic_info
 * @property string $email
 * @property string $tel
 * @property string $zip_code
 * @property string $prefecture
 * @property string $city
 * @property string $address
 * @property string|null $logo_image_path
 * @property string|null $url
 * @property string $ref_code
 * @property mixed|null $bank_info
 * @property int $status 0: 初期登録状態, 1:公開中, 2:掲載停止
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereBankInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereClinicInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereLogoImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionMedicalCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereOptionPet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic wherePrefecture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereRefCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Clinic whereZipCode($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClinicMenu[] $clinicMenus
 */
	class Clinic extends \Eloquent {}
}

namespace App{
/**
 * App\ClinicMenu
 *
 * @property-read \App\Clinic $clinic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $clinic_id
 * @property string $name
 * @property string $description
 * @property mixed $price_list
 * @property int $is_insurance_applicable 0: 保険適用不可, 1:保険適用可
 * @property mixed|null $option
 * @property int $status 0: 初期登録状態, 1:公開中, 2:掲載停止
 * @property int $del_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereClinicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereDelFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereIsInsuranceApplicable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu wherePriceList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClinicMenu whereUpdatedAt($value)
 */
	class ClinicMenu extends \Eloquent {}
}

namespace App{
/**
 * App\FavoriteClinic
 *
 * @property int $id
 * @property int $user_id
 * @property int $clinic_id
 * @property int $del_flg
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereClinicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereDelFlg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FavoriteClinic whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Clinic $clinic
 * @property-read \App\User $user
 */
	class FavoriteClinic extends \Eloquent {}
}

namespace App{
/**
 * App\Pet
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $name_kana
 * @property int $gender
 * @property int $type
 * @property string|null $type_other
 * @property string $genre
 * @property string $birthday
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereGenre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereTypeOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereUserId($value)
 * @property int $del_flg
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet whereDelFlg($value)
 */
	class Pet extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property int $role
 * @property string|null $tel
 * @property string|null $zip_code
 * @property string|null $prefecture
 * @property string|null $city
 * @property string|null $address
 * @property string|null $card_token
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastNameKana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePrefecture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereZipCode($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FavoriteClinic[] $favoriteClinics
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pet[] $pets
 */
	class User extends \Eloquent {}
}

