$(function() {
    var input_file = document.getElementById("photos");
    var deleted_file_ids = [];
    var dynm_id = 0;
    $("#photos").change(function(event) {
        var len = input_file.files.length;
        $("#preview_file_div ul").html("");

        for (var j = 0; j < len; j++) {
            var src = "";
            var name = event.target.files[j].name;
            var mime_type = event.target.files[j].type.split("/");
            if (mime_type[0] == "image") {
                src = URL.createObjectURL(event.target.files[j]);
            } else if (mime_type[0] == "video") {
                src = "icons/video.png";
            } else {
                src = "icons/file.png";
            }
            $("#preview_file_div ul").append(
                "<li id='" +
                    dynm_id +
                    "'><div class='ic-sing-file'><img id='" +
                    dynm_id +
                    "' src='" +
                    src +
                    "' title='" +
                    name +
                    "'><p class='close' id='" +
                    dynm_id +
                    "'>X</p></div></li>"
            );
            dynm_id++;
        }
    });
    $(document).on("click", "p.close", function() {
        var id = $(this).attr("id");
        deleted_file_ids.push(id);
        $("li#" + id).remove();
        if ("li".length == 0) document.getElementById("photos").value = "";
    });
});
