$(document).ready(function() {
    // enable fileuploader plugin
    if (flag == 0) {
        $('input[id="photos_upload"]').fileuploader({
            addMore: true,
            files: null
        });
    } else {
        var files = [];
        for (var i = 0; i < flag; i++) {
            if (diag_files[i].type == 2) {
                var file_type = diag_files[i].filename.slice(
                    diag_files[i].filename.length - 3
                );
                var tp = "video/" + file_type;
                console.log(tp);
            } else {
                var file_type = diag_files[i].filename.slice(
                    diag_files[i].filename.length - 3
                );
                var tp = "image/" + file_type;
                console.log(tp);
            }
            var obj = {
                name: diag_files[i].filename,
                size: 775702,
                type: tp,
                url: file_url + "/" + diag_files[i].filename,
                deleteUrl: file_url + "/" + diag_files[i].filename,
                deleteType: "DELETE"
            };
            console.log(file_url + "/" + diag_files[i].filename);
            files.push(obj);
        }
        $('input[id="photos_upload"]').fileuploader({
            addMore: true,
            files: files
        });
    }
});
